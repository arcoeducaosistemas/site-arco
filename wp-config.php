<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'cacto_arco');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A&Uzf1D1J5Ra~lxIcM!Odh}mx}.PFU:l`D1_P_[3~T]#qiY)m[_e6$jhK[=@qV-q');
define('SECURE_AUTH_KEY',  'xd.5MkvsHry#ih8k8T<`5S;R:h 7K=VqCumvg6cLf%9,wvD+jU?RQx,c7kV!ciTK');
define('LOGGED_IN_KEY',    '4uYk|JycKd~+_I.3qR?Zvxy<#qId|3XsHzz?c^cpZXxf#JhxCeR@8hLhBkg];0]3');
define('NONCE_KEY',        '#H[ny_}^~vVRi~g5t-`mVt@|FeHC{t}VF?4eIac0T8C%BTGK{5FO4Tvv@Fig+TO0');
define('AUTH_SALT',        '2#Lj>eTl1ZEh{3:xIO_dD-3BrrqA^Q1I6rgF<[/CR%>T)/:u?Y6a,C<0Y(~]`5eA');
define('SECURE_AUTH_SALT', 'ZB7`yIL9+  PJ?w<rD%1q1`WLs@lI52enwZqz`Lu`Y;h3g]xV@0MLL3kn-NM3iMZ');
define('LOGGED_IN_SALT',   '] Upfu#8>iVubLw1I[pV)iwK,#4Kh*Nq>Xa#^1-:hk36v*b(*=qi#I2[K|pO7svm');
define('NONCE_SALT',       '-}$@;bG$#ZNB%1SULCGsly`Ytq$=2+QV7l3Tk?K lDsE?Z!&p:SWSsn!ywqC?tbK');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
