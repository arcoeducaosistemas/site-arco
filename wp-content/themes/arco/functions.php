<?php
/**
 * circusdigital functions and definitions
 *
 * @package circusdigital
 */

/**
 * Initialize theme child default settings
 */
require_once('inc/child-settings.php');

/**
 * Register acf settings
 */
require_once('inc/acf.php');

/**
 * Register cpt settings
 */
require_once('inc/cpt.php');

function add_svg_to_upload_mimes( $upload_mimes ) {
	$upload_mimes['svg'] = 'image/svg+xml';
	$upload_mimes['svgz'] = 'image/svg+xml';
	return $upload_mimes;
}
add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

?>
