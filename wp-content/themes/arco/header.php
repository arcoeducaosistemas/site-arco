<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" href="/wp-content/themes/arco/img/favicon.png">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122425962-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-122425962-1');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSKVRVV');</script>
<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target=".navbar" data-offset="50" id="arco">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WSKVRVV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="hfeed site" id="page">
	<!-- ******************* The Navbar Area ******************* -->
	<?php
	// get logo from acf field
	$logo = get_field('logo_site', 'option')
	?>
	<div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content',
		'understrap' ); ?></a>

		<nav class="navbar navbar-expand-xl fixed-bottom navbar-dark nav-down d-xl-none d-lg-none" id="navbar_custom">
			<!-- The WordPress Menu goes here -->
			<?php wp_nav_menu(
				array(
					'theme_location'  => 'primary',
					'container_class' => 'collapse navbar-collapse',
					'container_id'    => 'collapsingNavbar',
					'menu_class'      => 'navbar-nav',
					'fallback_cb'     => '',
					'menu_id'         => 'main-menu',
					'walker'          => new understrap_WP_Bootstrap_Navwalker(),
				)
			); ?>
		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<a class="navbar-brand-center" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
								<img src="<?php echo $logo ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" class="img-fluid">

							</a>

						<?php else : ?>

							<a class="navbar-brand-center" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
								<img src="<?php echo $logo ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" class="img-fluid">

							</a>
						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

					<button class="navbar-toggler navbar-toggler-left" type="button" data-toggle="collapse" data-target="#collapsingNavbar" aria-controls="collapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">

    <div class="hamburger d-flex flex-column justify-content-around text-center">
      <div class="hamburger__patty"></div>
      <div class="hamburger__patty"></div>
      <div class="hamburger__patty"></div>
    </div>

  </button>



		</nav><!-- .site-navigation -->

	</div><!-- .wrapper-navbar end -->
	<div class="wrapper">
	  <!-- Sidebar  -->
	  <nav id="sidebar">
		<ul class="links p-0">			
			<li><a href="https://investor.arcoplatform.com">RI</a></li>
			<li><a href="https://arcoplatform.com">en</a></li>
		</ul>
		  <div class="sidebar-header text-center">
	  		<a class="navbar-icon" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
	  			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-icon.png" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" class="img-fluid">
	  		</a>
	  		<a class="navbar-icon-complete" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
	  			<img src="https://arcoeducacao.com.br/wp-content/uploads/2018/08/marca_site.png" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" class="img-fluid">
	  		</a>
	  	</div>

	  	<ul class="list-unstyled components">
	  		<?php wp_nav_menu(
	  			array(
	  				'theme_location'  => 'primary',
	  				'menu_id'         => 'main-sidebar',
	  			)
	  		); ?>
	  	</ul>
	  </nav>

	  
	  
	  <!-- Page Content  -->
	  <div id="content">

		  <nav class="navbar navbar-expand-lg navbar-light fixed-top bg-light" id="custom_navbar">
			  <div class="container mt-2">

				                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
				                        <?php
                // check if the repeater field has rows of data
                if( have_rows('icon-social-repeat', 'option') ):?>
				                        <ul class="nav navbar-nav ml-auto">
                            <?php // loop through the rows of data
                            while ( have_rows('icon-social-repeat', 'option') ) : the_row();?>
				                            <li class="nav-item active">
                                    <a class="nav-link" target="_blank" href="<?php the_sub_field('link-social'); ?>">
                                        <?php the_sub_field('icon-social'); ?>
                                   </a>
                               </li>
                               <!-- display a sub field value -->
                           <?php endwhile; else :
                           // no rows found ?>
                       </ul><!--/.list_social-->
                <?php endif;?>
				                    </div>
				                </div>
				            </nav>
