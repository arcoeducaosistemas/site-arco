<div id="carousel_banner" class="carousel slide" data-ride="carousel"  data-interval="5000">
    <div class="carousel-inner">
        <?php
        // check if the repeater field has rows of data
        if( have_rows('repeater_banner_gallery', 'option') ):?>
                <?php // loop through the rows of data
                while ( have_rows('repeater_banner_gallery', 'option') ) : the_row();
                if( $banner_count == 0 ){
                    $active_item="active";
                }
                else {
                    $active_item="";
                }
                ?>
                <?php $banner =  get_sub_field('image'); ?>
                <div class="carousel-item <?php echo $active_item; ?>" style="background:linear-gradient(to left, rgb(254, 145, 59, 0.7) 25%,rgba(230, 79, 67,0.7) 68%),  url('<?php echo $banner ?>')" data-interval="1000">
                    <div class="d-flex h-100 align-items-center justify-content-center">
                        <div class="container">
                            <div class="col-12">
                                <h2><?php the_sub_field ('title_image')?></h2>
                                <hr>
                            </div>
                            <div class="col-12 mt-3">
                                <h3><?php the_sub_field ('subtitle_image')?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $banner_count++; endwhile;?>

            <?php if( $banner_count > 1 ){ ?>
                <a class="carousel-control-prev" href="#carousel_banner" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel_banner" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <?php
            }
            else {
            }  else :
                // no rows found ?>
        </div><!--/.carousel_about-->
    <?php endif;?>
    </div>
</div>
