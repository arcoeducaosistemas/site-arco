

<section id="text">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-12">
                    <?php
                    // check if the repeater field has rows of data
                    if( have_rows('repeater_text', 'option') ):?>
                        <div class="owl-carousel" id="text_carousel">
                            <?php // loop through the rows of data
                            while ( have_rows('repeater_text', 'option') ) : the_row();?>
                               <div class="item">
                                   <div class="container">
                                       <div class="col-12">
                                           <h2><?php the_sub_field ('title_text')?></h2>
                                           <hr>
                                       </div>
                                       <div class="col-12 mt-3">
                                           <h3><?php the_sub_field ('subtitle_text')?></h3>
                                       </div>
                                   </div>                               </div>
                               <!-- display a sub field value -->
                           <?php endwhile; else :
                           // no rows found ?>
                       </div><!--/.carousel_about-->
                    <?php endif;?>

            </div>
        </div><!--/.title -->
    </div><!-- /. container -->
</section><!-- /. posts -->
