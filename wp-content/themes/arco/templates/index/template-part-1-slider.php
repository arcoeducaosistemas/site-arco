<div id="carousel" class="carousel slide" data-ride="carousel"  data-interval="5000">
    <ol class="carousel-indicators">
        <?php
            $args = array( 'post_type' => 'slider', 'order' => 'DESC');
            $my_query = new WP_Query( $args );
            $banner_count = 0;
            while ( $my_query->have_posts() ) : $my_query->the_post();
                if( $banner_count == 0 ){
                    $active_item="active";
                }
            else {
                $active_item="";
            }
        ?>
        <li data-target="#carousel" data-slide-to="<?php echo $banner_count; ?>" class="<?php echo $active_item; ?>"></li>
        <?php $banner_count++; endwhile; ?>

    </ol>
    <div class="carousel-inner">
        <?php
        $args = array( 'post_type' => 'slider', 'order' => 'DESC');

            $my_query = new WP_Query( $args );
            $banner_count = 0;
            while ( $my_query->have_posts() ) : $my_query->the_post();
            if( $banner_count == 0 ){
            $active_item="active";
        }
        else {
            $active_item="";
        }
        ?>
        <?php if (wp_is_mobile()){
            if (get_field('imagem_de_fundo_mobile')):
                $banner =  get_field('imagem_de_fundo_mobile'); 
            else: 
                $banner =  get_field('slider_background'); 
            endif;
         } else { 
            $banner =  get_field('slider_background'); 
         }
    
    
$link = get_field('banner_com_link_full');
if( $link ): 
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';
    ?>
    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" class="col-md-12">
		<style>
		a.col-md-12 {
    height: 100vh;
    width: 100%;
    position: absolute;
    padding: 100vh;
}
.carousel-item{
    background-image: url('<?php echo $banner;?>');
}
@media screen and (max-width: 992px){
    .carousel-item{
        margin-top: 5rem;
    }
}
		</style>
<?php endif; ?>
        <div class="carousel-item <?php echo $active_item; ?>" style="background-image:url('<?php echo $banner ?>');" data-interval="1000">


            <div class="d-flex h-100 align-items-end justify-content-self">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-md-12 col-sm-12">
                            <h1><?php the_field ('title_slider');?></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-5 col-md-12 col-sm-12">
                            <p><?php the_field ('subtitle_slider')?></p>
                        </div>
                        <div class="col-12 p-0">
                            <?php if( get_field('slider_link_btn') ): ?>
                                <a href="<?php the_field ('slider_link_btn')?>">
                                    <div class="btn btn-main">
                                        <img src="/wp-content/themes/arco/img/icon.png" class="pr-2"><?php the_field ('slider_text_btn')?>
                                    </div>
                                </a>
                                <?php endif; ?>
                        </div>
                    </div>


                </div>
            </div>
        </div>
		<?php 
$link = get_field('banner_com_link_full');
if( $link ): ?>
			</a>
<?php endif; ?>

		<?php $banner_count++; endwhile; ?>
        <?php if( $banner_count > 100 ){ ?>
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <?php
            }
            else {
            }    ?>
    </div>
</div>
