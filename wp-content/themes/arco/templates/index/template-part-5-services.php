<section id="services">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-12 col-sm-12 col-xs-12 mb-4 text-center">
                <h2>consultoria e serviços</h2>
                <p>Soluções completas para trazer eficiência, otimização de custos e os melhores resultados em gestão de benefícios e seguros corporativos. Mais tranquilidade no dia a dia dos RHs e garantia de satisfação dos colaboradores.
<br>
Conte ainda com os planos e seguros oferecidos pela Sciath, para promover a saúde e o bem-estar em sua empresa, com qualidade, efetividade e transparência.
</p>
            </div>
            <div class="col-md-12 p-0">
                <ul class="nav nav-tabs nav col-md-4" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#menu1">produtos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu2">serviços</a>
                    </li>
                </ul>
                <!-- Tab panes -->
            </div>
            <div class="tab-content">
                <div id="menu1" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-4" id="">

                        <div class="nav flex-column nav-pills col-md-12 p-0" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                            <?php
                            // check if the repeater field has rows of data
                            if( have_rows('repeater_products', 'option') ):
                                $i = 0;
                                // loop through the rows of data
                                while ( have_rows('repeater_products', 'option') ) : the_row();?>
                                <!-- display a sub field value -->
                                <?php if ($i == '0'){?>
                                    <a class="nav-link active" id="#sciath<?php echo $i ;?>-tab" data-toggle="tab" href="#sciath<?php echo $i ;?>" role="tab" aria-controls="#sciath<?php echo $i ;?>" aria-selected="true">

                                    <?php }
                                    else {?>
                                        <a class="nav-link" id="sciath<?php echo $i ;?>-tab" data-toggle="tab" href="#sciath<?php echo $i ;?>" role="tab" aria-controls="sciath<?php echo $i ;?>" aria-selected="false">

                                        <?php }?>
                                        <h3><?php the_sub_field('title_service')?></h3>
                                    </a>
                                    <?php $i++;
                                endwhile;

                                else :
                                    // no rows found
                                endif;
                                ?>
                            </div>
                    </div>
                    <div class="tab-content col-md-8" id="v-pills-tabContent">

                        <?php
                            // check if the repeater field has rows of data
                            if( have_rows('repeater_products', 'option') ):
                                $i = 0;
                                // loop through the rows of data
                                while ( have_rows('repeater_products', 'option') ) : the_row();?>
                                    <!-- display a sub field value -->
                                    <?php if ($i == '0'){?>
                                        <div class="tab-pane fade show active" id="sciath<?php echo $i ;?>" role="tabpanel" aria-labelledby="sciath<?php echo $i ;?>-tab">

                                    <?php }
                                    else {?>
                                        <div class="tab-pane fade" id="sciath<?php echo $i ;?>" role="tabpanel" aria-labelledby="sciath<?php echo $i ;?>-tab">

                                    <?php }?>
                                    <div class="row">
                                            <div class="col-md-7 text-center" id="text_description">
                                                <h3><?php the_sub_field('title_service')?></h3>
                                                <p><?php the_sub_field('description_service')?></p>
                                            </div>
                                            <div class="col-md-5 text-center">
                                                <img src="<?php the_sub_field('image_service')?>" class="img-fluid" alt="<?php the_sub_field('title_service')?>" title="<?php the_sub_field('title_service')?>">
                                            </div>
                                        </div>
                                    </div>

                                <?php $i++;
                                endwhile;

                                else :
                                // no rows found
                            endif;

                        ?>
                    </div>
                </div>
                </div>
                <div id="menu2" class="tab-pane">
                    <div class="row">
                        <div class="col-md-4" id="">

                        <div class="nav flex-column nav-pills col-md-12 p-0" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <?php
                            // check if the repeater field has rows of data
                            if( have_rows('repeater_services', 'option') ):
                                $i = 0;
                                // loop through the rows of data
                                while ( have_rows('repeater_services', 'option') ) : the_row();?>
                                <!-- display a sub field value -->
                                <?php if ($i == '0'){?>
                                    <a class="nav-link active" id="#sciaths<?php echo $i ;?>-tab" data-toggle="tab" href="#sciaths<?php echo $i ;?>" role="tab" aria-controls="#sciaths<?php echo $i ;?>" aria-selected="true">

                                    <?php }
                                    else {?>
                                        <a class="nav-link" id="sciaths<?php echo $i ;?>-tab" data-toggle="tab" href="#sciaths<?php echo $i ;?>" role="tab" aria-controls="sciaths<?php echo $i ;?>" aria-selected="false">

                                        <?php }?>
                                        <h3><?php the_sub_field('title_service')?></h3>
                                    </a>
                                    <?php $i++;
                                endwhile;

                                else :
                                    // no rows found
                                endif;
                                ?>
                            </div>
                    </div>
                    <div class="tab-content col-md-8" id="">
                        <?php
                            // check if the repeater field has rows of data
                            if( have_rows('repeater_services', 'option') ):
                                $i = 0;
                                // loop through the rows of data
                                while ( have_rows('repeater_services', 'option') ) : the_row();?>
                                    <!-- display a sub field value -->
                                    <?php if ($i == '0'){?>
                                        <div class="tab-pane fade show active" id="sciaths<?php echo $i ;?>" role="tabpanel" aria-labelledby="sciaths<?php echo $i ;?>-tab">

                                    <?php }
                                    else {?>
                                        <div class="tab-pane fade" id="sciaths<?php echo $i ;?>" role="tabpanel" aria-labelledby="sciaths<?php echo $i ;?>-tab">

                                    <?php }?>
                                    <div class="row" id="scrollabe">
                                        <?php if ($i == '3'){?>
                                            <div class="col-md-7 text-center scroll" id="scrollabe">
                                            <?php }
                                            else {?>
                                                <div class="col-md-7 text-center" id="text_description">
                                                <?php }?>

                                                <h3><?php the_sub_field('title_service')?></h3>
                                                <p><?php the_sub_field('description_service')?></p>
                                            </div>
                                            <div class="col-md-5 text-center">
                                                <img src="<?php the_sub_field('image_service')?>" class="img-fluid" alt="<?php the_sub_field('title_service')?>" title="<?php the_sub_field('title_service')?>">
                                            </div>
                                        </div>
                                    </div>

                                <?php $i++;
                                endwhile;

                                else :
                                // no rows found
                            endif;

                        ?>
                    </div>
                </div>
                </div>
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/.services-->
