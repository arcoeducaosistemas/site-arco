<section id="testimonials">
    <div class="container h-100">
      <div class="row h-100 justify-content-center align-items-center">
            <div class="owl-carousel text-center" id="carousel_testimonials">
                <?php $loop = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'ASC')); ?>
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <!--/. get featured image-->
                    <div class="item form-row h-100 justify-content-center align-items-center text-center" id="testimonial_item">
                        <!-- Media middle -->
                        <?php if( get_field('avatar_testimonial') ): ?>
                            <div class="col-12 col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center" id="avatar_testimonial">
                                <img src="<?php echo get_field('avatar_testimonial') ?>" class="media-object mx-auto d-block" alt="<?php echo get_the_title(); ?>" title="<?php echo get_the_title(); ?>">
                            </div>
                        <?php endif; ?>
                        <div class="col-12 col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center" id="text_testimonial">
                            <p><?php echo get_field('testimonial') ?></p>
                            <h3><?php echo get_the_title(); ?>
                                <?php if( get_field('profession') ): ?>
                                , <?php the_field('profession'); ?>
                            </h3>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--/.item <?php echo get_the_title( $post_id ); ?> -->
                <?php endwhile; ?>
            </div>
            <!-- /.end cols -->
        </div>
        <!-- /. row -->
    </div>
    <!-- /. container -->
</section>
<!-- /. testimonials-->
