

<section id="about">
    <div class="container">
        <div class="row h-justify-content-center align-items-center">
            <div class="col-xl-8 col-md-8">
                <h2><?php the_field('title_about', 'option')?></h2>
                <?php if( get_field('two_columns_about', 'option') ): ?>
                    <p><?php the_field('full_column_text', 'option')?></p>
                <?php endif; ?>
                <?php if(! get_field('two_columns_about', 'option') ): ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <p><?php the_field('first_column_text', 'option')?></p>
                        </div>
                        <div class="col-md-6 col-ms-12 col-xs-12">
                            <p><?php the_field('second_column_text', 'option')?></p>
                        </div>
                    </div>
                <?php endif; ?>
            </div><!--/.title_description-->
            <div class="col-xl-4 col-md-4" id="gallery_about">
                <?php
                // check if the repeater field has rows of data
                if( have_rows('repeater_about_gallery', 'option') ):?>
                    <div class="owl-carousel" id="carousel_about">
                        <?php // loop through the rows of data
                        while ( have_rows('repeater_about_gallery', 'option') ) : the_row();?>
                           <div class="item">
                               <img src="<?php the_sub_field('image'); ?>" class="img-fluid mx-auto" alt="<?php the_sub_field('title_image'); ?>" title="<?php the_sub_field('title_image'); ?>">
                           </div>
                           <!-- display a sub field value -->
                       <?php endwhile; else :
                       // no rows found ?>
                   </div><!--/.carousel_about-->
                <?php endif;?>
            </div><!--/. gallery_about-->
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/.about-->
