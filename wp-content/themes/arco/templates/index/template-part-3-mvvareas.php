
<section id="company">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-12 col-md-6 col-xl-5">
                <h2 class="mb-5"><?php the_field('mvv', 'option')?></h2>
                <?php if( get_field('mission', 'option') ): ?>
                    <div class="col-md-12 p-0" id="item">
                        <h3>Missão</h3>
                        <p><?php the_field('mission', 'option')?></p>
                    </div>
                <?php endif; ?>
                <?php if( get_field('vision', 'option') ): ?>
                    <div class="col-md-12 p-0" id="item">
                        <h3>Visão</h3>
                        <p><?php the_field('vision', 'option')?></p>
                    </div>
                <?php endif; ?>
                <?php if( get_field('value', 'option') ): ?>
                    <div class="col-md-12 p-0" id="item">
                        <h3>Valores</h3>
                        <p><?php the_field('value', 'option')?></p>
                    </div>
                <?php endif; ?>

            </div><!--/.mvv -->
            <div class="col-md-12 col-md-6 col-xl-7" id="map">
                <h2 class="mb-5">presença internacional</h2>
                <?php echo do_shortcode('[freehtml5map id="0"]	')?>
                <p class="sciath">powered by sciath</p>
            </div><!--/.mvv -->
        </div><!--/.row -->
    </div><!--/.container -->
</section><!--/.company-->
