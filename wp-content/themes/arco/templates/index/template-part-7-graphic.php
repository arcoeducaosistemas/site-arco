

<section id="partners">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Conheça quem faz diferente com a gente</h2>

                <div class="owl-carousel" id="owl_customers">
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('customers-repeat', 'option') ):
                            // loop through the rows of data
                            while ( have_rows('customers-repeat', 'option') ) : the_row();?>
                                <!-- display a sub field value -->
                                <div class="item">
                                    <?php
                                    $url = get_sub_field('customers-logo');

                                    ?>
                                    <img class="img-fluid center-img" alt="<?php the_sub_field('customers-name'); ?>" title="<?php the_sub_field('customers-name'); ?>" src="<?php the_sub_field('customers-logo'); ?>">
                                </div>
                            <?php endwhile;
                            else :
                            // no rows found
                        endif;
                    ?>
                </div>
                <!--/.owl-customers-->
            </div>
        </div><!--/.title -->
    </div><!-- /. container -->
</section><!-- /. posts -->
