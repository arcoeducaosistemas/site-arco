<?php
$name_contact_main =  get_field('name_form_contact', 'option');
$id_contact_main =  get_field('id_form_contact', 'option');
?>

<section id="contact">
    <div class="container h-100" id="custom_hseight">
  <div class="row h-100 justify-content-between align-items-center row-eq-height">            <div class="col-md-12">
                <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>

            </div>
      
            <div class="col-xl-5 col-md-12 col-sm-12 col-xs-12" id="talk_to_us">
                <h1><?php the_field('title_talk_to_us', 'option') ?></h1>
                <p class="mb-4"><?php the_field('description_talk_to_us', 'option') ?></p>
                <?php echo do_shortcode( '[contact-form-7 id="'.$id_contact_main.'" title="'.$name_contact_main.'"]' ); ?>
            </div><!--/.talk_to_us-->
            <div class="col-xl-6 col-md-12 col-sm-12 col-xs-12" id="address_map">
                <?php     $location = get_field('location', 'option');?>

                <?php if( !empty($location) ): ?>
                    <div class="acf-map" id="map_show">
                    	<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                    </div>
                <?php endif; ?>
                <!--/.$location-->
                <p class="address"><?php the_field('address_complete', 'option')?></p>

            </div><!--/.maps-->
        </div><!--/.row-->
    </div> <!-- /. container -->
</section><!-- /. contact -->
