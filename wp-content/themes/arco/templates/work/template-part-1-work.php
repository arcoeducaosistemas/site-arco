<section id="work">
    <div class="container h-100" id="custom_hseight">
        <div class="row h-100 justify-content-between align-items-center row-eq-height">
            <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
            <div class="col-xl-5 col-md-12 col-sm-12 col-xs-12" id="talk_to_us">
                <h1><?php the_field('title_work_us', 'option') ?></h1>
                <p><?php the_field('description_work_us', 'option')?></p>
                <div class="owl-carousel" id="carousel_work">
                    <?php
                    // check if the repeater field has rows of data
                    if( have_rows('repeater_work', 'option') ):?>
                        <?php // loop through the rows of data
                        while ( have_rows('repeater_work', 'option') ) : the_row();?>
                           <div class="item">
                               <img src="<?php the_sub_field('image'); ?>" class="img-fluid mx-auto" alt="<?php the_sub_field('title_image'); ?>" title="<?php the_sub_field('title_image'); ?>">
                               <p><?php the_sub_field('title_image'); ?></p>
                            </div>
                           <!-- display a sub field value -->
                       <?php endwhile; else :
                       // no rows found ?>
                   <?php endif;?>
               </div><!--/.carousel_work-->
               <a target="_blank" href="<?php the_field('work_link', 'option')?>">
                   <div class="btn btn-main">
                       <?php the_field('work_btn', 'option')?>
                   </div>
               </a>
            </div><!--/.talk_to_us-->
            <div class="col-xl-6 col-md-12 col-sm-12 col-xs-12" id="work_image">
				 <?php if( get_field('enable_image','option') ): ?>
                <img src="<?php the_field('image_work', 'option')?>" class="img-fluid mx-auto d-block" alt="<?php the_field('title_work_us', 'option') ?>" title="<?php the_field('title_work_us', 'option') ?>">
				<?php endif; ?>
				

								 <?php if( get_field('enable_video','option') ): ?>
				<div class="embed-container">
					<?php the_field('videos','option'); 
					?>
				
	
</div>
				<?php endif; ?>

            </div><!--/.image-->
        </div><!--/.row-->
    </div> <!-- /. container -->
</section><!-- /. contact -->
<style>
	iframe{
		width: 100%;
	}
</style>