<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
 <section id="work">
     <div class="container h-100" id="custom_hseight">
         <div class="row h-100 justify-content-between align-items-center row-eq-height">
             <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
             <div class="col-xl-4 col-md-12 col-sm-12 col-xs-12" id="talk_to_us">
                 <h1><?php the_field('title_work_us', $pageID) ?></h1>
                 <p><?php the_field('description_work_us', $pageID)?></p>

             </div><!--/.talk_to_us-->
             <div class="col-xl-6 col-md-12 col-sm-12 col-xs-12" id="work_image">
                 <img src="<?php the_field('image_work', $pageID)?>" class="img-fluid mx-auto d-block" alt="<?php the_field('title_work_us', $pageID) ?>" title="<?php the_field('title_work_us', $pageID) ?>">
             </div><!--/.image-->
         </div><!--/.row-->
     </div> <!-- /. container -->
 </section><!-- /. contact -->


<?php

$posts = get_field('related_page', 58);

if( $posts ): ?>
<section id="page_related">
    <div class="container-fluid">


        <div class="row">

        <?php  $i = 0; ?>
    	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
            <?php
                                          if (($i % 2) == 0){
                                          ?>
                                          <a href="<?php echo get_permalink( $p->ID ); ?>">

    	    <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_primary">

                    <div class="d-flex h-100 align-items-center justify-content-center">
                        <?php $url = get_field('icon_sub', $p->ID); ?>
                        <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                        <p>
                            <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                            <?php echo get_the_title( $p->ID ); ?>
                        </p>

                </div></a>
    	    </div>
             <?php } else{ ?>
                 <a href="<?php echo get_permalink( $p->ID ); ?>">

                 <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_secondary"><a href="<?php echo get_permalink( $p->ID ); ?>">
                     <div class="d-flex h-100 align-items-center justify-content-center">
                             <?php $url = get_field('icon_sub', $p->ID); ?>
                             <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                             <p>
                                 <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                 <?php echo get_the_title( $p->ID ); ?>
                             </p>
                     </div></a>
         	    </div>
             <?php } ?>


                           <?php $i++;?>
    	<?php endforeach; ?>


    </div></div>
    </section>

    <?php endif; ?>
