<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
<section id="contact" class="p-0  h-100">
    <?php $banner =  get_field('banner', $pageID); ?>
    <img src="<?php echo $banner ?>" class="img-fluid d-block mx-auto metodologia">


        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-between align-items-center row-eq-height  add" id="content_page">
                <div class="col-xl-8 col-md-12 col-sm-12 col-xs-12 mt-4" id="talk_to_us">
                    <h1><?php the_field('titulo',$pageID) ?></h1>
                </div>
                <div class="col-xl-10 col-md-12 col-sm-12 col-xs-12" id="talk_to_us_met">
                    <?php echo get_the_content();?>

                </div><!--/.maps-->
            </div><!--/.row-->
        </div> <!-- /. container -->
    </section><!-- /. contact -->

<?php

$posts = get_field('related_page', 58);

if( $posts ): ?>
<section id="page_related">
    <div class="container-fluid">


        <div class="row">

        <?php  $i = 0; ?>
    	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
            <?php
                                          if (($i % 2) == 0){
                                          ?>
                                          <a href="<?php echo get_permalink( $p->ID ); ?>">

    	    <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_primary">

                    <div class="d-flex h-100 align-items-center justify-content-center">
                        <?php $url = get_field('icon_sub', $p->ID); ?>
                        <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                        <p>
                            <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                            <?php echo get_the_title( $p->ID ); ?>
                        </p>

                </div></a>
    	    </div>
             <?php } else{ ?>
                 <a href="<?php echo get_permalink( $p->ID ); ?>">

                 <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_secondary"><a href="<?php echo get_permalink( $p->ID ); ?>">
                     <div class="d-flex h-100 align-items-center justify-content-center">
                             <?php $url = get_field('icon_sub', $p->ID); ?>
                             <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                             <p>
                                 <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                 <?php echo get_the_title( $p->ID ); ?>
                             </p>
                     </div></a>
         	    </div>
             <?php } ?>


                           <?php $i++;?>
    	<?php endforeach; ?>


    </div></div>
    </section>

    <?php endif; ?>
