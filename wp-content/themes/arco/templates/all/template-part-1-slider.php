<?php
global $wp_query;
$pageID = $wp_query->post->ID;
 ?>
<div id="carousel" class="carousel slide" data-ride="carousel"  data-interval="10000">

    <div class="carousel-inner">

        <?php $banner =  get_field('slider_background', $pageID); ?>

        <div class="carousel-item active" style="background-image:url('<?php echo $banner ?>');" data-interval="1000">

            <div class="d-flex h-100 align-items-center justify-content-self">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-xl-6 col-xs-12">
                            <h1><?php the_field ('title_slider', $pageID)?></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-xl-5 col-xs-12">
                            <p><?php the_field ('subtitle_slider', $pageID)?></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php

$posts = get_field('related_page', $pageID);

if( $posts ): ?>
<section id="page_related">
    <div class="container-fluid">


    <div class="row">

    <?php  $i = 0; ?>
	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
        <?php
                                      if (($i % 2) == 0){
                                      ?>
                                      <a href="<?php echo get_permalink( $p->ID ); ?>">

	    <div class="col-md-3 text-center cssClass col-3" id="bg_primary">

                <div class="d-flex h-100 align-items-center justify-content-center">
                    <?php $url = get_field('icon_sub', $p->ID); ?>
                    <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                    <p>
                        <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                        <?php echo get_the_title( $p->ID ); ?>
                    </p>

            </div></a>
	    </div>
         <?php } else{ ?>
             <a href="<?php echo get_permalink( $p->ID ); ?>">

             <div class="col-md-3 text-center cssClass col-3" id="bg_secondary"><a href="<?php echo get_permalink( $p->ID ); ?>">
                 <div class="d-flex h-100 align-items-center justify-content-center">
                         <?php $url = get_field('icon_sub', $p->ID); ?>
                         <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                         <p>
                             <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                             <?php echo get_the_title( $p->ID ); ?>
                         </p>
                 </div></a>
     	    </div>
         <?php } ?>


                       <?php $i++;?>
	<?php endforeach; ?>


</div></div>
</section>

<?php endif; ?>
