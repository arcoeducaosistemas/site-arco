new WOW().init();

//
// document.getElementById('custom_height').style.height = offsetHeightBody+'px';
// document.getElementById('contacts').style.height = offsetHeightBody+'px';

(function( $ ) {


// Instead of .addClass("newclass")
$("svg").attr("class", "oldclass newclass");
// Instead of .removeClass("newclass")
$("svg").attr("class", "oldclass");
    $('#sidebar').mouseover(function() {
            $('#sidebar').addClass('active');
            $('.overlay').addClass('active');

     });
     $('#sidebar').mouseout(function() {
         $('#sidebar').removeClass('active');
         $('.overlay').removeClass('active');


      });
    // $(document).ready(function () {
    //        $("#sidebar").mCustomScrollbar({
    //            theme: "minimal"
    //        });
    //
    //        $('#dismiss, .overlay').on('click', function () {
    //            // hide sidebar
    //            $('#sidebar').removeClass('active');
    //            // hide overlay
    //            $('.overlay').removeClass('active');
    //        });
    //
    //        $('#sidebarCollapse').on('click', function () {
    //            // open sidebar
    //            $('#sidebar').addClass('active');
    //            // fade in the overlay
    //            $('.overlay').addClass('active');
    //            $('.collapse.in').toggleClass('in');
    //            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    //        });
    //    });
    $(document).ready(function () {
           $('#sidebarCollapse').on('click', function () {
               $('#sidebar').toggleClass('active');
           });
       });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var iframe = $(e.relatedTarget.hash).find('iframe');
      var src = iframe.attr('src');
      iframe.attr('src', '');
      iframe.attr('src', src);
    });
    /*==============================================================
        page scrolling
     ==============================================================*/
     // Smooth scrolling using jQuery easing
      $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: (target.offset().top - 54)
            }, 1000, "easeInOutExpo");
            return false;
          }
        }
      });

      // Closes responsive menu when a scroll trigger link is clicked
      $('.js-scroll-trigger').click(function() {
        $('.navbar-collapse').collapse('hide');
      });

      // Activate scrollspy to add active class to navbar items on scroll
      $('body').scrollspy({
        target: '#navbar_custom',
        offset: 54
      });

    //add class navbar fixed top
    $(window).scroll(function() {
        var nav = $('#navbar_custom');
        var top = 400;
        if ($(window).scrollTop() >= top) {
            nav.addClass('navbar-bg');

        } else {
            nav.removeClass('navbar-bg');

        }
    });
    //add class scroll when services > 5
    // if ( $('#services .nav-pills a.nav-link').length > 1 ) {
    //     $( "#services #scrollabe" ).addClass( "scroll" );
    // }
    /*==============================================================
        hover effect blog image
     ==============================================================*/
    //Creates an event that fires every time the mouse moves over any div with the class of "img".
    $(".img-plan").mousemove(function(event){

      //Both the x and y value are calculated by taking the mouse x,y position on the page and subtracting it from the x,y position of the image on the page. "this" is the hovered element with the class of "img"
      var mousex = event.pageX - $(this).offset().left;
      var mousey = event.pageY - $(this).offset().top;


      //If you just used the mouse position values the translation effect will only go up and to the right, by subtracting half of the length / width of the imagevfrom the values  we get either a positive or negitive number so that the image will move in any direction.

      //The 40 controls the amount of "movement" that will happen by giving us a smaller number, feel free to change it to get the effect that you want.
      var imgx = (mousex - 300) / 40;
      var imgy = (mousey - 200) / 40;

      //Adds a translation css styles to the image element
      $(this).css("transform", "translate(" + imgx + "px," + imgy + "px)");
    });

    //This function will fire every time the user mouses off of the image. It resets the translation back to 0.
    $(".img-plan").mouseout(function(){
      $(this).css("transform", "translate(0px,0px)");
    });
    /*==============================================================
        close navbar when click
     ==============================================================*/
	$('.navbar-collapse a').click(function(){
		$(".navbar-collapse").collapse('hide');
	});
    /*==============================================================
        navbar fixed top
     ==============================================================*/
    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 100;
    var navbarHeight = $('header').outerHeight();

    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('.fixed-bottom').removeClass('nav-down').addClass('nav-up');
            $("#page_related").addClass("active_mobile");

        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
                $('.fixed-bottom').removeClass('nav-up').addClass('nav-down');
                $("#page_related").removeClass("active_mobile");

            }
        }

        lastScrollTop = st;
    }


    if( $('.tab-content .show ').hasClass('active') === true )
    {
     $('.carousel-item').addClass('active');
    }
    //acf maps

    /*
    *  new_map
    *
    *  This function will render a Google Map onto the selected jQuery element
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	$el (jQuery element)
    *  @return	n/a
    */

    function new_map( $el ) {

    	// var
    	var $markers = $el.find('.marker');


    	// vars
    	var args = {
    		zoom		: 16,
    		center		: new google.maps.LatLng(0, 0),
    		mapTypeId	: google.maps.MapTypeId.ROADMAP
    	};


    	// create map
    	var map = new google.maps.Map( $el[0], args);


    	// add a markers reference
    	map.markers = [];


    	// add markers
    	$markers.each(function(){

        	add_marker( $(this), map );

    	});


    	// center map
    	center_map( map );


    	// return
    	return map;

    }

    /*
    *  add_marker
    *
    *  This function will add a marker to the selected Google Map
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	$marker (jQuery element)
    *  @param	map (Google Map object)
    *  @return	n/a
    */

    function add_marker( $marker, map ) {

    	// var
    	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

    	// create marker
    	var marker = new google.maps.Marker({
    		position	: latlng,
            icon: '',
    		map			: map
    	});

    	// add to array
    	map.markers.push( marker );

    	// if marker contains HTML, add it to an infoWindow
    	if( $marker.html() )
    	{
    		// create info window
    		var infowindow = new google.maps.InfoWindow({
    			content		: $marker.html()
    		});

    		// show info window when marker is clicked
    		google.maps.event.addListener(marker, 'click', function() {

    			infowindow.open( map, marker );

    		});
    	}

    }

    /*
    *  center_map
    *
    *  This function will center the map, showing all markers attached to this map
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	map (Google Map object)
    *  @return	n/a
    */

    function center_map( map ) {

    	// vars
    	var bounds = new google.maps.LatLngBounds();

    	// loop through all markers and create bounds
    	$.each( map.markers, function( i, marker ){

    		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

    		bounds.extend( latlng );

    	});

    	// only 1 marker?
    	if( map.markers.length == 1 )
    	{
    		// set center of map
    	    map.setCenter( bounds.getCenter() );
    	    map.setZoom( 16 );
    	}
    	else
    	{
    		// fit to bounds
    		map.fitBounds( bounds );
    	}

    }

    /*
    *  document ready
    *
    *  This function will render each map when the document is ready (page has loaded)
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	5.0.0
    *
    *  @param	n/a
    *  @return	n/a
    */
    // global var
    var map = null;

    $(document).ready(function(){

    	$('.acf-map').each(function(){

    		// create map
    		map = new_map( $(this) );

    	});

    });
            $('#carousel_work').owlCarousel({
                items:3,
                margin: 10,

                responsive:{
                    580:{
                        items:1,
                        loop:true,
                        margin:30,
                        dots:true,
                        speed: 3000,
                        autoplay: true
                    },
                    600:{
                        items:3,
                        loop:true,
                        margin:30,
                        dots:true,
                        speed: 3000,
                        autoplay: true
                    },
                    992:{
                        items: 3
                    }
                }
            })
            $('#carousel_our').owlCarousel({
                items:1,
                margin: 10,

                responsive:{
                    580:{
                        items:1,
                        loop:true,
                        margin:30,
                        dots:true,
                        speed: 3000,
                        autoplay: true
                    },
                    600:{
                        items:3,
                        loop:true,
                        margin:30,
                        dots:true,
                        speed: 3000,
                        autoplay: true
                    },
                    992:{
                        items: 3
                    }
                }
            })

       /*==============================================================
           close navbar when click
        ==============================================================*/
   	$('.navbar-collapse a').click(function(){
   		$(".navbar-collapse").collapse('hide');
        $("ul.dropdown-menu").collapse('hide');

   	});
       $(document).ready(function(){

         $(".nav .dropdown").focusin( function (){
            $(this).find(".dropdown-menu").each(function(){
              $(this).css({"display":'block','opacity':'1','top':'60px'});
            });
         });

           $(".nav .dropdown").focusout( function (){
            $(this).find(".dropdown-menu").each(function(){
              $(this).css({"display":'block','opacity':'0','top':'0px'});
            });
         });


       //   $(".navbar-brand").click( function (){
       //    alert("js working");
       //   });

       });
       $(document).ready(function () {
        $($('.hamburger').parent().attr('data-target')).on('hide.bs.collapse', function () {
            $(this).parent().find('.hamburger').removeClass('hamburger--close');
        });
        $($('.hamburger').parent().attr('data-target')).on('show.bs.collapse', function () {
            $(this).parent().find('.hamburger').addClass('hamburger--close');
        });
        });

})(jQuery);
(function($) {
    $.fn.cycle = function(timeout, cls) {
        var l = this.length,
            current = 0,
            prev = 0,
            elements = this;

        function next() {
            elements.eq(prev).removeClass(cls);
            elements.eq(current).addClass(cls);
            prev = current;
            current = (current + 1) % l;
            setTimeout(next, timeout);
        }
        setTimeout(next, timeout);
        return this;
    };
    $('div.cssClass').cycle(3000, 'active_border');
	
}(jQuery));



var offsetHeightBody = document.getElementById('content_page').offsetHeight;
if (offsetHeightBody > 250){
    (function( $ ) {
        $('.page-template-template-sub .add').addClass('scrollbar');
    }(jQuery));
}

