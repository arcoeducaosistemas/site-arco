<?php
/* Template Name: Nosso jeito de trabalhar */
?>

<?php get_header(); ?>


<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
 <section id="work">
     <div class="container h-100" id="custom_hseight">
         <div class="row" id="our_lsist">
             <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
             <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12" id="talk_to_us">
                 <h1><?php the_field('title_work_us', $pageID) ?></h1>
                 <p><?php the_field('description_work_us', $pageID)?></p>

             </div><!--/.talk_to_us-->


                     <?php
                     $i = 0;
                     // check if the repeater field has rows of data
                     if( have_rows('repeater_our', $pageID) ):?>
                         <?php // loop through the rows of data
                         while ( have_rows('repeater_our', $pageID) ) : the_row();?>
                            <div class="col-xl-4 col-md-4 mb-4 text-left our_list" id="<?php echo $i; ?>">

                                <img src="<?php the_sub_field('image'); ?>" class="img-fluid mx-auto" alt="<?php the_sub_field('title_image'); ?>" title="<?php the_sub_field('title_image'); ?>">
                                <div class="media text-left">
                                    <div class="" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                                        <img class="align-self-start mr-3" src="/wp-content/themes/arco/img/icon_varia.png" alt="<?php the_sub_field('title_image'); ?>">
                                    </div>
                                  <div class="media-body">
                                      <div class="" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                                          <h2><?php the_sub_field('title_image'); ?></h2>
                                      </div>
                                      <div id="collapse<?php echo $i; ?>" class="collapse" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordion">
                                          <p><?php the_sub_field('description_image'); ?></p>
                                      </div>
                                  </div>
                                </div>
                            </div>
                            <!-- display a sub field value -->
                        <?php $i++; endwhile;  else :
                        // no rows found ?>
                    <?php endif;?>
                </div><!--/.carousel_work--></div>

             </div><!--/.image-->
         </div><!--/.row-->
     </div> <!-- /. container -->
 </section><!-- /. contact -->



<?php get_footer(); ?>
