<?php
/* Template Name: Nosso jeito */
?>

<?php get_header(); ?>


<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
 <section id="work">
     <div class="container h-100" id="custom_hseight">
         <div class="row h-100 justify-content-between align-items-center row-eq-height">
             <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
             <div class="col-xl-6 col-md-12 col-sm-12 col-xs-12" id="talk_to_us">
                 <h1><?php the_field('title_work_us', $pageID) ?></h1>
                 <p><?php the_field('description_work_us', $pageID)?></p>

             </div><!--/.talk_to_us-->
             <div class="col-xl-6 col-md-12 col-sm-12 col-xs-12" id="work_image">
				 
<?php if( get_field('enable_image', $pageID) ): ?>
 <img src="<?php the_field('image_work', $pageID)?>" class="img-fluid mx-auto d-block custom_size_image" alt="<?php the_field('title_work_us', $pageID) ?>" title="<?php the_field('title_work_us', $pageID) ?>">
                 <style>
                 .custom_size_image{
                     width: 90%;
margin-bottom: 60px;
                 }
                 iframe{
                    width: 100%;
                 }
                 </style>
                <?php endif; ?>
                

                                 <?php if( get_field('enable_video',$pageID) ): ?>
                <div class="embed-container">
                    <?php the_field('videos',$pageID); 
                    ?>
                
    
</div>
                <?php endif; ?>
                 <?php

                 $posts = get_field('related_page',  $pageID);

                 if( $posts ): ?>
                 <div id="page_related_inner" class="inner">
                     <div class="row row-eq-height">
                         <?php  $i = 0; ?>
                         <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
                             <?php  if (($i % 2) == 0){ ?>
                                 <div class="col-md-6 text-left col-12">
                                     <a href="<?php echo get_permalink( $p->ID ); ?>">
                                         <div class="col-md-12 text-center cssClasss col-12 item_<?php echo $i; ?>" id="bg_primary">
                                             <div class="d-flex h-100 align-items-center justify-content-center">
                                                 <?php $url = get_field('icon_sub', $p->ID); ?>
                                                 <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                                 <div class="media align-items-center">
                                                     <img src="<?php echo $url ?>" class="align-self-center mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                                     <div class="media-body">
                                                         <p class="text-left"><?php echo get_the_title( $p->ID ); ?></p>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </a>
                                 </div>
                            <?php } else{ ?>
						 <?php if( get_field('enable_team','option') ): ?>
	<div class="col-md-6 text-center col-12" style="min-height:93.8px;">
                                    <a href="<?php echo get_permalink( $p->ID ); ?>">
                                        <div class="col-md-12 text-center cssClasss col-12 item_<?php echo $i; ?>" id="bg_primary">
                                            <div class="d-flex h-100 align-items-center justify-content-center">
                                                <?php $url = get_field('icon_sub', $p->ID); ?>
                                                <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                                <div class="media align-items-center">
                                                    <img src="<?php echo $url ?>" class="align-self-center mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                                    <div class="media-body">
                                                        <p><?php echo get_the_title( $p->ID ); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
						 
	
<?php endif; ?>
						 			 <?php if (! get_field('enable_team','option') ): ?>
	<div class="col-md-6 text-center col-12 last-child">
                                    <a href="<?php echo get_permalink( $p->ID ); ?>">
                                        <div class="col-md-12 text-center cssClasss col-12 item_<?php echo $i; ?>" id="bg_primary">
                                            <div class="d-flex h-100 align-items-center justify-content-center">
                                                <?php $url = get_field('icon_sub', $p->ID); ?>
                                                <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                                <div class="media align-items-center">
                                                    <img src="<?php echo $url ?>" class="align-self-center mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                                    <div class="media-body">
                                                        <p><?php echo get_the_title( $p->ID ); ?> (em breve)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
						 
	
<?php endif; ?>
                                
                              <?php } ?>


                                            <?php $i++;?>
                     	<?php endforeach; ?>

                     </section>

                     <?php endif; ?>

 </div>

             </div><!--/.image-->
         </div><!--/.row-->
     </div> <!-- /. container -->
 </section><!-- /. contact -->



<?php get_footer(); ?>
