<?php
/* Template Name: Manifesto */
?>

<?php get_header(); ?>


<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>

                <section id="contact" class="p-0  h-100">
      <div class="tab-content" id="myTabContent">
               <?php

                // check if the repeater field has rows of data
                if( have_rows('repeater_timeline', $pageID) ):

                 	// loop through the rows of data
                  $i = 0;
                    while ( have_rows('repeater_timeline', $pageID) ) : the_row();?>

                      <div class="tab-pane fade <?php if($i == 0):?>show active<?php endif;?>" id="manifesto<?php echo $i;?>" role="tabpanel" aria-labelledby="home-tab">
                          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                  <div class="carousel-item " style="background-image:url('<?php the_sub_field('image');?>');" data-interval="1000">

                                      <img class="d-block w-100" src="" alt="">
                                      <div class="carousel-caption">
                                          <div class="col-12">
                                              <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
                                          </div>
                                          <div class="col-md-12">
                                            <h1><?php the_sub_field('frase_banner');?></h1>
                                            <p style="color:#fff"> Navegue pelo menu abaixo e descubra o que a gente pensa.</p>
                                          </div>

                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="container custom_position">
                             <?php the_sub_field('frase');?> 
                          </div>
                      </div>


                    <?php $i++; endwhile; 

                else :

                    // no rows found

                endif;
                ?>
</div>
  <section class="p-0 pb-5">
    <div class="container">
      <div class="row justify-content-between align-items-center row-eq-height  add" id="content_page">
        <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 mt-4" id="talk_to_us">
          <ul class="nav nav-pills nav-justified">
            <?php
            // check if the repeater field has rows of data
           if( have_rows('repeater_timeline', $pageID) ):
       
             // loop through the rows of data
             $im = 0;
               while ( have_rows('repeater_timeline', $pageID) ) : the_row();?>
               <li class="nav-item">
                 <a class="nav-link <?php if($im == 0):?>active<?php endif;?>" id="home-tab" data-toggle="tab" href="#manifesto<?php echo $im;?>" role="tab" aria-controls="home" aria-selected="true">
                     <img src="/wp-content/themes/arco/img/icon_arco.png" alt="">
                 </a>
               </li>
         <?php $im++; endwhile; 
          else :
          // no rows found
          endif;
          ?>
        </ul>
      </div>
    </div>
  </div>
</section><!-- /. contact -->
					





 <style>
 .nav-link.active img{
     display: block;
     transition: 0.3s ease all;
 }
 .nav-link img {
     display: none;
     transition: 0.3s ease all;
     margin: 0 auto;
 }
 div#myTabContent {
margin-bottom: -36px;
}
.container.custom_position {
top: 4em;
position: relative;
color: #001236;
font-weight: 700;
font-size: 1.5em;
}
@media screen and (max-width: 767px){
.container.custom_position {
 top: 9em;
}
.page-template-template-manifesto #contact .mt-4{
 margin-bottom: 5em;
}
}
@media (min-width: 1200px) and (max-width: 1440px){
.page-id-109 .container {
max-width: 980px;
}
}
@media (min-width: 1440px){
.page-id-109 .container {
max-width: 1330px;
}
}
.nav-justified .nav-item {
display: table-cell;
width: 1%;
padding: 0 10px;
border-radius: 0;
}
.page-template-template-manifesto .nav-justified .nav-link {
border-top: 5px solid #437ec1;
border-radius: 0;
min-height: 48px;
}
 </style>

























<?php

$posts = get_field('related_page', 96);

if( $posts ): ?>
<section id="page_related">
    <div class="container-fluid">


        <div class="row">

        <?php  $i = 0; ?>
    	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
            <?php
                                          if (($i % 2) == 0){
                                          ?>
                                          <a href="<?php echo get_permalink( $p->ID ); ?>">

    	    <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_primary">

                    <div class="d-flex h-100 align-items-center justify-content-center">
                        <?php $url = get_field('icon_sub', $p->ID); ?>
                        <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                        <p>
                            <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                            <?php echo get_the_title( $p->ID ); ?>
                        </p>

                </div></a>
    	    </div>
             <?php } else{ ?>
                 <a href="<?php echo get_permalink( $p->ID ); ?>">

                 <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_secondary"><a href="<?php echo get_permalink( $p->ID ); ?>">
                     <div class="d-flex h-100 align-items-center justify-content-center">
                             <?php $url = get_field('icon_sub', $p->ID); ?>
                             <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                             <p>
                                 <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                 <?php echo get_the_title( $p->ID ); ?>
                             </p>
                     </div></a>
         	    </div>
             <?php } ?>


                           <?php $i++;?>
    	<?php endforeach; ?>


    </div></div>
    </section>

    <?php endif; ?>

<script>
	var int_sec = 1000;
var interval = setTimeout(next_, int_sec);
var items_timeline = jQuery(".nav-pills .nav-item .nav-link");
var curr_item = 0;
var isPaused = false;
function next_(){
	if(!isPaused){
		jQuery(items_timeline[curr_item]).trigger("click");
		
		var total = items_timeline.length;
		if(curr_item == total-1){
			curr_item = 0;
		}else{
			curr_item++;
		}
		interval = setTimeout(next_, 7000);
		console.log("Próximo Item");
	}
}

jQuery(".nav-pills .nav-item .nav-link").each(function(index){
	jQuery(this).mouseenter(function(){
		setTimeout(function(){
			var curr_item_ = 0;
			jQuery(".nav-pills .nav-item .nav-link").each(function(){
				if(!jQuery(this).hasClass("active")){
					curr_item_++;
				}else{
					curr_item = curr_item_;
					return false;
				}
			});
		},1000);
		
		clearTimeout(interval);
		isPaused = true;
		
		jQuery("#myTabContent").mouseleave(function(){
			clearTimeout(interval);
			jQuery(this).off("mouseleave");
			isPaused = false;
			interval = setTimeout(next_, 7000);
		});
	});
});
	</script>

<?php get_footer(); ?>
