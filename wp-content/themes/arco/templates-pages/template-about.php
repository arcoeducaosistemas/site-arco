<?php
/* Template Name: Quem Somos */
?>

<?php get_header(); ?>


<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
<section id="contact" class="p-0  h-100">
    <?php $banner =  get_field('banner', $pageID); ?>
    <img src="<?php echo $banner ?>" class="img-fluid d-block mx-auto metodologia">


        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
                </div>
            </div>
        </div>
        <div class="container">
            <?php

// check if the repeater field has rows of data
if( have_rows('repeater_about', $pageID) ):

 	// loop through the rows of data
    while ( have_rows('repeater_about', $pageID) ) : the_row();?>

    <div class="row justify-content-between align-items-center row-eq-height  add" id="content_page">
        <div class="col-xl-9 col-md-12 col-sm-12 col-xs-12 mt-4" id="talk_to_us">
            <h1><?php the_sub_field('titulo') ?></h1>
        </div>
        <div class="col-xl-10 col-md-12 col-sm-12 col-xs-12" id="talk_to_us_met">
            <p><?php the_sub_field('description') ?></p>
        </div><!--/.maps-->
    </div><!--/.row-->

    <?php endwhile;

else :

    // no rows found

endif;

?>
</div>
<style>
#about{
    background: #95afe2;
    border-top-right-radius: 30px;
}
#about p{
    color: #fff;
        font-family: 'Fredoka One';
}
h2, h3, h4{
    color: #001236;
        font-family: 'Fredoka One';
}
#contact #content_page h1 {
    margin-bottom: 0.7em;
}
h4{
    font-size: 1.4em;
    margin-bottom: 30px;
        font-family: 'Fredoka One';
}
#custom_image {
    margin-top: 0;
    bottom: 0;
    position: relative;
}
@media only screen and (min-width: 992px){
#custom_image img {
    position: absolute;
    bottom: 0;
}
}
@media screen and (max-width: 992px){
    #about{
        padding-bottom: 0;
    }
}
@media screen and (min-width: 1199px) and (max-width: 1367px){
    .page-id-99 .container{
        max-width: 1000px;
    }
}
@media screen and (min-width: 767px) and (max-width: 1199px){
    .page-id-99 .container {
        max-width: 760px;
}
}
.page-id-99 #page_related .item_0#bg_primary {
  background: #ef4e37; }
.page-id-99 #page_related .cssClass:last-child, .page-id-99_how #page_related .cssClass:last-child, .page-template-template-manifesto #page_related .cssClass:last-child, .page-template-template-company #page_related .cssClass:last-child, .page-template-template-timeline #page_related .cssClass:last-child {
  padding-right: 0px; }

.page-id-99 #page_related #bg_secondary, .page-id-99 #page_related #bg_primary, .page-id-99_how #page_related #bg_secondary, .page-id-99_how #page_related #bg_primary, .page-template-template-manifesto #page_related #bg_secondary, .page-template-template-manifesto #page_related #bg_primary, .page-template-template-company #page_related #bg_secondary, .page-template-template-company #page_related #bg_primary, .page-template-template-timeline #page_related #bg_secondary, .page-template-template-timeline #page_related #bg_primary {
  min-height: 10px; }
  .page-id-99 #page_related #bg_secondary p, .page-id-99 #page_related #bg_primary p, .page-id-99_how #page_related #bg_secondary p, .page-id-99_how #page_related #bg_primary p, .page-template-template-manifesto #page_related #bg_secondary p, .page-template-template-manifesto #page_related #bg_primary p, .page-template-template-company #page_related #bg_secondary p, .page-template-template-company #page_related #bg_primary p, .page-template-template-timeline #page_related #bg_secondary p, .page-template-template-timeline #page_related #bg_primary p {
    opacity: 0;
    -webkit-transition: opacity 1s ease-in-out;
    -moz-transition: opacity 1s ease-in-out;
    -ms-transition: opacity 1s ease-in-out;
    -o-transition: opacity 1s ease-in-out;
    transition: opacity 1s ease-in-out;
    height: 20px; }

.page-id-99 #page_related:hover #bg_secondary, .page-id-99 #page_related:hover #bg_primary, .page-id-99_how #page_related:hover #bg_secondary, .page-id-99_how #page_related:hover #bg_primary, .page-template-template-manifesto #page_related:hover #bg_secondary, .page-template-template-manifesto #page_related:hover #bg_primary, .page-template-template-company #page_related:hover #bg_secondary, .page-template-template-company #page_related:hover #bg_primary, .page-template-template-timeline #page_related:hover #bg_secondary, .page-template-template-timeline #page_related:hover #bg_primary {
  min-height: 120px; }
  .page-id-99 #page_related:hover #bg_secondary p, .page-id-99 #page_related:hover #bg_primary p, .page-id-99_how #page_related:hover #bg_secondary p, .page-id-99_how #page_related:hover #bg_primary p, .page-template-template-manifesto #page_related:hover #bg_secondary p, .page-template-template-manifesto #page_related:hover #bg_primary p, .page-template-template-company #page_related:hover #bg_secondary p, .page-template-template-company #page_related:hover #bg_primary p, .page-template-template-timeline #page_related:hover #bg_secondary p, .page-template-template-timeline #page_related:hover #bg_primary p {
    opacity: 1;
    -webkit-transition: opacity 1s ease-in-out;
    -moz-transition: opacity 1s ease-in-out;
    -ms-transition: opacity 1s ease-in-out;
    -o-transition: opacity 1s ease-in-out;
    transition: opacity 1s ease-in-out; }

@media (max-width: 991.98px) {
  #sidebar, #page_srelated {
    display: none; }
  #page_related {
    bottom: 80px; }
    #page_related img {
      display: none; }
    #page_related .cssClass:last-child {
      padding-right: 0; }
    #page_related #bg_secondary, #page_related #bg_primary {
      min-height: 10px; }
      #page_related #bg_secondary p, #page_related #bg_primary p {
        opacity: 1;
        padding: 20px 0;
        -webkit-transition: opacity 1s ease-in-out;
        -moz-transition: opacity 1s ease-in-out;
        -ms-transition: opacity 1s ease-in-out;
        -o-transition: opacity 1s ease-in-out;
        transition: opacity 1s ease-in-out;
        height: auto; }
    #page_related:hover #bg_secondary, #page_related:hover #bg_primary {
      min-height: 120px; }
      #page_related:hover #bg_secondary p, #page_related:hover #bg_primary p {
        opacity: 1;
        -webkit-transition: opacity 1s ease-in-out;
        -moz-transition: opacity 1s ease-in-out;
        -ms-transition: opacity 1s ease-in-out;
        -o-transition: opacity 1s ease-in-out;
        transition: opacity 1s ease-in-out; } }

.custom_icon {
  display: none; }

</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2><?php the_field('titulo_azul', $pageID) ?></h2>
            <h4 class="mt-2"><?php the_field('subtitulo_azul', $pageID) ?></h4>

        </div>
    </div>
</div>
<section id="about">
    <div class="container">
        <div class="row h-justify-content-center align-items-end">
            <div class="col-md-5 mb-5 mt-3">
                <p><?php the_field('descricao_azul', $pageID) ?></p>
            </div>
            <div class="col-md-7" id="custom_image">
                <img src="<?php the_field('image_azul', $pageID) ?>" class="">
            </div>
        </div>
    </div>
</section>
<div class="container mb-5">

    <div class="row justify-content-center mb-5">
        <div class="col-xl-10 col-md-12 col-sm-12 col-xs-12 mt-4 text-center" id="talk_to_us">
            <h4><?php the_field('frase', $pageID) ?></h4>
        </div>
    </div>
        </div> <!-- /. container -->
    </section><!-- /. contact -->

<?php

$posts = get_field('related_page', 96);

if( $posts ): ?>
<section id="page_related">
    <div class="container-fluid">


        <div class="row">

        <?php  $i = 0; ?>
    	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
            <?php
                                          if (($i % 2) == 0){
                                          ?>
                                          <a href="<?php echo get_permalink( $p->ID ); ?>">

    	    <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_primary">

                    <div class="d-flex h-100 align-items-center justify-content-center">
                        <?php $url = get_field('icon_sub', $p->ID); ?>
                        <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                        <p>
                            <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                            <?php echo get_the_title( $p->ID ); ?>
                        </p>

                </div></a>
    	    </div>
             <?php } else{ ?>
                 <a href="<?php echo get_permalink( $p->ID ); ?>">

                 <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_secondary"><a href="<?php echo get_permalink( $p->ID ); ?>">
                     <div class="d-flex h-100 align-items-center justify-content-center">
                             <?php $url = get_field('icon_sub', $p->ID); ?>
                             <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                             <p>
                                 <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                 <?php echo get_the_title( $p->ID ); ?>
                             </p>
                     </div></a>
         	    </div>
             <?php } ?>


                           <?php $i++;?>
    	<?php endforeach; ?>


    </div>
		</div>
    </section>

    <?php endif; ?>



<?php get_footer(); ?>
