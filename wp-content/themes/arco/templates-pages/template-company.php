<?php
/* Template Name: Presença */
?>

<?php get_header(); ?>


<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
 <section id="work">
     <div class="container h-100" id="custom_hseight">
         <div class="row mb-4">
             <div class="col-md-12">
                 <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
             </div>
         </div>
         <div class="row h-10 justify-content-between align-items-center row-eq-height">
             <div class="col-xl-6 col-md-12 col-sm-12 col-xs-12" id="talk_to_us">
                 <h1><?php the_field('title_work_us', $pageID) ?></h1>
                 <p><?php the_field('description_work_us', $pageID)?></p>
                 <div class="row mt-4">
                     <style>
                     h2{
                         color: #001236;
                         font-weight: 700;
                         font-size: 1.1em;
                         margin-top: 15px;
                     }
                     </style>
                     <?php
                     // check if the repeater field has rows of data
                     if( have_rows('repeater_our', $pageID) ):?>
                         <?php // loop through the rows of data
                         while ( have_rows('repeater_our', $pageID) ) : the_row();?>
                            <div class="col-xl-6 col-md-6 mb-4 text-center">
                                <img src="<?php the_sub_field('image'); ?>" class="img-fluid mx-auto" alt="<?php the_sub_field('title_image'); ?>" title="<?php the_sub_field('title_image'); ?>">
                                  <h2><?php the_sub_field('title_image'); ?></h2>
                             </div>
                            <!-- display a sub field value -->
                        <?php endwhile; else :
                        // no rows found ?>
                    <?php endif;?>
                 </div>

             </div><!--/.talk_to_us-->
             <div class="col-xl-6 col-md-12 col-sm-12 col-xs-12" id="work_image" style="top: -40px; position: relative;">
                 <?php echo do_shortcode('[brazilhtml5map id="0"]')?>
				 
             </div><!--/.image-->
         </div><!--/.row-->
     </div> <!-- /. container -->
 </section><!-- /. contact -->

<?php

$posts = get_field('related_page', 96);

if( $posts ): ?>
<section id="page_related">
    <div class="container-fluid">


        <div class="row">

        <?php  $i = 0; ?>
    	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
            <?php
                                          if (($i % 2) == 0){
                                          ?>
                                          <a href="<?php echo get_permalink( $p->ID ); ?>">

    	    <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_primary">

                    <div class="d-flex h-100 align-items-center justify-content-center">
                        <?php $url = get_field('icon_sub', $p->ID); ?>
                        <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                        <p>
                            <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                            <?php echo get_the_title( $p->ID ); ?>
                        </p>

                </div></a>
    	    </div>
             <?php } else{ ?>
                 <a href="<?php echo get_permalink( $p->ID ); ?>">

                 <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_secondary"><a href="<?php echo get_permalink( $p->ID ); ?>">
                     <div class="d-flex h-100 align-items-center justify-content-center">
                             <?php $url = get_field('icon_sub', $p->ID); ?>
                             <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                             <p>
                                 <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                 <?php echo get_the_title( $p->ID ); ?>
                             </p>
                     </div></a>
         	    </div>
             <?php } ?>


                           <?php $i++;?>
    	<?php endforeach; ?>

<script>
    (function($) {
   
    $.fn.cycle = function(timeout, cls) {
        var l = this.length,
            current = 0,
            prev = 0,
            elements = this;

        function next() {
            elements.eq(prev).removeClass(cls);
            elements.eq(current).addClass(cls);
            prev = current;
            current = (current + 1) % l;
            setTimeout(next, timeout);
        }
        setTimeout(next, timeout);
        return this;
    };
    $('div.cssClass').cycle(3000, 'active_border');
		
}(jQuery));

</script>
    </div></div>
    </section>

    <?php endif; ?>



<?php get_footer(); ?>
