<?php
/* Template Name: Nosso time */
?>

<?php get_header(); ?>


<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
 <section id="work">
     <div class="container h-100" id="custom_hseight">
         <div class="row h-100 justify-content-between align-items-center row-eq-height">
             <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
             <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12" id="talk_to_us">
                 <h1><?php the_field('title_work_us', $pageID) ?></h1>
                 <p class="mb-4"><?php the_field('description_work_us', $pageID)?></p>

             </div><!--/.talk_to_us-->
             <?php $loop = new WP_Query(array('post_type' => 'lideres', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'ASC')); ?>
                 <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                 <!--/. get featured image-->
                 <?php $postid = get_the_ID(); ?>
                     <div class="col-md-3 mb-4 col-6" id="lideres_item">
                         <a href="#modal_<?php echo $postid; ?>" class="no" role="button" data-toggle="modal">

                         <!-- Media middle -->
                         <?php if( get_field('avatar_testimonial') ): ?>
                             <figure class="tint">
                                 <img src="<?php echo get_field('avatar_testimonial') ?>" class="media-object avatar_image mx-auto d-block" alt="<?php echo get_the_title(); ?>" title="<?php echo get_the_title(); ?>">
                                 <div class="carousel-caption pb-0">
                                     <img src="/wp-content/themes/arco/img/lider.png" alt="Liderança Arco">
                                      <p class="mt-2"><?php echo get_the_title(); ?></p>
                                      <p><?php if( get_field('profession') ): ?>
                                      <?php the_field('profession'); ?>                         <?php endif; ?>
                                     </p>
                                 </div>
                             </figure>
                         <?php endif; ?>
                     </a>
                    </div>
                    <style>
                    .close {
    float: none;}
    </style>
                    <div class="modal" id="modal_<?php echo $postid;  ?>" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-full" role="document">
   <div class="modal-content" id="lider">
       <div class="modal-header">

       </div>
       <div class="modal-body p-4  h-100 justify-content-center align-items-center row-eq-height" id="result">
               <div class="row h-100 justify-content-center align-items-center row-eq-height">
                   <div class="col-md-9 text-right" style="position: relative; top: 35px;">

                       <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true"><img src="/wp-content/themes/arco/img/close.png" style="width: 35px;"></span>
                       </button>
                   </div>
                   <div class="col-sm-6 col-lg-6 text-center" id="lider_bg">
                       <img src="/wp-content/themes/arco/img/lider_bg.png" alt="" class="custom_image img-fluid">
                       <img src="<?php echo get_field('avatar_testimonial') ?>" class="img-fluid avatar_image mx-auto d-block" alt="<?php echo get_the_title(); ?>" title="<?php echo get_the_title(); ?>">
                   </div>
                   <div class="col-sm-6 col-lg-5">
                       <div class="col-md-12">
                           <img src="/wp-content/themes/arco/img/lider.png" alt="Liderança Arco">
                           <h2 class="mt-3 mb-2"><?php echo get_the_title(); ?></h2>
                           <?php if( get_field('profession') ): ?>
                               <h3><?php the_field('profession'); ?>
                           </h3>
                           <?php endif; ?>
                       </div>
                       <div class="col-md-10 mt-5 mb-5">
 <p><?php echo get_field('testimonial') ?></p>
                       </div>
                       <div class="col-md-12">
                           <?php if( get_field('linkedin') ): ?>
                               <a href="<?php the_field('linkedin'); ?>" target="_blank">
                                   <i class="fa fa-linkedin"></i>
                               </a>
                           <?php endif; ?>
                       </div>

                   </div>
               </div>
            </div>
        </div>
    </div>

                </div>
                 <!--/.item <?php echo get_the_title( $post_id ); ?> -->
             <?php endwhile; ?>
             <?php $loop = new WP_Query(array('post_type' => 'colaboradores', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'ASC')); ?>
                 <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                 <!--/. get featured image-->
                 <?php $postid = get_the_ID(); ?>
                     <div class="col-md-3 mb-4 col-6" id="colabora_item">
                         <a href="#modal_<?php echo $postid; ?>" class="no" role="button" data-toggle="modal">

                         <!-- Media middle -->
                         <?php if( get_field('avatar_testimonial') ): ?>
                             <figure class="tint">
                                 <img src="<?php echo get_field('avatar_testimonial') ?>" class="media-object avatar_image mx-auto d-block" alt="<?php echo get_the_title(); ?>" title="<?php echo get_the_title(); ?>">
                                 <div class="carousel-caption pb-0">
                                     <img src="/wp-content/themes/arco/img/colabora.png" alt="Liderança Arco">
                                      <p class="mt-2"><?php echo get_the_title(); ?></p>
                                      <p><?php if( get_field('profession') ): ?>
                                      <?php the_field('profession'); ?>                         <?php endif; ?>
                                     </p>
                                 </div>
                             </figure>
                         <?php endif; ?>
                     </a>
                    </div>
                    <div class="modal" id="modal_<?php echo $postid;  ?>" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-full" role="document">
   <div class="modal-content" id="colabora">
       <div class="modal-header">

       </div>
       <div class="modal-body p-4  h-100 justify-content-center align-items-center row-eq-height" id="result">
               <div class="row h-100 justify-content-center align-items-center row-eq-height">
                   <div class="col-md-9 text-right" style="position: relative; top: 35px;">

                       <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true"><img src="/wp-content/themes/arco/img/close.png" style="width: 35px;"></span>
                       </button>
                   </div>
                   <div class="col-sm-6 col-lg-6 text-center" id="lider_bg">
                       <img src="/wp-content/themes/arco/img/colabora_bg.png" alt="" class="custom_image img-fluid">
                       <img src="<?php echo get_field('avatar_testimonial') ?>" class="img-fluid avatar_image mx-auto d-block" alt="<?php echo get_the_title(); ?>" title="<?php echo get_the_title(); ?>">
                   </div>
                   <div class="col-sm-6 col-lg-5">
                       <div class="col-md-12">
                           <img src="/wp-content/themes/arco/img/colabora.png" alt="Liderança Arco">
                           <h2 class="mt-3 mb-2"><?php echo get_the_title(); ?></h2>
                           <?php if( get_field('profession') ): ?>
                               <h3><?php the_field('profession'); ?>
                           </h3>
                           <?php endif; ?>
                       </div>
                       <div class="col-md-10 mt-5 mb-5">
 <p><?php echo get_field('testimonial') ?></p>
                       </div>
                       <div class="col-md-12">
                           <?php if( get_field('linkedin') ): ?>
                               <a href="<?php the_field('linkedin'); ?>" target="_blank">
                                   <i class="fa fa-linkedin"></i>
                               </a>
                           <?php endif; ?>
                       </div>

                   </div>
               </div>
            </div>
        </div>
    </div>

                </div>
                 <!--/.item <?php echo get_the_title( $post_id ); ?> -->
             <?php endwhile; ?>
         </div><!--/.row-->
     </div> <!-- /. container -->
 </section><!-- /. contact -->



<?php get_footer(); ?>
