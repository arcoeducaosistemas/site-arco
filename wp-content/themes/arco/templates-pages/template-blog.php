<?php
/* Template Name: Blog */
?>

<?php get_header(); ?>



<section id="blog" class="inner">
    <div class="container">
        <div class="row mb-3 bt-3">
            <h2>Confira nossos conteúdos preparados para você</h2>
        </div>
        <div class="row row-eq-height">
            <?php $loop =
                new WP_Query(array( 'post_type' => 'post', 'posts_per_page' => -1, 'order' => 'ASC'));
            ?>
           <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
               <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
               <!--/. get featured image-->
               <?php $post_date = get_the_date( 'j F Y' ); ?>
               <!--/. get date post-->

               <div class="col-md-4 col-sm-4 col-xs-12 mb-4">
                   <div class="col-md-12 nopadding-p" id="blog_post">
                       <a href="<?php the_permalink(); ?>">
                           <div class="cardss">
                               <div class="img-container">
                                   <img src="<?php echo $url ?>" class="max-auto d-block img-fluid blog-home" alt="<?php echo get_the_title( $post_id ); ?>" title="<?php echo get_the_title( $post_id ); ?>"/>
                                   <!--/. featured image-->
                               </div>
                           </div>
                       </a>
                       <a href="<?php the_permalink(); ?>">
                           <h3><?php echo get_the_title(); ?></h3>
                           <!-- <span class="author">por <b><?php echo get_the_author_link(); ?></b> em <?php echo $post_date ?></span>
                           <p><?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true); ?></p> -->
                       </a>
                       <!--/.link-->

                   </div>
               </div>
               <!--/.item <?php echo get_the_title( $post_id ); ?> -->
           <?php endwhile; ?>
        </div>
    </div>
</section>


<?php get_footer(); ?>
