<?php
/* Template Name: Página Principal */
?>

<?php get_header(); ?>


<?php get_template_part('/templates/index/template-part', '1-slider'); ?>


<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js'></script>
<script>

(function($) {
$("#carousel").swipe({

  swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

    if (direction == 'left') $(this).carousel('next');
    if (direction == 'right') $(this).carousel('prev');

  },
  allowPageScroll:"vertical"

});})(jQuery);
</script>
<?php get_footer(); ?>
