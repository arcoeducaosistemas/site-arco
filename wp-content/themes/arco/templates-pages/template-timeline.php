<?php
/* Template Name: Linha do tempos */
?>

<?php get_header(); ?>


<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
 <section id="work" style="height: 100vh;">
     <div class="container h-100" id="custom_hseight">
         <div class="row mb-4">
             <div class="col-md-12">
                 <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
             </div>
         </div>
         <style>
         @media screen and (min-width: 1199px) and (max-width: 1400px){
             #work{
                 height: 110vh !important;
             }
         }
         #time img, #time p{
                display: none;
         }
         .active#time img, .active#time p{
             display: block;
         }
         #work h1{
             color: #a4a6ac;
         }
         #work .active#time {
             color: #001236;
         }
         #work h1 {
    text-align: left;
    font-size: 1.5em;
}
         @media (min-width: 768px){
.col-md-2 {
    flex: 0 0 7.66667%;
    max-width: 16.66667%;
}
}
         </style>


        <style>
         .timeline-section1 .timeline-icon-section:after, .timeline-section2 .timeline-icon-section:after{
             content: '';
         }
         .timeline-section1 .timeline-icon-section, .timeline-section2 .timeline-content-top {
            position: relative;
            border-bottom: 1px dotted #9097a5;
            height: 210px;
            border-width: 2px;
        }
        .timeline-content-bottom {
    margin-top: 15px;
}
        .timeline-content-bottom p{
    min-height: 195px;
}
.borders{
    display: none;
}
.timeline-icon-section_custom  img{
    opacity: 0;
}
@media screen and (min-width: 767px){
    #work .timeline .timeline-section1 .active p,
     #work .timeline .timeline-section1.active p,  #work .timeline .timeline-section1.active h1, #work .timeline .timeline-section1 .active h1{
        color: #001236 !important;
    }
}
.timeline-content-bottom .timeline-icon-section_custom img{
    position: absolute;
    top: -360px;
}
.timeline-content-bottom:hover .timeline-icon-section_custom img, .timeline-section1.active .timeline-icon-section_custom img{
    opacity: 1;
    position: absolute;
top: -360px;
}}
.timeline-section1 .timeline-icon-section, .timeline-section2 .timeline-content-top {
    position: relative;
    border-bottom: 1px dotted #9097a5;
    border-width: 2px;
    padding: 20px;
    height: auto;
}
.img-thumbnail{
    border-top-right-radius: 22px;
    border-bottom-left-radius: 22px;
    top: -25px;
    position: relative;
        transform: scale(2);

}
         </style>
             <div class="row  h-100 justify-content-between align-items-center row-eq-height timeline text-center mt-5">
				      <?php

                // check if the repeater field has rows of data
                if( have_rows('repeater_timeline', $pageID) ):

                 	// loop through the rows of data
                    while ( have_rows('repeater_timeline', $pageID) ) : the_row();?>

                    <div class="col-lg-2 col-sm-12 col-12 timeline-section1 text-danger">
                      <div class="row">
                          <div class="col-lg-12 col-md-12 col-12 timeline-icon-section" style="min-height: 200px">
                              <img src="<?php the_sub_field('image');?>" class="scale_img img-thumbnail d-none">
                              <div class="borders"></div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-12 timeline-content-bottom">
                              <h1><?php the_sub_field('ano');?></h1>
                              <p><?php the_sub_field('frase');?></p>
   
                          <div class="col-lg-12 col-md-12 col-12 timeline-icon-section_custom">
                              <img src="<?php the_sub_field('image');?>" class="scale_img img-thumbnail">
                          </div></div>
                      </div>
                    </div>

                    <?php endwhile;

                else :

                    // no rows found

                endif;
                ?>
             
         </div><!--/.row-->
     </div> <!-- /. container -->
 </section><!-- /. contact -->

<?php

$posts = get_field('related_page', 96);

if( $posts ): ?>
<section id="page_related">
    <div class="container-fluid">


        <div class="row">

        <?php  $i = 0; ?>
    	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
            <?php
                                          if (($i % 2) == 0){
                                          ?>
                                          <a href="<?php echo get_permalink( $p->ID ); ?>">

    	    <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_primary">

                    <div class="d-flex h-100 align-items-center justify-content-center">
                        <?php $url = get_field('icon_sub', $p->ID); ?>
                        <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                        <p>
                            <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                            <?php echo get_the_title( $p->ID ); ?>
                        </p>

                </div></a>
    	    </div>
             <?php } else{ ?>
                 <a href="<?php echo get_permalink( $p->ID ); ?>">

                 <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_secondary"><a href="<?php echo get_permalink( $p->ID ); ?>">
                     <div class="d-flex h-100 align-items-center justify-content-center">
                             <?php $url = get_field('icon_sub', $p->ID); ?>
                             <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                             <p>
                                 <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                 <?php echo get_the_title( $p->ID ); ?>
                             </p>
                     </div></a>
         	    </div>
             <?php } ?>


                           <?php $i++;?>
    	<?php endforeach; ?>


    </div></div>
    </section>

    <?php endif; ?>



<?php get_footer(); ?>

<script>
var int_sec = 1000;
var interval = setTimeout(next_, int_sec);
var items_timeline = jQuery(".timeline .timeline-section1");
var curr_item = 0;
var isPaused = false;
function next_(){
	if(!isPaused){
		jQuery(".timeline .timeline-section1").removeClass("active");
		jQuery(items_timeline[curr_item]).addClass("active");
		
		var total = items_timeline.length;
		if(curr_item == total-1){
			curr_item = 0;
		}else{
			curr_item++;
		}
		interval = setTimeout(next_, int_sec);
	}
	int_sec = 5000;
}
jQuery(".timeline .timeline-section1").each(function(index){
	var curr_item_ = index;
	jQuery(this).on("mouseover",function(){
		clearTimeout(interval);
		curr_item = curr_item_;
		jQuery(".timeline .timeline-section1").removeClass("active");
		jQuery(this).addClass("active");
		isPaused = true;
	});
	jQuery(this).on("mouseout",function(){
    	jQuery('.timeline .timeline-section1').cycle('resume'); 
		isPaused = false;
		interval = setTimeout(next_, int_sec);
	});
});

</script>
