<?php
/* Template Name: Linha do tempo */
?>

<?php get_header(); ?>


<?php
global $wp_query;
$pageID = $pageID = $wp_query->post->ID;

 ?>
 <section id="work">
     <div class="container h-100" id="custom_hseight">
         <div class="row mb-4">
             <div class="col-md-12">
                 <?php get_template_part('/templates/global/template-part', '1-breadcrumbs'); ?>
             </div>
         </div>
             <div class="row  h-100 justify-content-between align-items-center row-eq-height timeline text-center">
                 <div class="col-lg-2 col-sm-12 col-12 timeline-section1 text-danger">
                   <div class="row">
                       <div class="col-lg-12 col-md-12 col-12 timeline-icon-section">
                           <img src="/wp-content/themes/arco/img/timeline.png" class="scale_img img-thumbnail">
                           <div class="borders"></div>
                       </div>
                       <div class="col-lg-12 col-md-12 col-12 timeline-content-bottom">
                           <h1>2004</h1>
                           <p> Plantamos a semente, criada a primeira plataforma de educação do grupo</p>
                       </div>
                   </div>
                 </div>
                 <div class="col-lg-2 col-sm-12 col-12 timeline-section2 text-info">
                   <div class="row">
                       <div class="col-lg-12 col-md-12 col-12 timeline-content-top custom_p">
                           <p>Nasce uma missão: multiplicar e ampliar a educação de excelência pelo país</p>
                           <h1 class="mt-3">2006</h1>
                       </div>
                       <div class="col-lg-12 col-md-12 col-12 timeline-icon-section">
                           <div class="borders"></div>
                           <img src="/wp-content/uploads/2018/07/2006-min.jpg" class="scale_img img-thumbnail">
                       </div>
                   </div>
                 </div>
                 <div class="col-lg-2 col-sm-12 col-12 timeline-section1 text-danger">
                   <div class="row">
                       <div class="col-lg-12 col-md-12 col-12 timeline-icon-section">
                           <img src="/wp-content/themes/arco/img/timeline.png" class="scale_img img-thumbnail">
                           <div class="borders"></div>
                       </div>
                       <div class="col-lg-12 col-md-12 col-12 timeline-content-bottom">
                           <h1>2010</h1>
                           <p>A Arco ganha capilaridade e inicia o processo de construção de uma grande marca</p>
                       </div>
                   </div>
                 </div>
                 <div class="col-lg-2 col-sm-12 col-12 timeline-section2 text-info">
                   <div class="row">
                       <div class="col-lg-12 col-md-12 col-12 timeline-content-top custom_p">
                           <p>Investimento da General Atlantic. Um novo parceiro estratégico adere à missão e fortalece o plano de sonhar grande</p>
                           <h1 class="mt-3">2014</h1>
                       </div>
                       <div class="col-lg-12 col-md-12 col-12 timeline-icon-section">
                           <div class="borders"></div>
                           <img src="/wp-content/themes/arco/img/timeline.png" class="scale_img img-thumbnail">
                       </div>
                   </div>
                 </div>
                 <div class="col-lg-2 col-sm-12 col-12 timeline-section1 text-danger">
                   <div class="row">
                       <div class="col-lg-12 col-md-12 col-12 timeline-icon-section">
                           <img src="/wp-content/themes/arco/img/timeline.png" class="scale_img img-thumbnail">
                           <div class="borders"></div>
                       </div>
                       <div class="col-lg-12 col-md-12 col-12 timeline-content-bottom">
                           <h1>2015</h1>
                           <p>Abrangência nacional. As soluções da Arco chegam a Santa Catarina, completando a presença nos 26 estados brasileiros e no Distrito Federal</p>
                       </div>
                   </div>
                 </div>
                 <div class="col-lg-2 col-sm-12 col-12 timeline-section2 text-info">
                   <div class="row">
                       <div class="col-lg-12 col-md-12 col-12 timeline-content-top custom_p">
                           <p> Ensino Adaptativo. Aplicação de inovações nas soluções desenvolvidas</p>
                           <h1 class="mt-3">2016</h1>
                       </div>
                       <div class="col-lg-12 col-md-12 col-12 timeline-icon-section">
                           <div class="borders"></div>
                           <img src="/wp-content/themes/arco/img/timeline.png" class="scale_img img-thumbnail">
                       </div>
                   </div>
                 </div>
                 <div class="col-lg-2 col-sm-12 col-12 timeline-section1 text-danger">
                   <div class="row">
                       <div class="col-lg-12 col-md-12 col-12 timeline-icon-section">
                           <img src="/wp-content/themes/arco/img/timeline.png" class="scale_img img-thumbnail">
                           <div class="borders"></div>
                       </div>
                       <div class="col-lg-12 col-md-12 col-12 timeline-content-bottom">
                           <h1>2018</h1>
                           <p>Time de peso. Equipe da Arco atinge a marca de mil colaboradores apaixonados e voltados a um mesmo propósito</p>
                       </div>
                   </div>
                 </div>
         </div><!--/.row-->
     </div> <!-- /. container -->
 </section><!-- /. contact -->

<?php

$posts = get_field('related_page', 96);

if( $posts ): ?>
<section id="page_related">
    <div class="container-fluid">


        <div class="row">

        <?php  $i = 0; ?>
    	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
            <?php
                                          if (($i % 2) == 0){
                                          ?>
                                          <a href="<?php echo get_permalink( $p->ID ); ?>">

    	    <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_primary">

                    <div class="d-flex h-100 align-items-center justify-content-center">
                        <?php $url = get_field('icon_sub', $p->ID); ?>
                        <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                        <p>
                            <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                            <?php echo get_the_title( $p->ID ); ?>
                        </p>

                </div></a>
    	    </div>
             <?php } else{ ?>
                 <a href="<?php echo get_permalink( $p->ID ); ?>">

                 <div class="col-md-3 text-center cssClass col-3 item_<?php echo $i; ?>" id="bg_secondary"><a href="<?php echo get_permalink( $p->ID ); ?>">
                     <div class="d-flex h-100 align-items-center justify-content-center">
                             <?php $url = get_field('icon_sub', $p->ID); ?>
                             <img src="<?php echo $url ?>" class="mx-auto mr-3 custom_icon" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>

                             <p>
                                 <img src="<?php echo $url ?>" class="mx-auto mr-3" alt="<?php echo get_the_title( $p->ID ); ?>" title="<?php echo get_the_title( $p->ID ); ?>"/>
                                 <?php echo get_the_title( $p->ID ); ?>
                             </p>
                     </div></a>
         	    </div>
             <?php } ?>


                           <?php $i++;?>
    	<?php endforeach; ?>


    </div></div>
    </section>

    <?php endif; ?>



<?php get_footer(); ?>
