<?php
/**
* 1. Removes the parent themes stylesheet and scripts from inc/enqueue.php
* 2. Enqueue child theme stylesheet and scripts from inc
* 3. Get custom css to login page and logo from acf field
* 4. Change text button login page
* 5. Disable the emoji's
*/

//1
/**
* Removes the parent themes stylesheet and scripts from inc/enqueue.php
*/
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );
// [location id=""]
function location_shortcode( $atts ) {

	extract( shortcode_atts(
		array(
			'id' => '2',
			), $atts )
	);

	if ( isset( $id ) && get_field('location', 'option') ) {

	$google_map = get_field('location', 'option');

$url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$google_map['lat'].','.$google_map['lng'].'&sensor=true';
$data = @file_get_contents($url);
$jsondata = json_decode($data,true);
if(is_array($jsondata) && $jsondata['status'] == "OK")
{

// city
foreach ($jsondata["results"] as $result) {
    foreach ($result["address_components"] as $address) {
        if (in_array("locality", $address["types"])) {
            $city = $address["long_name"];
        }
    }
}
// country
foreach ($jsondata["results"] as $result) {
    foreach ($result["address_components"] as $address) {
        if (in_array("country", $address["types"])) {
            $country = $address["long_name"];
        }
    }
}

}
$seperator = ', ';

$location = $city . $seperator . $country;

return $city;

}
}
add_shortcode( 'location', 'location_shortcode' );

//2
/**
* Enqueue child theme stylesheet and scripts from inc
*/
function theme_enqueue_styles() {
	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'wow-scripts', get_stylesheet_directory_uri() . '/js/wow.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script('owl', get_stylesheet_directory_uri() . '/js/owl-carousel.min.js', array('jquery'),'3', true);

    if ( is_page( '21' ) ) {
		wp_enqueue_script('maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA_xY08iAciVR4g_gzyUptWAXlGCVXqsec', array('jquery'),'20150825', true);
		
    }
   
    
    if ( is_page( '21' ) ) {
        wp_enqueue_script('height', get_stylesheet_directory_uri() . '/js/height.js', array('jquery'),'20150825', true);
    }

    wp_enqueue_script( 'sidebar', get_stylesheet_directory_uri() . '/js/jquery.mCustomScrollbar.concat.min.js', array('jquery'), '3.3.5', true );

    wp_enqueue_script('easing', get_stylesheet_directory_uri() . '/js/jquery.easing.min.js', array('jquery'),'3', true);
    wp_enqueue_script('main', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'),'20150825', true);
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

//3
/**
* Get custom css to login page
* Insert custom login css login page
* Get field from acf page options
*/
function my_custom_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/login/custom-login-styles.css" />';
    echo
    '<style type="text/css">
        h1 a {
            background-image: url('.get_field('logo_site', 'options'). ') !important;
         }
     </style>';
}
add_action('login_head', 'my_custom_login');

//4
/**
 * Text button login page
 *
*/
add_action( 'login_form', 'change_text_button_login' );

function change_text_button_login() {
    add_filter( 'gettext', 'change_text_button_login_page', 10, 2 );
}

function change_text_button_login_page( $translation, $text ){
    if ( 'Log In' == $text ) {
        return 'Entrar';
    }
    return $translation;
}

//5
/**
* Disable the emoji's
*/
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
* Filter function used to remove the tinymce emoji plugin.
*
* @param array $plugins
* @return array Difference betwen the two arrays
*/
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

/**
* Remove emoji CDN hostname from DNS prefetching hints.
*
* @param array $urls URLs to print for resource hints.
* @param string $relation_type The relation type the URLs are printed for.
* @return array Difference betwen the two arrays.
*/
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
        $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }
    return $urls;
    /* Disable WordPress Admin Bar for all users but admins. */
    show_admin_bar(false);
}

function bloglite_breadcrumb() {
    global $post;
    echo '<ul id="breadcrumbs">';
    if (!is_home()) {
        echo '<li><a href="';
        echo get_option('home');
        echo '">';
        echo 'Home';
        echo '</a></li><li class="separador"> > </li>';
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li class="separador"> > </li><li> ');
            if (is_single()) {
                echo '</li><li class="separador"> > </li><li>';
                the_title();
                echo '</li>';
            }
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {
                    $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separador">/</li>';
                }
                echo $output;
                echo '<strong title="'.$title.'"> '.$title.'</strong>';
            } else {
                echo '<li><strong> '.get_the_title().'</strong></li>';
            }
        }
    }
    elseif (is_tag()) { single_tag_title();}
    elseif (is_day()) { echo "<li>Arquivo de "; the_time('j \d\e F \d\e Y'); echo'</li>'; }
    elseif (is_month()) { echo "<li>Arquivo de "; the_time('F \d\e Y'); echo'</li>'; }
    elseif (is_year()) { echo "<li>Arquivo de "; the_time('Y'); echo'</li>'; }
    elseif (is_author()) { echo "<li>Arquivo do autor"; echo'</li>'; }
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { echo "<li>Arquivo do blog"; echo'</li>'; }
    elseif (is_search()) { echo "<li>Resultados da pesquisa"; echo'</li>'; }
    echo '</ul>';
}
?>
