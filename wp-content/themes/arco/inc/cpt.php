<?php
/*
    CPT Name: Slider
    Description: Custom post types slider.
    Author: Mateus Carvalho
    Author URI: http://www.circusdigital.com.br
*/

add_action( 'init', 'slider_items' );

function slider_items() {

register_post_type( 'slider', array(
  'labels' => array(
    'name' => 'Slider',
    'singular_name' => 'Slider',
   ),
  'description' => 'Cadastro de slider',
  'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
  'publicly_queryable' => true,  // you should be able to query it
  'show_ui' => true,  // you should be able to edit it in wp-admin
  'exclude_from_search' => true,  // you should exclude it from search results
  'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
  'has_archive' => false,  // it shouldn't have archive page
  'rewrite' => false,  // it shouldn't have rewrite rules
  'menu_position' => 21,
  'menu_icon' => 'dashicons-format-quote',
  'supports' => array( 'title')
));
}


/*
    CPT Name: Liderança
    Description: Custom post types lideranca.
    Author: Mateus Carvalho
    Author URI: http://www.circusdigital.com.br
*/

add_action( 'init', 'lideranca_items' );

function lideranca_items() {

register_post_type( 'lideres', array(
  'labels' => array(
    'name' => 'Líderes',
    'singular_name' => 'Líderes',
   ),
  'description' => 'Cadastro de líderes',
  'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
  'publicly_queryable' => true,  // you should be able to query it
  'show_ui' => true,  // you should be able to edit it in wp-admin
  'exclude_from_search' => true,  // you should exclude it from search results
  'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
  'has_archive' => false,  // it shouldn't have archive page
  'rewrite' => false,  // it shouldn't have rewrite rules
  'menu_position' => 21,
  'menu_icon' => 'dashicons-format-quote',
  'supports' => array( 'title')
));
}



/*
    CPT Name: Colaboradores
    Description: Custom post types Colaboradores.
    Author: Mateus Carvalho
    Author URI: http://www.circusdigital.com.br
*/

add_action( 'init', 'colaboradores_items' );

function colaboradores_items() {

register_post_type( 'colaboradores', array(
  'labels' => array(
    'name' => 'Colaboradores',
    'singular_name' => 'Colaboradores',
   ),
  'description' => 'Cadastro de colaboradores',
  'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
  'publicly_queryable' => true,  // you should be able to query it
  'show_ui' => true,  // you should be able to edit it in wp-admin
  'exclude_from_search' => true,  // you should exclude it from search results
  'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
  'has_archive' => false,  // it shouldn't have archive page
  'rewrite' => false,  // it shouldn't have rewrite rules
  'menu_position' => 21,
  'menu_icon' => 'dashicons-format-quote',
  'supports' => array( 'title')
));
}


?>
