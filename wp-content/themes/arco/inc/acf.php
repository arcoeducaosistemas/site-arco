<?php
    //Generate Custom CSS
    function generate_options_css() {
        $ss_dir = get_stylesheet_directory();
        ob_start(); // Capture all output into buffer
        require($ss_dir . '/inc/custom-styles.php'); // Grab the custom-style.php file
        $css = ob_get_clean(); // Store output in a variable, then flush the buffer
        file_put_contents($ss_dir . '/css/custom-style.css', $css, LOCK_EX); // Save it as a css file
    }
    add_action( 'acf/save_post', 'generate_options_css' ); //Parse the output and write the CSS file on post save

    //Create Options Page with Custom Fields
    if( function_exists('acf_add_options_page') ) {

    	acf_add_options_page(array(
    		'page_title' 	=> 'Configurações Gerais',
    		'menu_title'	=> 'Configurações',
            'position'      => 1,
    		'menu_slug' 	=> 'theme-general-settings',
    		'capability'	=> 'edit_posts',
    		'redirect'		=> false
    	));
   
         acf_add_options_sub_page(array(
            'page_title' 	=> 'Trabalhe conosco',
            'menu_title'	=> 'Trabalhe conosco',
            'parent_slug'	=> 'theme-general-settings',
        ));
        acf_add_options_sub_page(array(
            'page_title' 	=> 'Contato',
            'menu_title'	=> 'Contato',
            'parent_slug'	=> 'theme-general-settings',
        ));
  

    }

    function my_acf_init() {
        acf_update_setting('google_api_key', 'AIzaSyA_xY08iAciVR4g_gzyUptWAXlGCVXqsec');
    }

    add_action('acf/init', 'my_acf_init');

?>
