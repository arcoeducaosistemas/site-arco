!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="1.12.4",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a){return n.each(this,a)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(e.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(n.isPlainObject(c)||(b=n.isArray(c)))?(b?(b=!1,f=a&&n.isArray(a)?a:[]):f=a&&n.isPlainObject(a)?a:{},g[d]=n.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray||function(a){return"array"===n.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){var b=a&&a.toString();return!n.isArray(a)&&b-parseFloat(b)+1>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==n.type(a)||a.nodeType||n.isWindow(a))return!1;try{if(a.constructor&&!k.call(a,"constructor")&&!k.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(!l.ownFirst)for(b in a)return k.call(a,b);for(b in a);return void 0===b||k.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a},globalEval:function(b){b&&n.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b){var c,d=0;if(s(a)){for(c=a.length;c>d;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(h)return h.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,g=0,h=[];if(s(a))for(d=a.length;d>g;g++)e=b(a[g],g,c),null!=e&&h.push(e);else for(g in a)e=b(a[g],g,c),null!=e&&h.push(e);return f.apply([],h)},guid:1,proxy:function(a,b){var c,d,f;return"string"==typeof b&&(f=a[b],b=a,a=f),n.isFunction(a)?(c=e.call(arguments,2),d=function(){return a.apply(b||this,c.concat(e.call(arguments)))},d.guid=a.guid=a.guid||n.guid++,d):void 0},now:function(){return+new Date},support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()});function s(a){var b=!!a&&"length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},da=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a)))if(f=o[1]){if(9===x){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(o[2])return H.apply(d,b.getElementsByTagName(a)),d;if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName)return H.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x)w=b,s=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";while(h--)r[h]=l+" "+qa(r[h]);s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b}if(s)try{return H.apply(d,w.querySelectorAll(s)),d}catch(y){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(Q,"$1"),b,d,e)}function ga(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ha(a){return a[u]=!0,a}function ia(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ja(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function la(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ia(function(a){var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return ka(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},fa.matches=function(a,b){return fa(a,null,null,b)},fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fa(b,n,null,[a]).length>0},fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fa.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ha(function(a){return function(b){return fa(a,b).length>0}}),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Y.test(a.nodeName)},input:function(a){return X.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:na(function(){return[0]}),last:na(function(a,b){return[b-1]}),eq:na(function(a,b,c){return[0>c?c+b:c]}),even:na(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:na(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:na(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:na(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=la(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=ma(b);function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){c&&!(e=R.exec(h))||(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));for(g in d.filter)!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fa.error(a):z(a,i).slice(0)};function qa(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j,k=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f)return k[2]=h[2];if(i[d]=k,k[2]=a(b,c,g))return!0}}}function sa(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ta(a,b,c){for(var d=0,e=b.length;e>d;d++)fa(a,b[d],c);return c}function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(c&&!c(f,d,e)||(g.push(f),j&&b.push(h)));return g}function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b},h,!0),l=ra(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[ra(sa(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))}m.push(c)}return sa(m)}function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=F.call(i));u=ua(u)}H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ha(f):f}return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xa(e,d)),f.selector=a}return f},i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=W.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ia(function(a){return null==a.getAttribute("disabled")})||ja(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fa}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},v=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;function z(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(y.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return n.inArray(a,b)>-1!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;e>b;b++)if(n.contains(d[b],this))return!0}));for(b=0;e>b;b++)n.find(a,d[b],c);return c=this.pushStack(e>1?n.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(z(this,a||[],!1))},not:function(a){return this.pushStack(z(this,a||[],!0))},is:function(a){return!!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length}});var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||A,"string"==typeof a){if(e="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b))for(e in b)n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}if(f=d.getElementById(e[2]),f&&f.parentNode){if(f.id!==e[2])return A.find(a);this.length=1,this[0]=f}return this.context=d,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};C.prototype=n.fn,A=n(d);var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};n.fn.extend({has:function(a){var b,c=n(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(n.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?n.inArray(this[0],n(a)):n.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function F(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return u(a,"parentNode")},parentsUntil:function(a,b,c){return u(a,"parentNode",c)},next:function(a){return F(a,"nextSibling")},prev:function(a){return F(a,"previousSibling")},nextAll:function(a){return u(a,"nextSibling")},prevAll:function(a){return u(a,"previousSibling")},nextUntil:function(a,b,c){return u(a,"nextSibling",c)},prevUntil:function(a,b,c){return u(a,"previousSibling",c)},siblings:function(a){return v((a.parentNode||{}).firstChild,a)},children:function(a){return v(a.firstChild)},contents:function(a){return n.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||(e=n.uniqueSort(e)),D.test(a)&&(e=e.reverse())),this.pushStack(e)}});var G=/\S+/g;function H(a){var b={};return n.each(a.match(G)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return n.each(arguments,function(a,b){var c;while((c=n.inArray(b,f,c))>-1)f.splice(c,1),h>=c&&h--}),this},has:function(a){return a?n.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=!0,c||j.disable(),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(d>1)for(i=new Array(d),j=new Array(d),k=new Array(d);d>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f;return f||g.resolveWith(k,c),g.promise()}});var I;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))}});function J(){d.addEventListener?(d.removeEventListener("DOMContentLoaded",K),a.removeEventListener("load",K)):(d.detachEvent("onreadystatechange",K),a.detachEvent("onload",K))}function K(){(d.addEventListener||"load"===a.event.type||"complete"===d.readyState)&&(J(),n.ready())}n.ready.promise=function(b){if(!I)if(I=n.Deferred(),"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll)a.setTimeout(n.ready);else if(d.addEventListener)d.addEventListener("DOMContentLoaded",K),a.addEventListener("load",K);else{d.attachEvent("onreadystatechange",K),a.attachEvent("onload",K);var c=!1;try{c=null==a.frameElement&&d.documentElement}catch(e){}c&&c.doScroll&&!function f(){if(!n.isReady){try{c.doScroll("left")}catch(b){return a.setTimeout(f,50)}J(),n.ready()}}()}return I.promise(b)},n.ready.promise();var L;for(L in n(l))break;l.ownFirst="0"===L,l.inlineBlockNeedsLayout=!1,n(function(){var a,b,c,e;c=d.getElementsByTagName("body")[0],c&&c.style&&(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",l.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(e))}),function(){var a=d.createElement("div");l.deleteExpando=!0;try{delete a.test}catch(b){l.deleteExpando=!1}a=null}();var M=function(a){var b=n.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b},N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;function P(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(O,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c}catch(e){}n.data(a,b,c)}else c=void 0;
}return c}function Q(a){var b;for(b in a)if(("data"!==b||!n.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;return!0}function R(a,b,d,e){if(M(a)){var f,g,h=n.expando,i=a.nodeType,j=i?n.cache:a,k=i?a[h]:a[h]&&h;if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||n.guid++:h),j[k]||(j[k]=i?{}:{toJSON:n.noop}),"object"!=typeof b&&"function"!=typeof b||(e?j[k]=n.extend(j[k],b):j[k].data=n.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[n.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[n.camelCase(b)])):f=g,f}}function S(a,b,c){if(M(a)){var d,e,f=a.nodeType,g=f?n.cache:a,h=f?a[n.expando]:n.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){n.isArray(b)?b=b.concat(n.map(b,n.camelCase)):b in d?b=[b]:(b=n.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!Q(d):!n.isEmptyObject(d))return}(c||(delete g[h].data,Q(g[h])))&&(f?n.cleanData([a],!0):l.deleteExpando||g!=g.window?delete g[h]:g[h]=void 0)}}}n.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?n.cache[a[n.expando]]:a[n.expando],!!a&&!Q(a)},data:function(a,b,c){return R(a,b,c)},removeData:function(a,b){return S(a,b)},_data:function(a,b,c){return R(a,b,c,!0)},_removeData:function(a,b){return S(a,b,!0)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=n.data(f),1===f.nodeType&&!n._data(f,"parsedAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d])));n._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){n.data(this,a)}):arguments.length>1?this.each(function(){n.data(this,a,b)}):f?P(f,a,n.data(f,a)):void 0},removeData:function(a){return this.each(function(){n.removeData(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=n._data(a,b),c&&(!d||n.isArray(c)?d=n._data(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return n._data(a,c)||n._data(a,c,{empty:n.Callbacks("once memory").add(function(){n._removeData(a,b+"queue"),n._removeData(a,c)})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=n._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}}),function(){var a;l.shrinkWrapBlocks=function(){if(null!=a)return a;a=!1;var b,c,e;return c=d.getElementsByTagName("body")[0],c&&c.style?(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(d.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(e),a):void 0}}();var T=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,U=new RegExp("^(?:([+-])=|)("+T+")([a-z%]*)$","i"),V=["Top","Right","Bottom","Left"],W=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)};function X(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return n.css(a,b,"")},i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&U.exec(n.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,n.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var Y=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)Y(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},Z=/^(?:checkbox|radio)$/i,$=/<([\w:-]+)/,_=/^$|\/(?:java|ecma)script/i,aa=/^\s+/,ba="abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";function ca(a){var b=ba.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}!function(){var a=d.createElement("div"),b=d.createDocumentFragment(),c=d.createElement("input");a.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",l.leadingWhitespace=3===a.firstChild.nodeType,l.tbody=!a.getElementsByTagName("tbody").length,l.htmlSerialize=!!a.getElementsByTagName("link").length,l.html5Clone="<:nav></:nav>"!==d.createElement("nav").cloneNode(!0).outerHTML,c.type="checkbox",c.checked=!0,b.appendChild(c),l.appendChecked=c.checked,a.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!a.cloneNode(!0).lastChild.defaultValue,b.appendChild(a),c=d.createElement("input"),c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),a.appendChild(c),l.checkClone=a.cloneNode(!0).cloneNode(!0).lastChild.checked,l.noCloneEvent=!!a.addEventListener,a[n.expando]=1,l.attributes=!a.getAttribute(n.expando)}();var da={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:l.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]};da.optgroup=da.option,da.tbody=da.tfoot=da.colgroup=da.caption=da.thead,da.th=da.td;function ea(a,b){var c,d,e=0,f="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||n.nodeName(d,b)?f.push(d):n.merge(f,ea(d,b));return void 0===b||b&&n.nodeName(a,b)?n.merge([a],f):f}function fa(a,b){for(var c,d=0;null!=(c=a[d]);d++)n._data(c,"globalEval",!b||n._data(b[d],"globalEval"))}var ga=/<|&#?\w+;/,ha=/<tbody/i;function ia(a){Z.test(a.type)&&(a.defaultChecked=a.checked)}function ja(a,b,c,d,e){for(var f,g,h,i,j,k,m,o=a.length,p=ca(b),q=[],r=0;o>r;r++)if(g=a[r],g||0===g)if("object"===n.type(g))n.merge(q,g.nodeType?[g]:g);else if(ga.test(g)){i=i||p.appendChild(b.createElement("div")),j=($.exec(g)||["",""])[1].toLowerCase(),m=da[j]||da._default,i.innerHTML=m[1]+n.htmlPrefilter(g)+m[2],f=m[0];while(f--)i=i.lastChild;if(!l.leadingWhitespace&&aa.test(g)&&q.push(b.createTextNode(aa.exec(g)[0])),!l.tbody){g="table"!==j||ha.test(g)?"<table>"!==m[1]||ha.test(g)?0:i:i.firstChild,f=g&&g.childNodes.length;while(f--)n.nodeName(k=g.childNodes[f],"tbody")&&!k.childNodes.length&&g.removeChild(k)}n.merge(q,i.childNodes),i.textContent="";while(i.firstChild)i.removeChild(i.firstChild);i=p.lastChild}else q.push(b.createTextNode(g));i&&p.removeChild(i),l.appendChecked||n.grep(ea(q,"input"),ia),r=0;while(g=q[r++])if(d&&n.inArray(g,d)>-1)e&&e.push(g);else if(h=n.contains(g.ownerDocument,g),i=ea(p.appendChild(g),"script"),h&&fa(i),c){f=0;while(g=i[f++])_.test(g.type||"")&&c.push(g)}return i=null,p}!function(){var b,c,e=d.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(l[b]=c in a)||(e.setAttribute(c,"t"),l[b]=e.attributes[c].expando===!1);e=null}();var ka=/^(?:input|select|textarea)$/i,la=/^key/,ma=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,na=/^(?:focusinfocus|focusoutblur)$/,oa=/^([^.]*)(?:\.(.+)|)/;function pa(){return!0}function qa(){return!1}function ra(){try{return d.activeElement}catch(a){}}function sa(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)sa(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=qa;else if(!e)return a;return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)})}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=n.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return"undefined"==typeof n||a&&n.event.triggered===a.type?void 0:n.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(G)||[""],h=b.length;while(h--)f=oa.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=n.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=n.event.special[o]||{},l=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},i),(m=g[o])||(m=g[o]=[],m.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,l):m.push(l),n.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n.hasData(a)&&n._data(a);if(r&&(k=r.events)){b=(b||"").match(G)||[""],j=b.length;while(j--)if(h=oa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=m.length;while(f--)g=m[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(m.splice(f,1),g.selector&&m.delegateCount--,l.remove&&l.remove.call(a,g));i&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(k)&&(delete r.handle,n._removeData(a,"events"))}},trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];if(i=m=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!na.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),h=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),l=n.event.special[q]||{},f||!l.trigger||l.trigger.apply(e,c)!==!1)){if(!f&&!l.noBubble&&!n.isWindow(e)){for(j=l.delegateType||q,na.test(j+q)||(i=i.parentNode);i;i=i.parentNode)p.push(i),m=i;m===(e.ownerDocument||d)&&p.push(m.defaultView||m.parentWindow||a)}o=0;while((i=p[o++])&&!b.isPropagationStopped())b.type=o>1?j:l.bindType||q,g=(n._data(i,"events")||{})[b.type]&&n._data(i,"handle"),g&&g.apply(i,c),g=h&&i[h],g&&g.apply&&M(i)&&(b.result=g.apply(i,c),b.result===!1&&b.preventDefault());if(b.type=q,!f&&!b.isDefaultPrevented()&&(!l._default||l._default.apply(p.pop(),c)===!1)&&M(e)&&h&&e[q]&&!n.isWindow(e)){m=e[h],m&&(e[h]=null),n.event.triggered=q;try{e[q]()}catch(s){}n.event.triggered=void 0,m&&(e[h]=m)}return b.result}},dispatch:function(a){a=n.event.fix(a);var b,c,d,f,g,h=[],i=e.call(arguments),j=(n._data(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())a.rnamespace&&!a.rnamespace.test(g.namespace)||(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[n.expando])return a;var b,c,e,f=a.type,g=a,h=this.fixHooks[f];h||(this.fixHooks[f]=h=ma.test(f)?this.mouseHooks:la.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;while(b--)c=e[b],a[c]=g[c];return a.target||(a.target=g.srcElement||d),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,h.filter?h.filter(a,g):a},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button,h=b.fromElement;return null==a.pageX&&null!=b.clientX&&(e=a.target.ownerDocument||d,f=e.documentElement,c=e.body,a.pageX=b.clientX+(f&&f.scrollLeft||c&&c.scrollLeft||0)-(f&&f.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(f&&f.scrollTop||c&&c.scrollTop||0)-(f&&f.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&h&&(a.relatedTarget=h===a.target?b.toElement:h),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==ra()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===ra()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return n.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});n.event.trigger(d,null,b),d.isDefaultPrevented()&&c.preventDefault()}},n.removeEvent=d.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)}:function(a,b,c){var d="on"+b;a.detachEvent&&("undefined"==typeof a[d]&&(a[d]=null),a.detachEvent(d,c))},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?pa:qa):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={constructor:n.Event,isDefaultPrevented:qa,isPropagationStopped:qa,isImmediatePropagationStopped:qa,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=pa,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=pa,a&&!this.isSimulated&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=pa,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return e&&(e===d||n.contains(d,e))||(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),l.submit||(n.event.special.submit={setup:function(){return n.nodeName(this,"form")?!1:void n.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=n.nodeName(b,"input")||n.nodeName(b,"button")?n.prop(b,"form"):void 0;c&&!n._data(c,"submit")&&(n.event.add(c,"submit._submit",function(a){a._submitBubble=!0}),n._data(c,"submit",!0))})},postDispatch:function(a){a._submitBubble&&(delete a._submitBubble,this.parentNode&&!a.isTrigger&&n.event.simulate("submit",this.parentNode,a))},teardown:function(){return n.nodeName(this,"form")?!1:void n.event.remove(this,"._submit")}}),l.change||(n.event.special.change={setup:function(){return ka.test(this.nodeName)?("checkbox"!==this.type&&"radio"!==this.type||(n.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._justChanged=!0)}),n.event.add(this,"click._change",function(a){this._justChanged&&!a.isTrigger&&(this._justChanged=!1),n.event.simulate("change",this,a)})),!1):void n.event.add(this,"beforeactivate._change",function(a){var b=a.target;ka.test(b.nodeName)&&!n._data(b,"change")&&(n.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||n.event.simulate("change",this.parentNode,a)}),n._data(b,"change",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return n.event.remove(this,"._change"),!ka.test(this.nodeName)}}),l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=n._data(d,b);e||d.addEventListener(a,c,!0),n._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=n._data(d,b)-1;e?n._data(d,b,e):(d.removeEventListener(a,c,!0),n._removeData(d,b))}}}),n.fn.extend({on:function(a,b,c,d){return sa(this,a,b,c,d)},one:function(a,b,c,d){return sa(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return b!==!1&&"function"!=typeof b||(c=b,b=void 0),c===!1&&(c=qa),this.each(function(){n.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}});var ta=/ jQuery\d+="(?:null|\d+)"/g,ua=new RegExp("<(?:"+ba+")[\\s/>]","i"),va=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,wa=/<script|<style|<link/i,xa=/checked\s*(?:[^=]|=\s*.checked.)/i,ya=/^true\/(.*)/,za=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,Aa=ca(d),Ba=Aa.appendChild(d.createElement("div"));function Ca(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function Da(a){return a.type=(null!==n.find.attr(a,"type"))+"/"+a.type,a}function Ea(a){var b=ya.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function Fa(a,b){if(1===b.nodeType&&n.hasData(a)){var c,d,e,f=n._data(a),g=n._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)n.event.add(b,c,h[c][d])}g.data&&(g.data=n.extend({},g.data))}}function Ga(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!l.noCloneEvent&&b[n.expando]){e=n._data(b);for(d in e.events)n.removeEvent(b,d,e.handle);b.removeAttribute(n.expando)}"script"===c&&b.text!==a.text?(Da(b).text=a.text,Ea(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),l.html5Clone&&a.innerHTML&&!n.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&Z.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:"input"!==c&&"textarea"!==c||(b.defaultValue=a.defaultValue)}}function Ha(a,b,c,d){b=f.apply([],b);var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);if(r||o>1&&"string"==typeof q&&!l.checkClone&&xa.test(q))return a.each(function(e){var f=a.eq(e);r&&(b[0]=q.call(this,e,f.html())),Ha(f,b,c,d)});if(o&&(k=ja(b,a[0].ownerDocument,!1,a,d),e=k.firstChild,1===k.childNodes.length&&(k=e),e||d)){for(i=n.map(ea(k,"script"),Da),h=i.length;o>m;m++)g=k,m!==p&&(g=n.clone(g,!0,!0),h&&n.merge(i,ea(g,"script"))),c.call(a[m],g,m);if(h)for(j=i[i.length-1].ownerDocument,n.map(i,Ea),m=0;h>m;m++)g=i[m],_.test(g.type||"")&&!n._data(g,"globalEval")&&n.contains(j,g)&&(g.src?n._evalUrl&&n._evalUrl(g.src):n.globalEval((g.text||g.textContent||g.innerHTML||"").replace(za,"")));k=e=null}return a}function Ia(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||n.cleanData(ea(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&fa(ea(d,"script")),d.parentNode.removeChild(d));return a}n.extend({htmlPrefilter:function(a){return a.replace(va,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h,i=n.contains(a.ownerDocument,a);if(l.html5Clone||n.isXMLDoc(a)||!ua.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(Ba.innerHTML=a.outerHTML,Ba.removeChild(f=Ba.firstChild)),!(l.noCloneEvent&&l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(d=ea(f),h=ea(a),g=0;null!=(e=h[g]);++g)d[g]&&Ga(e,d[g]);if(b)if(c)for(h=h||ea(a),d=d||ea(f),g=0;null!=(e=h[g]);g++)Fa(e,d[g]);else Fa(a,f);return d=ea(f,"script"),d.length>0&&fa(d,!i&&ea(a,"script")),d=h=e=null,f},cleanData:function(a,b){for(var d,e,f,g,h=0,i=n.expando,j=n.cache,k=l.attributes,m=n.event.special;null!=(d=a[h]);h++)if((b||M(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)m[e]?n.event.remove(d,e):n.removeEvent(d,e,g.handle);j[f]&&(delete j[f],k||"undefined"==typeof d.removeAttribute?d[i]=void 0:d.removeAttribute(i),c.push(f))}}}),n.fn.extend({domManip:Ha,detach:function(a){return Ia(this,a,!0)},remove:function(a){return Ia(this,a)},text:function(a){return Y(this,function(a){return void 0===a?n.text(this):this.empty().append((this[0]&&this[0].ownerDocument||d).createTextNode(a))},null,a,arguments.length)},append:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.appendChild(a)}})},prepend:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&n.cleanData(ea(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&n.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return Y(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(ta,""):void 0;if("string"==typeof a&&!wa.test(a)&&(l.htmlSerialize||!ua.test(a))&&(l.leadingWhitespace||!aa.test(a))&&!da[($.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(ea(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return Ha(this,arguments,function(b){var c=this.parentNode;n.inArray(this,a)<0&&(n.cleanData(ea(this)),c&&c.replaceChild(b,this))},a)}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=0,e=[],f=n(a),h=f.length-1;h>=d;d++)c=d===h?this:this.clone(!0),n(f[d])[b](c),g.apply(e,c.get());return this.pushStack(e)}});var Ja,Ka={HTML:"block",BODY:"block"};function La(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");return c.detach(),d}function Ma(a){var b=d,c=Ka[a];return c||(c=La(a,b),"none"!==c&&c||(Ja=(Ja||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Ja[0].contentWindow||Ja[0].contentDocument).document,b.write(),b.close(),c=La(a,b),Ja.detach()),Ka[a]=c),c}var Na=/^margin/,Oa=new RegExp("^("+T+")(?!px)[a-z%]+$","i"),Pa=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e},Qa=d.documentElement;!function(){var b,c,e,f,g,h,i=d.createElement("div"),j=d.createElement("div");if(j.style){j.style.cssText="float:left;opacity:.5",l.opacity="0.5"===j.style.opacity,l.cssFloat=!!j.style.cssFloat,j.style.backgroundClip="content-box",j.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===j.style.backgroundClip,i=d.createElement("div"),i.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",j.innerHTML="",i.appendChild(j),l.boxSizing=""===j.style.boxSizing||""===j.style.MozBoxSizing||""===j.style.WebkitBoxSizing,n.extend(l,{reliableHiddenOffsets:function(){return null==b&&k(),f},boxSizingReliable:function(){return null==b&&k(),e},pixelMarginRight:function(){return null==b&&k(),c},pixelPosition:function(){return null==b&&k(),b},reliableMarginRight:function(){return null==b&&k(),g},reliableMarginLeft:function(){return null==b&&k(),h}});function k(){var k,l,m=d.documentElement;m.appendChild(i),j.style.cssText="-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",b=e=h=!1,c=g=!0,a.getComputedStyle&&(l=a.getComputedStyle(j),b="1%"!==(l||{}).top,h="2px"===(l||{}).marginLeft,e="4px"===(l||{width:"4px"}).width,j.style.marginRight="50%",c="4px"===(l||{marginRight:"4px"}).marginRight,k=j.appendChild(d.createElement("div")),k.style.cssText=j.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",k.style.marginRight=k.style.width="0",j.style.width="1px",g=!parseFloat((a.getComputedStyle(k)||{}).marginRight),j.removeChild(k)),j.style.display="none",f=0===j.getClientRects().length,f&&(j.style.display="",j.innerHTML="<table><tr><td></td><td>t</td></tr></table>",j.childNodes[0].style.borderCollapse="separate",k=j.getElementsByTagName("td"),k[0].style.cssText="margin:0;border:0;padding:0;display:none",f=0===k[0].offsetHeight,f&&(k[0].style.display="",k[1].style.display="none",f=0===k[0].offsetHeight)),m.removeChild(i)}}}();var Ra,Sa,Ta=/^(top|right|bottom|left)$/;a.getComputedStyle?(Ra=function(b){var c=b.ownerDocument.defaultView;return c&&c.opener||(c=a),c.getComputedStyle(b)},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c.getPropertyValue(b)||c[b]:void 0,""!==g&&void 0!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),c&&!l.pixelMarginRight()&&Oa.test(g)&&Na.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f),void 0===g?g:g+""}):Qa.currentStyle&&(Ra=function(a){return a.currentStyle},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Oa.test(g)&&!Ta.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function Ua(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Va=/alpha\([^)]*\)/i,Wa=/opacity\s*=\s*([^)]*)/i,Xa=/^(none|table(?!-c[ea]).+)/,Ya=new RegExp("^("+T+")(.*)$","i"),Za={position:"absolute",visibility:"hidden",display:"block"},$a={letterSpacing:"0",fontWeight:"400"},_a=["Webkit","O","Moz","ms"],ab=d.createElement("div").style;function bb(a){if(a in ab)return a;var b=a.charAt(0).toUpperCase()+a.slice(1),c=_a.length;while(c--)if(a=_a[c]+b,a in ab)return a}function cb(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=n._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&W(d)&&(f[g]=n._data(d,"olddisplay",Ma(d.nodeName)))):(e=W(d),(c&&"none"!==c||!e)&&n._data(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function db(a,b,c){var d=Ya.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function eb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+V[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+V[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+V[f]+"Width",!0,e))):(g+=n.css(a,"padding"+V[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+V[f]+"Width",!0,e)));return g}function fb(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=Ra(a),g=l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=Sa(a,b,f),(0>e||null==e)&&(e=a.style[b]),Oa.test(e))return e;d=g&&(l.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+eb(a,b,c||(g?"border":"content"),d,f)+"px"}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Sa(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":l.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;if(b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=U.exec(c))&&e[1]&&(c=X(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Sa(a,b,d)),"normal"===f&&b in $a&&(f=$a[b]),""===c||c?(e=parseFloat(f),c===!0||isFinite(e)?e||0:f):f}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Xa.test(n.css(a,"display"))&&0===a.offsetWidth?Pa(a,Za,function(){return fb(a,b,d)}):fb(a,b,d):void 0},set:function(a,c,d){var e=d&&Ra(a);return db(a,c,d?eb(a,b,d,l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,e),e):0)}}}),l.opacity||(n.cssHooks.opacity={get:function(a,b){return Wa.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=n.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===n.trim(f.replace(Va,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Va.test(f)?f.replace(Va,e):f+" "+e)}}),n.cssHooks.marginRight=Ua(l.reliableMarginRight,function(a,b){return b?Pa(a,{display:"inline-block"},Sa,[a,"marginRight"]):void 0}),n.cssHooks.marginLeft=Ua(l.reliableMarginLeft,function(a,b){return b?(parseFloat(Sa(a,"marginLeft"))||(n.contains(a.ownerDocument,a)?a.getBoundingClientRect().left-Pa(a,{
marginLeft:0},function(){return a.getBoundingClientRect().left}):0))+"px":void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+V[d]+b]=f[d]||f[d-2]||f[0];return e}},Na.test(a)||(n.cssHooks[a+b].set=db)}),n.fn.extend({css:function(a,b){return Y(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=Ra(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return cb(this,!0)},hide:function(){return cb(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){W(this)?n(this).show():n(this).hide()})}});function gb(a,b,c,d,e){return new gb.prototype.init(a,b,c,d,e)}n.Tween=gb,gb.prototype={constructor:gb,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=gb.propHooks[this.prop];return a&&a.get?a.get(this):gb.propHooks._default.get(this)},run:function(a){var b,c=gb.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):gb.propHooks._default.set(this),this}},gb.prototype.init.prototype=gb.prototype,gb.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)}}},gb.propHooks.scrollTop=gb.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},n.fx=gb.prototype.init,n.fx.step={};var hb,ib,jb=/^(?:toggle|show|hide)$/,kb=/queueHooks$/;function lb(){return a.setTimeout(function(){hb=void 0}),hb=n.now()}function mb(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=V[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function nb(a,b,c){for(var d,e=(qb.tweeners[b]||[]).concat(qb.tweeners["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function ob(a,b,c){var d,e,f,g,h,i,j,k,m=this,o={},p=a.style,q=a.nodeType&&W(a),r=n._data(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,m.always(function(){m.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=n.css(a,"display"),k="none"===j?n._data(a,"olddisplay")||Ma(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(l.inlineBlockNeedsLayout&&"inline"!==Ma(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",l.shrinkWrapBlocks()||m.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],jb.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(o))"inline"===("none"===j?Ma(a.nodeName):j)&&(p.display=j);else{r?"hidden"in r&&(q=r.hidden):r=n._data(a,"fxshow",{}),f&&(r.hidden=!q),q?n(a).show():m.done(function(){n(a).hide()}),m.done(function(){var b;n._removeData(a,"fxshow");for(b in o)n.style(a,b,o[b])});for(d in o)g=nb(q?r[d]:0,d,m),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function pb(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand (f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function qb(a,b,c){var d,e,f=0,g=qb.prefilters.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=hb||lb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:hb||lb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for(pb(k,j.opts.specialEasing);g>f;f++)if(d=qb.prefilters[f].call(j,a,k,j.opts))return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d;return n.map(k,nb,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(qb,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return X(c.elem,a,U.exec(b),c),c}]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);for(var c,d=0,e=a.length;e>d;d++)c=a[d],qb.tweeners[c]=qb.tweeners[c]||[],qb.tweeners[c].unshift(b)},prefilters:[ob],prefilter:function(a,b){b?qb.prefilters.unshift(a):qb.prefilters.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,null!=d.queue&&d.queue!==!0||(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(W).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=qb(this,n.extend({},a),f);(e||n._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=n._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&kb.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));!b&&c||n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=n._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(mb(b,!0),a,d,e)}}),n.each({slideDown:mb("show"),slideUp:mb("hide"),slideToggle:mb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=n.timers,c=0;for(hb=n.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||n.fx.stop(),hb=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){ib||(ib=a.setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){a.clearInterval(ib),ib=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a,b=d.createElement("input"),c=d.createElement("div"),e=d.createElement("select"),f=e.appendChild(d.createElement("option"));c=d.createElement("div"),c.setAttribute("className","t"),c.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=c.getElementsByTagName("a")[0],b.setAttribute("type","checkbox"),c.appendChild(b),a=c.getElementsByTagName("a")[0],a.style.cssText="top:1px",l.getSetAttribute="t"!==c.className,l.style=/top/.test(a.getAttribute("style")),l.hrefNormalized="/a"===a.getAttribute("href"),l.checkOn=!!b.value,l.optSelected=f.selected,l.enctype=!!d.createElement("form").enctype,e.disabled=!0,l.optDisabled=!f.disabled,b=d.createElement("input"),b.setAttribute("value",""),l.input=""===b.getAttribute("value"),b.value="t",b.setAttribute("type","radio"),l.radioValue="t"===b.value}();var rb=/\r/g,sb=/[\x20\t\r\n\f]+/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(rb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.trim(n.text(a)).replace(sb," ")}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)if(d=e[g],n.inArray(n.valHooks.option.get(d),f)>-1)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var tb,ub,vb=n.expr.attrHandle,wb=/^(?:checked|selected)$/i,xb=l.getSetAttribute,yb=l.input;n.fn.extend({attr:function(a,b){return Y(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?ub:tb)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)?yb&&xb||!wb.test(c)?a[d]=!1:a[n.camelCase("default-"+c)]=a[d]=!1:n.attr(a,c,""),a.removeAttribute(xb?c:d)}}),ub={set:function(a,b,c){return b===!1?n.removeAttr(a,c):yb&&xb||!wb.test(c)?a.setAttribute(!xb&&n.propFix[c]||c,c):a[n.camelCase("default-"+c)]=a[c]=!0,c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=vb[b]||n.find.attr;yb&&xb||!wb.test(b)?vb[b]=function(a,b,d){var e,f;return d||(f=vb[b],vb[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,vb[b]=f),e}:vb[b]=function(a,b,c){return c?void 0:a[n.camelCase("default-"+b)]?b.toLowerCase():null}}),yb&&xb||(n.attrHooks.value={set:function(a,b,c){return n.nodeName(a,"input")?void(a.defaultValue=b):tb&&tb.set(a,b,c)}}),xb||(tb={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},vb.id=vb.name=vb.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},n.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:tb.set},n.attrHooks.contenteditable={set:function(a,b,c){tb.set(a,""===b?!1:b,c)}},n.each(["width","height"],function(a,b){n.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),l.style||(n.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var zb=/^(?:input|select|textarea|button|object)$/i,Ab=/^(?:a|area)$/i;n.fn.extend({prop:function(a,b){return Y(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return a=n.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");return b?parseInt(b,10):zb.test(a.nodeName)||Ab.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),l.hrefNormalized||n.each(["href","src"],function(a,b){n.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null},set:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex)}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this}),l.enctype||(n.propFix.enctype="encoding");var Bb=/[\t\r\n\f]/g;function Cb(a){return n.attr(a,"class")||""}n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,Cb(this)))});if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Cb(c),d=1===c.nodeType&&(" "+e+" ").replace(Bb," ")){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,Cb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Cb(c),d=1===c.nodeType&&(" "+e+" ").replace(Bb," ")){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,Cb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=n(this),f=a.match(G)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else void 0!==a&&"boolean"!==c||(b=Cb(this),b&&n._data(this,"__className__",b),n.attr(this,"class",b||a===!1?"":n._data(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+Cb(c)+" ").replace(Bb," ").indexOf(b)>-1)return!0;return!1}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}});var Db=a.location,Eb=n.now(),Fb=/\?/,Gb=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;n.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=n.trim(b+"");return e&&!n.trim(e.replace(Gb,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():n.error("Invalid JSON: "+b)},n.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new a.DOMParser,c=d.parseFromString(b,"text/xml")):(c=new a.ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c};var Hb=/#.*$/,Ib=/([?&])_=[^&]*/,Jb=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Kb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Lb=/^(?:GET|HEAD)$/,Mb=/^\/\//,Nb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Ob={},Pb={},Qb="*/".concat("*"),Rb=Db.href,Sb=Nb.exec(Rb.toLowerCase())||[];function Tb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(G)||[];if(n.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Ub(a,b,c,d){var e={},f=a===Pb;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Vb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&n.extend(!0,a,c),a}function Wb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Xb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Rb,type:"GET",isLocal:Kb.test(Sb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Qb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Vb(Vb(a,n.ajaxSettings),b):Vb(n.ajaxSettings,a)},ajaxPrefilter:Tb(Ob),ajaxTransport:Tb(Pb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var d,e,f,g,h,i,j,k,l=n.ajaxSetup({},c),m=l.context||l,o=l.context&&(m.nodeType||m.jquery)?n(m):n.event,p=n.Deferred(),q=n.Callbacks("once memory"),r=l.statusCode||{},s={},t={},u=0,v="canceled",w={readyState:0,getResponseHeader:function(a){var b;if(2===u){if(!k){k={};while(b=Jb.exec(g))k[b[1].toLowerCase()]=b[2]}b=k[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===u?g:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return u||(a=t[c]=t[c]||a,s[a]=b),this},overrideMimeType:function(a){return u||(l.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>u)for(b in a)r[b]=[r[b],a[b]];else w.always(a[w.status]);return this},abort:function(a){var b=a||v;return j&&j.abort(b),y(0,b),this}};if(p.promise(w).complete=q.add,w.success=w.done,w.error=w.fail,l.url=((b||l.url||Rb)+"").replace(Hb,"").replace(Mb,Sb[1]+"//"),l.type=c.method||c.type||l.method||l.type,l.dataTypes=n.trim(l.dataType||"*").toLowerCase().match(G)||[""],null==l.crossDomain&&(d=Nb.exec(l.url.toLowerCase()),l.crossDomain=!(!d||d[1]===Sb[1]&&d[2]===Sb[2]&&(d[3]||("http:"===d[1]?"80":"443"))===(Sb[3]||("http:"===Sb[1]?"80":"443")))),l.data&&l.processData&&"string"!=typeof l.data&&(l.data=n.param(l.data,l.traditional)),Ub(Ob,l,c,w),2===u)return w;i=n.event&&l.global,i&&0===n.active++&&n.event.trigger("ajaxStart"),l.type=l.type.toUpperCase(),l.hasContent=!Lb.test(l.type),f=l.url,l.hasContent||(l.data&&(f=l.url+=(Fb.test(f)?"&":"?")+l.data,delete l.data),l.cache===!1&&(l.url=Ib.test(f)?f.replace(Ib,"$1_="+Eb++):f+(Fb.test(f)?"&":"?")+"_="+Eb++)),l.ifModified&&(n.lastModified[f]&&w.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&w.setRequestHeader("If-None-Match",n.etag[f])),(l.data&&l.hasContent&&l.contentType!==!1||c.contentType)&&w.setRequestHeader("Content-Type",l.contentType),w.setRequestHeader("Accept",l.dataTypes[0]&&l.accepts[l.dataTypes[0]]?l.accepts[l.dataTypes[0]]+("*"!==l.dataTypes[0]?", "+Qb+"; q=0.01":""):l.accepts["*"]);for(e in l.headers)w.setRequestHeader(e,l.headers[e]);if(l.beforeSend&&(l.beforeSend.call(m,w,l)===!1||2===u))return w.abort();v="abort";for(e in{success:1,error:1,complete:1})w[e](l[e]);if(j=Ub(Pb,l,c,w)){if(w.readyState=1,i&&o.trigger("ajaxSend",[w,l]),2===u)return w;l.async&&l.timeout>0&&(h=a.setTimeout(function(){w.abort("timeout")},l.timeout));try{u=1,j.send(s,y)}catch(x){if(!(2>u))throw x;y(-1,x)}}else y(-1,"No Transport");function y(b,c,d,e){var k,s,t,v,x,y=c;2!==u&&(u=2,h&&a.clearTimeout(h),j=void 0,g=e||"",w.readyState=b>0?4:0,k=b>=200&&300>b||304===b,d&&(v=Wb(l,w,d)),v=Xb(l,v,w,k),k?(l.ifModified&&(x=w.getResponseHeader("Last-Modified"),x&&(n.lastModified[f]=x),x=w.getResponseHeader("etag"),x&&(n.etag[f]=x)),204===b||"HEAD"===l.type?y="nocontent":304===b?y="notmodified":(y=v.state,s=v.data,t=v.error,k=!t)):(t=y,!b&&y||(y="error",0>b&&(b=0))),w.status=b,w.statusText=(c||y)+"",k?p.resolveWith(m,[s,y,w]):p.rejectWith(m,[w,y,t]),w.statusCode(r),r=void 0,i&&o.trigger(k?"ajaxSuccess":"ajaxError",[w,l,k?s:t]),q.fireWith(m,[w,y]),i&&(o.trigger("ajaxComplete",[w,l]),--n.active||n.event.trigger("ajaxStop")))}return w},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){if(n.isFunction(a))return this.each(function(b){n(this).wrapAll(a.call(this,b))});if(this[0]){var b=n(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))}):this.each(function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}});function Yb(a){return a.style&&a.style.display||n.css(a,"display")}function Zb(a){if(!n.contains(a.ownerDocument||d,a))return!0;while(a&&1===a.nodeType){if("none"===Yb(a)||"hidden"===a.type)return!0;a=a.parentNode}return!1}n.expr.filters.hidden=function(a){return l.reliableHiddenOffsets()?a.offsetWidth<=0&&a.offsetHeight<=0&&!a.getClientRects().length:Zb(a)},n.expr.filters.visible=function(a){return!n.expr.filters.hidden(a)};var $b=/%20/g,_b=/\[\]$/,ac=/\r?\n/g,bc=/^(?:submit|button|image|reset|file)$/i,cc=/^(?:input|select|textarea|keygen)/i;function dc(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||_b.test(a)?d(a,e):dc(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)dc(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)dc(c,a[c],b,e);return d.join("&").replace($b,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&cc.test(this.nodeName)&&!bc.test(a)&&(this.checked||!Z.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(ac,"\r\n")}}):{name:b.name,value:c.replace(ac,"\r\n")}}).get()}}),n.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return this.isLocal?ic():d.documentMode>8?hc():/^(get|post|head|put|delete|options)$/i.test(this.type)&&hc()||ic()}:hc;var ec=0,fc={},gc=n.ajaxSettings.xhr();a.attachEvent&&a.attachEvent("onunload",function(){for(var a in fc)fc[a](void 0,!0)}),l.cors=!!gc&&"withCredentials"in gc,gc=l.ajax=!!gc,gc&&n.ajaxTransport(function(b){if(!b.crossDomain||l.cors){var c;return{send:function(d,e){var f,g=b.xhr(),h=++ec;if(g.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(f in b.xhrFields)g[f]=b.xhrFields[f];b.mimeType&&g.overrideMimeType&&g.overrideMimeType(b.mimeType),b.crossDomain||d["X-Requested-With"]||(d["X-Requested-With"]="XMLHttpRequest");for(f in d)void 0!==d[f]&&g.setRequestHeader(f,d[f]+"");g.send(b.hasContent&&b.data||null),c=function(a,d){var f,i,j;if(c&&(d||4===g.readyState))if(delete fc[h],c=void 0,g.onreadystatechange=n.noop,d)4!==g.readyState&&g.abort();else{j={},f=g.status,"string"==typeof g.responseText&&(j.text=g.responseText);try{i=g.statusText}catch(k){i=""}f||!b.isLocal||b.crossDomain?1223===f&&(f=204):f=j.text?200:404}j&&e(f,i,j,g.getAllResponseHeaders())},b.async?4===g.readyState?a.setTimeout(c):g.onreadystatechange=fc[h]=c:c()},abort:function(){c&&c(void 0,!0)}}}});function hc(){try{return new a.XMLHttpRequest}catch(b){}}function ic(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=d.head||n("head")[0]||d.documentElement;return{send:function(e,f){b=d.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||f(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var jc=[],kc=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=jc.pop()||n.expando+"_"+Eb++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(kc.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&kc.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(kc,"$1"+e):b.jsonp!==!1&&(b.url+=(Fb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,jc.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||d;var e=x.exec(a),f=!c&&[];return e?[b.createElement(e[1])]:(e=ja([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))};var lc=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&lc)return lc.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=n.trim(a.slice(h,a.length)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(this,f||[a.responseText,b,a])})}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};function mc(a){return n.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&n.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,n.contains(b,e)?("undefined"!=typeof e.getBoundingClientRect&&(d=e.getBoundingClientRect()),c=mc(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===n.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(c=a.offset()),c.top+=n.css(a[0],"borderTopWidth",!0),c.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-c.top-n.css(d,"marginTop",!0),left:b.left-c.left-n.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Qa})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);n.fn[a]=function(d){return Y(this,function(a,d,e){var f=mc(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?n(f).scrollLeft():e,c?e:n(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ua(l.pixelPosition,function(a,c){return c?(c=Sa(a,b),Oa.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({
padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return Y(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}}),n.fn.size=function(){return this.length},n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var nc=a.jQuery,oc=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=oc),b&&a.jQuery===n&&(a.jQuery=nc),n},b||(a.jQuery=a.$=n),n});
jQuery.noConflict();
"undefined"==typeof jQuery.migrateMute&&(jQuery.migrateMute=!0),function(a,b,c){function d(c){var d=b.console;f[c]||(f[c]=!0,a.migrateWarnings.push(c),d&&d.warn&&!a.migrateMute&&(d.warn("JQMIGRATE: "+c),a.migrateTrace&&d.trace&&d.trace()))}function e(b,c,e,f){if(Object.defineProperty)try{return void Object.defineProperty(b,c,{configurable:!0,enumerable:!0,get:function(){return d(f),e},set:function(a){d(f),e=a}})}catch(g){}a._definePropertyBroken=!0,b[c]=e}a.migrateVersion="1.4.1";var f={};a.migrateWarnings=[],b.console&&b.console.log&&b.console.log("JQMIGRATE: Migrate is installed"+(a.migrateMute?"":" with logging active")+", version "+a.migrateVersion),a.migrateTrace===c&&(a.migrateTrace=!0),a.migrateReset=function(){f={},a.migrateWarnings.length=0},"BackCompat"===document.compatMode&&d("jQuery is not compatible with Quirks Mode");var g=a("<input/>",{size:1}).attr("size")&&a.attrFn,h=a.attr,i=a.attrHooks.value&&a.attrHooks.value.get||function(){return null},j=a.attrHooks.value&&a.attrHooks.value.set||function(){return c},k=/^(?:input|button)$/i,l=/^[238]$/,m=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,n=/^(?:checked|selected)$/i;e(a,"attrFn",g||{},"jQuery.attrFn is deprecated"),a.attr=function(b,e,f,i){var j=e.toLowerCase(),o=b&&b.nodeType;return i&&(h.length<4&&d("jQuery.fn.attr(props, pass) is deprecated"),b&&!l.test(o)&&(g?e in g:a.isFunction(a.fn[e])))?a(b)[e](f):("type"===e&&f!==c&&k.test(b.nodeName)&&b.parentNode&&d("Can't change the 'type' of an input or button in IE 6/7/8"),!a.attrHooks[j]&&m.test(j)&&(a.attrHooks[j]={get:function(b,d){var e,f=a.prop(b,d);return f===!0||"boolean"!=typeof f&&(e=b.getAttributeNode(d))&&e.nodeValue!==!1?d.toLowerCase():c},set:function(b,c,d){var e;return c===!1?a.removeAttr(b,d):(e=a.propFix[d]||d,e in b&&(b[e]=!0),b.setAttribute(d,d.toLowerCase())),d}},n.test(j)&&d("jQuery.fn.attr('"+j+"') might use property instead of attribute")),h.call(a,b,e,f))},a.attrHooks.value={get:function(a,b){var c=(a.nodeName||"").toLowerCase();return"button"===c?i.apply(this,arguments):("input"!==c&&"option"!==c&&d("jQuery.fn.attr('value') no longer gets properties"),b in a?a.value:null)},set:function(a,b){var c=(a.nodeName||"").toLowerCase();return"button"===c?j.apply(this,arguments):("input"!==c&&"option"!==c&&d("jQuery.fn.attr('value', val) no longer sets properties"),void(a.value=b))}};var o,p,q=a.fn.init,r=a.find,s=a.parseJSON,t=/^\s*</,u=/\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/,v=/\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g,w=/^([^<]*)(<[\w\W]+>)([^>]*)$/;a.fn.init=function(b,e,f){var g,h;return b&&"string"==typeof b&&!a.isPlainObject(e)&&(g=w.exec(a.trim(b)))&&g[0]&&(t.test(b)||d("$(html) HTML strings must start with '<' character"),g[3]&&d("$(html) HTML text after last tag is ignored"),"#"===g[0].charAt(0)&&(d("HTML string cannot start with a '#' character"),a.error("JQMIGRATE: Invalid selector string (XSS)")),e&&e.context&&e.context.nodeType&&(e=e.context),a.parseHTML)?q.call(this,a.parseHTML(g[2],e&&e.ownerDocument||e||document,!0),e,f):(h=q.apply(this,arguments),b&&b.selector!==c?(h.selector=b.selector,h.context=b.context):(h.selector="string"==typeof b?b:"",b&&(h.context=b.nodeType?b:e||document)),h)},a.fn.init.prototype=a.fn,a.find=function(a){var b=Array.prototype.slice.call(arguments);if("string"==typeof a&&u.test(a))try{document.querySelector(a)}catch(c){a=a.replace(v,function(a,b,c,d){return"["+b+c+'"'+d+'"]'});try{document.querySelector(a),d("Attribute selector with '#' must be quoted: "+b[0]),b[0]=a}catch(e){d("Attribute selector with '#' was not fixed: "+b[0])}}return r.apply(this,b)};var x;for(x in r)Object.prototype.hasOwnProperty.call(r,x)&&(a.find[x]=r[x]);a.parseJSON=function(a){return a?s.apply(this,arguments):(d("jQuery.parseJSON requires a valid JSON string"),null)},a.uaMatch=function(a){a=a.toLowerCase();var b=/(chrome)[ \/]([\w.]+)/.exec(a)||/(webkit)[ \/]([\w.]+)/.exec(a)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a)||/(msie) ([\w.]+)/.exec(a)||a.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a)||[];return{browser:b[1]||"",version:b[2]||"0"}},a.browser||(o=a.uaMatch(navigator.userAgent),p={},o.browser&&(p[o.browser]=!0,p.version=o.version),p.chrome?p.webkit=!0:p.webkit&&(p.safari=!0),a.browser=p),e(a,"browser",a.browser,"jQuery.browser is deprecated"),a.boxModel=a.support.boxModel="CSS1Compat"===document.compatMode,e(a,"boxModel",a.boxModel,"jQuery.boxModel is deprecated"),e(a.support,"boxModel",a.support.boxModel,"jQuery.support.boxModel is deprecated"),a.sub=function(){function b(a,c){return new b.fn.init(a,c)}a.extend(!0,b,this),b.superclass=this,b.fn=b.prototype=this(),b.fn.constructor=b,b.sub=this.sub,b.fn.init=function(d,e){var f=a.fn.init.call(this,d,e,c);return f instanceof b?f:b(f)},b.fn.init.prototype=b.fn;var c=b(document);return d("jQuery.sub() is deprecated"),b},a.fn.size=function(){return d("jQuery.fn.size() is deprecated; use the .length property"),this.length};var y=!1;a.swap&&a.each(["height","width","reliableMarginRight"],function(b,c){var d=a.cssHooks[c]&&a.cssHooks[c].get;d&&(a.cssHooks[c].get=function(){var a;return y=!0,a=d.apply(this,arguments),y=!1,a})}),a.swap=function(a,b,c,e){var f,g,h={};y||d("jQuery.swap() is undocumented and deprecated");for(g in b)h[g]=a.style[g],a.style[g]=b[g];f=c.apply(a,e||[]);for(g in b)a.style[g]=h[g];return f},a.ajaxSetup({converters:{"text json":a.parseJSON}});var z=a.fn.data;a.fn.data=function(b){var e,f,g=this[0];return!g||"events"!==b||1!==arguments.length||(e=a.data(g,b),f=a._data(g,b),e!==c&&e!==f||f===c)?z.apply(this,arguments):(d("Use of jQuery.fn.data('events') is deprecated"),f)};var A=/\/(java|ecma)script/i;a.clean||(a.clean=function(b,c,e,f){c=c||document,c=!c.nodeType&&c[0]||c,c=c.ownerDocument||c,d("jQuery.clean() is deprecated");var g,h,i,j,k=[];if(a.merge(k,a.buildFragment(b,c).childNodes),e)for(i=function(a){return!a.type||A.test(a.type)?f?f.push(a.parentNode?a.parentNode.removeChild(a):a):e.appendChild(a):void 0},g=0;null!=(h=k[g]);g++)a.nodeName(h,"script")&&i(h)||(e.appendChild(h),"undefined"!=typeof h.getElementsByTagName&&(j=a.grep(a.merge([],h.getElementsByTagName("script")),i),k.splice.apply(k,[g+1,0].concat(j)),g+=j.length));return k});var B=a.event.add,C=a.event.remove,D=a.event.trigger,E=a.fn.toggle,F=a.fn.live,G=a.fn.die,H=a.fn.load,I="ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",J=new RegExp("\\b(?:"+I+")\\b"),K=/(?:^|\s)hover(\.\S+|)\b/,L=function(b){return"string"!=typeof b||a.event.special.hover?b:(K.test(b)&&d("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"),b&&b.replace(K,"mouseenter$1 mouseleave$1"))};a.event.props&&"attrChange"!==a.event.props[0]&&a.event.props.unshift("attrChange","attrName","relatedNode","srcElement"),a.event.dispatch&&e(a.event,"handle",a.event.dispatch,"jQuery.event.handle is undocumented and deprecated"),a.event.add=function(a,b,c,e,f){a!==document&&J.test(b)&&d("AJAX events should be attached to document: "+b),B.call(this,a,L(b||""),c,e,f)},a.event.remove=function(a,b,c,d,e){C.call(this,a,L(b)||"",c,d,e)},a.each(["load","unload","error"],function(b,c){a.fn[c]=function(){var a=Array.prototype.slice.call(arguments,0);return"load"===c&&"string"==typeof a[0]?H.apply(this,a):(d("jQuery.fn."+c+"() is deprecated"),a.splice(0,0,c),arguments.length?this.bind.apply(this,a):(this.triggerHandler.apply(this,a),this))}}),a.fn.toggle=function(b,c){if(!a.isFunction(b)||!a.isFunction(c))return E.apply(this,arguments);d("jQuery.fn.toggle(handler, handler...) is deprecated");var e=arguments,f=b.guid||a.guid++,g=0,h=function(c){var d=(a._data(this,"lastToggle"+b.guid)||0)%g;return a._data(this,"lastToggle"+b.guid,d+1),c.preventDefault(),e[d].apply(this,arguments)||!1};for(h.guid=f;g<e.length;)e[g++].guid=f;return this.click(h)},a.fn.live=function(b,c,e){return d("jQuery.fn.live() is deprecated"),F?F.apply(this,arguments):(a(this.context).on(b,this.selector,c,e),this)},a.fn.die=function(b,c){return d("jQuery.fn.die() is deprecated"),G?G.apply(this,arguments):(a(this.context).off(b,this.selector||"**",c),this)},a.event.trigger=function(a,b,c,e){return c||J.test(a)||d("Global events are undocumented and deprecated"),D.call(this,a,b,c||document,e)},a.each(I.split("|"),function(b,c){a.event.special[c]={setup:function(){var b=this;return b!==document&&(a.event.add(document,c+"."+a.guid,function(){a.event.trigger(c,Array.prototype.slice.call(arguments,1),b,!0)}),a._data(this,c,a.guid++)),!1},teardown:function(){return this!==document&&a.event.remove(document,c+"."+a._data(this,c)),!1}}}),a.event.special.ready={setup:function(){this===document&&d("'ready' event is deprecated")}};var M=a.fn.andSelf||a.fn.addBack,N=a.fn.find;if(a.fn.andSelf=function(){return d("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"),M.apply(this,arguments)},a.fn.find=function(a){var b=N.apply(this,arguments);return b.context=this.context,b.selector=this.selector?this.selector+" "+a:a,b},a.Callbacks){var O=a.Deferred,P=[["resolve","done",a.Callbacks("once memory"),a.Callbacks("once memory"),"resolved"],["reject","fail",a.Callbacks("once memory"),a.Callbacks("once memory"),"rejected"],["notify","progress",a.Callbacks("memory"),a.Callbacks("memory")]];a.Deferred=function(b){var c=O(),e=c.promise();return c.pipe=e.pipe=function(){var b=arguments;return d("deferred.pipe() is deprecated"),a.Deferred(function(d){a.each(P,function(f,g){var h=a.isFunction(b[f])&&b[f];c[g[1]](function(){var b=h&&h.apply(this,arguments);b&&a.isFunction(b.promise)?b.promise().done(d.resolve).fail(d.reject).progress(d.notify):d[g[0]+"With"](this===e?d.promise():this,h?[b]:arguments)})}),b=null}).promise()},c.isResolved=function(){return d("deferred.isResolved is deprecated"),"resolved"===c.state()},c.isRejected=function(){return d("deferred.isRejected is deprecated"),"rejected"===c.state()},b&&b.call(c,c),c}}}(jQuery,window);
(function(jQuery){
var domfocus=false;
var mousefocus=false;
var zoomactive=false;
var tabindexcounter=5000;
var ascrailcounter=2000;
var globalmaxzindex=0;
var $=jQuery;
function getScriptPath(){
var scripts=document.getElementsByTagName('script');
var path=scripts[scripts.length-1].src.split('?')[0];
return (path.split('/').length>0) ? path.split('/').slice(0,-1).join('/')+'/':'';
}
var scriptpath=getScriptPath();
var vendors=['ms','moz','webkit','o'];
var setAnimationFrame=window.requestAnimationFrame||false;
var clearAnimationFrame=window.cancelAnimationFrame||false;
if(!setAnimationFrame){
for(var vx in vendors){
var v=vendors[vx];
if(!setAnimationFrame) setAnimationFrame=window[v+'RequestAnimationFrame'];
if(!clearAnimationFrame) clearAnimationFrame=window[v+'CancelAnimationFrame']||window[v+'CancelRequestAnimationFrame'];
}}
var clsMutationObserver=window.MutationObserver||window.WebKitMutationObserver||false;
var _globaloptions={
zindex:"auto",
cursoropacitymin:0,
cursoropacitymax:1,
cursorcolor:"#424242",
cursorwidth:"5px",
cursorborder:"1px solid #fff",
cursorborderradius:"5px",
scrollspeed:60,
mousescrollstep:8*3,
touchbehavior:false,
hwacceleration:true,
usetransition:true,
boxzoom:false,
dblclickzoom:true,
gesturezoom:true,
grabcursorenabled:true,
autohidemode:true,
background:"",
iframeautoresize:true,
cursorminheight:32,
preservenativescrolling:true,
railoffset:false,
bouncescroll:true,
spacebarenabled:true,
railpadding:{top:0,right:0,left:0,bottom:0},
disableoutline:true,
horizrailenabled:true,
railalign:"right",
railvalign:"bottom",
enabletranslate3d:true,
enablemousewheel:true,
enablekeyboard:true,
smoothscroll:true,
sensitiverail:true,
enablemouselockapi:true,
cursorfixedheight:false,
directionlockdeadzone:6,
hidecursordelay:400,
nativeparentscrolling:true,
enablescrollonselection:true,
overflowx:true,
overflowy:true,
cursordragspeed:0.3,
rtlmode:false,
cursordragontouch:false,
oneaxismousemode:"auto"
}
var browserdetected=false;
var getBrowserDetection=function(){
if(browserdetected) return browserdetected;
var domtest=document.createElement('DIV');
var d={};
d.haspointerlock="pointerLockElement" in document||"mozPointerLockElement" in document||"webkitPointerLockElement" in document;
d.isopera=("opera" in window);
d.isopera12=(d.isopera&&("getUserMedia" in navigator));
d.isoperamini=(Object.prototype.toString.call(window.operamini)==="[object OperaMini]");
d.isie=(("all" in document)&&("attachEvent" in domtest)&&!d.isopera);
d.isieold=(d.isie&&!("msInterpolationMode" in domtest.style));
d.isie7=d.isie&&!d.isieold&&(!("documentMode" in document)||(document.documentMode==7));
d.isie8=d.isie&&("documentMode" in document)&&(document.documentMode==8);
d.isie9=d.isie&&("performance" in window)&&(document.documentMode>=9);
d.isie10=d.isie&&("performance" in window)&&(document.documentMode>=10);
d.isie9mobile=/iemobile.9/i.test(navigator.userAgent);
if(d.isie9mobile) d.isie9=false;
d.isie7mobile=(!d.isie9mobile&&d.isie7)&&/iemobile/i.test(navigator.userAgent);
d.ismozilla=("MozAppearance" in domtest.style);
d.iswebkit=("WebkitAppearance" in domtest.style);
d.ischrome=("chrome" in window);
d.ischrome22=(d.ischrome&&d.haspointerlock);
d.ischrome26=(d.ischrome&&("transition" in domtest.style));
d.cantouch=("ontouchstart" in document.documentElement)||("ontouchstart" in window);
d.hasmstouch=(window.navigator.msPointerEnabled||false);
d.ismac=/^mac$/i.test(navigator.platform);
d.isios=(d.cantouch&&/iphone|ipad|ipod/i.test(navigator.platform));
d.isios4=((d.isios)&&!("seal" in Object));
d.isandroid=(/android/i.test(navigator.userAgent));
d.trstyle=false;
d.hastransform=false;
d.hastranslate3d=false;
d.transitionstyle=false;
d.hastransition=false;
d.transitionend=false;
var check=['transform','msTransform','webkitTransform','MozTransform','OTransform'];
for(var a=0;a<check.length;a++){
if(typeof domtest.style[check[a]]!="undefined"){
d.trstyle=check[a];
break;
}}
d.hastransform=(d.trstyle!=false);
if(d.hastransform){
domtest.style[d.trstyle]="translate3d(1px,2px,3px)";
d.hastranslate3d=/translate3d/.test(domtest.style[d.trstyle]);
}
d.transitionstyle=false;
d.prefixstyle='';
d.transitionend=false;
var check=['transition','webkitTransition','MozTransition','OTransition','OTransition','msTransition','KhtmlTransition'];
var prefix=['','-webkit-','-moz-','-o-','-o','-ms-','-khtml-'];
var evs=['transitionend','webkitTransitionEnd','transitionend','otransitionend','oTransitionEnd','msTransitionEnd','KhtmlTransitionEnd'];
for(var a=0;a<check.length;a++){
if(check[a] in domtest.style){
d.transitionstyle=check[a];
d.prefixstyle=prefix[a];
d.transitionend=evs[a];
break;
}}
if(d.ischrome26){
d.prefixstyle=prefix[1];
}
d.hastransition=(d.transitionstyle);
function detectCursorGrab(){
var lst=['-moz-grab','-webkit-grab','grab'];
if((d.ischrome&&!d.ischrome22)||d.isie) lst=[];
for(var a=0;a<lst.length;a++){
var p=lst[a];
domtest.style['cursor']=p;
if(domtest.style['cursor']==p) return p;
}
return 'url(http://www.google.com/intl/en_ALL/mapfiles/openhand.cur),n-resize';  // thank you google for custom cursor!
}
d.cursorgrabvalue=detectCursorGrab();
d.hasmousecapture=("setCapture" in domtest);
d.hasMutationObserver=(clsMutationObserver!==false);
domtest=null;
browserdetected=d;
return d;
}
var NiceScrollClass=function(myopt,me){
var self=this;
this.version='3.5.0 BETA5';
this.name='nicescroll';
this.me=me;
this.opt={
doc:$("body"),
win:false
};
$.extend(this.opt,_globaloptions);
this.opt.snapbackspeed=80;
if(myopt||false){
for(var a in self.opt){
if(typeof myopt[a]!="undefined") self.opt[a]=myopt[a];
}}
this.doc=self.opt.doc;
this.iddoc=(this.doc&&this.doc[0])?this.doc[0].id||'':'';
this.ispage=/BODY|HTML/.test((self.opt.win)?self.opt.win[0].nodeName:this.doc[0].nodeName);
this.haswrapper=(self.opt.win!==false);
this.win=self.opt.win||(this.ispage?$(window):this.doc);
this.docscroll=(this.ispage&&!this.haswrapper)?$(window):this.win;
this.body=$("body");
this.viewport=false;
this.isfixed=false;
this.iframe=false;
this.isiframe=((this.doc[0].nodeName=='IFRAME')&&(this.win[0].nodeName=='IFRAME'));
this.istextarea=(this.win[0].nodeName=='TEXTAREA');
this.forcescreen=false;
this.canshowonmouseevent=(self.opt.autohidemode!="scroll");
this.onmousedown=false;
this.onmouseup=false;
this.onmousemove=false;
this.onmousewheel=false;
this.onkeypress=false;
this.ongesturezoom=false;
this.onclick=false;
this.onscrollstart=false;
this.onscrollend=false;
this.onscrollcancel=false;
this.onzoomin=false;
this.onzoomout=false;
this.view=false;
this.page=false;
this.scroll={x:0,y:0};
this.scrollratio={x:0,y:0};
this.cursorheight=20;
this.scrollvaluemax=0;
this.checkrtlmode=false;
this.scrollrunning=false;
this.scrollmom=false;
this.observer=false;
this.observerremover=false;
do {
this.id="ascrail"+(ascrailcounter++);
} while (document.getElementById(this.id));
this.rail=false;
this.cursor=false;
this.cursorfreezed=false;
this.selectiondrag=false;
this.zoom=false;
this.zoomactive=false;
this.hasfocus=false;
this.hasmousefocus=false;
this.visibility=true;
this.locked=false;
this.hidden=false;
this.cursoractive=true;
this.overflowx=self.opt.overflowx;
this.overflowy=self.opt.overflowy;
this.nativescrollingarea=false;
this.checkarea=0;
this.events=[];
this.saved={};
this.delaylist={};
this.synclist={};
this.lastdeltax=0;
this.lastdeltay=0;
this.detected=getBrowserDetection();
var cap=$.extend({},this.detected);
this.canhwscroll=(cap.hastransform&&self.opt.hwacceleration);
this.ishwscroll=(this.canhwscroll&&self.haswrapper);
this.istouchcapable=false;
if(cap.cantouch&&cap.ischrome&&!cap.isios&&!cap.isandroid){
this.istouchcapable=true;
cap.cantouch=false;
}
if(cap.cantouch&&cap.ismozilla&&!cap.isios&&!cap.isandroid){
this.istouchcapable=true;
cap.cantouch=false;
}
if(!self.opt.enablemouselockapi){
cap.hasmousecapture=false;
cap.haspointerlock=false;
}
this.delayed=function(name,fn,tm,lazy){
var dd=self.delaylist[name];
var nw=(new Date()).getTime();
if(!lazy&&dd&&dd.tt) return false;
if(dd&&dd.tt) clearTimeout(dd.tt);
if(dd&&dd.last+tm>nw&&!dd.tt){
self.delaylist[name]={
last:nw+tm,
tt:setTimeout(function(){self.delaylist[name].tt=0;fn.call();},tm)
}}
else if(!dd||!dd.tt){
self.delaylist[name]={
last:nw,
tt:0
}
setTimeout(function(){fn.call();},0);
}};
this.debounced=function(name,fn,tm){
var dd=self.delaylist[name];
var nw=(new Date()).getTime();
self.delaylist[name]=fn;
if(!dd){
setTimeout(function(){var fn=self.delaylist[name];self.delaylist[name]=false;fn.call();},tm);
}}
this.synched=function(name,fn){
function requestSync(){
if(self.onsync) return;
setAnimationFrame(function(){
self.onsync=false;
for(name in self.synclist){
var fn=self.synclist[name];
if(fn) fn.call(self);
self.synclist[name]=false;
}});
self.onsync=true;
};
self.synclist[name]=fn;
requestSync();
return name;
};
this.unsynched=function(name){
if(self.synclist[name]) self.synclist[name]=false;
}
this.css=function(el,pars){
for(var n in pars){
self.saved.css.push([el,n,el.css(n)]);
el.css(n,pars[n]);
}};
this.scrollTop=function(val){
return (typeof val=="undefined") ? self.getScrollTop():self.setScrollTop(val);
};
this.scrollLeft=function(val){
return (typeof val=="undefined") ? self.getScrollLeft():self.setScrollLeft(val);
};
BezierClass=function(st,ed,spd,p1,p2,p3,p4){
this.st=st;
this.ed=ed;
this.spd=spd;
this.p1=p1||0;
this.p2=p2||1;
this.p3=p3||0;
this.p4=p4||1;
this.ts=(new Date()).getTime();
this.df=this.ed-this.st;
};
BezierClass.prototype={
B2:function(t){ return 3*t*t*(1-t) },
B3:function(t){ return 3*t*(1-t)*(1-t) },
B4:function(t){ return (1-t)*(1-t)*(1-t) },
getNow:function(){
var nw=(new Date()).getTime();
var pc=1-((nw-this.ts)/this.spd);
var bz=this.B2(pc) + this.B3(pc) + this.B4(pc);
return (pc<0) ? this.ed:this.st+Math.round(this.df*bz);
},
update:function(ed,spd){
this.st=this.getNow();
this.ed=ed;
this.spd=spd;
this.ts=(new Date()).getTime();
this.df=this.ed-this.st;
return this;
}};
if(this.ishwscroll){
this.doc.translate={x:0,y:0,tx:"0px",ty:"0px"};
if(cap.hastranslate3d&&cap.isios) this.doc.css("-webkit-backface-visibility","hidden");
function getMatrixValues(){
var tr=self.doc.css(cap.trstyle);
if(tr&&(tr.substr(0,6)=="matrix")){
return tr.replace(/^.*\((.*)\)$/g, "$1").replace(/px/g,'').split(/, +/);
}
return false;
}
this.getScrollTop=function(last){
if(!last){
var mtx=getMatrixValues();
if(mtx) return (mtx.length==16) ? -mtx[13]:-mtx[5];
if(self.timerscroll&&self.timerscroll.bz) return self.timerscroll.bz.getNow();
}
return self.doc.translate.y;
};
this.getScrollLeft=function(last){
if(!last){
var mtx=getMatrixValues();
if(mtx) return (mtx.length==16) ? -mtx[12]:-mtx[4];
if(self.timerscroll&&self.timerscroll.bh) return self.timerscroll.bh.getNow();
}
return self.doc.translate.x;
};
if(document.createEvent){
this.notifyScrollEvent=function(el){
var e=document.createEvent("UIEvents");
e.initUIEvent("scroll", false, true, window, 1);
el.dispatchEvent(e);
};}
else if(document.fireEvent){
this.notifyScrollEvent=function(el){
var e=document.createEventObject();
el.fireEvent("onscroll");
e.cancelBubble=true;
};}else{
this.notifyScrollEvent=function(el,add){};}
if(cap.hastranslate3d&&self.opt.enabletranslate3d){
this.setScrollTop=function(val,silent){
self.doc.translate.y=val;
self.doc.translate.ty=(val*-1)+"px";
self.doc.css(cap.trstyle,"translate3d("+self.doc.translate.tx+","+self.doc.translate.ty+",0px)");
if(!silent) self.notifyScrollEvent(self.win[0]);
};
this.setScrollLeft=function(val,silent){
self.doc.translate.x=val;
self.doc.translate.tx=(val*-1)+"px";
self.doc.css(cap.trstyle,"translate3d("+self.doc.translate.tx+","+self.doc.translate.ty+",0px)");
if(!silent) self.notifyScrollEvent(self.win[0]);
};}else{
this.setScrollTop=function(val,silent){
self.doc.translate.y=val;
self.doc.translate.ty=(val*-1)+"px";
self.doc.css(cap.trstyle,"translate("+self.doc.translate.tx+","+self.doc.translate.ty+")");
if(!silent) self.notifyScrollEvent(self.win[0]);
};
this.setScrollLeft=function(val,silent){
self.doc.translate.x=val;
self.doc.translate.tx=(val*-1)+"px";
self.doc.css(cap.trstyle,"translate("+self.doc.translate.tx+","+self.doc.translate.ty+")");
if(!silent) self.notifyScrollEvent(self.win[0]);
};}}else{
this.getScrollTop=function(){
return self.docscroll.scrollTop();
};
this.setScrollTop=function(val){
return self.docscroll.scrollTop(val);
};
this.getScrollLeft=function(){
return self.docscroll.scrollLeft();
};
this.setScrollLeft=function(val){
return self.docscroll.scrollLeft(val);
};}
this.getTarget=function(e){
if(!e) return false;
if(e.target) return e.target;
if(e.srcElement) return e.srcElement;
return false;
};
this.hasParent=function(e,id){
if(!e) return false;
var el=e.target||e.srcElement||e||false;
while (el&&el.id!=id){
el=el.parentNode||false;
}
return (el!==false);
};
function getZIndex(){
var dom=self.win;
if("zIndex" in dom) return dom.zIndex();
while (dom.length>0){
if(dom[0].nodeType==9) return false;
var zi=dom.css('zIndex');
if(!isNaN(zi)&&zi!=0) return parseInt(zi);
dom=dom.parent();
}
return false;
};
var _convertBorderWidth={"thin":1,"medium":3,"thick":5};
function getWidthToPixel(dom,prop,chkheight){
var wd=dom.css(prop);
var px=parseFloat(wd);
if(isNaN(px)){
px=_convertBorderWidth[wd]||0;
var brd=(px==3) ? ((chkheight)?(self.win.outerHeight() - self.win.innerHeight()):(self.win.outerWidth() - self.win.innerWidth())):1;
if(self.isie8&&px) px+=1;
return (brd) ? px:0;
}
return px;
};
this.getOffset=function(){
if(self.isfixed) return {top:parseFloat(self.win.css('top')),left:parseFloat(self.win.css('left'))};
if(!self.viewport) return self.win.offset();
var ww=self.win.offset();
var vp=self.viewport.offset();
return {top:ww.top-vp.top+self.viewport.scrollTop(),left:ww.left-vp.left+self.viewport.scrollLeft()};};
this.updateScrollBar=function(len){
if(self.ishwscroll){
self.rail.css({height:self.win.innerHeight()});
if(self.railh) self.railh.css({width:self.win.innerWidth()});
}else{
var wpos=self.getOffset();
var pos={top:wpos.top,left:wpos.left};
pos.top+=getWidthToPixel(self.win,'border-top-width',true);
var brd=(self.win.outerWidth() - self.win.innerWidth())/2;
pos.left+=(self.rail.align) ? self.win.outerWidth() - getWidthToPixel(self.win,'border-right-width') - self.rail.width:getWidthToPixel(self.win,'border-left-width');
var off=self.opt.railoffset;
if(off){
if(off.top) pos.top+=off.top;
if(self.rail.align&&off.left) pos.left+=off.left;
}
if(!self.locked) self.rail.css({top:pos.top,left:pos.left,height:(len)?len.h:self.win.innerHeight()});
if(self.zoom){
self.zoom.css({top:pos.top+1,left:(self.rail.align==1) ? pos.left-20:pos.left+self.rail.width+4});
}
if(self.railh&&!self.locked){
var pos={top:wpos.top,left:wpos.left};
var y=(self.railh.align) ? pos.top + getWidthToPixel(self.win,'border-top-width',true) + self.win.innerHeight() - self.railh.height:pos.top + getWidthToPixel(self.win,'border-top-width',true);
var x=pos.left + getWidthToPixel(self.win,'border-left-width');
self.railh.css({top:y,left:x,width:self.railh.width});
}}
};
this.doRailClick=function(e,dbl,hr){
var fn,pg,cur,pos;
if(self.locked) return;
self.cancelEvent(e);
if(dbl){
fn=(hr) ? self.doScrollLeft:self.doScrollTop;
cur=(hr) ? ((e.pageX - self.railh.offset().left - (self.cursorwidth/2)) * self.scrollratio.x):((e.pageY - self.rail.offset().top - (self.cursorheight/2)) * self.scrollratio.y);
fn(cur);
}else{
fn=(hr) ? self.doScrollLeftBy:self.doScrollBy;
cur=(hr) ? self.scroll.x:self.scroll.y;
pos=(hr) ? e.pageX - self.railh.offset().left:e.pageY - self.rail.offset().top;
pg=(hr) ? self.view.w:self.view.h;
(cur>=pos) ? fn(pg):fn(-pg);
}}
self.hasanimationframe=(setAnimationFrame);
self.hascancelanimationframe=(clearAnimationFrame);
if(!self.hasanimationframe){
setAnimationFrame=function(fn){return setTimeout(fn,15-Math.floor((+new Date)/1000)%16)};
clearAnimationFrame=clearInterval;
}
else if(!self.hascancelanimationframe) clearAnimationFrame=function(){self.cancelAnimationFrame=true};
this.init=function(){
self.saved.css=[];
if(cap.isie7mobile) return true;
if(cap.isoperamini) return true;
if(cap.hasmstouch) self.css((self.ispage)?$("html"):self.win,{'-ms-touch-action':'none'});
self.zindex="auto";
if(!self.ispage&&self.opt.zindex=="auto"){
self.zindex=getZIndex()||"auto";
}else{
self.zindex=self.opt.zindex;
}
if(!self.ispage&&self.zindex!="auto"){
if(self.zindex>globalmaxzindex) globalmaxzindex=self.zindex;
}
if(self.isie&&self.zindex==0&&self.opt.zindex=="auto"){
self.zindex="auto";
}
if(!self.ispage||(!cap.cantouch&&!cap.isieold&&!cap.isie9mobile)){
var cont=self.docscroll;
if(self.ispage) cont=(self.haswrapper)?self.win:self.doc;
if(!cap.isie9mobile) self.css(cont,{'overflow-y':'hidden'});
if(self.ispage&&cap.isie7){
if(self.doc[0].nodeName=='BODY') self.css($("html"),{'overflow-y':'hidden'});
else if(self.doc[0].nodeName=='HTML') self.css($("body"),{'overflow-y':'hidden'});
}
if(cap.isios&&!self.ispage&&!self.haswrapper) self.css($("body"),{"-webkit-overflow-scrolling":"touch"});
var cursor=$(document.createElement('div'));
cursor.css({
position:"relative",top:0,"float":"right",width:self.opt.cursorwidth,height:"0px",
'background-color':self.opt.cursorcolor,
border:self.opt.cursorborder,
'background-clip':'padding-box',
'-webkit-border-radius':self.opt.cursorborderradius,
'-moz-border-radius':self.opt.cursorborderradius,
'border-radius':self.opt.cursorborderradius
});
cursor.hborder=parseFloat(cursor.outerHeight() - cursor.innerHeight());
self.cursor=cursor;
var rail=$(document.createElement('div'));
rail.attr('id',self.id);
rail.addClass('nicescroll-rails');
var v,a,kp=["left","right"];  //"top","bottom"
for(var n in kp){
a=kp[n];
v=self.opt.railpadding[a];
(v) ? rail.css("padding-"+a,v+"px"):self.opt.railpadding[a]=0;
}
rail.append(cursor);
rail.width=Math.max(parseFloat(self.opt.cursorwidth),cursor.outerWidth()) + self.opt.railpadding['left'] + self.opt.railpadding['right'];
rail.css({width:rail.width+"px",'zIndex':self.zindex,"background":self.opt.background,cursor:"default"});
rail.visibility=true;
rail.scrollable=true;
rail.align=(self.opt.railalign=="left") ? 0:1;
self.rail=rail;
self.rail.drag=false;
var zoom=false;
if(self.opt.boxzoom&&!self.ispage&&!cap.isieold){
zoom=document.createElement('div');
self.bind(zoom,"click",self.doZoom);
self.zoom=$(zoom);
self.zoom.css({"cursor":"pointer",'z-index':self.zindex,'backgroundImage':'url('+scriptpath+'zoomico.png)','height':18,'width':18,'backgroundPosition':'0px 0px'});
if(self.opt.dblclickzoom) self.bind(self.win,"dblclick",self.doZoom);
if(cap.cantouch&&self.opt.gesturezoom){
self.ongesturezoom=function(e){
if(e.scale>1.5) self.doZoomIn(e);
if(e.scale<0.8) self.doZoomOut(e);
return self.cancelEvent(e);
};
self.bind(self.win,"gestureend",self.ongesturezoom);
}}
self.railh=false;
if(self.opt.horizrailenabled){
self.css(cont,{'overflow-x':'hidden'});
var cursor=$(document.createElement('div'));
cursor.css({
position:"relative",top:0,height:self.opt.cursorwidth,width:"0px",
'background-color':self.opt.cursorcolor,
border:self.opt.cursorborder,
'background-clip':'padding-box',
'-webkit-border-radius':self.opt.cursorborderradius,
'-moz-border-radius':self.opt.cursorborderradius,
'border-radius':self.opt.cursorborderradius
});
cursor.wborder=parseFloat(cursor.outerWidth() - cursor.innerWidth());
self.cursorh=cursor;
var railh=$(document.createElement('div'));
railh.attr('id',self.id+'-hr');
railh.addClass('nicescroll-rails');
railh.height=Math.max(parseFloat(self.opt.cursorwidth),cursor.outerHeight());
railh.css({height:railh.height+"px",'zIndex':self.zindex,"background":self.opt.background});
railh.append(cursor);
railh.visibility=true;
railh.scrollable=true;
railh.align=(self.opt.railvalign=="top") ? 0:1;
self.railh=railh;
self.railh.drag=false;
}
if(self.ispage){
rail.css({position:"fixed",top:"0px",height:"100%"});
(rail.align) ? rail.css({right:"0px"}):rail.css({left:"0px"});
self.body.append(rail);
if(self.railh){
railh.css({position:"fixed",left:"0px",width:"100%"});
(railh.align) ? railh.css({bottom:"0px"}):railh.css({top:"0px"});
self.body.append(railh);
}}else{
if(self.ishwscroll){
if(self.win.css('position')=='static') self.css(self.win,{'position':'relative'});
var bd=(self.win[0].nodeName=='HTML') ? self.body:self.win;
if(self.zoom){
self.zoom.css({position:"absolute",top:1,right:0,"margin-right":rail.width+4});
bd.append(self.zoom);
}
rail.css({position:"absolute",top:0});
(rail.align) ? rail.css({right:0}):rail.css({left:0});
bd.append(rail);
if(railh){
railh.css({position:"absolute",left:0,bottom:0});
(railh.align) ? railh.css({bottom:0}):railh.css({top:0});
bd.append(railh);
}}else{
self.isfixed=(self.win.css("position")=="fixed");
var rlpos=(self.isfixed) ? "fixed":"absolute";
if(!self.isfixed) self.viewport=self.getViewport(self.win[0]);
if(self.viewport){
self.body=self.viewport;
if((/relative|absolute/.test(self.viewport.css("position")))==false) self.css(self.viewport,{"position":"relative"});
}
rail.css({position:rlpos});
if(self.zoom) self.zoom.css({position:rlpos});
self.updateScrollBar();
self.body.append(rail);
if(self.zoom) self.body.append(self.zoom);
if(self.railh){
railh.css({position:rlpos});
self.body.append(railh);
}}
if(cap.isios) self.css(self.win,{'-webkit-tap-highlight-color':'rgba(0,0,0,0)','-webkit-touch-callout':'none'});
if(cap.isie&&self.opt.disableoutline) self.win.attr("hideFocus","true");
if(cap.iswebkit&&self.opt.disableoutline) self.win.css({"outline":"none"});
}
if(self.opt.autohidemode===false){
self.autohidedom=false;
self.rail.css({opacity:self.opt.cursoropacitymax});
if(self.railh) self.railh.css({opacity:self.opt.cursoropacitymax});
}
else if(self.opt.autohidemode===true){
self.autohidedom=$().add(self.rail);
if(cap.isie8) self.autohidedom=self.autohidedom.add(self.cursor);
if(self.railh) self.autohidedom=self.autohidedom.add(self.railh);
if(self.railh&&cap.isie8) self.autohidedom=self.autohidedom.add(self.cursorh);
}
else if(self.opt.autohidemode=="scroll"){
self.autohidedom=$().add(self.rail);
if(self.railh) self.autohidedom=self.autohidedom.add(self.railh);
}
else if(self.opt.autohidemode=="cursor"){
self.autohidedom=$().add(self.cursor);
if(self.railh) self.autohidedom=self.autohidedom.add(self.cursorh);
}
else if(self.opt.autohidemode=="hidden"){
self.autohidedom=false;
self.hide();
self.locked=false;
}
if(cap.isie9mobile){
self.scrollmom=new ScrollMomentumClass2D(self);
/*
var trace=function(msg){
var db=$("#debug");
if(isNaN(msg)&&(typeof msg!="string")){
var x=[];
for(var a in msg){
x.push(a+":"+msg[a]);
}
msg="{"+x.join(",")+"}";
}
if(db.children().length>0){
db.children().eq(0).before("<div>"+msg+"</div>");
}else{
db.append("<div>"+msg+"</div>");
}}
window.onerror=function(msg,url,ln){
trace("ERR: "+msg+" at "+ln);
}
*/
self.onmangotouch=function(e){
var py=self.getScrollTop();
var px=self.getScrollLeft();
if((py==self.scrollmom.lastscrolly)&&(px==self.scrollmom.lastscrollx)) return true;
var dfy=py-self.mangotouch.sy;
var dfx=px-self.mangotouch.sx;
var df=Math.round(Math.sqrt(Math.pow(dfx,2)+Math.pow(dfy,2)));
if(df==0) return;
var dry=(dfy<0)?-1:1;
var drx=(dfx<0)?-1:1;
var tm=+new Date();
if(self.mangotouch.lazy) clearTimeout(self.mangotouch.lazy);
if(((tm-self.mangotouch.tm)>80)||(self.mangotouch.dry!=dry)||(self.mangotouch.drx!=drx)){
self.scrollmom.stop();
self.scrollmom.reset(px,py);
self.mangotouch.sy=py;
self.mangotouch.ly=py;
self.mangotouch.sx=px;
self.mangotouch.lx=px;
self.mangotouch.dry=dry;
self.mangotouch.drx=drx;
self.mangotouch.tm=tm;
}else{
self.scrollmom.stop();
self.scrollmom.update(self.mangotouch.sx-dfx,self.mangotouch.sy-dfy);
var gap=tm - self.mangotouch.tm;
self.mangotouch.tm=tm;
var ds=Math.max(Math.abs(self.mangotouch.ly-py),Math.abs(self.mangotouch.lx-px));
self.mangotouch.ly=py;
self.mangotouch.lx=px;
if(ds>2){
self.mangotouch.lazy=setTimeout(function(){
self.mangotouch.lazy=false;
self.mangotouch.dry=0;
self.mangotouch.drx=0;
self.mangotouch.tm=0;
self.scrollmom.doMomentum(30);
},100);
}}
}
var top=self.getScrollTop();
var lef=self.getScrollLeft();
self.mangotouch={sy:top,ly:top,dry:0,sx:lef,lx:lef,drx:0,lazy:false,tm:0};
self.bind(self.docscroll,"scroll",self.onmangotouch);
}else{
if(cap.cantouch||self.istouchcapable||self.opt.touchbehavior||cap.hasmstouch){
self.scrollmom=new ScrollMomentumClass2D(self);
self.ontouchstart=function(e){
if(e.pointerType&&e.pointerType!=2) return false;
if(!self.locked){
if(cap.hasmstouch){
var tg=(e.target) ? e.target:false;
while (tg){
var nc=$(tg).getNiceScroll();
if((nc.length>0)&&(nc[0].me==self.me)) break;
if(nc.length>0) return false;
if((tg.nodeName=='DIV')&&(tg.id==self.id)) break;
tg=(tg.parentNode) ? tg.parentNode:false;
}}
self.cancelScroll();
var tg=self.getTarget(e);
if(tg){
var skp=(/INPUT/i.test(tg.nodeName))&&(/range/i.test(tg.type));
if(skp) return self.stopPropagation(e);
}
if(!("clientX" in e)&&("changedTouches" in e)){
e.clientX=e.changedTouches[0].clientX;
e.clientY=e.changedTouches[0].clientY;
}
if(self.forcescreen){
var le=e;
var e={"original":(e.original)?e.original:e};
e.clientX=le.screenX;
e.clientY=le.screenY;
}
self.rail.drag={x:e.clientX,y:e.clientY,sx:self.scroll.x,sy:self.scroll.y,st:self.getScrollTop(),sl:self.getScrollLeft(),pt:2,dl:false};
if(self.ispage||!self.opt.directionlockdeadzone){
self.rail.drag.dl="f";
}else{
var view={
w:$(window).width(),
h:$(window).height()
};
var page={
w:Math.max(document.body.scrollWidth,document.documentElement.scrollWidth),
h:Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)
}
var maxh=Math.max(0,page.h - view.h);
var maxw=Math.max(0,page.w - view.w);
if(!self.rail.scrollable&&self.railh.scrollable) self.rail.drag.ck=(maxh>0) ? "v":false;
else if(self.rail.scrollable&&!self.railh.scrollable) self.rail.drag.ck=(maxw>0) ? "h":false;
else self.rail.drag.ck=false;
if(!self.rail.drag.ck) self.rail.drag.dl="f";
}
if(self.opt.touchbehavior&&self.isiframe&&cap.isie){
var wp=self.win.position();
self.rail.drag.x+=wp.left;
self.rail.drag.y+=wp.top;
}
self.hasmoving=false;
self.lastmouseup=false;
self.scrollmom.reset(e.clientX,e.clientY);
if(!cap.cantouch&&!this.istouchcapable&&!cap.hasmstouch){
var ip=(tg)?/INPUT|SELECT|TEXTAREA/i.test(tg.nodeName):false;
if(!ip){
if(!self.ispage&&cap.hasmousecapture) tg.setCapture();
return (self.opt.touchbehavior) ? self.cancelEvent(e):self.stopPropagation(e);
}
if(/SUBMIT|CANCEL|BUTTON/i.test($(tg).attr('type'))){
pc={"tg":tg,"click":false};
self.preventclick=pc;
}}
}};
self.ontouchend=function(e){
if(e.pointerType&&e.pointerType!=2) return false;
if(self.rail.drag&&(self.rail.drag.pt==2)){
self.scrollmom.doMomentum();
self.rail.drag=false;
if(self.hasmoving){
self.hasmoving=false;
self.lastmouseup=true;
self.hideCursor();
if(cap.hasmousecapture) document.releaseCapture();
if(!cap.cantouch) return self.cancelEvent(e);
}}
};
var moveneedoffset=(self.opt.touchbehavior&&self.isiframe&&!cap.hasmousecapture);
self.ontouchmove=function(e,byiframe){
if(e.pointerType&&e.pointerType!=2) return false;
if(self.rail.drag&&(self.rail.drag.pt==2)){
if(cap.cantouch&&(typeof e.original=="undefined")) return true;
self.hasmoving=true;
if(self.preventclick&&!self.preventclick.click){
self.preventclick.click=self.preventclick.tg.onclick||false;
self.preventclick.tg.onclick=self.onpreventclick;
}
var ev=$.extend({"original":e},e);
e=ev;
if(("changedTouches" in e)){
e.clientX=e.changedTouches[0].clientX;
e.clientY=e.changedTouches[0].clientY;
}
if(self.forcescreen){
var le=e;
var e={"original":(e.original)?e.original:e};
e.clientX=le.screenX;
e.clientY=le.screenY;
}
var ofx=ofy=0;
if(moveneedoffset&&!byiframe){
var wp=self.win.position();
ofx=-wp.left;
ofy=-wp.top;
}
var fy=e.clientY + ofy;
var my=(fy-self.rail.drag.y);
var fx=e.clientX + ofx;
var mx=(fx-self.rail.drag.x);
var ny=self.rail.drag.st-my;
if(self.ishwscroll&&self.opt.bouncescroll){
if(ny<0){
ny=Math.round(ny/2);
}
else if(ny>self.page.maxh){
ny=self.page.maxh+Math.round((ny-self.page.maxh)/2);
}}else{
if(ny<0){ny=0;fy=0}
if(ny>self.page.maxh){ny=self.page.maxh;fy=0}}
if(self.railh&&self.railh.scrollable){
var nx=self.rail.drag.sl-mx;
if(self.ishwscroll&&self.opt.bouncescroll){
if(nx<0){
nx=Math.round(nx/2);
}
else if(nx>self.page.maxw){
nx=self.page.maxw+Math.round((nx-self.page.maxw)/2);
}}else{
if(nx<0){nx=0;fx=0}
if(nx>self.page.maxw){nx=self.page.maxw;fx=0}}
}
var grabbed=false;
if(self.rail.drag.dl){
grabbed=true;
if(self.rail.drag.dl=="v") nx=self.rail.drag.sl;
else if(self.rail.drag.dl=="h") ny=self.rail.drag.st;
}else{
var ay=Math.abs(my);
var ax=Math.abs(mx);
var dz=self.opt.directionlockdeadzone;
if(self.rail.drag.ck=="v"){
if(ay>dz&&(ax<=(ay*0.3))){
self.rail.drag=false;
return true;
}
else if(ax>dz){
self.rail.drag.dl="f";
$("body").scrollTop($("body").scrollTop());
}}
else if(self.rail.drag.ck=="h"){
if(ax>dz&&(ay<=(ax*0.3))){
self.rail.drag=false;
return true;
}
else if(ay>dz){
self.rail.drag.dl="f";
$("body").scrollLeft($("body").scrollLeft());
}}
}
self.synched("touchmove",function(){
if(self.rail.drag&&(self.rail.drag.pt==2)){
if(self.prepareTransition) self.prepareTransition(0);
if(self.rail.scrollable) self.setScrollTop(ny);
self.scrollmom.update(fx,fy);
if(self.railh&&self.railh.scrollable){
self.setScrollLeft(nx);
self.showCursor(ny,nx);
}else{
self.showCursor(ny);
}
if(cap.isie10) document.selection.clear();
}});
if(cap.ischrome&&self.istouchcapable) grabbed=false;
if(grabbed) return self.cancelEvent(e);
}};}
self.onmousedown=function(e,hronly){
if(self.rail.drag&&self.rail.drag.pt!=1) return;
if(self.locked) return self.cancelEvent(e);
self.cancelScroll();
self.rail.drag={x:e.clientX,y:e.clientY,sx:self.scroll.x,sy:self.scroll.y,pt:1,hr:(!!hronly)};
var tg=self.getTarget(e);
if(!self.ispage&&cap.hasmousecapture) tg.setCapture();
if(self.isiframe&&!cap.hasmousecapture){
self.saved["csspointerevents"]=self.doc.css("pointer-events");
self.css(self.doc,{"pointer-events":"none"});
}
return self.cancelEvent(e);
};
self.onmouseup=function(e){
if(self.rail.drag){
if(cap.hasmousecapture) document.releaseCapture();
if(self.isiframe&&!cap.hasmousecapture) self.doc.css("pointer-events",self.saved["csspointerevents"]);
if(self.rail.drag.pt!=1)return;
self.rail.drag=false;
return self.cancelEvent(e);
}};
self.onmousemove=function(e){
if(self.rail.drag){
if(self.rail.drag.pt!=1)return;
if(cap.ischrome&&e.which==0) return self.onmouseup(e);
self.cursorfreezed=true;
if(self.rail.drag.hr){
self.scroll.x=self.rail.drag.sx + (e.clientX-self.rail.drag.x);
if(self.scroll.x<0) self.scroll.x=0;
var mw=self.scrollvaluemaxw;
if(self.scroll.x>mw) self.scroll.x=mw;
}else{
self.scroll.y=self.rail.drag.sy + (e.clientY-self.rail.drag.y);
if(self.scroll.y<0) self.scroll.y=0;
var my=self.scrollvaluemax;
if(self.scroll.y>my) self.scroll.y=my;
}
self.synched('mousemove',function(){
if(self.rail.drag&&(self.rail.drag.pt==1)){
self.showCursor();
if(self.rail.drag.hr) self.doScrollLeft(Math.round(self.scroll.x*self.scrollratio.x),self.opt.cursordragspeed);
else self.doScrollTop(Math.round(self.scroll.y*self.scrollratio.y),self.opt.cursordragspeed);
}});
return self.cancelEvent(e);
}
};
if(cap.cantouch||self.opt.touchbehavior){
self.onpreventclick=function(e){
if(self.preventclick){
self.preventclick.tg.onclick=self.preventclick.click;
self.preventclick=false;
return self.cancelEvent(e);
}}
self.bind(self.win,"mousedown",self.ontouchstart);
self.onclick=(cap.isios) ? false:function(e){
if(self.lastmouseup){
self.lastmouseup=false;
return self.cancelEvent(e);
}else{
return true;
}};
if(self.opt.grabcursorenabled&&cap.cursorgrabvalue){
self.css((self.ispage)?self.doc:self.win,{'cursor':cap.cursorgrabvalue});
self.css(self.rail,{'cursor':cap.cursorgrabvalue});
}}else{
function checkSelectionScroll(e){
if(!self.selectiondrag) return;
if(e){
var ww=self.win.outerHeight();
var df=(e.pageY - self.selectiondrag.top);
if(df>0&&df<ww) df=0;
if(df>=ww) df-=ww;
self.selectiondrag.df=df;
}
if(self.selectiondrag.df==0) return;
var rt=-Math.floor(self.selectiondrag.df/6)*2;
self.doScrollBy(rt);
self.debounced("doselectionscroll",function(){checkSelectionScroll()},50);
}
if("getSelection" in document){
self.hasTextSelected=function(){
return (document.getSelection().rangeCount>0);
}}
else if("selection" in document){
self.hasTextSelected=function(){
return (document.selection.type!="None");
}}else{
self.hasTextSelected=function(){
return false;
}}
self.onselectionstart=function(e){
if(self.ispage) return;
self.selectiondrag=self.win.offset();
}
self.onselectionend=function(e){
self.selectiondrag=false;
}
self.onselectiondrag=function(e){
if(!self.selectiondrag) return;
if(self.hasTextSelected()) self.debounced("selectionscroll",function(){checkSelectionScroll(e)},250);
}}
if(cap.hasmstouch){
self.css(self.rail,{'-ms-touch-action':'none'});
self.css(self.cursor,{'-ms-touch-action':'none'});
self.bind(self.win,"MSPointerDown",self.ontouchstart);
self.bind(document,"MSPointerUp",self.ontouchend);
self.bind(document,"MSPointerMove",self.ontouchmove);
self.bind(self.cursor,"MSGestureHold",function(e){e.preventDefault()});
self.bind(self.cursor,"contextmenu",function(e){e.preventDefault()});
}
if(this.istouchcapable){
self.bind(self.win,"touchstart",self.ontouchstart);
self.bind(document,"touchend",self.ontouchend);
self.bind(document,"touchcancel",self.ontouchend);
self.bind(document,"touchmove",self.ontouchmove);
}
self.bind(self.cursor,"mousedown",self.onmousedown);
self.bind(self.cursor,"mouseup",self.onmouseup);
if(self.railh){
self.bind(self.cursorh,"mousedown",function(e){self.onmousedown(e,true)});
self.bind(self.cursorh,"mouseup",function(e){
if(self.rail.drag&&self.rail.drag.pt==2) return;
self.rail.drag=false;
self.hasmoving=false;
self.hideCursor();
if(cap.hasmousecapture) document.releaseCapture();
return self.cancelEvent(e);
});
}
if(self.opt.cursordragontouch||!cap.cantouch&&!self.opt.touchbehavior){
self.rail.css({"cursor":"default"});
self.railh&&self.railh.css({"cursor":"default"});
self.jqbind(self.rail,"mouseenter",function(){
if(self.canshowonmouseevent) self.showCursor();
self.rail.active=true;
});
self.jqbind(self.rail,"mouseleave",function(){
self.rail.active=false;
if(!self.rail.drag) self.hideCursor();
});
if(self.opt.sensitiverail){
self.bind(self.rail,"click",function(e){self.doRailClick(e,false,false)});
self.bind(self.rail,"dblclick",function(e){self.doRailClick(e,true,false)});
self.bind(self.cursor,"click",function(e){self.cancelEvent(e)});
self.bind(self.cursor,"dblclick",function(e){self.cancelEvent(e)});
}
if(self.railh){
self.jqbind(self.railh,"mouseenter",function(){
if(self.canshowonmouseevent) self.showCursor();
self.rail.active=true;
});
self.jqbind(self.railh,"mouseleave",function(){
self.rail.active=false;
if(!self.rail.drag) self.hideCursor();
});
if(self.opt.sensitiverail){
self.bind(self.railh, "click", function(e){self.doRailClick(e,false,true)});
self.bind(self.railh, "dblclick", function(e){self.doRailClick(e, true, true) });
self.bind(self.cursorh, "click", function (e){ self.cancelEvent(e) });
self.bind(self.cursorh, "dblclick", function (e){ self.cancelEvent(e) });
}}
}
if(!cap.cantouch&&!self.opt.touchbehavior){
self.bind((cap.hasmousecapture)?self.win:document,"mouseup",self.onmouseup);
self.bind(document,"mousemove",self.onmousemove);
if(self.onclick) self.bind(document,"click",self.onclick);
if(!self.ispage&&self.opt.enablescrollonselection){
self.bind(self.win[0],"mousedown",self.onselectionstart);
self.bind(document,"mouseup",self.onselectionend);
self.bind(self.cursor,"mouseup",self.onselectionend);
if(self.cursorh) self.bind(self.cursorh,"mouseup",self.onselectionend);
self.bind(document,"mousemove",self.onselectiondrag);
}
if(self.zoom){
self.jqbind(self.zoom,"mouseenter",function(){
if(self.canshowonmouseevent) self.showCursor();
self.rail.active=true;
});
self.jqbind(self.zoom,"mouseleave",function(){
self.rail.active=false;
if(!self.rail.drag) self.hideCursor();
});
}}else{
self.bind((cap.hasmousecapture)?self.win:document,"mouseup",self.ontouchend);
self.bind(document,"mousemove",self.ontouchmove);
if(self.onclick) self.bind(document,"click",self.onclick);
if(self.opt.cursordragontouch){
self.bind(self.cursor,"mousedown",self.onmousedown);
self.bind(self.cursor,"mousemove",self.onmousemove);
self.cursorh&&self.bind(self.cursorh,"mousedown",self.onmousedown);
self.cursorh&&self.bind(self.cursorh,"mousemove",self.onmousemove);
}}
if(self.opt.enablemousewheel){
if(!self.isiframe) self.bind((cap.isie&&self.ispage) ? document:self.win  ,"mousewheel",self.onmousewheel);
self.bind(self.rail,"mousewheel",self.onmousewheel);
if(self.railh) self.bind(self.railh,"mousewheel",self.onmousewheelhr);
}
if(!self.ispage&&!cap.cantouch&&!(/HTML|BODY/.test(self.win[0].nodeName))){
if(!self.win.attr("tabindex")) self.win.attr({"tabindex":tabindexcounter++});
self.jqbind(self.win,"focus",function(e){
domfocus=(self.getTarget(e)).id||true;
self.hasfocus=true;
if(self.canshowonmouseevent) self.noticeCursor();
});
self.jqbind(self.win,"blur",function(e){
domfocus=false;
self.hasfocus=false;
});
self.jqbind(self.win,"mouseenter",function(e){
mousefocus=(self.getTarget(e)).id||true;
self.hasmousefocus=true;
if(self.canshowonmouseevent) self.noticeCursor();
});
self.jqbind(self.win,"mouseleave",function(){
mousefocus=false;
self.hasmousefocus=false;
});
};}
self.onkeypress=function(e){
if(self.locked&&self.page.maxh==0) return true;
e=(e) ? e:window.e;
var tg=self.getTarget(e);
if(tg&&/INPUT|TEXTAREA|SELECT|OPTION/.test(tg.nodeName)){
var tp=tg.getAttribute('type')||tg.type||false;
if((!tp)||!(/submit|button|cancel/i.tp)) return true;
}
if(self.hasfocus||(self.hasmousefocus&&!domfocus)||(self.ispage&&!domfocus&&!mousefocus)){
var key=e.keyCode;
if(self.locked&&key!=27) return self.cancelEvent(e);
var ctrl=e.ctrlKey||false;
var shift=e.shiftKey||false;
var ret=false;
switch (key){
case 38:
case 63233:
self.doScrollBy(24*3);
ret=true;
break;
case 40:
case 63235:
self.doScrollBy(-24*3);
ret=true;
break;
case 37:
case 63232:
if(self.railh){
(ctrl) ? self.doScrollLeft(0):self.doScrollLeftBy(24*3);
ret=true;
}
break;
case 39:
case 63234:
if(self.railh){
(ctrl) ? self.doScrollLeft(self.page.maxw):self.doScrollLeftBy(-24*3);
ret=true;
}
break;
case 33:
case 63276:
self.doScrollBy(self.view.h);
ret=true;
break;
case 34:
case 63277:
self.doScrollBy(-self.view.h);
ret=true;
break;
case 36:
case 63273:
(self.railh&&ctrl) ? self.doScrollPos(0,0):self.doScrollTo(0);
ret=true;
break;
case 35:
case 63275:
(self.railh&&ctrl) ? self.doScrollPos(self.page.maxw,self.page.maxh):self.doScrollTo(self.page.maxh);
ret=true;
break;
case 32:
if(self.opt.spacebarenabled){
(shift) ? self.doScrollBy(self.view.h):self.doScrollBy(-self.view.h);
ret=true;
}
break;
case 27:
if(self.zoomactive){
self.doZoom();
ret=true;
}
break;
}
if(ret) return self.cancelEvent(e);
}};
if(self.opt.enablekeyboard) self.bind(document,(cap.isopera&&!cap.isopera12)?"keypress":"keydown",self.onkeypress);
self.bind(window,'resize',self.lazyResize);
self.bind(window,'orientationchange',self.lazyResize);
self.bind(window,"load",self.lazyResize);
if(cap.ischrome&&!self.ispage&&!self.haswrapper){
var tmp=self.win.attr("style");
var ww=parseFloat(self.win.css("width"))+1;
self.win.css('width',ww);
self.synched("chromefix",function(){self.win.attr("style",tmp)});
}
self.onAttributeChange=function(e){
self.lazyResize(250);
}
if(!self.ispage&&!self.haswrapper){
if(clsMutationObserver!==false){
self.observer=new clsMutationObserver(function(mutations){
mutations.forEach(self.onAttributeChange);
});
self.observer.observe(self.win[0],{childList: true, characterData: false, attributes: true, subtree: false});
self.observerremover=new clsMutationObserver(function(mutations){
mutations.forEach(function(mo){
if(mo.removedNodes.length>0){
for (var dd in mo.removedNodes){
if(mo.removedNodes[dd]==self.win[0]) return self.remove();
}}
});
});
self.observerremover.observe(self.win[0].parentNode,{childList: true, characterData: false, attributes: false, subtree: false});
}else{
self.bind(self.win,(cap.isie&&!cap.isie9)?"propertychange":"DOMAttrModified",self.onAttributeChange);
if(cap.isie9) self.win[0].attachEvent("onpropertychange",self.onAttributeChange);
self.bind(self.win,"DOMNodeRemoved",function(e){
if(e.target==self.win[0]) self.remove();
});
}}
if(!self.ispage&&self.opt.boxzoom) self.bind(window,"resize",self.resizeZoom);
if(self.istextarea) self.bind(self.win,"mouseup",self.lazyResize);
self.checkrtlmode=true;
self.lazyResize(30);
}
if(this.doc[0].nodeName=='IFRAME'){
function oniframeload(e){
self.iframexd=false;
try {
var doc='contentDocument' in this ? this.contentDocument:this.contentWindow.document;
var a=doc.domain;
} catch(e){self.iframexd=true;doc=false};
if(self.iframexd){
if("console" in window) console.log('NiceScroll error: policy restriced iframe');
return true;
}
self.forcescreen=true;
if(self.isiframe){
self.iframe={
"doc":$(doc),
"html":self.doc.contents().find('html')[0],
"body":self.doc.contents().find('body')[0]
};
self.getContentSize=function(){
return {
w:Math.max(self.iframe.html.scrollWidth,self.iframe.body.scrollWidth),
h:Math.max(self.iframe.html.scrollHeight,self.iframe.body.scrollHeight)
}}
self.docscroll=$(self.iframe.body);//$(this.contentWindow);
}
if(!cap.isios&&self.opt.iframeautoresize&&!self.isiframe){
self.win.scrollTop(0);
self.doc.height("");
var hh=Math.max(doc.getElementsByTagName('html')[0].scrollHeight,doc.body.scrollHeight);
self.doc.height(hh);
}
self.lazyResize(30);
if(cap.isie7) self.css($(self.iframe.html),{'overflow-y':'hidden'});
self.css($(self.iframe.body),{'overflow-y':'hidden'});
if(cap.isios&&self.haswrapper){
self.css($(doc.body),{'-webkit-transform':'translate3d(0,0,0)'});
console.log(1);
}
if('contentWindow' in this){
self.bind(this.contentWindow,"scroll",self.onscroll);
}else{
self.bind(doc,"scroll",self.onscroll);
}
if(self.opt.enablemousewheel){
self.bind(doc,"mousewheel",self.onmousewheel);
}
if(self.opt.enablekeyboard) self.bind(doc,(cap.isopera)?"keypress":"keydown",self.onkeypress);
if(cap.cantouch||self.opt.touchbehavior){
self.bind(doc,"mousedown",self.ontouchstart);
self.bind(doc,"mousemove",function(e){self.ontouchmove(e,true)});
if(self.opt.grabcursorenabled&&cap.cursorgrabvalue) self.css($(doc.body),{'cursor':cap.cursorgrabvalue});
}
self.bind(doc,"mouseup",self.ontouchend);
if(self.zoom){
if(self.opt.dblclickzoom) self.bind(doc,'dblclick',self.doZoom);
if(self.ongesturezoom) self.bind(doc,"gestureend",self.ongesturezoom);
}};
if(this.doc[0].readyState&&this.doc[0].readyState=="complete"){
setTimeout(function(){oniframeload.call(self.doc[0],false)},500);
}
self.bind(this.doc,"load",oniframeload);
}};
this.showCursor=function(py,px){
if(self.cursortimeout){
clearTimeout(self.cursortimeout);
self.cursortimeout=0;
}
if(!self.rail) return;
if(self.autohidedom){
self.autohidedom.stop().css({opacity:self.opt.cursoropacitymax});
self.cursoractive=true;
}
if(!self.rail.drag||self.rail.drag.pt!=1){
if((typeof py!="undefined")&&(py!==false)){
self.scroll.y=Math.round(py * 1/self.scrollratio.y);
}
if(typeof px!="undefined"){
self.scroll.x=Math.round(px * 1/self.scrollratio.x);
}}
self.cursor.css({height:self.cursorheight,top:self.scroll.y});
if(self.cursorh){
(!self.rail.align&&self.rail.visibility) ? self.cursorh.css({width:self.cursorwidth,left:self.scroll.x+self.rail.width}):self.cursorh.css({width:self.cursorwidth,left:self.scroll.x});
self.cursoractive=true;
}
if(self.zoom) self.zoom.stop().css({opacity:self.opt.cursoropacitymax});
};
this.hideCursor=function(tm){
if(self.cursortimeout) return;
if(!self.rail) return;
if(!self.autohidedom) return;
self.cursortimeout=setTimeout(function(){
if(!self.rail.active||!self.showonmouseevent){
self.autohidedom.stop().animate({opacity:self.opt.cursoropacitymin});
if(self.zoom) self.zoom.stop().animate({opacity:self.opt.cursoropacitymin});
self.cursoractive=false;
}
self.cursortimeout=0;
},tm||self.opt.hidecursordelay);
};
this.noticeCursor=function(tm,py,px){
self.showCursor(py,px);
if(!self.rail.active) self.hideCursor(tm);
};
this.getContentSize =
(self.ispage) ?
function(){
return {
w:Math.max(document.body.scrollWidth,document.documentElement.scrollWidth),
h:Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)
}}
: (self.haswrapper) ?
function(){
return {
w:self.doc.outerWidth()+parseInt(self.win.css('paddingLeft'))+parseInt(self.win.css('paddingRight')),
h:self.doc.outerHeight()+parseInt(self.win.css('paddingTop'))+parseInt(self.win.css('paddingBottom'))
}}
: function(){
return {
w:self.docscroll[0].scrollWidth,
h:self.docscroll[0].scrollHeight
}};
this.onResize=function(e,page){
if(!self.win) return false;
if(!self.haswrapper&&!self.ispage){
if(self.win.css('display')=='none'){
if(self.visibility) self.hideRail().hideRailHr();
return false;
}else{
if(!self.hidden&&!self.visibility) self.showRail().showRailHr();
}}
var premaxh=self.page.maxh;
var premaxw=self.page.maxw;
var preview={h:self.view.h,w:self.view.w};
self.view={
w:(self.ispage) ? self.win.width():parseInt(self.win[0].clientWidth),
h:(self.ispage) ? self.win.height():parseInt(self.win[0].clientHeight)
};
self.page=(page) ? page:self.getContentSize();
self.page.maxh=Math.max(0,self.page.h - self.view.h);
self.page.maxw=Math.max(0,self.page.w - self.view.w);
if((self.page.maxh==premaxh)&&(self.page.maxw==premaxw)&&(self.view.w==preview.w)){
if(!self.ispage){
var pos=self.win.offset();
if(self.lastposition){
var lst=self.lastposition;
if((lst.top==pos.top)&&(lst.left==pos.left)) return self;
}
self.lastposition=pos;
}else{
return self;
}}
if(self.page.maxh==0){
self.hideRail();
self.scrollvaluemax=0;
self.scroll.y=0;
self.scrollratio.y=0;
self.cursorheight=0;
self.setScrollTop(0);
self.rail.scrollable=false;
}else{
self.rail.scrollable=true;
}
if(self.page.maxw==0){
self.hideRailHr();
self.scrollvaluemaxw=0;
self.scroll.x=0;
self.scrollratio.x=0;
self.cursorwidth=0;
self.setScrollLeft(0);
self.railh.scrollable=false;
}else{
self.railh.scrollable=true;
}
self.locked=(self.page.maxh==0)&&(self.page.maxw==0);
if(self.locked){
if(!self.ispage) self.updateScrollBar(self.view);
return false;
}
if(!self.hidden&&!self.visibility){
self.showRail().showRailHr();
}
else if(!self.hidden&&!self.railh.visibility) self.showRailHr();
if(self.istextarea&&self.win.css('resize')&&self.win.css('resize')!='none') self.view.h-=20;
self.cursorheight=Math.min(self.view.h,Math.round(self.view.h * (self.view.h / self.page.h)));
self.cursorheight=(self.opt.cursorfixedheight) ? self.opt.cursorfixedheight:Math.max(self.opt.cursorminheight,self.cursorheight);
self.cursorwidth=Math.min(self.view.w,Math.round(self.view.w * (self.view.w / self.page.w)));
self.cursorwidth=(self.opt.cursorfixedheight) ? self.opt.cursorfixedheight:Math.max(self.opt.cursorminheight,self.cursorwidth);
self.scrollvaluemax=self.view.h-self.cursorheight-self.cursor.hborder;
if(self.railh){
self.railh.width=(self.page.maxh>0) ? (self.view.w-self.rail.width):self.view.w;
self.scrollvaluemaxw=self.railh.width-self.cursorwidth-self.cursorh.wborder;
}
if(self.checkrtlmode&&self.railh){
self.checkrtlmode=false;
if(self.opt.rtlmode&&self.scroll.x==0) self.setScrollLeft(self.page.maxw);
}
if(!self.ispage) self.updateScrollBar(self.view);
self.scrollratio={
x:(self.page.maxw/self.scrollvaluemaxw),
y:(self.page.maxh/self.scrollvaluemax)
};
var sy=self.getScrollTop();
if(sy>self.page.maxh){
self.doScrollTop(self.page.maxh);
}else{
self.scroll.y=Math.round(self.getScrollTop() * (1/self.scrollratio.y));
self.scroll.x=Math.round(self.getScrollLeft() * (1/self.scrollratio.x));
if(self.cursoractive) self.noticeCursor();
}
if(self.scroll.y&&(self.getScrollTop()==0)) self.doScrollTo(Math.floor(self.scroll.y*self.scrollratio.y));
return self;
};
this.resize=self.onResize;
this.lazyResize=function(tm){
tm=(isNaN(tm)) ? 30:tm;
self.delayed('resize',self.resize,tm);
return self;
}
function _modernWheelEvent(dom,name,fn,bubble){
self._bind(dom,name,function(e){
var  e=(e) ? e:window.event;
var event={
original: e,
target: e.target||e.srcElement,
type: "wheel",
deltaMode: e.type=="MozMousePixelScroll" ? 0:1,
deltaX: 0,
deltaZ: 0,
preventDefault: function(){
e.preventDefault ? e.preventDefault():e.returnValue=false;
return false;
},
stopImmediatePropagation: function(){
(e.stopImmediatePropagation) ? e.stopImmediatePropagation():e.cancelBubble=true;
}};
if(name=="mousewheel"){
event.deltaY=- 1/40 * e.wheelDelta;
e.wheelDeltaX&&(event.deltaX=- 1/40 * e.wheelDeltaX);
}else{
event.deltaY=e.detail;
}
return fn.call(dom,event);
},bubble);
};
this._bind=function(el,name,fn,bubble){
self.events.push({e:el,n:name,f:fn,b:bubble,q:false});
if(el.addEventListener){
el.addEventListener(name,fn,bubble||false);
}
else if(el.attachEvent){
el.attachEvent("on"+name,fn);
}else{
el["on"+name]=fn;
}};
this.jqbind=function(dom,name,fn){
self.events.push({e:dom,n:name,f:fn,q:true});
$(dom).bind(name,fn);
}
this.bind=function(dom,name,fn,bubble){
var el=("jquery" in dom) ? dom[0]:dom;
if(name=='mousewheel'){
if("onwheel" in self.win){
self._bind(el,"wheel",fn,bubble||false);
}else{
var wname=(typeof document.onmousewheel!="undefined") ? "mousewheel":"DOMMouseScroll";
_modernWheelEvent(el,wname,fn,bubble||false);
if(wname=="DOMMouseScroll") _modernWheelEvent(el,"MozMousePixelScroll",fn,bubble||false);
}}
else if(el.addEventListener){
if(cap.cantouch&&/mouseup|mousedown|mousemove/.test(name)){
var tt=(name=='mousedown')?'touchstart':(name=='mouseup')?'touchend':'touchmove';
self._bind(el,tt,function(e){
if(e.touches){
if(e.touches.length<2){var ev=(e.touches.length)?e.touches[0]:e;ev.original=e;fn.call(this,ev);}}
else if(e.changedTouches){var ev=e.changedTouches[0];ev.original=e;fn.call(this,ev);}},bubble||false);
}
self._bind(el,name,fn,bubble||false);
if(cap.cantouch&&name=="mouseup") self._bind(el,"touchcancel",fn,bubble||false);
}else{
self._bind(el,name,function(e){
e=e||window.event||false;
if(e){
if(e.srcElement) e.target=e.srcElement;
}
if(!("pageY" in e)){
e.pageX=e.clientX + document.documentElement.scrollLeft;
e.pageY=e.clientY + document.documentElement.scrollTop;
}
return ((fn.call(el,e)===false)||bubble===false) ? self.cancelEvent(e):true;
});
}};
this._unbind=function(el,name,fn,bub){
if(el.removeEventListener){
el.removeEventListener(name,fn,bub);
}
else if(el.detachEvent){
el.detachEvent('on'+name,fn);
}else{
el['on'+name]=false;
}};
this.unbindAll=function(){
for(var a=0;a<self.events.length;a++){
var r=self.events[a];
(r.q) ? r.e.unbind(r.n,r.f):self._unbind(r.e,r.n,r.f,r.b);
}};
this.cancelEvent=function(e){
var e=(e.original) ? e.original:(e) ? e:window.event||false;
if(!e) return false;
if(e.preventDefault) e.preventDefault();
if(e.stopPropagation) e.stopPropagation();
if(e.preventManipulation) e.preventManipulation();
e.cancelBubble=true;
e.cancel=true;
e.returnValue=false;
return false;
};
this.stopPropagation=function(e){
var e=(e.original) ? e.original:(e) ? e:window.event||false;
if(!e) return false;
if(e.stopPropagation) return e.stopPropagation();
if(e.cancelBubble) e.cancelBubble=true;
return false;
}
this.showRail=function(){
if((self.page.maxh!=0)&&(self.ispage||self.win.css('display')!='none')){
self.visibility=true;
self.rail.visibility=true;
self.rail.css('display','block');
}
return self;
};
this.showRailHr=function(){
if(!self.railh) return self;
if((self.page.maxw!=0)&&(self.ispage||self.win.css('display')!='none')){
self.railh.visibility=true;
self.railh.css('display','block');
}
return self;
};
this.hideRail=function(){
self.visibility=false;
self.rail.visibility=false;
self.rail.css('display','none');
return self;
};
this.hideRailHr=function(){
if(!self.railh) return self;
self.railh.visibility=false;
self.railh.css('display','none');
return self;
};
this.show=function(){
self.hidden=false;
self.locked=false;
return self.showRail().showRailHr();
};
this.hide=function(){
self.hidden=true;
self.locked=true;
return self.hideRail().hideRailHr();
};
this.toggle=function(){
return (self.hidden) ? self.show():self.hide();
};
this.remove=function(){
self.stop();
if(self.cursortimeout) clearTimeout(self.cursortimeout);
self.doZoomOut();
self.unbindAll();
if(cap.isie9) self.win[0].detachEvent("onpropertychange",self.onAttributeChange);
if(self.observer!==false) self.observer.disconnect();
if(self.observerremover!==false) self.observerremover.disconnect();
self.events=null;
if(self.cursor){
self.cursor.remove();
}
if(self.cursorh){
self.cursorh.remove();
}
if(self.rail){
self.rail.remove();
}
if(self.railh){
self.railh.remove();
}
if(self.zoom){
self.zoom.remove();
}
for(var a=0;a<self.saved.css.length;a++){
var d=self.saved.css[a];
d[0].css(d[1],(typeof d[2]=="undefined") ? '':d[2]);
}
self.saved=false;
self.me.data('__nicescroll','');
var lst=$.nicescroll;
lst.each(function(i){
if(!this) return;
if(this.id===self.id){
delete lst[i];
for(var b=++i;b<lst.length;b++,i++) lst[i]=lst[b];
lst.length--;
if(lst.length) delete lst[lst.length];
}});
for (var i in self){
self[i]=null;
delete self[i];
}
self=null;
};
this.scrollstart=function(fn){
this.onscrollstart=fn;
return self;
}
this.scrollend=function(fn){
this.onscrollend=fn;
return self;
}
this.scrollcancel=function(fn){
this.onscrollcancel=fn;
return self;
}
this.zoomin=function(fn){
this.onzoomin=fn;
return self;
}
this.zoomout=function(fn){
this.onzoomout=fn;
return self;
}
this.isScrollable=function(e){
var dom=(e.target) ? e.target:e;
if(dom.nodeName=='OPTION') return true;
while (dom&&(dom.nodeType==1)&&!(/BODY|HTML/.test(dom.nodeName))){
var dd=$(dom);
var ov=dd.css('overflowY')||dd.css('overflowX')||dd.css('overflow')||'';
if(/scroll|auto/.test(ov)) return (dom.clientHeight!=dom.scrollHeight);
dom=(dom.parentNode) ? dom.parentNode:false;
}
return false;
};
this.getViewport=function(me){
var dom=(me&&me.parentNode) ? me.parentNode:false;
while (dom&&(dom.nodeType==1)&&!(/BODY|HTML/.test(dom.nodeName))){
var dd=$(dom);
var ov=dd.css('overflowY')||dd.css('overflowX')||dd.css('overflow')||'';
if((/scroll|auto/.test(ov))&&(dom.clientHeight!=dom.scrollHeight)) return dd;
if(dd.getNiceScroll().length>0) return dd;
dom=(dom.parentNode) ? dom.parentNode:false;
}
return false;
};
function execScrollWheel(e,hr,chkscroll){
var px,py;
var rt=1;
if(e.deltaMode==0){
px=-Math.floor(e.deltaX*(self.opt.mousescrollstep/(18*3)));
py=-Math.floor(e.deltaY*(self.opt.mousescrollstep/(18*3)));
}
else if(e.deltaMode==1){
px=-Math.floor(e.deltaX*self.opt.mousescrollstep);
py=-Math.floor(e.deltaY*self.opt.mousescrollstep);
}
if(hr&&self.opt.oneaxismousemode&&(px==0)&&py){
px=py;
py=0;
}
if(px){
if(self.scrollmom){self.scrollmom.stop()}
self.lastdeltax+=px;
self.debounced("mousewheelx",function(){var dt=self.lastdeltax;self.lastdeltax=0;if(!self.rail.drag){self.doScrollLeftBy(dt)}},120);
}
if(py){
if(self.opt.nativeparentscrolling&&chkscroll&&!self.ispage&&!self.zoomactive){
if(py<0){
if(self.getScrollTop()>=self.page.maxh) return true;
}else{
if(self.getScrollTop()<=0) return true;
}}
if(self.scrollmom){self.scrollmom.stop()}
self.lastdeltay+=py;
self.debounced("mousewheely",function(){var dt=self.lastdeltay;self.lastdeltay=0;if(!self.rail.drag){self.doScrollBy(dt)}},120);
}
e.stopImmediatePropagation();
return e.preventDefault();
};
this.onmousewheel=function(e){
if(self.locked){
self.debounced("checkunlock",self.resize,250);
return true;
}
if(self.rail.drag) return self.cancelEvent(e);
if(self.opt.oneaxismousemode=="auto"&&e.deltaX!=0) self.opt.oneaxismousemode=false;
if(self.opt.oneaxismousemode&&e.deltaX==0){
if(!self.rail.scrollable){
if(self.railh&&self.railh.scrollable){
return self.onmousewheelhr(e);
}else{
return true;
}}
}
var nw=+(new Date());
var chk=false;
if(self.opt.preservenativescrolling&&((self.checkarea+600)<nw)){
self.nativescrollingarea=self.isScrollable(e);
chk=true;
}
self.checkarea=nw;
if(self.nativescrollingarea) return true;
var ret=execScrollWheel(e,false,chk);
if(ret) self.checkarea=0;
return ret;
};
this.onmousewheelhr=function(e){
if(self.locked||!self.railh.scrollable) return true;
if(self.rail.drag) return self.cancelEvent(e);
var nw=+(new Date());
var chk=false;
if(self.opt.preservenativescrolling&&((self.checkarea+600)<nw)){
self.nativescrollingarea=self.isScrollable(e);
chk=true;
}
self.checkarea=nw;
if(self.nativescrollingarea) return true;
if(self.locked) return self.cancelEvent(e);
return execScrollWheel(e,true,chk);
};
this.stop=function(){
self.cancelScroll();
if(self.scrollmon) self.scrollmon.stop();
self.cursorfreezed=false;
self.scroll.y=Math.round(self.getScrollTop() * (1/self.scrollratio.y));
self.noticeCursor();
return self;
};
this.getTransitionSpeed=function(dif){
var sp=Math.round(self.opt.scrollspeed*10);
var ex=Math.min(sp,Math.round((dif / 20) * self.opt.scrollspeed));
return (ex>20) ? ex:0;
}
if(!self.opt.smoothscroll){
this.doScrollLeft=function(x,spd){
var y=self.getScrollTop();
self.doScrollPos(x,y,spd);
}
this.doScrollTop=function(y,spd){
var x=self.getScrollLeft();
self.doScrollPos(x,y,spd);
}
this.doScrollPos=function(x,y,spd){
var nx=(x>self.page.maxw) ? self.page.maxw:x;
if(nx<0) nx=0;
var ny=(y>self.page.maxh) ? self.page.maxh:y;
if(ny<0) ny=0;
self.synched('scroll',function(){
self.setScrollTop(ny);
self.setScrollLeft(nx);
});
}
this.cancelScroll=function(){};}
else if(self.ishwscroll&&cap.hastransition&&self.opt.usetransition){
this.prepareTransition=function(dif,istime){
var ex=(istime) ? ((dif>20)?dif:0):self.getTransitionSpeed(dif);
var trans=(ex) ? cap.prefixstyle+'transform '+ex+'ms ease-out':'';
if(!self.lasttransitionstyle||self.lasttransitionstyle!=trans){
self.lasttransitionstyle=trans;
self.doc.css(cap.transitionstyle,trans);
}
return ex;
};
this.doScrollLeft=function(x,spd){
var y=(self.scrollrunning) ? self.newscrolly:self.getScrollTop();
self.doScrollPos(x,y,spd);
}
this.doScrollTop=function(y,spd){
var x=(self.scrollrunning) ? self.newscrollx:self.getScrollLeft();
self.doScrollPos(x,y,spd);
}
this.doScrollPos=function(x,y,spd){
var py=self.getScrollTop();
var px=self.getScrollLeft();
if(((self.newscrolly-py)*(y-py)<0)||((self.newscrollx-px)*(x-px)<0)) self.cancelScroll();
if(self.opt.bouncescroll==false){
if(y<0) y=0;
else if(y>self.page.maxh) y=self.page.maxh;
if(x<0) x=0;
else if(x>self.page.maxw) x=self.page.maxw;
}
if(self.scrollrunning&&x==self.newscrollx&&y==self.newscrolly) return false;
self.newscrolly=y;
self.newscrollx=x;
self.newscrollspeed=spd||false;
if(self.timer) return false;
self.timer=setTimeout(function(){
var top=self.getScrollTop();
var lft=self.getScrollLeft();
var dst={};
dst.x=x-lft;
dst.y=y-top;
dst.px=lft;
dst.py=top;
var dd=Math.round(Math.sqrt(Math.pow(dst.x,2)+Math.pow(dst.y,2)));
var ms=(self.newscrollspeed&&self.newscrollspeed>1) ? self.newscrollspeed:self.getTransitionSpeed(dd);
if(self.newscrollspeed&&self.newscrollspeed<=1) ms*=self.newscrollspeed;
self.prepareTransition(ms,true);
if(self.timerscroll&&self.timerscroll.tm) clearInterval(self.timerscroll.tm);
if(ms>0){
if(!self.scrollrunning&&self.onscrollstart){
var info={"type":"scrollstart","current":{"x":lft,"y":top},"request":{"x":x,"y":y},"end":{"x":self.newscrollx,"y":self.newscrolly},"speed":ms};
self.onscrollstart.call(self,info);
}
if(cap.transitionend){
if(!self.scrollendtrapped){
self.scrollendtrapped=true;
self.bind(self.doc,cap.transitionend,self.onScrollEnd,false);
}}else{
if(self.scrollendtrapped) clearTimeout(self.scrollendtrapped);
self.scrollendtrapped=setTimeout(self.onScrollEnd,ms);
}
var py=top;
var px=lft;
self.timerscroll={
bz: new BezierClass(py,self.newscrolly,ms,0,0,0.58,1),
bh: new BezierClass(px,self.newscrollx,ms,0,0,0.58,1)
};
if(!self.cursorfreezed) self.timerscroll.tm=setInterval(function(){self.showCursor(self.getScrollTop(),self.getScrollLeft())},60);
}
self.synched("doScroll-set",function(){
self.timer=0;
if(self.scrollendtrapped) self.scrollrunning=true;
self.setScrollTop(self.newscrolly);
self.setScrollLeft(self.newscrollx);
if(!self.scrollendtrapped) self.onScrollEnd();
});
},50);
};
this.cancelScroll=function(){
if(!self.scrollendtrapped) return true;
var py=self.getScrollTop();
var px=self.getScrollLeft();
self.scrollrunning=false;
if(!cap.transitionend) clearTimeout(cap.transitionend);
self.scrollendtrapped=false;
self._unbind(self.doc,cap.transitionend,self.onScrollEnd);
self.prepareTransition(0);
self.setScrollTop(py);
if(self.railh) self.setScrollLeft(px);
if(self.timerscroll&&self.timerscroll.tm) clearInterval(self.timerscroll.tm);
self.timerscroll=false;
self.cursorfreezed=false;
self.showCursor(py,px);
return self;
};
this.onScrollEnd=function(){
if(self.scrollendtrapped) self._unbind(self.doc,cap.transitionend,self.onScrollEnd);
self.scrollendtrapped=false;
self.prepareTransition(0);
if(self.timerscroll&&self.timerscroll.tm) clearInterval(self.timerscroll.tm);
self.timerscroll=false;
var py=self.getScrollTop();
var px=self.getScrollLeft();
self.setScrollTop(py);
if(self.railh) self.setScrollLeft(px);
self.noticeCursor(false,py,px);
self.cursorfreezed=false;
if(py<0) py=0
else if(py>self.page.maxh) py=self.page.maxh;
if(px<0) px=0
else if(px>self.page.maxw) px=self.page.maxw;
if((py!=self.newscrolly)||(px!=self.newscrollx)) return self.doScrollPos(px,py,self.opt.snapbackspeed);
if(self.onscrollend&&self.scrollrunning){
var info={"type":"scrollend","current":{"x":px,"y":py},"end":{"x":self.newscrollx,"y":self.newscrolly}};
self.onscrollend.call(self,info);
}
self.scrollrunning=false;
};}else{
this.doScrollLeft=function(x,spd){
var y=(self.scrollrunning) ? self.newscrolly:self.getScrollTop();
self.doScrollPos(x,y,spd);
}
this.doScrollTop=function(y,spd){
var x=(self.scrollrunning) ? self.newscrollx:self.getScrollLeft();
self.doScrollPos(x,y,spd);
}
this.doScrollPos=function(x,y,spd){
var y=((typeof y=="undefined")||(y===false)) ? self.getScrollTop(true):y;
if((self.timer)&&(self.newscrolly==y)&&(self.newscrollx==x)) return true;
if(self.timer) clearAnimationFrame(self.timer);
self.timer=0;
var py=self.getScrollTop();
var px=self.getScrollLeft();
if(((self.newscrolly-py)*(y-py)<0)||((self.newscrollx-px)*(x-px)<0)) self.cancelScroll();
self.newscrolly=y;
self.newscrollx=x;
if(!self.bouncescroll||!self.rail.visibility){
if(self.newscrolly<0){
self.newscrolly=0;
}
else if(self.newscrolly>self.page.maxh){
self.newscrolly=self.page.maxh;
}}
if(!self.bouncescroll||!self.railh.visibility){
if(self.newscrollx<0){
self.newscrollx=0;
}
else if(self.newscrollx>self.page.maxw){
self.newscrollx=self.page.maxw;
}}
self.dst={};
self.dst.x=x-px;
self.dst.y=y-py;
self.dst.px=px;
self.dst.py=py;
var dst=Math.round(Math.sqrt(Math.pow(self.dst.x,2)+Math.pow(self.dst.y,2)));
self.dst.ax=self.dst.x / dst;
self.dst.ay=self.dst.y / dst;
var pa=0;
var pe=dst;
if(self.dst.x==0){
pa=py;
pe=y;
self.dst.ay=1;
self.dst.py=0;
}else if(self.dst.y==0){
pa=px;
pe=x;
self.dst.ax=1;
self.dst.px=0;
}
var ms=self.getTransitionSpeed(dst);
if(spd&&spd<=1) ms*=spd;
if(ms>0){
self.bzscroll=(self.bzscroll) ? self.bzscroll.update(pe,ms):new BezierClass(pa,pe,ms,0,1,0,1);
}else{
self.bzscroll=false;
}
if(self.timer) return;
if((py==self.page.maxh&&y>=self.page.maxh)||(px==self.page.maxw&&x>=self.page.maxw)) self.checkContentSize();
var sync=1;
function scrolling(){
if(self.cancelAnimationFrame) return true;
self.scrollrunning=true;
sync=1-sync;
if(sync) return (self.timer=setAnimationFrame(scrolling)||1);
var done=0;
var sc=sy=self.getScrollTop();
if(self.dst.ay){
sc=(self.bzscroll) ? self.dst.py + (self.bzscroll.getNow()*self.dst.ay):self.newscrolly;
var dr=sc-sy;
if((dr<0&&sc<self.newscrolly)||(dr>0&&sc>self.newscrolly)) sc=self.newscrolly;
self.setScrollTop(sc);
if(sc==self.newscrolly) done=1;
}else{
done=1;
}
var scx=sx=self.getScrollLeft();
if(self.dst.ax){
scx=(self.bzscroll) ? self.dst.px + (self.bzscroll.getNow()*self.dst.ax):self.newscrollx;
var dr=scx-sx;
if((dr<0&&scx<self.newscrollx)||(dr>0&&scx>self.newscrollx)) scx=self.newscrollx;
self.setScrollLeft(scx);
if(scx==self.newscrollx) done+=1;
}else{
done+=1;
}
if(done==2){
self.timer=0;
self.cursorfreezed=false;
self.bzscroll=false;
self.scrollrunning=false;
if(sc<0) sc=0;
else if(sc>self.page.maxh) sc=self.page.maxh;
if(scx<0) scx=0;
else if(scx>self.page.maxw) scx=self.page.maxw;
if((scx!=self.newscrollx)||(sc!=self.newscrolly)) self.doScrollPos(scx,sc);
else {
if(self.onscrollend){
var info={"type":"scrollend","current":{"x":sx,"y":sy},"end":{"x":self.newscrollx,"y":self.newscrolly}};
self.onscrollend.call(self,info);
}}
}else{
self.timer=setAnimationFrame(scrolling)||1;
}};
self.cancelAnimationFrame=false;
self.timer=1;
if(self.onscrollstart&&!self.scrollrunning){
var info={"type":"scrollstart","current":{"x":px,"y":py},"request":{"x":x,"y":y},"end":{"x":self.newscrollx,"y":self.newscrolly},"speed":ms};
self.onscrollstart.call(self,info);
}
scrolling();
if((py==self.page.maxh&&y>=py)||(px==self.page.maxw&&x>=px)) self.checkContentSize();
self.noticeCursor();
};
this.cancelScroll=function(){
if(self.timer) clearAnimationFrame(self.timer);
self.timer=0;
self.bzscroll=false;
self.scrollrunning=false;
return self;
};}
this.doScrollBy=function(stp,relative){
var ny=0;
if(relative){
ny=Math.floor((self.scroll.y-stp)*self.scrollratio.y)
}else{
var sy=(self.timer) ? self.newscrolly:self.getScrollTop(true);
ny=sy-stp;
}
if(self.bouncescroll){
var haf=Math.round(self.view.h/2);
if(ny<-haf) ny=-haf
else if(ny>(self.page.maxh+haf)) ny=(self.page.maxh+haf);
}
self.cursorfreezed=false;
py=self.getScrollTop(true);
if(ny<0&&py<=0) return self.noticeCursor();
else if(ny>self.page.maxh&&py>=self.page.maxh){
self.checkContentSize();
return self.noticeCursor();
}
self.doScrollTop(ny);
};
this.doScrollLeftBy=function(stp,relative){
var nx=0;
if(relative){
nx=Math.floor((self.scroll.x-stp)*self.scrollratio.x)
}else{
var sx=(self.timer) ? self.newscrollx:self.getScrollLeft(true);
nx=sx-stp;
}
if(self.bouncescroll){
var haf=Math.round(self.view.w/2);
if(nx<-haf) nx=-haf
else if(nx>(self.page.maxw+haf)) nx=(self.page.maxw+haf);
}
self.cursorfreezed=false;
px=self.getScrollLeft(true);
if(nx<0&&px<=0) return self.noticeCursor();
else if(nx>self.page.maxw&&px>=self.page.maxw) return self.noticeCursor();
self.doScrollLeft(nx);
};
this.doScrollTo=function(pos,relative){
var ny=(relative) ? Math.round(pos*self.scrollratio.y):pos;
if(ny<0) ny=0
else if(ny>self.page.maxh) ny=self.page.maxh;
self.cursorfreezed=false;
self.doScrollTop(pos);
};
this.checkContentSize=function(){
var pg=self.getContentSize();
if((pg.h!=self.page.h)||(pg.w!=self.page.w)) self.resize(false,pg);
};
self.onscroll=function(e){
if(self.rail.drag) return;
if(!self.cursorfreezed){
self.synched('scroll',function(){
self.scroll.y=Math.round(self.getScrollTop() * (1/self.scrollratio.y));
if(self.railh) self.scroll.x=Math.round(self.getScrollLeft() * (1/self.scrollratio.x));
self.noticeCursor();
});
}};
self.bind(self.docscroll,"scroll",self.onscroll);
this.doZoomIn=function(e){
if(self.zoomactive) return;
self.zoomactive=true;
self.zoomrestore={
style:{}};
var lst=['position','top','left','zIndex','backgroundColor','marginTop','marginBottom','marginLeft','marginRight'];
var win=self.win[0].style;
for(var a in lst){
var pp=lst[a];
self.zoomrestore.style[pp]=(typeof win[pp]!="undefined") ? win[pp]:'';
}
self.zoomrestore.style.width=self.win.css('width');
self.zoomrestore.style.height=self.win.css('height');
self.zoomrestore.padding={
w:self.win.outerWidth()-self.win.width(),
h:self.win.outerHeight()-self.win.height()
};
if(cap.isios4){
self.zoomrestore.scrollTop=$(window).scrollTop();
$(window).scrollTop(0);
}
self.win.css({
"position":(cap.isios4)?"absolute":"fixed",
"top":0,
"left":0,
"z-index":globalmaxzindex+100,
"margin":"0px"
});
var bkg=self.win.css("backgroundColor");
if(bkg==""||/transparent|rgba\(0, 0, 0, 0\)|rgba\(0,0,0,0\)/.test(bkg)) self.win.css("backgroundColor","#fff");
self.rail.css({"z-index":globalmaxzindex+101});
self.zoom.css({"z-index":globalmaxzindex+102});
self.zoom.css('backgroundPosition','0px -18px');
self.resizeZoom();
if(self.onzoomin) self.onzoomin.call(self);
return self.cancelEvent(e);
};
this.doZoomOut=function(e){
if(!self.zoomactive) return;
self.zoomactive=false;
self.win.css("margin","");
self.win.css(self.zoomrestore.style);
if(cap.isios4){
$(window).scrollTop(self.zoomrestore.scrollTop);
}
self.rail.css({"z-index":self.zindex});
self.zoom.css({"z-index":self.zindex});
self.zoomrestore=false;
self.zoom.css('backgroundPosition','0px 0px');
self.onResize();
if(self.onzoomout) self.onzoomout.call(self);
return self.cancelEvent(e);
};
this.doZoom=function(e){
return (self.zoomactive) ? self.doZoomOut(e):self.doZoomIn(e);
};
this.resizeZoom=function(){
if(!self.zoomactive) return;
var py=self.getScrollTop();
self.win.css({
width:$(window).width()-self.zoomrestore.padding.w+"px",
height:$(window).height()-self.zoomrestore.padding.h+"px"
});
self.onResize();
self.setScrollTop(Math.min(self.page.maxh,py));
};
this.init();
$.nicescroll.push(this);
};
var ScrollMomentumClass2D=function(nc){
var self=this;
this.nc=nc;
this.lastx=0;
this.lasty=0;
this.speedx=0;
this.speedy=0;
this.lasttime=0;
this.steptime=0;
this.snapx=false;
this.snapy=false;
this.demulx=0;
this.demuly=0;
this.lastscrollx=-1;
this.lastscrolly=-1;
this.chkx=0;
this.chky=0;
this.timer=0;
this.time=function(){
return +new Date();
};
this.reset=function(px,py){
self.stop();
var now=self.time();
self.steptime=0;
self.lasttime=now;
self.speedx=0;
self.speedy=0;
self.lastx=px;
self.lasty=py;
self.lastscrollx=-1;
self.lastscrolly=-1;
};
this.update=function(px,py){
var now=self.time();
self.steptime=now - self.lasttime;
self.lasttime=now;
var dy=py - self.lasty;
var dx=px - self.lastx;
var sy=self.nc.getScrollTop();
var sx=self.nc.getScrollLeft();
var newy=sy + dy;
var newx=sx + dx;
self.snapx=(newx<0)||(newx>self.nc.page.maxw);
self.snapy=(newy<0)||(newy>self.nc.page.maxh);
self.speedx=dx;
self.speedy=dy;
self.lastx=px;
self.lasty=py;
};
this.stop=function(){
self.nc.unsynched("domomentum2d");
if(self.timer) clearTimeout(self.timer);
self.timer=0;
self.lastscrollx=-1;
self.lastscrolly=-1;
};
this.doSnapy=function(nx,ny){
var snap=false;
if(ny<0){
ny=0;
snap=true;
}
else if(ny>self.nc.page.maxh){
ny=self.nc.page.maxh;
snap=true;
}
if(nx<0){
nx=0;
snap=true;
}
else if(nx>self.nc.page.maxw){
nx=self.nc.page.maxw;
snap=true;
}
if(snap) self.nc.doScrollPos(nx,ny,self.nc.opt.snapbackspeed);
};
this.doMomentum=function(gp){
var t=self.time();
var l=(gp) ? t+gp:self.lasttime;
var sl=self.nc.getScrollLeft();
var st=self.nc.getScrollTop();
var pageh=self.nc.page.maxh;
var pagew=self.nc.page.maxw;
self.speedx=(pagew>0) ? Math.min(60,self.speedx):0;
self.speedy=(pageh>0) ? Math.min(60,self.speedy):0;
var chk=l&&(t - l) <=60;
if((st<0)||(st>pageh)||(sl<0)||(sl>pagew)) chk=false;
var sy=(self.speedy&&chk) ? self.speedy:false;
var sx=(self.speedx&&chk) ? self.speedx:false;
if(sy||sx){
var tm=Math.max(16,self.steptime);
if(tm>50){
var xm=tm/50;
self.speedx*=xm;
self.speedy*=xm;
tm=50;
}
self.demulxy=0;
self.lastscrollx=self.nc.getScrollLeft();
self.chkx=self.lastscrollx;
self.lastscrolly=self.nc.getScrollTop();
self.chky=self.lastscrolly;
var nx=self.lastscrollx;
var ny=self.lastscrolly;
var onscroll=function(){
var df=((self.time()-t)>600) ? 0.04:0.02;
if(self.speedx){
nx=Math.floor(self.lastscrollx - (self.speedx*(1-self.demulxy)));
self.lastscrollx=nx;
if((nx<0)||(nx>pagew)) df=0.10;
}
if(self.speedy){
ny=Math.floor(self.lastscrolly - (self.speedy*(1-self.demulxy)));
self.lastscrolly=ny;
if((ny<0)||(ny>pageh)) df=0.10;
}
self.demulxy=Math.min(1,self.demulxy+df);
self.nc.synched("domomentum2d",function(){
if(self.speedx){
var scx=self.nc.getScrollLeft();
if(scx!=self.chkx) self.stop();
self.chkx=nx;
self.nc.setScrollLeft(nx);
}
if(self.speedy){
var scy=self.nc.getScrollTop();
if(scy!=self.chky) self.stop();
self.chky=ny;
self.nc.setScrollTop(ny);
}
if(!self.timer){
self.nc.hideCursor();
self.doSnapy(nx,ny);
}});
if(self.demulxy<1){
self.timer=setTimeout(onscroll,tm);
}else{
self.stop();
self.nc.hideCursor();
self.doSnapy(nx,ny);
}};
onscroll();
}else{
self.doSnapy(self.nc.getScrollLeft(),self.nc.getScrollTop());
}}
};
var _scrollTop=jQuery.fn.scrollTop;
jQuery.cssHooks["pageYOffset"]={
get: function(elem,computed,extra){
var nice=$.data(elem,'__nicescroll')||false;
return (nice&&nice.ishwscroll) ? nice.getScrollTop():_scrollTop.call(elem);
},
set: function(elem,value){
var nice=$.data(elem,'__nicescroll')||false;
(nice&&nice.ishwscroll) ? nice.setScrollTop(parseInt(value)):_scrollTop.call(elem,value);
return this;
}};
/*
$.fx.step["scrollTop"]=function(fx){
$.cssHooks["scrollTop"].set(fx.elem, fx.now + fx.unit);
};
*/
jQuery.fn.scrollTop=function(value){
if(typeof value=="undefined"){
var nice=(this[0]) ? $.data(this[0],'__nicescroll')||false:false;
return (nice&&nice.ishwscroll) ? nice.getScrollTop():_scrollTop.call(this);
}else{
return this.each(function(){
var nice=$.data(this,'__nicescroll')||false;
(nice&&nice.ishwscroll) ? nice.setScrollTop(parseInt(value)):_scrollTop.call($(this),value);
});
}}
var _scrollLeft=jQuery.fn.scrollLeft;
$.cssHooks.pageXOffset={
get: function(elem,computed,extra){
var nice=$.data(elem,'__nicescroll')||false;
return (nice&&nice.ishwscroll) ? nice.getScrollLeft():_scrollLeft.call(elem);
},
set: function(elem,value){
var nice=$.data(elem,'__nicescroll')||false;
(nice&&nice.ishwscroll) ? nice.setScrollLeft(parseInt(value)):_scrollLeft.call(elem,value);
return this;
}};
/*
$.fx.step["scrollLeft"]=function(fx){
$.cssHooks["scrollLeft"].set(fx.elem, fx.now + fx.unit);
};
*/
jQuery.fn.scrollLeft=function(value){
if(typeof value=="undefined"){
var nice=(this[0]) ? $.data(this[0],'__nicescroll')||false:false;
return (nice&&nice.ishwscroll) ? nice.getScrollLeft():_scrollLeft.call(this);
}else{
return this.each(function(){
var nice=$.data(this,'__nicescroll')||false;
(nice&&nice.ishwscroll) ? nice.setScrollLeft(parseInt(value)):_scrollLeft.call($(this),value);
});
}}
var NiceScrollArray=function(doms){
var self=this;
this.length=0;
this.name="nicescrollarray";
this.each=function(fn){
for(var a=0,i=0;a<self.length;a++) fn.call(self[a],i++);
return self;
};
this.push=function(nice){
self[self.length]=nice;
self.length++;
};
this.eq=function(idx){
return self[idx];
};
if(doms){
for(a=0;a<doms.length;a++){
var nice=$.data(doms[a],'__nicescroll')||false;
if(nice){
this[this.length]=nice;
this.length++;
}};}
return this;
};
function mplex(el,lst,fn){
for(var a=0;a<lst.length;a++) fn(el,lst[a]);
};
mplex(
NiceScrollArray.prototype,
['show','hide','toggle','onResize','resize','remove','stop','doScrollPos'],
function(e,n){
e[n]=function(){
var args=arguments;
return this.each(function(){
this[n].apply(this,args);
});
};}
);
jQuery.fn.getNiceScroll=function(index){
if(typeof index=="undefined"){
return new NiceScrollArray(this);
}else{
var nice=this[index]&&$.data(this[index],'__nicescroll')||false;
return nice;
}};
jQuery.extend(jQuery.expr[':'], {
nicescroll: function(a){
return ($.data(a,'__nicescroll'))?true:false;
}});
$.fn.niceScroll=function(wrapper,opt){
if(typeof opt=="undefined"){
if((typeof wrapper=="object")&&!("jquery" in wrapper)){
opt=wrapper;
wrapper=false;
}}
var ret=new NiceScrollArray();
if(typeof opt=="undefined") opt={};
if(wrapper||false){
opt.doc=$(wrapper);
opt.win=$(this);
}
var docundef = !("doc" in opt);
if(!docundef&&!("win" in opt)) opt.win=$(this);
this.each(function(){
var nice=$(this).data('__nicescroll')||false;
if(!nice){
opt.doc=(docundef) ? $(this):opt.doc;
nice=new NiceScrollClass(opt,$(this));
$(this).data('__nicescroll',nice);
}
ret.push(nice);
});
return (ret.length==1) ? ret[0]:ret;
};
window.NiceScroll={
getjQuery:function(){return jQuery}};
if(!$.nicescroll){
$.nicescroll=new NiceScrollArray();
$.nicescroll.options=_globaloptions;
}})(jQuery);
!function(e,t,n){function o(e){var t=x(),n=t.querySelector("h2"),o=t.querySelector("p"),r=t.querySelector("button.cancel"),a=t.querySelector("button.confirm");if(n.innerHTML=e.html?e.title:E(e.title).split("\n").join("<br>"),o.innerHTML=e.html?e.text:E(e.text||"").split("\n").join("<br>"),e.text&&I(o),e.customClass)q(t,e.customClass),t.setAttribute("data-custom-class",e.customClass);else{var s=t.getAttribute("data-custom-class");B(t,s),t.setAttribute("data-custom-class","")}if(M(t.querySelectorAll(".sa-icon")),e.type&&!f()){for(var c=!1,l=0;l<h.length;l++)if(e.type===h[l]){c=!0;break}if(!c)return p("Unknown alert type: "+e.type),!1;var u,d=["success","error","warning","info"];-1!==d.indexOf(e.type)&&(u=t.querySelector(".sa-icon.sa-"+e.type),I(u));var m=C();switch(e.type){case"success":q(u,"animate"),q(u.querySelector(".sa-tip"),"animateSuccessTip"),q(u.querySelector(".sa-long"),"animateSuccessLong");break;case"error":q(u,"animateErrorIcon"),q(u.querySelector(".sa-x-mark"),"animateXMark");break;case"warning":q(u,"pulseWarning"),q(u.querySelector(".sa-body"),"pulseWarningIns"),q(u.querySelector(".sa-dot"),"pulseWarningIns");break;case"input":case"prompt":m.setAttribute("type",e.inputType),q(t,"show-input"),setTimeout(function(){m.focus(),m.addEventListener("keyup",g.resetInputError)},400)}}if(e.imageUrl){var y=t.querySelector(".sa-icon.sa-custom");y.style.backgroundImage="url("+e.imageUrl+")",I(y);var v=80,b=80;if(e.imageSize){var w=e.imageSize.toString().split("x"),S=w[0],k=w[1];S&&k?(v=S,b=k):p("Parameter imageSize expects value with format WIDTHxHEIGHT, got "+e.imageSize)}y.setAttribute("style",y.getAttribute("style")+"width:"+v+"px; height:"+b+"px")}t.setAttribute("data-has-cancel-button",e.showCancelButton),e.showCancelButton?r.style.display="inline-block":M(r),t.setAttribute("data-has-confirm-button",e.showConfirmButton),e.showConfirmButton?a.style.display="inline-block":M(a),e.cancelButtonText&&(r.innerHTML=E(e.cancelButtonText)),e.confirmButtonText&&(a.innerHTML=E(e.confirmButtonText)),e.confirmButtonColor&&(a.style.backgroundColor=e.confirmButtonColor,i(a,e.confirmButtonColor)),t.setAttribute("data-allow-ouside-click",e.allowOutsideClick);var T=e.doneFunction?!0:!1;t.setAttribute("data-has-done-function",T),e.animation?"string"==typeof e.animation?t.setAttribute("data-animation",e.animation):t.setAttribute("data-animation","pop"):t.setAttribute("data-animation","none"),t.setAttribute("data-timer",e.timer)}function r(e,t){e=String(e).replace(/[^0-9a-f]/gi,""),e.length<6&&(e=e[0]+e[0]+e[1]+e[1]+e[2]+e[2]),t=t||0;var n,o,r="#";for(o=0;3>o;o++)n=parseInt(e.substr(2*o,2),16),n=Math.round(Math.min(Math.max(0,n+n*t),255)).toString(16),r+=("00"+n).substr(n.length);return r}function a(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e}function s(e){var t=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);return t?parseInt(t[1],16)+", "+parseInt(t[2],16)+", "+parseInt(t[3],16):null}function i(e,t){var n=s(t);e.style.boxShadow="0 0 2px rgba("+n+", 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)"}function c(){var e=x();H(k(),10),I(e),q(e,"showSweetAlert"),B(e,"hideSweetAlert"),d=t.activeElement;var n=e.querySelector("button.confirm");n.focus(),setTimeout(function(){q(e,"visible")},500);var o=e.getAttribute("data-timer");"null"!==o&&""!==o&&(e.timeout=setTimeout(function(){v.close()},o))}function l(){var e=x(),t=C();B(e,"show-input"),t.value="",t.setAttribute("type",S.inputType),g.resetInputError()}function u(){var e=x();e.style.marginTop=D(x())}function f(){return e.attachEvent&&!e.addEventListener?!0:!1}function p(t){e.console&&e.console.log("SweetAlert: "+t)}var d,m,y,v,g,b=".sweet-alert",w=".sweet-overlay",h=["error","warning","info","success","input","prompt"],S={title:"",text:"",type:null,allowOutsideClick:!1,showConfirmButton:!0,showCancelButton:!1,closeOnConfirm:!0,closeOnCancel:!0,confirmButtonText:"OK",confirmButtonColor:"#AEDEF4",cancelButtonText:"Cancel",imageUrl:null,imageSize:null,timer:null,customClass:"",html:!1,animation:!0,allowEscapeKey:!0,inputType:"text"},x=function(){var e=t.querySelector(b);return e||(j(),e=x()),e},C=function(){var e=x();return e?e.querySelector("input"):void 0},k=function(){return t.querySelector(w)},T=function(e,t){return new RegExp(" "+t+" ").test(" "+e.className+" ")},q=function(e,t){T(e,t)||(e.className+=" "+t)},B=function(e,t){var n=" "+e.className.replace(/[\t\r\n]/g," ")+" ";if(T(e,t)){for(;n.indexOf(" "+t+" ")>=0;)n=n.replace(" "+t+" "," ");e.className=n.replace(/^\s+|\s+$/g,"")}},E=function(e){var n=t.createElement("div");return n.appendChild(t.createTextNode(e)),n.innerHTML},A=function(e){e.style.opacity="",e.style.display="block"},I=function(e){if(e&&!e.length)return A(e);for(var t=0;t<e.length;++t)A(e[t])},O=function(e){e.style.opacity="",e.style.display="none"},M=function(e){if(e&&!e.length)return O(e);for(var t=0;t<e.length;++t)O(e[t])},N=function(e,t){for(var n=t.parentNode;null!==n;){if(n===e)return!0;n=n.parentNode}return!1},D=function(e){e.style.left="-9999px",e.style.display="block";var t,n=e.clientHeight;return t="undefined"!=typeof getComputedStyle?parseInt(getComputedStyle(e).getPropertyValue("padding-top"),10):parseInt(e.currentStyle.padding),e.style.left="",e.style.display="none","-"+parseInt((n+t)/2)+"px"},H=function(e,t){if(+e.style.opacity<1){t=t||16,e.style.opacity=0,e.style.display="block";var n=+new Date,o=function(){e.style.opacity=+e.style.opacity+(new Date-n)/100,n=+new Date,+e.style.opacity<1&&setTimeout(o,t)};o()}e.style.display="block"},L=function(e,t){t=t||16,e.style.opacity=1;var n=+new Date,o=function(){e.style.opacity=+e.style.opacity-(new Date-n)/100,n=+new Date,+e.style.opacity>0?setTimeout(o,t):e.style.display="none"};o()},P=function(n){if("function"==typeof MouseEvent){var o=new MouseEvent("click",{view:e,bubbles:!1,cancelable:!0});n.dispatchEvent(o)}else if(t.createEvent){var r=t.createEvent("MouseEvents");r.initEvent("click",!1,!1),n.dispatchEvent(r)}else t.createEventObject?n.fireEvent("onclick"):"function"==typeof n.onclick&&n.onclick()},U=function(t){"function"==typeof t.stopPropagation?(t.stopPropagation(),t.preventDefault()):e.event&&e.event.hasOwnProperty("cancelBubble")&&(e.event.cancelBubble=!0)},j=function(){var e='<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert"><div class="sa-icon sa-error"><span class="sa-x-mark"><span class="sa-line sa-left"></span><span class="sa-line sa-right"></span></span></div><div class="sa-icon sa-warning"> <span class="sa-body"></span> <span class="sa-dot"></span> </div> <div class="sa-icon sa-info"></div> <div class="sa-icon sa-success"> <span class="sa-line sa-tip"></span> <span class="sa-line sa-long"></span> <div class="sa-placeholder"></div> <div class="sa-fix"></div> </div> <div class="sa-icon sa-custom"></div> <h2>Title</h2><p>Text</p><fieldset><input type="text" tabIndex="3" /><div class="sa-input-error"></div></fieldset> <div class="sa-error-container"><div class="icon">!</div> <p>Not valid!</p></div> <button class="cancel" tabIndex="2">Cancel</button><button class="confirm" tabIndex="1">OK</button></div>',n=t.createElement("div");for(n.innerHTML=e;n.firstChild;)t.body.appendChild(n.firstChild)};v=g=function(){function s(e){var t=b;return"undefined"!=typeof t[e]?t[e]:S[e]}function f(){var e=!0;T(A,"show-input")&&(e=A.querySelector("input").value,e||(e="")),w.doneFunction(e),w.closeOnConfirm&&v.close()}function d(){var e=String(w.doneFunction).replace(/\s/g,""),t="function("===e.substring(0,9)&&")"!==e.substring(9,10);t&&w.doneFunction(!1),w.closeOnCancel&&v.close()}function g(t){var o=t||e.event,r=o.keyCode||o.which;if(-1!==[9,13,32,27].indexOf(r)){for(var a=o.target||o.srcElement,s=-1,c=0;c<L.length;c++)if(a===L[c]){s=c;break}9===r?(a=-1===s?D:s===L.length-1?L[0]:L[s+1],U(o),a.focus(),w.confirmButtonColor&&i(a,w.confirmButtonColor)):13===r?("INPUT"===a.tagName&&(a=D,D.focus()),a=-1===s?D:n):27===r&&w.allowEscapeKey===!0?(a=H,P(a,o)):a=n}}var b=arguments[0];if(q(t.body,"stop-scrolling"),l(),arguments[0]===n)return p("SweetAlert expects at least 1 attribute!"),!1;var w=a({},S);switch(typeof arguments[0]){case"string":w.title=arguments[0],w.text=arguments[1]||"",w.type=arguments[2]||"";break;case"object":if(arguments[0].title===n)return p('Missing "title" argument!'),!1;w.title=arguments[0].title;for(var h=["text","type","customClass","allowOutsideClick","showConfirmButton","showCancelButton","closeOnConfirm","closeOnCancel","timer","confirmButtonColor","cancelButtonText","imageUrl","imageSize","html","animation","allowEscapeKey","inputType"],C=h.length,B=0;C>B;B++){var E=h[B];w[E]=s(E)}w.confirmButtonText=w.showCancelButton?"Confirm":S.confirmButtonText,w.confirmButtonText=s("confirmButtonText"),w.doneFunction=arguments[1]||null;break;default:return p('Unexpected type of argument! Expected "string" or "object", got '+typeof arguments[0]),!1}o(w),u(),c();for(var A=x(),I=function(t){var n=t||e.event,o=n.target||n.srcElement,a=-1!==o.className.indexOf("confirm"),s=-1!==o.className.indexOf("sweet-overlay"),i=T(A,"visible"),c=w.doneFunction&&"true"===A.getAttribute("data-has-done-function");switch(n.type){case"mouseover":a&&w.confirmButtonColor&&(o.style.backgroundColor=r(w.confirmButtonColor,-.04));break;case"mouseout":a&&w.confirmButtonColor&&(o.style.backgroundColor=w.confirmButtonColor);break;case"mousedown":a&&w.confirmButtonColor&&(o.style.backgroundColor=r(w.confirmButtonColor,-.14));break;case"mouseup":a&&w.confirmButtonColor&&(o.style.backgroundColor=r(w.confirmButtonColor,-.04));break;case"focus":var l=A.querySelector("button.confirm"),u=A.querySelector("button.cancel");a?u.style.boxShadow="none":l.style.boxShadow="none";break;case"click":a&&c&&i?f():c&&i||s?d():N(A,o)&&"BUTTON"===o.tagName&&v.close()}},O=A.querySelectorAll("button"),M=0;M<O.length;M++)O[M].onclick=I,O[M].onmouseover=I,O[M].onmouseout=I,O[M].onmousedown=I,O[M].onmouseup=I,O[M].onfocus=I;k().onclick=I;var D=A.querySelector("button.confirm"),H=A.querySelector("button.cancel"),L=A.querySelectorAll("button[tabindex]");m=e.onkeydown,e.onkeydown=g,e.onfocus=function(){e.setTimeout(function(){y!==n&&(y.focus(),y=n)},0)}},v.setDefaults=g.setDefaults=function(e){if(!e)throw new Error("userParams is required");if("object"!=typeof e)throw new Error("userParams has to be a object");a(S,e)},v.close=g.close=function(){var o=x();L(k(),5),L(o,5),B(o,"showSweetAlert"),q(o,"hideSweetAlert"),B(o,"visible");var r=o.querySelector(".sa-icon.sa-success");B(r,"animate"),B(r.querySelector(".sa-tip"),"animateSuccessTip"),B(r.querySelector(".sa-long"),"animateSuccessLong");var a=o.querySelector(".sa-icon.sa-error");B(a,"animateErrorIcon"),B(a.querySelector(".sa-x-mark"),"animateXMark");var s=o.querySelector(".sa-icon.sa-warning");B(s,"pulseWarning"),B(s.querySelector(".sa-body"),"pulseWarningIns"),B(s.querySelector(".sa-dot"),"pulseWarningIns"),B(t.body,"stop-scrolling"),e.onkeydown=m,d&&d.focus(),y=n,clearTimeout(o.timeout)},v.showInputError=g.showInputError=function(e){var t=x(),n=t.querySelector(".sa-input-error");q(n,"show");var o=t.querySelector(".sa-error-container");q(o,"show"),o.querySelector("p").innerHTML=e,t.querySelector("input").focus()},v.resetInputError=g.resetInputError=function(e){if(e&&13===e.keyCode)return!1;var t=x(),n=t.querySelector(".sa-input-error");B(n,"show");var o=t.querySelector(".sa-error-container");B(o,"show")},"function"==typeof define&&define.amd?define(function(){return v}):"undefined"!=typeof module&&module.exports?module.exports=v:"undefined"!=typeof e&&(e.sweetAlert=e.swal=v)}(window,document);
(function(){var a,b,c,d,e,f=function(a,b){return function(){return a.apply(b,arguments)}},g=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(b in this&&this[b]===a)return b;return-1};b=function(){function a(){}return a.prototype.extend=function(a,b){var c,d;for(c in b)d=b[c],null==a[c]&&(a[c]=d);return a},a.prototype.isMobile=function(a){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)},a.prototype.createEvent=function(a,b,c,d){var e;return null==b&&(b=!1),null==c&&(c=!1),null==d&&(d=null),null!=document.createEvent?(e=document.createEvent("CustomEvent"),e.initCustomEvent(a,b,c,d)):null!=document.createEventObject?(e=document.createEventObject(),e.eventType=a):e.eventName=a,e},a.prototype.emitEvent=function(a,b){return null!=a.dispatchEvent?a.dispatchEvent(b):b in(null!=a)?a[b]():"on"+b in(null!=a)?a["on"+b]():void 0},a.prototype.addEvent=function(a,b,c){return null!=a.addEventListener?a.addEventListener(b,c,!1):null!=a.attachEvent?a.attachEvent("on"+b,c):a[b]=c},a.prototype.removeEvent=function(a,b,c){return null!=a.removeEventListener?a.removeEventListener(b,c,!1):null!=a.detachEvent?a.detachEvent("on"+b,c):delete a[b]},a.prototype.innerHeight=function(){return"innerHeight"in window?window.innerHeight:document.documentElement.clientHeight},a}(),c=this.WeakMap||this.MozWeakMap||(c=function(){function a(){this.keys=[],this.values=[]}return a.prototype.get=function(a){var b,c,d,e,f;for(f=this.keys,b=d=0,e=f.length;e>d;b=++d)if(c=f[b],c===a)return this.values[b]},a.prototype.set=function(a,b){var c,d,e,f,g;for(g=this.keys,c=e=0,f=g.length;f>e;c=++e)if(d=g[c],d===a)return void(this.values[c]=b);return this.keys.push(a),this.values.push(b)},a}()),a=this.MutationObserver||this.WebkitMutationObserver||this.MozMutationObserver||(a=function(){function a(){"undefined"!=typeof console&&null!==console&&console.warn("MutationObserver is not supported by your browser."),"undefined"!=typeof console&&null!==console&&console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")}return a.notSupported=!0,a.prototype.observe=function(){},a}()),d=this.getComputedStyle||function(a,b){return this.getPropertyValue=function(b){var c;return"float"===b&&(b="styleFloat"),e.test(b)&&b.replace(e,function(a,b){return b.toUpperCase()}),(null!=(c=a.currentStyle)?c[b]:void 0)||null},this},e=/(\-([a-z]){1})/g,this.WOW=function(){function e(a){null==a&&(a={}),this.scrollCallback=f(this.scrollCallback,this),this.scrollHandler=f(this.scrollHandler,this),this.resetAnimation=f(this.resetAnimation,this),this.start=f(this.start,this),this.scrolled=!0,this.config=this.util().extend(a,this.defaults),null!=a.scrollContainer&&(this.config.scrollContainer=document.querySelector(a.scrollContainer)),this.animationNameCache=new c,this.wowEvent=this.util().createEvent(this.config.boxClass)}return e.prototype.defaults={boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:null,scrollContainer:null},e.prototype.init=function(){var a;return this.element=window.document.documentElement,"interactive"===(a=document.readyState)||"complete"===a?this.start():this.util().addEvent(document,"DOMContentLoaded",this.start),this.finished=[]},e.prototype.start=function(){var b,c,d,e;if(this.stopped=!1,this.boxes=function(){var a,c,d,e;for(d=this.element.querySelectorAll("."+this.config.boxClass),e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.all=function(){var a,c,d,e;for(d=this.boxes,e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.boxes.length)if(this.disabled())this.resetStyle();else for(e=this.boxes,c=0,d=e.length;d>c;c++)b=e[c],this.applyStyle(b,!0);return this.disabled()||(this.util().addEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().addEvent(window,"resize",this.scrollHandler),this.interval=setInterval(this.scrollCallback,50)),this.config.live?new a(function(a){return function(b){var c,d,e,f,g;for(g=[],c=0,d=b.length;d>c;c++)f=b[c],g.push(function(){var a,b,c,d;for(c=f.addedNodes||[],d=[],a=0,b=c.length;b>a;a++)e=c[a],d.push(this.doSync(e));return d}.call(a));return g}}(this)).observe(document.body,{childList:!0,subtree:!0}):void 0},e.prototype.stop=function(){return this.stopped=!0,this.util().removeEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().removeEvent(window,"resize",this.scrollHandler),null!=this.interval?clearInterval(this.interval):void 0},e.prototype.sync=function(b){return a.notSupported?this.doSync(this.element):void 0},e.prototype.doSync=function(a){var b,c,d,e,f;if(null==a&&(a=this.element),1===a.nodeType){for(a=a.parentNode||a,e=a.querySelectorAll("."+this.config.boxClass),f=[],c=0,d=e.length;d>c;c++)b=e[c],g.call(this.all,b)<0?(this.boxes.push(b),this.all.push(b),this.stopped||this.disabled()?this.resetStyle():this.applyStyle(b,!0),f.push(this.scrolled=!0)):f.push(void 0);return f}},e.prototype.show=function(a){return this.applyStyle(a),a.className=a.className+" "+this.config.animateClass,null!=this.config.callback&&this.config.callback(a),this.util().emitEvent(a,this.wowEvent),this.util().addEvent(a,"animationend",this.resetAnimation),this.util().addEvent(a,"oanimationend",this.resetAnimation),this.util().addEvent(a,"webkitAnimationEnd",this.resetAnimation),this.util().addEvent(a,"MSAnimationEnd",this.resetAnimation),a},e.prototype.applyStyle=function(a,b){var c,d,e;return d=a.getAttribute("data-wow-duration"),c=a.getAttribute("data-wow-delay"),e=a.getAttribute("data-wow-iteration"),this.animate(function(f){return function(){return f.customStyle(a,b,d,c,e)}}(this))},e.prototype.animate=function(){return"requestAnimationFrame"in window?function(a){return window.requestAnimationFrame(a)}:function(a){return a()}}(),e.prototype.resetStyle=function(){var a,b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],e.push(a.style.visibility="visible");return e},e.prototype.resetAnimation=function(a){var b;return a.type.toLowerCase().indexOf("animationend")>=0?(b=a.target||a.srcElement,b.className=b.className.replace(this.config.animateClass,"").trim()):void 0},e.prototype.customStyle=function(a,b,c,d,e){return b&&this.cacheAnimationName(a),a.style.visibility=b?"hidden":"visible",c&&this.vendorSet(a.style,{animationDuration:c}),d&&this.vendorSet(a.style,{animationDelay:d}),e&&this.vendorSet(a.style,{animationIterationCount:e}),this.vendorSet(a.style,{animationName:b?"none":this.cachedAnimationName(a)}),a},e.prototype.vendors=["moz","webkit"],e.prototype.vendorSet=function(a,b){var c,d,e,f;d=[];for(c in b)e=b[c],a[""+c]=e,d.push(function(){var b,d,g,h;for(g=this.vendors,h=[],b=0,d=g.length;d>b;b++)f=g[b],h.push(a[""+f+c.charAt(0).toUpperCase()+c.substr(1)]=e);return h}.call(this));return d},e.prototype.vendorCSS=function(a,b){var c,e,f,g,h,i;for(h=d(a),g=h.getPropertyCSSValue(b),f=this.vendors,c=0,e=f.length;e>c;c++)i=f[c],g=g||h.getPropertyCSSValue("-"+i+"-"+b);return g},e.prototype.animationName=function(a){var b;try{b=this.vendorCSS(a,"animation-name").cssText}catch(c){b=d(a).getPropertyValue("animation-name")}return"none"===b?"":b},e.prototype.cacheAnimationName=function(a){return this.animationNameCache.set(a,this.animationName(a))},e.prototype.cachedAnimationName=function(a){return this.animationNameCache.get(a)},e.prototype.scrollHandler=function(){return this.scrolled=!0},e.prototype.scrollCallback=function(){var a;return!this.scrolled||(this.scrolled=!1,this.boxes=function(){var b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],a&&(this.isVisible(a)?this.show(a):e.push(a));return e}.call(this),this.boxes.length||this.config.live)?void 0:this.stop()},e.prototype.offsetTop=function(a){for(var b;void 0===a.offsetTop;)a=a.parentNode;for(b=a.offsetTop;a=a.offsetParent;)b+=a.offsetTop;return b},e.prototype.isVisible=function(a){var b,c,d,e,f;return c=a.getAttribute("data-wow-offset")||this.config.offset,f=this.config.scrollContainer&&this.config.scrollContainer.scrollTop||window.pageYOffset,e=f+Math.min(this.element.clientHeight,this.util().innerHeight())-c,d=this.offsetTop(a),b=d+a.clientHeight,e>=d&&b>=f},e.prototype.util=function(){return null!=this._util?this._util:this._util=new b},e.prototype.disabled=function(){return!this.config.mobile&&this.util().isMobile(navigator.userAgent)},e}()}).call(this);