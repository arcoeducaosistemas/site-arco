new WOW().init();
(function($){
$('#sidebar').mouseover(function(){
$('#sidebar').addClass('active');
$('.overlay').addClass('active');
});
$('#sidebar').mouseout(function(){
$('#sidebar').removeClass('active');
$('.overlay').removeClass('active');
});
$(document).ready(function (){
$('#sidebarCollapse').on('click', function (){
$('#sidebar').toggleClass('active');
});
});
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e){
var iframe=$(e.relatedTarget.hash).find('iframe');
var src=iframe.attr('src');
iframe.attr('src', '');
iframe.attr('src', src);
});
$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function(){
if(location.pathname.replace(/^\//, '')==this.pathname.replace(/^\//, '')&&location.hostname==this.hostname){
var target=$(this.hash);
target=target.length ? target:$('[name=' + this.hash.slice(1) + ']');
if(target.length){
$('html, body').animate({
scrollTop: (target.offset().top - 54)
}, 1000, "easeInOutExpo");
return false;
}}
});
$('.js-scroll-trigger').click(function(){
$('.navbar-collapse').collapse('hide');
});
$('body').scrollspy({
target: '#navbar_custom',
offset: 54
});
$(window).scroll(function(){
var nav=$('#navbar_custom');
var top=400;
if($(window).scrollTop() >=top){
nav.addClass('navbar-bg');
}else{
nav.removeClass('navbar-bg');
}});
$(".img-plan").mousemove(function(event){
var mousex=event.pageX - $(this).offset().left;
var mousey=event.pageY - $(this).offset().top;
var imgx=(mousex - 300) / 40;
var imgy=(mousey - 200) / 40;
$(this).css("transform", "translate(" + imgx + "px," + imgy + "px)");
});
$(".img-plan").mouseout(function(){
$(this).css("transform", "translate(0px,0px)");
});
$('.navbar-collapse a').click(function(){
$(".navbar-collapse").collapse('hide');
});
var didScroll;
var lastScrollTop=0;
var delta=100;
var navbarHeight=$('header').outerHeight();
$(window).scroll(function(event){
didScroll=true;
});
setInterval(function(){
if(didScroll){
hasScrolled();
didScroll=false;
}}, 250);
function hasScrolled(){
var st=$(this).scrollTop();
if(Math.abs(lastScrollTop - st) <=delta)
return;
if(st > lastScrollTop&&st > navbarHeight){
$('.fixed-bottom').removeClass('nav-down').addClass('nav-up');
$("#page_related").addClass("active_mobile");
}else{
if(st + $(window).height() < $(document).height()){
$('.fixed-bottom').removeClass('nav-up').addClass('nav-down');
$("#page_related").removeClass("active_mobile");
}}
lastScrollTop=st;
}
if($('.tab-content .show ').hasClass('active')===true){
$('.carousel-item').addClass('active');
}
function new_map($el){
var $markers=$el.find('.marker');
var args={
zoom:16,
center:new google.maps.LatLng(0, 0),
mapTypeId:google.maps.MapTypeId.ROADMAP
};
var map=new google.maps.Map($el[0], args);
map.markers=[];
$markers.each(function(){
add_marker($(this), map);
});
center_map(map);
return map;
}
function add_marker($marker, map){
var latlng=new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
var marker=new google.maps.Marker({
position:latlng,
icon: '',
map:map
});
map.markers.push(marker);
if($marker.html()){
var infowindow=new google.maps.InfoWindow({
content:$marker.html()
});
google.maps.event.addListener(marker, 'click', function(){
infowindow.open(map, marker);
});
}}
function center_map(map){
var bounds=new google.maps.LatLngBounds();
$.each(map.markers, function(i, marker){
var latlng=new google.maps.LatLng(marker.position.lat(), marker.position.lng());
bounds.extend(latlng);
});
if(map.markers.length==1){
map.setCenter(bounds.getCenter());
map.setZoom(16);
}else{
map.fitBounds(bounds);
}}
var map=null;
$(document).ready(function(){
$('.acf-map').each(function(){
map=new_map($(this));
});
});
$('#carousel_work').owlCarousel({
items:3,
margin: 10,
responsive:{
580:{
items:1,
loop:true,
margin:30,
dots:true,
speed: 3000,
autoplay: true
},
600:{
items:3,
loop:true,
margin:30,
dots:true,
speed: 3000,
autoplay: true
},
992:{
items: 3
}}
})
$('#carousel_our').owlCarousel({
items:1,
margin: 10,
responsive:{
580:{
items:1,
loop:true,
margin:30,
dots:true,
speed: 3000,
autoplay: true
},
600:{
items:3,
loop:true,
margin:30,
dots:true,
speed: 3000,
autoplay: true
},
992:{
items: 3
}}
})
$('.navbar-collapse a').click(function(){
$(".navbar-collapse").collapse('hide');
$("ul.dropdown-menu").collapse('hide');
});
$(document).ready(function(){
$(".nav .dropdown").focusin(function (){
$(this).find(".dropdown-menu").each(function(){
$(this).css({"display":'block','opacity':'1','top':'60px'});
});
});
$(".nav .dropdown").focusout(function (){
$(this).find(".dropdown-menu").each(function(){
$(this).css({"display":'block','opacity':'0','top':'0px'});
});
});
});
$(document).ready(function (){
$($('.hamburger').parent().attr('data-target')).on('hide.bs.collapse', function (){
$(this).parent().find('.hamburger').removeClass('hamburger--close');
});
$($('.hamburger').parent().attr('data-target')).on('show.bs.collapse', function (){
$(this).parent().find('.hamburger').addClass('hamburger--close');
});
});
})(jQuery);
(function($){
$.fn.cycle=function(timeout, cls){
var l=this.length,
current=0,
prev=0,
elements=this;
function next(){
elements.eq(prev).removeClass(cls);
elements.eq(current).addClass(cls);
prev=current;
current=(current + 1) % l;
setTimeout(next, timeout);
}
setTimeout(next, timeout);
return this;
};
$('div.cssClass').cycle(3000, 'active_border');
}(jQuery));
var offsetHeightBody=document.getElementById('content_page').offsetHeight;
if(offsetHeightBody > 250){
(function($){
$('.page-template-template-sub .add').addClass('scrollbar');
}(jQuery));
};