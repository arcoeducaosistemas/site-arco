(function($){
'use strict';
if(typeof wpcf7==='undefined'||wpcf7===null){
return;
}
wpcf7=$.extend({
cached: 0,
inputs: []
}, wpcf7);
$(function(){
wpcf7.supportHtml5=(function(){
var features={};
var input=document.createElement('input');
features.placeholder='placeholder' in input;
var inputTypes=[ 'email', 'url', 'tel', 'number', 'range', 'date' ];
$.each(inputTypes, function(index, value){
input.setAttribute('type', value);
features[ value ]=input.type!=='text';
});
return features;
})();
$('div.wpcf7 > form').each(function(){
var $form=$(this);
wpcf7.initForm($form);
if(wpcf7.cached){
wpcf7.refill($form);
}});
});
wpcf7.getId=function(form){
return parseInt($('input[name="_wpcf7"]', form).val(), 10);
};
wpcf7.initForm=function(form){
var $form=$(form);
$form.submit(function(event){
if(typeof window.FormData!=='function'){
return;
}
wpcf7.submit($form);
event.preventDefault();
});
$('.wpcf7-submit', $form).after('<span class="ajax-loader"></span>');
wpcf7.toggleSubmit($form);
$form.on('click', '.wpcf7-acceptance', function(){
wpcf7.toggleSubmit($form);
});
$('.wpcf7-exclusive-checkbox', $form).on('click', 'input:checkbox', function(){
var name=$(this).attr('name');
$form.find('input:checkbox[name="' + name + '"]').not(this).prop('checked', false);
});
$('.wpcf7-list-item.has-free-text', $form).each(function(){
var $freetext=$(':input.wpcf7-free-text', this);
var $wrap=$(this).closest('.wpcf7-form-control');
if($(':checkbox, :radio', this).is(':checked')){
$freetext.prop('disabled', false);
}else{
$freetext.prop('disabled', true);
}
$wrap.on('change', ':checkbox, :radio', function(){
var $cb=$('.has-free-text', $wrap).find(':checkbox, :radio');
if($cb.is(':checked')){
$freetext.prop('disabled', false).focus();
}else{
$freetext.prop('disabled', true);
}});
});
if(! wpcf7.supportHtml5.placeholder){
$('[placeholder]', $form).each(function(){
$(this).val($(this).attr('placeholder'));
$(this).addClass('placeheld');
$(this).focus(function(){
if($(this).hasClass('placeheld')){
$(this).val('').removeClass('placeheld');
}});
$(this).blur(function(){
if(''===$(this).val()){
$(this).val($(this).attr('placeholder'));
$(this).addClass('placeheld');
}});
});
}
if(wpcf7.jqueryUi&&! wpcf7.supportHtml5.date){
$form.find('input.wpcf7-date[type="date"]').each(function(){
$(this).datepicker({
dateFormat: 'yy-mm-dd',
minDate: new Date($(this).attr('min')),
maxDate: new Date($(this).attr('max'))
});
});
}
if(wpcf7.jqueryUi&&! wpcf7.supportHtml5.number){
$form.find('input.wpcf7-number[type="number"]').each(function(){
$(this).spinner({
min: $(this).attr('min'),
max: $(this).attr('max'),
step: $(this).attr('step')
});
});
}
$('.wpcf7-character-count', $form).each(function(){
var $count=$(this);
var name=$count.attr('data-target-name');
var down=$count.hasClass('down');
var starting=parseInt($count.attr('data-starting-value'), 10);
var maximum=parseInt($count.attr('data-maximum-value'), 10);
var minimum=parseInt($count.attr('data-minimum-value'), 10);
var updateCount=function(target){
var $target=$(target);
var length=$target.val().length;
var count=down ? starting - length:length;
$count.attr('data-current-value', count);
$count.text(count);
if(maximum&&maximum < length){
$count.addClass('too-long');
}else{
$count.removeClass('too-long');
}
if(minimum&&length < minimum){
$count.addClass('too-short');
}else{
$count.removeClass('too-short');
}};
$(':input[name="' + name + '"]', $form).each(function(){
updateCount(this);
$(this).keyup(function(){
updateCount(this);
});
});
});
$form.on('change', '.wpcf7-validates-as-url', function(){
var val=$.trim($(this).val());
if(val
&& ! val.match(/^[a-z][a-z0-9.+-]*:/i)
&& -1!==val.indexOf('.')){
val=val.replace(/^\/+/, '');
val='http://' + val;
}
$(this).val(val);
});
};
wpcf7.submit=function(form){
if(typeof window.FormData!=='function'){
return;
}
var $form=$(form);
$('.ajax-loader', $form).addClass('is-active');
$('[placeholder].placeheld', $form).each(function(i, n){
$(n).val('');
});
wpcf7.clearResponse($form);
var formData=new FormData($form.get(0));
var detail={
id: $form.closest('div.wpcf7').attr('id'),
status: 'init',
inputs: [],
formData: formData
};
$.each($form.serializeArray(), function(i, field){
if('_wpcf7'==field.name){
detail.contactFormId=field.value;
}else if('_wpcf7_version'==field.name){
detail.pluginVersion=field.value;
}else if('_wpcf7_locale'==field.name){
detail.contactFormLocale=field.value;
}else if('_wpcf7_unit_tag'==field.name){
detail.unitTag=field.value;
}else if('_wpcf7_container_post'==field.name){
detail.containerPostId=field.value;
}else if(field.name.match(/^_wpcf7_\w+_free_text_/)){
var owner=field.name.replace(/^_wpcf7_\w+_free_text_/, '');
detail.inputs.push({
name: owner + '-free-text',
value: field.value
});
}else if(field.name.match(/^_/)){
}else{
detail.inputs.push(field);
}});
wpcf7.triggerEvent($form.closest('div.wpcf7'), 'beforesubmit', detail);
var ajaxSuccess=function(data, status, xhr, $form){
detail.id=$(data.into).attr('id');
detail.status=data.status;
detail.apiResponse=data;
var $message=$('.wpcf7-response-output', $form);
switch(data.status){
case 'validation_failed':
$.each(data.invalidFields, function(i, n){
$(n.into, $form).each(function(){
wpcf7.notValidTip(this, n.message);
$('.wpcf7-form-control', this).addClass('wpcf7-not-valid');
$('[aria-invalid]', this).attr('aria-invalid', 'true');
});
});
$message.addClass('wpcf7-validation-errors');
$form.addClass('invalid');
wpcf7.triggerEvent(data.into, 'invalid', detail);
break;
case 'acceptance_missing':
$message.addClass('wpcf7-acceptance-missing');
$form.addClass('unaccepted');
wpcf7.triggerEvent(data.into, 'unaccepted', detail);
break;
case 'spam':
$message.addClass('wpcf7-spam-blocked');
$form.addClass('spam');
$('[name="g-recaptcha-response"]', $form).each(function(){
if(''===$(this).val()){
var $recaptcha=$(this).closest('.wpcf7-form-control-wrap');
wpcf7.notValidTip($recaptcha, wpcf7.recaptcha.messages.empty);
}});
wpcf7.triggerEvent(data.into, 'spam', detail);
break;
case 'aborted':
$message.addClass('wpcf7-aborted');
$form.addClass('aborted');
wpcf7.triggerEvent(data.into, 'aborted', detail);
break;
case 'mail_sent':
$message.addClass('wpcf7-mail-sent-ok');
$form.addClass('sent');
wpcf7.triggerEvent(data.into, 'mailsent', detail);
break;
case 'mail_failed':
$message.addClass('wpcf7-mail-sent-ng');
$form.addClass('failed');
wpcf7.triggerEvent(data.into, 'mailfailed', detail);
break;
default:
var customStatusClass='custom-'
+ data.status.replace(/[^0-9a-z]+/i, '-');
$message.addClass('wpcf7-' + customStatusClass);
$form.addClass(customStatusClass);
}
wpcf7.refill($form, data);
wpcf7.triggerEvent(data.into, 'submit', detail);
if('mail_sent'==data.status){
$form.each(function(){
this.reset();
});
wpcf7.toggleSubmit($form);
}
$form.find('[placeholder].placeheld').each(function(i, n){
$(n).val($(n).attr('placeholder'));
});
$message.html('').append(data.message).slideDown('fast');
$message.attr('role', 'alert');
$('.screen-reader-response', $form.closest('.wpcf7')).each(function(){
var $response=$(this);
$response.html('').attr('role', '').append(data.message);
if(data.invalidFields){
var $invalids=$('<ul></ul>');
$.each(data.invalidFields, function(i, n){
if(n.idref){
var $li=$('<li></li>').append($('<a></a>').attr('href', '#' + n.idref).append(n.message));
}else{
var $li=$('<li></li>').append(n.message);
}
$invalids.append($li);
});
$response.append($invalids);
}
$response.attr('role', 'alert').focus();
});
};
$.ajax({
type: 'POST',
url: wpcf7.apiSettings.getRoute('/contact-forms/' + wpcf7.getId($form) + '/feedback'),
data: formData,
dataType: 'json',
processData: false,
contentType: false
}).done(function(data, status, xhr){
ajaxSuccess(data, status, xhr, $form);
$('.ajax-loader', $form).removeClass('is-active');
}).fail(function(xhr, status, error){
var $e=$('<div class="ajax-error"></div>').text(error.message);
$form.after($e);
});
};
wpcf7.triggerEvent=function(target, name, detail){
var $target=$(target);
var event=new CustomEvent('wpcf7' + name, {
bubbles: true,
detail: detail
});
$target.get(0).dispatchEvent(event);
$target.trigger('wpcf7:' + name, detail);
$target.trigger(name + '.wpcf7', detail);
};
wpcf7.toggleSubmit=function(form, state){
var $form=$(form);
var $submit=$('input:submit', $form);
if(typeof state!=='undefined'){
$submit.prop('disabled', ! state);
return;
}
if($form.hasClass('wpcf7-acceptance-as-validation')){
return;
}
$submit.prop('disabled', false);
$('.wpcf7-acceptance', $form).each(function(){
var $span=$(this);
var $input=$('input:checkbox', $span);
if(! $span.hasClass('optional')){
if($span.hasClass('invert')&&$input.is(':checked')
|| ! $span.hasClass('invert')&&! $input.is(':checked')){
$submit.prop('disabled', true);
return false;
}}
});
};
wpcf7.notValidTip=function(target, message){
var $target=$(target);
$('.wpcf7-not-valid-tip', $target).remove();
$('<span role="alert" class="wpcf7-not-valid-tip"></span>')
.text(message).appendTo($target);
if($target.is('.use-floating-validation-tip *')){
var fadeOut=function(target){
$(target).not(':hidden').animate({
opacity: 0
}, 'fast', function(){
$(this).css({ 'z-index': -100 });
});
};
$target.on('mouseover', '.wpcf7-not-valid-tip', function(){
fadeOut(this);
});
$target.on('focus', ':input', function(){
fadeOut($('.wpcf7-not-valid-tip', $target));
});
}};
wpcf7.refill=function(form, data){
var $form=$(form);
var refillCaptcha=function($form, items){
$.each(items, function(i, n){
$form.find(':input[name="' + i + '"]').val('');
$form.find('img.wpcf7-captcha-' + i).attr('src', n);
var match=/([0-9]+)\.(png|gif|jpeg)$/.exec(n);
$form.find('input:hidden[name="_wpcf7_captcha_challenge_' + i + '"]').attr('value', match[ 1 ]);
});
};
var refillQuiz=function($form, items){
$.each(items, function(i, n){
$form.find(':input[name="' + i + '"]').val('');
$form.find(':input[name="' + i + '"]').siblings('span.wpcf7-quiz-label').text(n[ 0 ]);
$form.find('input:hidden[name="_wpcf7_quiz_answer_' + i + '"]').attr('value', n[ 1 ]);
});
};
if(typeof data==='undefined'){
$.ajax({
type: 'GET',
url: wpcf7.apiSettings.getRoute('/contact-forms/' + wpcf7.getId($form) + '/refill'),
beforeSend: function(xhr){
var nonce=$form.find(':input[name="_wpnonce"]').val();
if(nonce){
xhr.setRequestHeader('X-WP-Nonce', nonce);
}},
dataType: 'json'
}).done(function(data, status, xhr){
if(data.captcha){
refillCaptcha($form, data.captcha);
}
if(data.quiz){
refillQuiz($form, data.quiz);
}});
}else{
if(data.captcha){
refillCaptcha($form, data.captcha);
}
if(data.quiz){
refillQuiz($form, data.quiz);
}}
};
wpcf7.clearResponse=function(form){
var $form=$(form);
$form.removeClass('invalid spam sent failed');
$form.siblings('.screen-reader-response').html('').attr('role', '');
$('.wpcf7-not-valid-tip', $form).remove();
$('[aria-invalid]', $form).attr('aria-invalid', 'false');
$('.wpcf7-form-control', $form).removeClass('wpcf7-not-valid');
$('.wpcf7-response-output', $form)
.hide().empty().removeAttr('role')
.removeClass('wpcf7-mail-sent-ok wpcf7-mail-sent-ng wpcf7-validation-errors wpcf7-spam-blocked');
};
wpcf7.apiSettings.getRoute=function(path){
var url=wpcf7.apiSettings.root;
url=url.replace(wpcf7.apiSettings.namespace,
wpcf7.apiSettings.namespace + path);
return url;
};})(jQuery);
(function (){
if(typeof window.CustomEvent==="function") return false;
function CustomEvent(event, params){
params=params||{ bubbles: false, cancelable: false, detail: undefined };
var evt=document.createEvent('CustomEvent');
evt.initCustomEvent(event,
params.bubbles, params.cancelable, params.detail);
return evt;
}
CustomEvent.prototype=window.Event.prototype;
window.CustomEvent=CustomEvent;
})();
(function(e,t){'object'==typeof exports&&'undefined'!=typeof module?module.exports=t():'function'==typeof define&&define.amd?define(t):e.Popper=t()})(this,function(){'use strict';function e(e){return e&&'[object Function]'==={}.toString.call(e)}function t(e,t){if(1!==e.nodeType)return[];var o=getComputedStyle(e,null);return t?o[t]:o}function o(e){return'HTML'===e.nodeName?e:e.parentNode||e.host}function n(e){if(!e)return document.body;switch(e.nodeName){case'HTML':case'BODY':return e.ownerDocument.body;case'#document':return e.body;}var i=t(e),r=i.overflow,p=i.overflowX,s=i.overflowY;return /(auto|scroll|overlay)/.test(r+s+p)?e:n(o(e))}function r(e){return 11===e?re:10===e?pe:re||pe}function p(e){if(!e)return document.documentElement;for(var o=r(10)?document.body:null,n=e.offsetParent;n===o&&e.nextElementSibling;)n=(e=e.nextElementSibling).offsetParent;var i=n&&n.nodeName;return i&&'BODY'!==i&&'HTML'!==i?-1!==['TD','TABLE'].indexOf(n.nodeName)&&'static'===t(n,'position')?p(n):n:e?e.ownerDocument.documentElement:document.documentElement}function s(e){var t=e.nodeName;return'BODY'!==t&&('HTML'===t||p(e.firstElementChild)===e)}function d(e){return null===e.parentNode?e:d(e.parentNode)}function a(e,t){if(!e||!e.nodeType||!t||!t.nodeType)return document.documentElement;var o=e.compareDocumentPosition(t)&Node.DOCUMENT_POSITION_FOLLOWING,n=o?e:t,i=o?t:e,r=document.createRange();r.setStart(n,0),r.setEnd(i,0);var l=r.commonAncestorContainer;if(e!==l&&t!==l||n.contains(i))return s(l)?l:p(l);var f=d(e);return f.host?a(f.host,t):a(e,d(t).host)}function l(e){var t=1<arguments.length&&void 0!==arguments[1]?arguments[1]:'top',o='top'===t?'scrollTop':'scrollLeft',n=e.nodeName;if('BODY'===n||'HTML'===n){var i=e.ownerDocument.documentElement,r=e.ownerDocument.scrollingElement||i;return r[o]}return e[o]}function f(e,t){var o=2<arguments.length&&void 0!==arguments[2]&&arguments[2],n=l(t,'top'),i=l(t,'left'),r=o?-1:1;return e.top+=n*r,e.bottom+=n*r,e.left+=i*r,e.right+=i*r,e}function m(e,t){var o='x'===t?'Left':'Top',n='Left'==o?'Right':'Bottom';return parseFloat(e['border'+o+'Width'],10)+parseFloat(e['border'+n+'Width'],10)}function h(e,t,o,n){return $(t['offset'+e],t['scroll'+e],o['client'+e],o['offset'+e],o['scroll'+e],r(10)?o['offset'+e]+n['margin'+('Height'===e?'Top':'Left')]+n['margin'+('Height'===e?'Bottom':'Right')]:0)}function c(){var e=document.body,t=document.documentElement,o=r(10)&&getComputedStyle(t);return{height:h('Height',e,t,o),width:h('Width',e,t,o)}}function g(e){return le({},e,{right:e.left+e.width,bottom:e.top+e.height})}function u(e){var o={};try{if(r(10)){o=e.getBoundingClientRect();var n=l(e,'top'),i=l(e,'left');o.top+=n,o.left+=i,o.bottom+=n,o.right+=i}else o=e.getBoundingClientRect()}catch(t){}var p={left:o.left,top:o.top,width:o.right-o.left,height:o.bottom-o.top},s='HTML'===e.nodeName?c():{},d=s.width||e.clientWidth||p.right-p.left,a=s.height||e.clientHeight||p.bottom-p.top,f=e.offsetWidth-d,h=e.offsetHeight-a;if(f||h){var u=t(e);f-=m(u,'x'),h-=m(u,'y'),p.width-=f,p.height-=h}return g(p)}function b(e,o){var i=2<arguments.length&&void 0!==arguments[2]&&arguments[2],p=r(10),s='HTML'===o.nodeName,d=u(e),a=u(o),l=n(e),m=t(o),h=parseFloat(m.borderTopWidth,10),c=parseFloat(m.borderLeftWidth,10);i&&'HTML'===o.nodeName&&(a.top=$(a.top,0),a.left=$(a.left,0));var b=g({top:d.top-a.top-h,left:d.left-a.left-c,width:d.width,height:d.height});if(b.marginTop=0,b.marginLeft=0,!p&&s){var y=parseFloat(m.marginTop,10),w=parseFloat(m.marginLeft,10);b.top-=h-y,b.bottom-=h-y,b.left-=c-w,b.right-=c-w,b.marginTop=y,b.marginLeft=w}return(p&&!i?o.contains(l):o===l&&'BODY'!==l.nodeName)&&(b=f(b,o)),b}function y(e){var t=1<arguments.length&&void 0!==arguments[1]&&arguments[1],o=e.ownerDocument.documentElement,n=b(e,o),i=$(o.clientWidth,window.innerWidth||0),r=$(o.clientHeight,window.innerHeight||0),p=t?0:l(o),s=t?0:l(o,'left'),d={top:p-n.top+n.marginTop,left:s-n.left+n.marginLeft,width:i,height:r};return g(d)}function w(e){var n=e.nodeName;return'BODY'===n||'HTML'===n?!1:'fixed'===t(e,'position')||w(o(e))}function E(e){if(!e||!e.parentElement||r())return document.documentElement;for(var o=e.parentElement;o&&'none'===t(o,'transform');)o=o.parentElement;return o||document.documentElement}function v(e,t,i,r){var p=4<arguments.length&&void 0!==arguments[4]&&arguments[4],s={top:0,left:0},d=p?E(e):a(e,t);if('viewport'===r)s=y(d,p);else{var l;'scrollParent'===r?(l=n(o(t)),'BODY'===l.nodeName&&(l=e.ownerDocument.documentElement)):'window'===r?l=e.ownerDocument.documentElement:l=r;var f=b(l,d,p);if('HTML'===l.nodeName&&!w(d)){var m=c(),h=m.height,g=m.width;s.top+=f.top-f.marginTop,s.bottom=h+f.top,s.left+=f.left-f.marginLeft,s.right=g+f.left}else s=f}return s.left+=i,s.top+=i,s.right-=i,s.bottom-=i,s}function x(e){var t=e.width,o=e.height;return t*o}function O(e,t,o,n,i){var r=5<arguments.length&&void 0!==arguments[5]?arguments[5]:0;if(-1===e.indexOf('auto'))return e;var p=v(o,n,r,i),s={top:{width:p.width,height:t.top-p.top},right:{width:p.right-t.right,height:p.height},bottom:{width:p.width,height:p.bottom-t.bottom},left:{width:t.left-p.left,height:p.height}},d=Object.keys(s).map(function(e){return le({key:e},s[e],{area:x(s[e])})}).sort(function(e,t){return t.area-e.area}),a=d.filter(function(e){var t=e.width,n=e.height;return t>=o.clientWidth&&n>=o.clientHeight}),l=0<a.length?a[0].key:d[0].key,f=e.split('-')[1];return l+(f?'-'+f:'')}function L(e,t,o){var n=3<arguments.length&&void 0!==arguments[3]?arguments[3]:null,i=n?E(t):a(t,o);return b(o,i,n)}function S(e){var t=getComputedStyle(e),o=parseFloat(t.marginTop)+parseFloat(t.marginBottom),n=parseFloat(t.marginLeft)+parseFloat(t.marginRight),i={width:e.offsetWidth+n,height:e.offsetHeight+o};return i}function T(e){var t={left:'right',right:'left',bottom:'top',top:'bottom'};return e.replace(/left|right|bottom|top/g,function(e){return t[e]})}function C(e,t,o){o=o.split('-')[0];var n=S(e),i={width:n.width,height:n.height},r=-1!==['right','left'].indexOf(o),p=r?'top':'left',s=r?'left':'top',d=r?'height':'width',a=r?'width':'height';return i[p]=t[p]+t[d]/2-n[d]/2,i[s]=o===s?t[s]-n[a]:t[T(s)],i}function D(e,t){return Array.prototype.find?e.find(t):e.filter(t)[0]}function N(e,t,o){if(Array.prototype.findIndex)return e.findIndex(function(e){return e[t]===o});var n=D(e,function(e){return e[t]===o});return e.indexOf(n)}function P(t,o,n){var i=void 0===n?t:t.slice(0,N(t,'name',n));return i.forEach(function(t){t['function']&&console.warn('`modifier.function` is deprecated, use `modifier.fn`!');var n=t['function']||t.fn;t.enabled&&e(n)&&(o.offsets.popper=g(o.offsets.popper),o.offsets.reference=g(o.offsets.reference),o=n(o,t))}),o}function k(){if(!this.state.isDestroyed){var e={instance:this,styles:{},arrowStyles:{},attributes:{},flipped:!1,offsets:{}};e.offsets.reference=L(this.state,this.popper,this.reference,this.options.positionFixed),e.placement=O(this.options.placement,e.offsets.reference,this.popper,this.reference,this.options.modifiers.flip.boundariesElement,this.options.modifiers.flip.padding),e.originalPlacement=e.placement,e.positionFixed=this.options.positionFixed,e.offsets.popper=C(this.popper,e.offsets.reference,e.placement),e.offsets.popper.position=this.options.positionFixed?'fixed':'absolute',e=P(this.modifiers,e),this.state.isCreated?this.options.onUpdate(e):(this.state.isCreated=!0,this.options.onCreate(e))}}function W(e,t){return e.some(function(e){var o=e.name,n=e.enabled;return n&&o===t})}function B(e){for(var t=[!1,'ms','Webkit','Moz','O'],o=e.charAt(0).toUpperCase()+e.slice(1),n=0;n<t.length;n++){var i=t[n],r=i?''+i+o:e;if('undefined'!=typeof document.body.style[r])return r}return null}function H(){return this.state.isDestroyed=!0,W(this.modifiers,'applyStyle')&&(this.popper.removeAttribute('x-placement'),this.popper.style.position='',this.popper.style.top='',this.popper.style.left='',this.popper.style.right='',this.popper.style.bottom='',this.popper.style.willChange='',this.popper.style[B('transform')]=''),this.disableEventListeners(),this.options.removeOnDestroy&&this.popper.parentNode.removeChild(this.popper),this}function A(e){var t=e.ownerDocument;return t?t.defaultView:window}function M(e,t,o,i){var r='BODY'===e.nodeName,p=r?e.ownerDocument.defaultView:e;p.addEventListener(t,o,{passive:!0}),r||M(n(p.parentNode),t,o,i),i.push(p)}function I(e,t,o,i){o.updateBound=i,A(e).addEventListener('resize',o.updateBound,{passive:!0});var r=n(e);return M(r,'scroll',o.updateBound,o.scrollParents),o.scrollElement=r,o.eventsEnabled=!0,o}function F(){this.state.eventsEnabled||(this.state=I(this.reference,this.options,this.state,this.scheduleUpdate))}function R(e,t){return A(e).removeEventListener('resize',t.updateBound),t.scrollParents.forEach(function(e){e.removeEventListener('scroll',t.updateBound)}),t.updateBound=null,t.scrollParents=[],t.scrollElement=null,t.eventsEnabled=!1,t}function U(){this.state.eventsEnabled&&(cancelAnimationFrame(this.scheduleUpdate),this.state=R(this.reference,this.state))}function Y(e){return''!==e&&!isNaN(parseFloat(e))&&isFinite(e)}function j(e,t){Object.keys(t).forEach(function(o){var n='';-1!==['width','height','top','right','bottom','left'].indexOf(o)&&Y(t[o])&&(n='px'),e.style[o]=t[o]+n})}function K(e,t){Object.keys(t).forEach(function(o){var n=t[o];!1===n?e.removeAttribute(o):e.setAttribute(o,t[o])})}function q(e,t,o){var n=D(e,function(e){var o=e.name;return o===t}),i=!!n&&e.some(function(e){return e.name===o&&e.enabled&&e.order<n.order});if(!i){var r='`'+t+'`';console.warn('`'+o+'`'+' modifier is required by '+r+' modifier in order to work, be sure to include it before '+r+'!')}return i}function G(e){return'end'===e?'start':'start'===e?'end':e}function z(e){var t=1<arguments.length&&void 0!==arguments[1]&&arguments[1],o=me.indexOf(e),n=me.slice(o+1).concat(me.slice(0,o));return t?n.reverse():n}function V(e,t,o,n){var i=e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),r=+i[1],p=i[2];if(!r)return e;if(0===p.indexOf('%')){var s;switch(p){case'%p':s=o;break;case'%':case'%r':default:s=n;}var d=g(s);return d[t]/100*r}if('vh'===p||'vw'===p){var a;return a='vh'===p?$(document.documentElement.clientHeight,window.innerHeight||0):$(document.documentElement.clientWidth,window.innerWidth||0),a/100*r}return r}function _(e,t,o,n){var i=[0,0],r=-1!==['right','left'].indexOf(n),p=e.split(/(\+|\-)/).map(function(e){return e.trim()}),s=p.indexOf(D(p,function(e){return-1!==e.search(/,|\s/)}));p[s]&&-1===p[s].indexOf(',')&&console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');var d=/\s*,\s*|\s+/,a=-1===s?[p]:[p.slice(0,s).concat([p[s].split(d)[0]]),[p[s].split(d)[1]].concat(p.slice(s+1))];return a=a.map(function(e,n){var i=(1===n?!r:r)?'height':'width',p=!1;return e.reduce(function(e,t){return''===e[e.length-1]&&-1!==['+','-'].indexOf(t)?(e[e.length-1]=t,p=!0,e):p?(e[e.length-1]+=t,p=!1,e):e.concat(t)},[]).map(function(e){return V(e,i,t,o)})}),a.forEach(function(e,t){e.forEach(function(o,n){Y(o)&&(i[t]+=o*('-'===e[n-1]?-1:1))})}),i}function X(e,t){var o,n=t.offset,i=e.placement,r=e.offsets,p=r.popper,s=r.reference,d=i.split('-')[0];return o=Y(+n)?[+n,0]:_(n,p,s,d),'left'===d?(p.top+=o[0],p.left-=o[1]):'right'===d?(p.top+=o[0],p.left+=o[1]):'top'===d?(p.left+=o[0],p.top-=o[1]):'bottom'===d&&(p.left+=o[0],p.top+=o[1]),e.popper=p,e}for(var J=Math.min,Q=Math.round,Z=Math.floor,$=Math.max,ee='undefined'!=typeof window&&'undefined'!=typeof document,te=['Edge','Trident','Firefox'],oe=0,ne=0;ne<te.length;ne+=1)if(ee&&0<=navigator.userAgent.indexOf(te[ne])){oe=1;break}var i=ee&&window.Promise,ie=i?function(e){var t=!1;return function(){t||(t=!0,window.Promise.resolve().then(function(){t=!1,e()}))}}:function(e){var t=!1;return function(){t||(t=!0,setTimeout(function(){t=!1,e()},oe))}},re=ee&&!!(window.MSInputMethodContext&&document.documentMode),pe=ee&&/MSIE 10/.test(navigator.userAgent),se=function(e,t){if(!(e instanceof t))throw new TypeError('Cannot call a class as a function')},de=function(){function e(e,t){for(var o,n=0;n<t.length;n++)o=t[n],o.enumerable=o.enumerable||!1,o.configurable=!0,'value'in o&&(o.writable=!0),Object.defineProperty(e,o.key,o)}return function(t,o,n){return o&&e(t.prototype,o),n&&e(t,n),t}}(),ae=function(e,t,o){return t in e?Object.defineProperty(e,t,{value:o,enumerable:!0,configurable:!0,writable:!0}):e[t]=o,e},le=Object.assign||function(e){for(var t,o=1;o<arguments.length;o++)for(var n in t=arguments[o],t)Object.prototype.hasOwnProperty.call(t,n)&&(e[n]=t[n]);return e},fe=['auto-start','auto','auto-end','top-start','top','top-end','right-start','right','right-end','bottom-end','bottom','bottom-start','left-end','left','left-start'],me=fe.slice(3),he={FLIP:'flip',CLOCKWISE:'clockwise',COUNTERCLOCKWISE:'counterclockwise'},ce=function(){function t(o,n){var i=this,r=2<arguments.length&&void 0!==arguments[2]?arguments[2]:{};se(this,t),this.scheduleUpdate=function(){return requestAnimationFrame(i.update)},this.update=ie(this.update.bind(this)),this.options=le({},t.Defaults,r),this.state={isDestroyed:!1,isCreated:!1,scrollParents:[]},this.reference=o&&o.jquery?o[0]:o,this.popper=n&&n.jquery?n[0]:n,this.options.modifiers={},Object.keys(le({},t.Defaults.modifiers,r.modifiers)).forEach(function(e){i.options.modifiers[e]=le({},t.Defaults.modifiers[e]||{},r.modifiers?r.modifiers[e]:{})}),this.modifiers=Object.keys(this.options.modifiers).map(function(e){return le({name:e},i.options.modifiers[e])}).sort(function(e,t){return e.order-t.order}),this.modifiers.forEach(function(t){t.enabled&&e(t.onLoad)&&t.onLoad(i.reference,i.popper,i.options,t,i.state)}),this.update();var p=this.options.eventsEnabled;p&&this.enableEventListeners(),this.state.eventsEnabled=p}return de(t,[{key:'update',value:function(){return k.call(this)}},{key:'destroy',value:function(){return H.call(this)}},{key:'enableEventListeners',value:function(){return F.call(this)}},{key:'disableEventListeners',value:function(){return U.call(this)}}]),t}();return ce.Utils=('undefined'==typeof window?global:window).PopperUtils,ce.placements=fe,ce.Defaults={placement:'bottom',positionFixed:!1,eventsEnabled:!0,removeOnDestroy:!1,onCreate:function(){},onUpdate:function(){},modifiers:{shift:{order:100,enabled:!0,fn:function(e){var t=e.placement,o=t.split('-')[0],n=t.split('-')[1];if(n){var i=e.offsets,r=i.reference,p=i.popper,s=-1!==['bottom','top'].indexOf(o),d=s?'left':'top',a=s?'width':'height',l={start:ae({},d,r[d]),end:ae({},d,r[d]+r[a]-p[a])};e.offsets.popper=le({},p,l[n])}return e}},offset:{order:200,enabled:!0,fn:X,offset:0},preventOverflow:{order:300,enabled:!0,fn:function(e,t){var o=t.boundariesElement||p(e.instance.popper);e.instance.reference===o&&(o=p(o));var n=B('transform'),i=e.instance.popper.style,r=i.top,s=i.left,d=i[n];i.top='',i.left='',i[n]='';var a=v(e.instance.popper,e.instance.reference,t.padding,o,e.positionFixed);i.top=r,i.left=s,i[n]=d,t.boundaries=a;var l=t.priority,f=e.offsets.popper,m={primary:function(e){var o=f[e];return f[e]<a[e]&&!t.escapeWithReference&&(o=$(f[e],a[e])),ae({},e,o)},secondary:function(e){var o='right'===e?'left':'top',n=f[o];return f[e]>a[e]&&!t.escapeWithReference&&(n=J(f[o],a[e]-('right'===e?f.width:f.height))),ae({},o,n)}};return l.forEach(function(e){var t=-1===['left','top'].indexOf(e)?'secondary':'primary';f=le({},f,m[t](e))}),e.offsets.popper=f,e},priority:['left','right','top','bottom'],padding:5,boundariesElement:'scrollParent'},keepTogether:{order:400,enabled:!0,fn:function(e){var t=e.offsets,o=t.popper,n=t.reference,i=e.placement.split('-')[0],r=Z,p=-1!==['top','bottom'].indexOf(i),s=p?'right':'bottom',d=p?'left':'top',a=p?'width':'height';return o[s]<r(n[d])&&(e.offsets.popper[d]=r(n[d])-o[a]),o[d]>r(n[s])&&(e.offsets.popper[d]=r(n[s])),e}},arrow:{order:500,enabled:!0,fn:function(e,o){var n;if(!q(e.instance.modifiers,'arrow','keepTogether'))return e;var i=o.element;if('string'==typeof i){if(i=e.instance.popper.querySelector(i),!i)return e;}else if(!e.instance.popper.contains(i))return console.warn('WARNING: `arrow.element` must be child of its popper element!'),e;var r=e.placement.split('-')[0],p=e.offsets,s=p.popper,d=p.reference,a=-1!==['left','right'].indexOf(r),l=a?'height':'width',f=a?'Top':'Left',m=f.toLowerCase(),h=a?'left':'top',c=a?'bottom':'right',u=S(i)[l];d[c]-u<s[m]&&(e.offsets.popper[m]-=s[m]-(d[c]-u)),d[m]+u>s[c]&&(e.offsets.popper[m]+=d[m]+u-s[c]),e.offsets.popper=g(e.offsets.popper);var b=d[m]+d[l]/2-u/2,y=t(e.instance.popper),w=parseFloat(y['margin'+f],10),E=parseFloat(y['border'+f+'Width'],10),v=b-e.offsets.popper[m]-w-E;return v=$(J(s[l]-u,v),0),e.arrowElement=i,e.offsets.arrow=(n={},ae(n,m,Q(v)),ae(n,h,''),n),e},element:'[x-arrow]'},flip:{order:600,enabled:!0,fn:function(e,t){if(W(e.instance.modifiers,'inner'))return e;if(e.flipped&&e.placement===e.originalPlacement)return e;var o=v(e.instance.popper,e.instance.reference,t.padding,t.boundariesElement,e.positionFixed),n=e.placement.split('-')[0],i=T(n),r=e.placement.split('-')[1]||'',p=[];switch(t.behavior){case he.FLIP:p=[n,i];break;case he.CLOCKWISE:p=z(n);break;case he.COUNTERCLOCKWISE:p=z(n,!0);break;default:p=t.behavior;}return p.forEach(function(s,d){if(n!==s||p.length===d+1)return e;n=e.placement.split('-')[0],i=T(n);var a=e.offsets.popper,l=e.offsets.reference,f=Z,m='left'===n&&f(a.right)>f(l.left)||'right'===n&&f(a.left)<f(l.right)||'top'===n&&f(a.bottom)>f(l.top)||'bottom'===n&&f(a.top)<f(l.bottom),h=f(a.left)<f(o.left),c=f(a.right)>f(o.right),g=f(a.top)<f(o.top),u=f(a.bottom)>f(o.bottom),b='left'===n&&h||'right'===n&&c||'top'===n&&g||'bottom'===n&&u,y=-1!==['top','bottom'].indexOf(n),w=!!t.flipVariations&&(y&&'start'===r&&h||y&&'end'===r&&c||!y&&'start'===r&&g||!y&&'end'===r&&u);(m||b||w)&&(e.flipped=!0,(m||b)&&(n=p[d+1]),w&&(r=G(r)),e.placement=n+(r?'-'+r:''),e.offsets.popper=le({},e.offsets.popper,C(e.instance.popper,e.offsets.reference,e.placement)),e=P(e.instance.modifiers,e,'flip'))}),e},behavior:'flip',padding:5,boundariesElement:'viewport'},inner:{order:700,enabled:!1,fn:function(e){var t=e.placement,o=t.split('-')[0],n=e.offsets,i=n.popper,r=n.reference,p=-1!==['left','right'].indexOf(o),s=-1===['top','left'].indexOf(o);return i[p?'left':'top']=r[o]-(s?i[p?'width':'height']:0),e.placement=T(t),e.offsets.popper=g(i),e}},hide:{order:800,enabled:!0,fn:function(e){if(!q(e.instance.modifiers,'hide','preventOverflow'))return e;var t=e.offsets.reference,o=D(e.instance.modifiers,function(e){return'preventOverflow'===e.name}).boundaries;if(t.bottom<o.top||t.left>o.right||t.top>o.bottom||t.right<o.left){if(!0===e.hide)return e;e.hide=!0,e.attributes['x-out-of-boundaries']=''}else{if(!1===e.hide)return e;e.hide=!1,e.attributes['x-out-of-boundaries']=!1}return e}},computeStyle:{order:850,enabled:!0,fn:function(e,t){var o=t.x,n=t.y,i=e.offsets.popper,r=D(e.instance.modifiers,function(e){return'applyStyle'===e.name}).gpuAcceleration;void 0!==r&&console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');var s,d,a=void 0===r?t.gpuAcceleration:r,l=p(e.instance.popper),f=u(l),m={position:i.position},h={left:Z(i.left),top:Q(i.top),bottom:Q(i.bottom),right:Z(i.right)},c='bottom'===o?'top':'bottom',g='right'===n?'left':'right',b=B('transform');if(d='bottom'==c?-f.height+h.bottom:h.top,s='right'==g?-f.width+h.right:h.left,a&&b)m[b]='translate3d('+s+'px, '+d+'px, 0)',m[c]=0,m[g]=0,m.willChange='transform';else{var y='bottom'==c?-1:1,w='right'==g?-1:1;m[c]=d*y,m[g]=s*w,m.willChange=c+', '+g}var E={"x-placement":e.placement};return e.attributes=le({},E,e.attributes),e.styles=le({},m,e.styles),e.arrowStyles=le({},e.offsets.arrow,e.arrowStyles),e},gpuAcceleration:!0,x:'bottom',y:'right'},applyStyle:{order:900,enabled:!0,fn:function(e){return j(e.instance.popper,e.styles),K(e.instance.popper,e.attributes),e.arrowElement&&Object.keys(e.arrowStyles).length&&j(e.arrowElement,e.arrowStyles),e},onLoad:function(e,t,o,n,i){var r=L(i,t,e,o.positionFixed),p=O(o.placement,r,t,e,o.modifiers.flip.boundariesElement,o.modifiers.flip.padding);return t.setAttribute('x-placement',p),j(t,{position:o.positionFixed?'fixed':'absolute'}),o},gpuAcceleration:void 0}}},ce});
!function(t,e){"object"==typeof exports&&"undefined"!=typeof module?e(exports,require("jquery"),require("popper.js")):"function"==typeof define&&define.amd?define(["exports","jquery","popper.js"],e):e(t.bootstrap={},t.jQuery,t.Popper)}(this,function(t,e,c){"use strict";function i(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function o(t,e,n){return e&&i(t.prototype,e),n&&i(t,n),t}function u(){return(u=Object.assign||function(t){for(var e=1;e<arguments.length;e++){var n=arguments[e];for(var i in n)Object.prototype.hasOwnProperty.call(n,i)&&(t[i]=n[i])}return t}).apply(this,arguments)}e=e&&e.hasOwnProperty("default")?e.default:e,c=c&&c.hasOwnProperty("default")?c.default:c;var s,n,r,a,l,h,d,f,_,g,m,p,v,E,T,C,y,I,A,b,D,S,w,N,O,k,P,L,R,j,H,x,W,M,U,K,V,B,Q,F,Y,G,q,z,X,Z,$,J,tt,et,nt,it,st,rt,ot,at,lt,ht,ct,ut,dt,ft,_t,gt,mt,pt,vt,Et,Tt,Ct,yt,It,At,bt,Dt,St,wt,Nt,Ot,kt,Pt,Lt,Rt,jt,Ht,xt,Wt,Mt,Ut,Kt,Vt,Bt,Qt,Ft,Yt,Gt,qt,zt,Xt,Zt,$t,Jt,te,ee,ne,ie,se,re,oe,ae,le,he,ce,ue,de,fe,_e,ge,me,pe,ve,Ee,Te,Ce,ye,Ie,Ae,be,De,Se,we,Ne,Oe,ke,Pe,Le,Re,je,He,xe,We,Me,Ue,Ke,Ve,Be,Qe,Fe,Ye,Ge,qe,ze,Xe,Ze,$e,Je,tn,en,nn,sn,rn,on,an,ln,hn,cn,un,dn,fn,_n,gn,mn,pn,vn,En,Tn,Cn,yn,In=function(i){var e=!1;function t(t){var e=this,n=!1;return i(this).one(l.TRANSITION_END,function(){n=!0}),setTimeout(function(){n||l.triggerTransitionEnd(e)},t),this}var l={TRANSITION_END:"bsTransitionEnd",getUID:function(t){for(;t+=~~(1e6*Math.random()),document.getElementById(t););return t},getSelectorFromElement:function(t){var e,n=t.getAttribute("data-target");n&&"#"!==n||(n=t.getAttribute("href")||""),"#"===n.charAt(0)&&(e=n,n=e="function"==typeof i.escapeSelector?i.escapeSelector(e).substr(1):e.replace(/(:|\.|\[|\]|,|=|@)/g,"\\$1"));try{return 0<i(document).find(n).length?n:null}catch(t){return null}},reflow:function(t){return t.offsetHeight},triggerTransitionEnd:function(t){i(t).trigger(e.end)},supportsTransitionEnd:function(){return Boolean(e)},isElement:function(t){return(t[0]||t).nodeType},typeCheckConfig:function(t,e,n){for(var i in n)if(Object.prototype.hasOwnProperty.call(n,i)){var s=n[i],r=e[i],o=r&&l.isElement(r)?"element":(a=r,{}.toString.call(a).match(/\s([a-zA-Z]+)/)[1].toLowerCase());if(!new RegExp(s).test(o))throw new Error(t.toUpperCase()+': Option "'+i+'" provided type "'+o+'" but expected type "'+s+'".')}var a}};return e=("undefined"==typeof window||!window.QUnit)&&{end:"transitionend"},i.fn.emulateTransitionEnd=t,l.supportsTransitionEnd()&&(i.event.special[l.TRANSITION_END]={bindType:e.end,delegateType:e.end,handle:function(t){if(i(t.target).is(this))return t.handleObj.handler.apply(this,arguments)}}),l}(e),An=(n="alert",a="."+(r="bs.alert"),l=(s=e).fn[n],h={CLOSE:"close"+a,CLOSED:"closed"+a,CLICK_DATA_API:"click"+a+".data-api"},d="alert",f="fade",_="show",g=function(){function i(t){this._element=t}var t=i.prototype;return t.close=function(t){t=t||this._element;var e=this._getRootElement(t);this._triggerCloseEvent(e).isDefaultPrevented()||this._removeElement(e)},t.dispose=function(){s.removeData(this._element,r),this._element=null},t._getRootElement=function(t){var e=In.getSelectorFromElement(t),n=!1;return e&&(n=s(e)[0]),n||(n=s(t).closest("."+d)[0]),n},t._triggerCloseEvent=function(t){var e=s.Event(h.CLOSE);return s(t).trigger(e),e},t._removeElement=function(e){var n=this;s(e).removeClass(_),In.supportsTransitionEnd()&&s(e).hasClass(f)?s(e).one(In.TRANSITION_END,function(t){return n._destroyElement(e,t)}).emulateTransitionEnd(150):this._destroyElement(e)},t._destroyElement=function(t){s(t).detach().trigger(h.CLOSED).remove()},i._jQueryInterface=function(n){return this.each(function(){var t=s(this),e=t.data(r);e||(e=new i(this),t.data(r,e)),"close"===n&&e[n](this)})},i._handleDismiss=function(e){return function(t){t&&t.preventDefault(),e.close(this)}},o(i,null,[{key:"VERSION",get:function(){return"4.0.0"}}]),i}(),s(document).on(h.CLICK_DATA_API,'[data-dismiss="alert"]',g._handleDismiss(new g)),s.fn[n]=g._jQueryInterface,s.fn[n].Constructor=g,s.fn[n].noConflict=function(){return s.fn[n]=l,g._jQueryInterface},g),bn=(p="button",E="."+(v="bs.button"),T=".data-api",C=(m=e).fn[p],y="active",I="btn",b='[data-toggle^="button"]',D='[data-toggle="buttons"]',S="input",w=".active",N=".btn",O={CLICK_DATA_API:"click"+E+T,FOCUS_BLUR_DATA_API:(A="focus")+E+T+" blur"+E+T},k=function(){function n(t){this._element=t}var t=n.prototype;return t.toggle=function(){var t=!0,e=!0,n=m(this._element).closest(D)[0];if(n){var i=m(this._element).find(S)[0];if(i){if("radio"===i.type)if(i.checked&&m(this._element).hasClass(y))t=!1;else{var s=m(n).find(w)[0];s&&m(s).removeClass(y)}if(t){if(i.hasAttribute("disabled")||n.hasAttribute("disabled")||i.classList.contains("disabled")||n.classList.contains("disabled"))return;i.checked=!m(this._element).hasClass(y),m(i).trigger("change")}i.focus(),e=!1}}e&&this._element.setAttribute("aria-pressed",!m(this._element).hasClass(y)),t&&m(this._element).toggleClass(y)},t.dispose=function(){m.removeData(this._element,v),this._element=null},n._jQueryInterface=function(e){return this.each(function(){var t=m(this).data(v);t||(t=new n(this),m(this).data(v,t)),"toggle"===e&&t[e]()})},o(n,null,[{key:"VERSION",get:function(){return"4.0.0"}}]),n}(),m(document).on(O.CLICK_DATA_API,b,function(t){t.preventDefault();var e=t.target;m(e).hasClass(I)||(e=m(e).closest(N)),k._jQueryInterface.call(m(e),"toggle")}).on(O.FOCUS_BLUR_DATA_API,b,function(t){var e=m(t.target).closest(N)[0];m(e).toggleClass(A,/^focus(in)?$/.test(t.type))}),m.fn[p]=k._jQueryInterface,m.fn[p].Constructor=k,m.fn[p].noConflict=function(){return m.fn[p]=C,k._jQueryInterface},k),Dn=(L="carousel",j="."+(R="bs.carousel"),H=".data-api",x=(P=e).fn[L],W={interval:5e3,keyboard:!0,slide:!1,pause:"hover",wrap:!0},M={interval:"(number|boolean)",keyboard:"boolean",slide:"(boolean|string)",pause:"(string|boolean)",wrap:"boolean"},U="next",K="prev",V="left",B="right",Q={SLIDE:"slide"+j,SLID:"slid"+j,KEYDOWN:"keydown"+j,MOUSEENTER:"mouseenter"+j,MOUSELEAVE:"mouseleave"+j,TOUCHEND:"touchend"+j,LOAD_DATA_API:"load"+j+H,CLICK_DATA_API:"click"+j+H},F="carousel",Y="active",G="slide",q="carousel-item-right",z="carousel-item-left",X="carousel-item-next",Z="carousel-item-prev",$={ACTIVE:".active",ACTIVE_ITEM:".active.carousel-item",ITEM:".carousel-item",NEXT_PREV:".carousel-item-next, .carousel-item-prev",INDICATORS:".carousel-indicators",DATA_SLIDE:"[data-slide], [data-slide-to]",DATA_RIDE:'[data-ride="carousel"]'},J=function(){function r(t,e){this._items=null,this._interval=null,this._activeElement=null,this._isPaused=!1,this._isSliding=!1,this.touchTimeout=null,this._config=this._getConfig(e),this._element=P(t)[0],this._indicatorsElement=P(this._element).find($.INDICATORS)[0],this._addEventListeners()}var t=r.prototype;return t.next=function(){this._isSliding||this._slide(U)},t.nextWhenVisible=function(){!document.hidden&&P(this._element).is(":visible")&&"hidden"!==P(this._element).css("visibility")&&this.next()},t.prev=function(){this._isSliding||this._slide(K)},t.pause=function(t){t||(this._isPaused=!0),P(this._element).find($.NEXT_PREV)[0]&&In.supportsTransitionEnd()&&(In.triggerTransitionEnd(this._element),this.cycle(!0)),clearInterval(this._interval),this._interval=null},t.cycle=function(t){t||(this._isPaused=!1),this._interval&&(clearInterval(this._interval),this._interval=null),this._config.interval&&!this._isPaused&&(this._interval=setInterval((document.visibilityState?this.nextWhenVisible:this.next).bind(this),this._config.interval))},t.to=function(t){var e=this;this._activeElement=P(this._element).find($.ACTIVE_ITEM)[0];var n=this._getItemIndex(this._activeElement);if(!(t>this._items.length-1||t<0))if(this._isSliding)P(this._element).one(Q.SLID,function(){return e.to(t)});else{if(n===t)return this.pause(),void this.cycle();var i=n<t?U:K;this._slide(i,this._items[t])}},t.dispose=function(){P(this._element).off(j),P.removeData(this._element,R),this._items=null,this._config=null,this._element=null,this._interval=null,this._isPaused=null,this._isSliding=null,this._activeElement=null,this._indicatorsElement=null},t._getConfig=function(t){return t=u({},W,t),In.typeCheckConfig(L,t,M),t},t._addEventListeners=function(){var e=this;this._config.keyboard&&P(this._element).on(Q.KEYDOWN,function(t){return e._keydown(t)}),"hover"===this._config.pause&&(P(this._element).on(Q.MOUSEENTER,function(t){return e.pause(t)}).on(Q.MOUSELEAVE,function(t){return e.cycle(t)}),"ontouchstart"in document.documentElement&&P(this._element).on(Q.TOUCHEND,function(){e.pause(),e.touchTimeout&&clearTimeout(e.touchTimeout),e.touchTimeout=setTimeout(function(t){return e.cycle(t)},500+e._config.interval)}))},t._keydown=function(t){if(!/input|textarea/i.test(t.target.tagName))switch(t.which){case 37:t.preventDefault(),this.prev();break;case 39:t.preventDefault(),this.next()}},t._getItemIndex=function(t){return this._items=P.makeArray(P(t).parent().find($.ITEM)),this._items.indexOf(t)},t._getItemByDirection=function(t,e){var n=t===U,i=t===K,s=this._getItemIndex(e),r=this._items.length-1;if((i&&0===s||n&&s===r)&&!this._config.wrap)return e;var o=(s+(t===K?-1:1))%this._items.length;return-1===o?this._items[this._items.length-1]:this._items[o]},t._triggerSlideEvent=function(t,e){var n=this._getItemIndex(t),i=this._getItemIndex(P(this._element).find($.ACTIVE_ITEM)[0]),s=P.Event(Q.SLIDE,{relatedTarget:t,direction:e,from:i,to:n});return P(this._element).trigger(s),s},t._setActiveIndicatorElement=function(t){if(this._indicatorsElement){P(this._indicatorsElement).find($.ACTIVE).removeClass(Y);var e=this._indicatorsElement.children[this._getItemIndex(t)];e&&P(e).addClass(Y)}},t._slide=function(t,e){var n,i,s,r=this,o=P(this._element).find($.ACTIVE_ITEM)[0],a=this._getItemIndex(o),l=e||o&&this._getItemByDirection(t,o),h=this._getItemIndex(l),c=Boolean(this._interval);if(t===U?(n=z,i=X,s=V):(n=q,i=Z,s=B),l&&P(l).hasClass(Y))this._isSliding=!1;else if(!this._triggerSlideEvent(l,s).isDefaultPrevented()&&o&&l){this._isSliding=!0,c&&this.pause(),this._setActiveIndicatorElement(l);var u=P.Event(Q.SLID,{relatedTarget:l,direction:s,from:a,to:h});In.supportsTransitionEnd()&&P(this._element).hasClass(G)?(P(l).addClass(i),In.reflow(l),P(o).addClass(n),P(l).addClass(n),P(o).one(In.TRANSITION_END,function(){P(l).removeClass(n+" "+i).addClass(Y),P(o).removeClass(Y+" "+i+" "+n),r._isSliding=!1,setTimeout(function(){return P(r._element).trigger(u)},0)}).emulateTransitionEnd(600)):(P(o).removeClass(Y),P(l).addClass(Y),this._isSliding=!1,P(this._element).trigger(u)),c&&this.cycle()}},r._jQueryInterface=function(i){return this.each(function(){var t=P(this).data(R),e=u({},W,P(this).data());"object"==typeof i&&(e=u({},e,i));var n="string"==typeof i?i:e.slide;if(t||(t=new r(this,e),P(this).data(R,t)),"number"==typeof i)t.to(i);else if("string"==typeof n){if(void 0===t[n])throw new TypeError('No method named "'+n+'"');t[n]()}else e.interval&&(t.pause(),t.cycle())})},r._dataApiClickHandler=function(t){var e=In.getSelectorFromElement(this);if(e){var n=P(e)[0];if(n&&P(n).hasClass(F)){var i=u({},P(n).data(),P(this).data()),s=this.getAttribute("data-slide-to");s&&(i.interval=!1),r._jQueryInterface.call(P(n),i),s&&P(n).data(R).to(s),t.preventDefault()}}},o(r,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return W}}]),r}(),P(document).on(Q.CLICK_DATA_API,$.DATA_SLIDE,J._dataApiClickHandler),P(window).on(Q.LOAD_DATA_API,function(){P($.DATA_RIDE).each(function(){var t=P(this);J._jQueryInterface.call(t,t.data())})}),P.fn[L]=J._jQueryInterface,P.fn[L].Constructor=J,P.fn[L].noConflict=function(){return P.fn[L]=x,J._jQueryInterface},J),Sn=(et="collapse",it="."+(nt="bs.collapse"),st=(tt=e).fn[et],rt={toggle:!0,parent:""},ot={toggle:"boolean",parent:"(string|element)"},at={SHOW:"show"+it,SHOWN:"shown"+it,HIDE:"hide"+it,HIDDEN:"hidden"+it,CLICK_DATA_API:"click"+it+".data-api"},lt="show",ht="collapse",ct="collapsing",ut="collapsed",dt="width",ft="height",_t={ACTIVES:".show, .collapsing",DATA_TOGGLE:'[data-toggle="collapse"]'},gt=function(){function a(t,e){this._isTransitioning=!1,this._element=t,this._config=this._getConfig(e),this._triggerArray=tt.makeArray(tt('[data-toggle="collapse"][href="#'+t.id+'"],[data-toggle="collapse"][data-target="#'+t.id+'"]'));for(var n=tt(_t.DATA_TOGGLE),i=0;i<n.length;i++){var s=n[i],r=In.getSelectorFromElement(s);null!==r&&0<tt(r).filter(t).length&&(this._selector=r,this._triggerArray.push(s))}this._parent=this._config.parent?this._getParent():null,this._config.parent||this._addAriaAndCollapsedClass(this._element,this._triggerArray),this._config.toggle&&this.toggle()}var t=a.prototype;return t.toggle=function(){tt(this._element).hasClass(lt)?this.hide():this.show()},t.show=function(){var t,e,n=this;if(!this._isTransitioning&&!tt(this._element).hasClass(lt)&&(this._parent&&0===(t=tt.makeArray(tt(this._parent).find(_t.ACTIVES).filter('[data-parent="'+this._config.parent+'"]'))).length&&(t=null),!(t&&(e=tt(t).not(this._selector).data(nt))&&e._isTransitioning))){var i=tt.Event(at.SHOW);if(tt(this._element).trigger(i),!i.isDefaultPrevented()){t&&(a._jQueryInterface.call(tt(t).not(this._selector),"hide"),e||tt(t).data(nt,null));var s=this._getDimension();tt(this._element).removeClass(ht).addClass(ct),(this._element.style[s]=0)<this._triggerArray.length&&tt(this._triggerArray).removeClass(ut).attr("aria-expanded",!0),this.setTransitioning(!0);var r=function(){tt(n._element).removeClass(ct).addClass(ht).addClass(lt),n._element.style[s]="",n.setTransitioning(!1),tt(n._element).trigger(at.SHOWN)};if(In.supportsTransitionEnd()){var o="scroll"+(s[0].toUpperCase()+s.slice(1));tt(this._element).one(In.TRANSITION_END,r).emulateTransitionEnd(600),this._element.style[s]=this._element[o]+"px"}else r()}}},t.hide=function(){var t=this;if(!this._isTransitioning&&tt(this._element).hasClass(lt)){var e=tt.Event(at.HIDE);if(tt(this._element).trigger(e),!e.isDefaultPrevented()){var n=this._getDimension();if(this._element.style[n]=this._element.getBoundingClientRect()[n]+"px",In.reflow(this._element),tt(this._element).addClass(ct).removeClass(ht).removeClass(lt),0<this._triggerArray.length)for(var i=0;i<this._triggerArray.length;i++){var s=this._triggerArray[i],r=In.getSelectorFromElement(s);if(null!==r)tt(r).hasClass(lt)||tt(s).addClass(ut).attr("aria-expanded",!1)}this.setTransitioning(!0);var o=function(){t.setTransitioning(!1),tt(t._element).removeClass(ct).addClass(ht).trigger(at.HIDDEN)};this._element.style[n]="",In.supportsTransitionEnd()?tt(this._element).one(In.TRANSITION_END,o).emulateTransitionEnd(600):o()}}},t.setTransitioning=function(t){this._isTransitioning=t},t.dispose=function(){tt.removeData(this._element,nt),this._config=null,this._parent=null,this._element=null,this._triggerArray=null,this._isTransitioning=null},t._getConfig=function(t){return(t=u({},rt,t)).toggle=Boolean(t.toggle),In.typeCheckConfig(et,t,ot),t},t._getDimension=function(){return tt(this._element).hasClass(dt)?dt:ft},t._getParent=function(){var n=this,t=null;In.isElement(this._config.parent)?(t=this._config.parent,void 0!==this._config.parent.jquery&&(t=this._config.parent[0])):t=tt(this._config.parent)[0];var e='[data-toggle="collapse"][data-parent="'+this._config.parent+'"]';return tt(t).find(e).each(function(t,e){n._addAriaAndCollapsedClass(a._getTargetFromElement(e),[e])}),t},t._addAriaAndCollapsedClass=function(t,e){if(t){var n=tt(t).hasClass(lt);0<e.length&&tt(e).toggleClass(ut,!n).attr("aria-expanded",n)}},a._getTargetFromElement=function(t){var e=In.getSelectorFromElement(t);return e?tt(e)[0]:null},a._jQueryInterface=function(i){return this.each(function(){var t=tt(this),e=t.data(nt),n=u({},rt,t.data(),"object"==typeof i&&i);if(!e&&n.toggle&&/show|hide/.test(i)&&(n.toggle=!1),e||(e=new a(this,n),t.data(nt,e)),"string"==typeof i){if(void 0===e[i])throw new TypeError('No method named "'+i+'"');e[i]()}})},o(a,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return rt}}]),a}(),tt(document).on(at.CLICK_DATA_API,_t.DATA_TOGGLE,function(t){"A"===t.currentTarget.tagName&&t.preventDefault();var n=tt(this),e=In.getSelectorFromElement(this);tt(e).each(function(){var t=tt(this),e=t.data(nt)?"toggle":n.data();gt._jQueryInterface.call(t,e)})}),tt.fn[et]=gt._jQueryInterface,tt.fn[et].Constructor=gt,tt.fn[et].noConflict=function(){return tt.fn[et]=st,gt._jQueryInterface},gt),wn=(pt="dropdown",Et="."+(vt="bs.dropdown"),Tt=".data-api",Ct=(mt=e).fn[pt],yt=new RegExp("38|40|27"),It={HIDE:"hide"+Et,HIDDEN:"hidden"+Et,SHOW:"show"+Et,SHOWN:"shown"+Et,CLICK:"click"+Et,CLICK_DATA_API:"click"+Et+Tt,KEYDOWN_DATA_API:"keydown"+Et+Tt,KEYUP_DATA_API:"keyup"+Et+Tt},At="disabled",bt="show",Dt="dropup",St="dropright",wt="dropleft",Nt="dropdown-menu-right",Ot="dropdown-menu-left",kt="position-static",Pt='[data-toggle="dropdown"]',Lt=".dropdown form",Rt=".dropdown-menu",jt=".navbar-nav",Ht=".dropdown-menu .dropdown-item:not(.disabled)",xt="top-start",Wt="top-end",Mt="bottom-start",Ut="bottom-end",Kt="right-start",Vt="left-start",Bt={offset:0,flip:!0,boundary:"scrollParent"},Qt={offset:"(number|string|function)",flip:"boolean",boundary:"(string|element)"},Ft=function(){function l(t,e){this._element=t,this._popper=null,this._config=this._getConfig(e),this._menu=this._getMenuElement(),this._inNavbar=this._detectNavbar(),this._addEventListeners()}var t=l.prototype;return t.toggle=function(){if(!this._element.disabled&&!mt(this._element).hasClass(At)){var t=l._getParentFromElement(this._element),e=mt(this._menu).hasClass(bt);if(l._clearMenus(),!e){var n={relatedTarget:this._element},i=mt.Event(It.SHOW,n);if(mt(t).trigger(i),!i.isDefaultPrevented()){if(!this._inNavbar){if(void 0===c)throw new TypeError("Bootstrap dropdown require Popper.js (https://popper.js.org)");var s=this._element;mt(t).hasClass(Dt)&&(mt(this._menu).hasClass(Ot)||mt(this._menu).hasClass(Nt))&&(s=t),"scrollParent"!==this._config.boundary&&mt(t).addClass(kt),this._popper=new c(s,this._menu,this._getPopperConfig())}"ontouchstart"in document.documentElement&&0===mt(t).closest(jt).length&&mt("body").children().on("mouseover",null,mt.noop),this._element.focus(),this._element.setAttribute("aria-expanded",!0),mt(this._menu).toggleClass(bt),mt(t).toggleClass(bt).trigger(mt.Event(It.SHOWN,n))}}}},t.dispose=function(){mt.removeData(this._element,vt),mt(this._element).off(Et),this._element=null,(this._menu=null)!==this._popper&&(this._popper.destroy(),this._popper=null)},t.update=function(){this._inNavbar=this._detectNavbar(),null!==this._popper&&this._popper.scheduleUpdate()},t._addEventListeners=function(){var e=this;mt(this._element).on(It.CLICK,function(t){t.preventDefault(),t.stopPropagation(),e.toggle()})},t._getConfig=function(t){return t=u({},this.constructor.Default,mt(this._element).data(),t),In.typeCheckConfig(pt,t,this.constructor.DefaultType),t},t._getMenuElement=function(){if(!this._menu){var t=l._getParentFromElement(this._element);this._menu=mt(t).find(Rt)[0]}return this._menu},t._getPlacement=function(){var t=mt(this._element).parent(),e=Mt;return t.hasClass(Dt)?(e=xt,mt(this._menu).hasClass(Nt)&&(e=Wt)):t.hasClass(St)?e=Kt:t.hasClass(wt)?e=Vt:mt(this._menu).hasClass(Nt)&&(e=Ut),e},t._detectNavbar=function(){return 0<mt(this._element).closest(".navbar").length},t._getPopperConfig=function(){var e=this,t={};return"function"==typeof this._config.offset?t.fn=function(t){return t.offsets=u({},t.offsets,e._config.offset(t.offsets)||{}),t}:t.offset=this._config.offset,{placement:this._getPlacement(),modifiers:{offset:t,flip:{enabled:this._config.flip},preventOverflow:{boundariesElement:this._config.boundary}}}},l._jQueryInterface=function(e){return this.each(function(){var t=mt(this).data(vt);if(t||(t=new l(this,"object"==typeof e?e:null),mt(this).data(vt,t)),"string"==typeof e){if(void 0===t[e])throw new TypeError('No method named "'+e+'"');t[e]()}})},l._clearMenus=function(t){if(!t||3!==t.which&&("keyup"!==t.type||9===t.which))for(var e=mt.makeArray(mt(Pt)),n=0;n<e.length;n++){var i=l._getParentFromElement(e[n]),s=mt(e[n]).data(vt),r={relatedTarget:e[n]};if(s){var o=s._menu;if(mt(i).hasClass(bt)&&!(t&&("click"===t.type&&/input|textarea/i.test(t.target.tagName)||"keyup"===t.type&&9===t.which)&&mt.contains(i,t.target))){var a=mt.Event(It.HIDE,r);mt(i).trigger(a),a.isDefaultPrevented()||("ontouchstart"in document.documentElement&&mt("body").children().off("mouseover",null,mt.noop),e[n].setAttribute("aria-expanded","false"),mt(o).removeClass(bt),mt(i).removeClass(bt).trigger(mt.Event(It.HIDDEN,r)))}}}},l._getParentFromElement=function(t){var e,n=In.getSelectorFromElement(t);return n&&(e=mt(n)[0]),e||t.parentNode},l._dataApiKeydownHandler=function(t){if((/input|textarea/i.test(t.target.tagName)?!(32===t.which||27!==t.which&&(40!==t.which&&38!==t.which||mt(t.target).closest(Rt).length)):yt.test(t.which))&&(t.preventDefault(),t.stopPropagation(),!this.disabled&&!mt(this).hasClass(At))){var e=l._getParentFromElement(this),n=mt(e).hasClass(bt);if((n||27===t.which&&32===t.which)&&(!n||27!==t.which&&32!==t.which)){var i=mt(e).find(Ht).get();if(0!==i.length){var s=i.indexOf(t.target);38===t.which&&0<s&&s--,40===t.which&&s<i.length-1&&s++,s<0&&(s=0),i[s].focus()}}else{if(27===t.which){var r=mt(e).find(Pt)[0];mt(r).trigger("focus")}mt(this).trigger("click")}}},o(l,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return Bt}},{key:"DefaultType",get:function(){return Qt}}]),l}(),mt(document).on(It.KEYDOWN_DATA_API,Pt,Ft._dataApiKeydownHandler).on(It.KEYDOWN_DATA_API,Rt,Ft._dataApiKeydownHandler).on(It.CLICK_DATA_API+" "+It.KEYUP_DATA_API,Ft._clearMenus).on(It.CLICK_DATA_API,Pt,function(t){t.preventDefault(),t.stopPropagation(),Ft._jQueryInterface.call(mt(this),"toggle")}).on(It.CLICK_DATA_API,Lt,function(t){t.stopPropagation()}),mt.fn[pt]=Ft._jQueryInterface,mt.fn[pt].Constructor=Ft,mt.fn[pt].noConflict=function(){return mt.fn[pt]=Ct,Ft._jQueryInterface},Ft),Nn=(Gt="modal",zt="."+(qt="bs.modal"),Xt=(Yt=e).fn[Gt],Zt={backdrop:!0,keyboard:!0,focus:!0,show:!0},$t={backdrop:"(boolean|string)",keyboard:"boolean",focus:"boolean",show:"boolean"},Jt={HIDE:"hide"+zt,HIDDEN:"hidden"+zt,SHOW:"show"+zt,SHOWN:"shown"+zt,FOCUSIN:"focusin"+zt,RESIZE:"resize"+zt,CLICK_DISMISS:"click.dismiss"+zt,KEYDOWN_DISMISS:"keydown.dismiss"+zt,MOUSEUP_DISMISS:"mouseup.dismiss"+zt,MOUSEDOWN_DISMISS:"mousedown.dismiss"+zt,CLICK_DATA_API:"click"+zt+".data-api"},te="modal-scrollbar-measure",ee="modal-backdrop",ne="modal-open",ie="fade",se="show",re={DIALOG:".modal-dialog",DATA_TOGGLE:'[data-toggle="modal"]',DATA_DISMISS:'[data-dismiss="modal"]',FIXED_CONTENT:".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",STICKY_CONTENT:".sticky-top",NAVBAR_TOGGLER:".navbar-toggler"},oe=function(){function s(t,e){this._config=this._getConfig(e),this._element=t,this._dialog=Yt(t).find(re.DIALOG)[0],this._backdrop=null,this._isShown=!1,this._isBodyOverflowing=!1,this._ignoreBackdropClick=!1,this._originalBodyPadding=0,this._scrollbarWidth=0}var t=s.prototype;return t.toggle=function(t){return this._isShown?this.hide():this.show(t)},t.show=function(t){var e=this;if(!this._isTransitioning&&!this._isShown){In.supportsTransitionEnd()&&Yt(this._element).hasClass(ie)&&(this._isTransitioning=!0);var n=Yt.Event(Jt.SHOW,{relatedTarget:t});Yt(this._element).trigger(n),this._isShown||n.isDefaultPrevented()||(this._isShown=!0,this._checkScrollbar(),this._setScrollbar(),this._adjustDialog(),Yt(document.body).addClass(ne),this._setEscapeEvent(),this._setResizeEvent(),Yt(this._element).on(Jt.CLICK_DISMISS,re.DATA_DISMISS,function(t){return e.hide(t)}),Yt(this._dialog).on(Jt.MOUSEDOWN_DISMISS,function(){Yt(e._element).one(Jt.MOUSEUP_DISMISS,function(t){Yt(t.target).is(e._element)&&(e._ignoreBackdropClick=!0)})}),this._showBackdrop(function(){return e._showElement(t)}))}},t.hide=function(t){var e=this;if(t&&t.preventDefault(),!this._isTransitioning&&this._isShown){var n=Yt.Event(Jt.HIDE);if(Yt(this._element).trigger(n),this._isShown&&!n.isDefaultPrevented()){this._isShown=!1;var i=In.supportsTransitionEnd()&&Yt(this._element).hasClass(ie);i&&(this._isTransitioning=!0),this._setEscapeEvent(),this._setResizeEvent(),Yt(document).off(Jt.FOCUSIN),Yt(this._element).removeClass(se),Yt(this._element).off(Jt.CLICK_DISMISS),Yt(this._dialog).off(Jt.MOUSEDOWN_DISMISS),i?Yt(this._element).one(In.TRANSITION_END,function(t){return e._hideModal(t)}).emulateTransitionEnd(300):this._hideModal()}}},t.dispose=function(){Yt.removeData(this._element,qt),Yt(window,document,this._element,this._backdrop).off(zt),this._config=null,this._element=null,this._dialog=null,this._backdrop=null,this._isShown=null,this._isBodyOverflowing=null,this._ignoreBackdropClick=null,this._scrollbarWidth=null},t.handleUpdate=function(){this._adjustDialog()},t._getConfig=function(t){return t=u({},Zt,t),In.typeCheckConfig(Gt,t,$t),t},t._showElement=function(t){var e=this,n=In.supportsTransitionEnd()&&Yt(this._element).hasClass(ie);this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE||document.body.appendChild(this._element),this._element.style.display="block",this._element.removeAttribute("aria-hidden"),this._element.scrollTop=0,n&&In.reflow(this._element),Yt(this._element).addClass(se),this._config.focus&&this._enforceFocus();var i=Yt.Event(Jt.SHOWN,{relatedTarget:t}),s=function(){e._config.focus&&e._element.focus(),e._isTransitioning=!1,Yt(e._element).trigger(i)};n?Yt(this._dialog).one(In.TRANSITION_END,s).emulateTransitionEnd(300):s()},t._enforceFocus=function(){var e=this;Yt(document).off(Jt.FOCUSIN).on(Jt.FOCUSIN,function(t){document!==t.target&&e._element!==t.target&&0===Yt(e._element).has(t.target).length&&e._element.focus()})},t._setEscapeEvent=function(){var e=this;this._isShown&&this._config.keyboard?Yt(this._element).on(Jt.KEYDOWN_DISMISS,function(t){27===t.which&&(t.preventDefault(),e.hide())}):this._isShown||Yt(this._element).off(Jt.KEYDOWN_DISMISS)},t._setResizeEvent=function(){var e=this;this._isShown?Yt(window).on(Jt.RESIZE,function(t){return e.handleUpdate(t)}):Yt(window).off(Jt.RESIZE)},t._hideModal=function(){var t=this;this._element.style.display="none",this._element.setAttribute("aria-hidden",!0),this._isTransitioning=!1,this._showBackdrop(function(){Yt(document.body).removeClass(ne),t._resetAdjustments(),t._resetScrollbar(),Yt(t._element).trigger(Jt.HIDDEN)})},t._removeBackdrop=function(){this._backdrop&&(Yt(this._backdrop).remove(),this._backdrop=null)},t._showBackdrop=function(t){var e=this,n=Yt(this._element).hasClass(ie)?ie:"";if(this._isShown&&this._config.backdrop){var i=In.supportsTransitionEnd()&&n;if(this._backdrop=document.createElement("div"),this._backdrop.className=ee,n&&Yt(this._backdrop).addClass(n),Yt(this._backdrop).appendTo(document.body),Yt(this._element).on(Jt.CLICK_DISMISS,function(t){e._ignoreBackdropClick?e._ignoreBackdropClick=!1:t.target===t.currentTarget&&("static"===e._config.backdrop?e._element.focus():e.hide())}),i&&In.reflow(this._backdrop),Yt(this._backdrop).addClass(se),!t)return;if(!i)return void t();Yt(this._backdrop).one(In.TRANSITION_END,t).emulateTransitionEnd(150)}else if(!this._isShown&&this._backdrop){Yt(this._backdrop).removeClass(se);var s=function(){e._removeBackdrop(),t&&t()};In.supportsTransitionEnd()&&Yt(this._element).hasClass(ie)?Yt(this._backdrop).one(In.TRANSITION_END,s).emulateTransitionEnd(150):s()}else t&&t()},t._adjustDialog=function(){var t=this._element.scrollHeight>document.documentElement.clientHeight;!this._isBodyOverflowing&&t&&(this._element.style.paddingLeft=this._scrollbarWidth+"px"),this._isBodyOverflowing&&!t&&(this._element.style.paddingRight=this._scrollbarWidth+"px")},t._resetAdjustments=function(){this._element.style.paddingLeft="",this._element.style.paddingRight=""},t._checkScrollbar=function(){var t=document.body.getBoundingClientRect();this._isBodyOverflowing=t.left+t.right<window.innerWidth,this._scrollbarWidth=this._getScrollbarWidth()},t._setScrollbar=function(){var s=this;if(this._isBodyOverflowing){Yt(re.FIXED_CONTENT).each(function(t,e){var n=Yt(e)[0].style.paddingRight,i=Yt(e).css("padding-right");Yt(e).data("padding-right",n).css("padding-right",parseFloat(i)+s._scrollbarWidth+"px")}),Yt(re.STICKY_CONTENT).each(function(t,e){var n=Yt(e)[0].style.marginRight,i=Yt(e).css("margin-right");Yt(e).data("margin-right",n).css("margin-right",parseFloat(i)-s._scrollbarWidth+"px")}),Yt(re.NAVBAR_TOGGLER).each(function(t,e){var n=Yt(e)[0].style.marginRight,i=Yt(e).css("margin-right");Yt(e).data("margin-right",n).css("margin-right",parseFloat(i)+s._scrollbarWidth+"px")});var t=document.body.style.paddingRight,e=Yt("body").css("padding-right");Yt("body").data("padding-right",t).css("padding-right",parseFloat(e)+this._scrollbarWidth+"px")}},t._resetScrollbar=function(){Yt(re.FIXED_CONTENT).each(function(t,e){var n=Yt(e).data("padding-right");void 0!==n&&Yt(e).css("padding-right",n).removeData("padding-right")}),Yt(re.STICKY_CONTENT+", "+re.NAVBAR_TOGGLER).each(function(t,e){var n=Yt(e).data("margin-right");void 0!==n&&Yt(e).css("margin-right",n).removeData("margin-right")});var t=Yt("body").data("padding-right");void 0!==t&&Yt("body").css("padding-right",t).removeData("padding-right")},t._getScrollbarWidth=function(){var t=document.createElement("div");t.className=te,document.body.appendChild(t);var e=t.getBoundingClientRect().width-t.clientWidth;return document.body.removeChild(t),e},s._jQueryInterface=function(n,i){return this.each(function(){var t=Yt(this).data(qt),e=u({},s.Default,Yt(this).data(),"object"==typeof n&&n);if(t||(t=new s(this,e),Yt(this).data(qt,t)),"string"==typeof n){if(void 0===t[n])throw new TypeError('No method named "'+n+'"');t[n](i)}else e.show&&t.show(i)})},o(s,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return Zt}}]),s}(),Yt(document).on(Jt.CLICK_DATA_API,re.DATA_TOGGLE,function(t){var e,n=this,i=In.getSelectorFromElement(this);i&&(e=Yt(i)[0]);var s=Yt(e).data(qt)?"toggle":u({},Yt(e).data(),Yt(this).data());"A"!==this.tagName&&"AREA"!==this.tagName||t.preventDefault();var r=Yt(e).one(Jt.SHOW,function(t){t.isDefaultPrevented()||r.one(Jt.HIDDEN,function(){Yt(n).is(":visible")&&n.focus()})});oe._jQueryInterface.call(Yt(e),s,this)}),Yt.fn[Gt]=oe._jQueryInterface,Yt.fn[Gt].Constructor=oe,Yt.fn[Gt].noConflict=function(){return Yt.fn[Gt]=Xt,oe._jQueryInterface},oe),On=(le="tooltip",ce="."+(he="bs.tooltip"),ue=(ae=e).fn[le],de="bs-tooltip",fe=new RegExp("(^|\\s)"+de+"\\S+","g"),_e={animation:"boolean",template:"string",title:"(string|element|function)",trigger:"string",delay:"(number|object)",html:"boolean",selector:"(string|boolean)",placement:"(string|function)",offset:"(number|string)",container:"(string|element|boolean)",fallbackPlacement:"(string|array)",boundary:"(string|element)"},ge={AUTO:"auto",TOP:"top",RIGHT:"right",BOTTOM:"bottom",LEFT:"left"},me={animation:!0,template:'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,selector:!1,placement:"top",offset:0,container:!1,fallbackPlacement:"flip",boundary:"scrollParent"},ve="out",Ee={HIDE:"hide"+ce,HIDDEN:"hidden"+ce,SHOW:(pe="show")+ce,SHOWN:"shown"+ce,INSERTED:"inserted"+ce,CLICK:"click"+ce,FOCUSIN:"focusin"+ce,FOCUSOUT:"focusout"+ce,MOUSEENTER:"mouseenter"+ce,MOUSELEAVE:"mouseleave"+ce},Te="fade",Ce="show",ye=".tooltip-inner",Ie=".arrow",Ae="hover",be="focus",De="click",Se="manual",we=function(){function h(t,e){if(void 0===c)throw new TypeError("Bootstrap tooltips require Popper.js (https://popper.js.org)");this._isEnabled=!0,this._timeout=0,this._hoverState="",this._activeTrigger={},this._popper=null,this.element=t,this.config=this._getConfig(e),this.tip=null,this._setListeners()}var t=h.prototype;return t.enable=function(){this._isEnabled=!0},t.disable=function(){this._isEnabled=!1},t.toggleEnabled=function(){this._isEnabled=!this._isEnabled},t.toggle=function(t){if(this._isEnabled)if(t){var e=this.constructor.DATA_KEY,n=ae(t.currentTarget).data(e);n||(n=new this.constructor(t.currentTarget,this._getDelegateConfig()),ae(t.currentTarget).data(e,n)),n._activeTrigger.click=!n._activeTrigger.click,n._isWithActiveTrigger()?n._enter(null,n):n._leave(null,n)}else{if(ae(this.getTipElement()).hasClass(Ce))return void this._leave(null,this);this._enter(null,this)}},t.dispose=function(){clearTimeout(this._timeout),ae.removeData(this.element,this.constructor.DATA_KEY),ae(this.element).off(this.constructor.EVENT_KEY),ae(this.element).closest(".modal").off("hide.bs.modal"),this.tip&&ae(this.tip).remove(),this._isEnabled=null,this._timeout=null,this._hoverState=null,(this._activeTrigger=null)!==this._popper&&this._popper.destroy(),this._popper=null,this.element=null,this.config=null,this.tip=null},t.show=function(){var e=this;if("none"===ae(this.element).css("display"))throw new Error("Please use show on visible elements");var t=ae.Event(this.constructor.Event.SHOW);if(this.isWithContent()&&this._isEnabled){ae(this.element).trigger(t);var n=ae.contains(this.element.ownerDocument.documentElement,this.element);if(t.isDefaultPrevented()||!n)return;var i=this.getTipElement(),s=In.getUID(this.constructor.NAME);i.setAttribute("id",s),this.element.setAttribute("aria-describedby",s),this.setContent(),this.config.animation&&ae(i).addClass(Te);var r="function"==typeof this.config.placement?this.config.placement.call(this,i,this.element):this.config.placement,o=this._getAttachment(r);this.addAttachmentClass(o);var a=!1===this.config.container?document.body:ae(this.config.container);ae(i).data(this.constructor.DATA_KEY,this),ae.contains(this.element.ownerDocument.documentElement,this.tip)||ae(i).appendTo(a),ae(this.element).trigger(this.constructor.Event.INSERTED),this._popper=new c(this.element,i,{placement:o,modifiers:{offset:{offset:this.config.offset},flip:{behavior:this.config.fallbackPlacement},arrow:{element:Ie},preventOverflow:{boundariesElement:this.config.boundary}},onCreate:function(t){t.originalPlacement!==t.placement&&e._handlePopperPlacementChange(t)},onUpdate:function(t){e._handlePopperPlacementChange(t)}}),ae(i).addClass(Ce),"ontouchstart"in document.documentElement&&ae("body").children().on("mouseover",null,ae.noop);var l=function(){e.config.animation&&e._fixTransition();var t=e._hoverState;e._hoverState=null,ae(e.element).trigger(e.constructor.Event.SHOWN),t===ve&&e._leave(null,e)};In.supportsTransitionEnd()&&ae(this.tip).hasClass(Te)?ae(this.tip).one(In.TRANSITION_END,l).emulateTransitionEnd(h._TRANSITION_DURATION):l()}},t.hide=function(t){var e=this,n=this.getTipElement(),i=ae.Event(this.constructor.Event.HIDE),s=function(){e._hoverState!==pe&&n.parentNode&&n.parentNode.removeChild(n),e._cleanTipClass(),e.element.removeAttribute("aria-describedby"),ae(e.element).trigger(e.constructor.Event.HIDDEN),null!==e._popper&&e._popper.destroy(),t&&t()};ae(this.element).trigger(i),i.isDefaultPrevented()||(ae(n).removeClass(Ce),"ontouchstart"in document.documentElement&&ae("body").children().off("mouseover",null,ae.noop),this._activeTrigger[De]=!1,this._activeTrigger[be]=!1,this._activeTrigger[Ae]=!1,In.supportsTransitionEnd()&&ae(this.tip).hasClass(Te)?ae(n).one(In.TRANSITION_END,s).emulateTransitionEnd(150):s(),this._hoverState="")},t.update=function(){null!==this._popper&&this._popper.scheduleUpdate()},t.isWithContent=function(){return Boolean(this.getTitle())},t.addAttachmentClass=function(t){ae(this.getTipElement()).addClass(de+"-"+t)},t.getTipElement=function(){return this.tip=this.tip||ae(this.config.template)[0],this.tip},t.setContent=function(){var t=ae(this.getTipElement());this.setElementContent(t.find(ye),this.getTitle()),t.removeClass(Te+" "+Ce)},t.setElementContent=function(t,e){var n=this.config.html;"object"==typeof e&&(e.nodeType||e.jquery)?n?ae(e).parent().is(t)||t.empty().append(e):t.text(ae(e).text()):t[n?"html":"text"](e)},t.getTitle=function(){var t=this.element.getAttribute("data-original-title");return t||(t="function"==typeof this.config.title?this.config.title.call(this.element):this.config.title),t},t._getAttachment=function(t){return ge[t.toUpperCase()]},t._setListeners=function(){var i=this;this.config.trigger.split(" ").forEach(function(t){if("click"===t)ae(i.element).on(i.constructor.Event.CLICK,i.config.selector,function(t){return i.toggle(t)});else if(t!==Se){var e=t===Ae?i.constructor.Event.MOUSEENTER:i.constructor.Event.FOCUSIN,n=t===Ae?i.constructor.Event.MOUSELEAVE:i.constructor.Event.FOCUSOUT;ae(i.element).on(e,i.config.selector,function(t){return i._enter(t)}).on(n,i.config.selector,function(t){return i._leave(t)})}ae(i.element).closest(".modal").on("hide.bs.modal",function(){return i.hide()})}),this.config.selector?this.config=u({},this.config,{trigger:"manual",selector:""}):this._fixTitle()},t._fixTitle=function(){var t=typeof this.element.getAttribute("data-original-title");(this.element.getAttribute("title")||"string"!==t)&&(this.element.setAttribute("data-original-title",this.element.getAttribute("title")||""),this.element.setAttribute("title",""))},t._enter=function(t,e){var n=this.constructor.DATA_KEY;(e=e||ae(t.currentTarget).data(n))||(e=new this.constructor(t.currentTarget,this._getDelegateConfig()),ae(t.currentTarget).data(n,e)),t&&(e._activeTrigger["focusin"===t.type?be:Ae]=!0),ae(e.getTipElement()).hasClass(Ce)||e._hoverState===pe?e._hoverState=pe:(clearTimeout(e._timeout),e._hoverState=pe,e.config.delay&&e.config.delay.show?e._timeout=setTimeout(function(){e._hoverState===pe&&e.show()},e.config.delay.show):e.show())},t._leave=function(t,e){var n=this.constructor.DATA_KEY;(e=e||ae(t.currentTarget).data(n))||(e=new this.constructor(t.currentTarget,this._getDelegateConfig()),ae(t.currentTarget).data(n,e)),t&&(e._activeTrigger["focusout"===t.type?be:Ae]=!1),e._isWithActiveTrigger()||(clearTimeout(e._timeout),e._hoverState=ve,e.config.delay&&e.config.delay.hide?e._timeout=setTimeout(function(){e._hoverState===ve&&e.hide()},e.config.delay.hide):e.hide())},t._isWithActiveTrigger=function(){for(var t in this._activeTrigger)if(this._activeTrigger[t])return!0;return!1},t._getConfig=function(t){return"number"==typeof(t=u({},this.constructor.Default,ae(this.element).data(),t)).delay&&(t.delay={show:t.delay,hide:t.delay}),"number"==typeof t.title&&(t.title=t.title.toString()),"number"==typeof t.content&&(t.content=t.content.toString()),In.typeCheckConfig(le,t,this.constructor.DefaultType),t},t._getDelegateConfig=function(){var t={};if(this.config)for(var e in this.config)this.constructor.Default[e]!==this.config[e]&&(t[e]=this.config[e]);return t},t._cleanTipClass=function(){var t=ae(this.getTipElement()),e=t.attr("class").match(fe);null!==e&&0<e.length&&t.removeClass(e.join(""))},t._handlePopperPlacementChange=function(t){this._cleanTipClass(),this.addAttachmentClass(this._getAttachment(t.placement))},t._fixTransition=function(){var t=this.getTipElement(),e=this.config.animation;null===t.getAttribute("x-placement")&&(ae(t).removeClass(Te),this.config.animation=!1,this.hide(),this.show(),this.config.animation=e)},h._jQueryInterface=function(n){return this.each(function(){var t=ae(this).data(he),e="object"==typeof n&&n;if((t||!/dispose|hide/.test(n))&&(t||(t=new h(this,e),ae(this).data(he,t)),"string"==typeof n)){if(void 0===t[n])throw new TypeError('No method named "'+n+'"');t[n]()}})},o(h,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return me}},{key:"NAME",get:function(){return le}},{key:"DATA_KEY",get:function(){return he}},{key:"Event",get:function(){return Ee}},{key:"EVENT_KEY",get:function(){return ce}},{key:"DefaultType",get:function(){return _e}}]),h}(),ae.fn[le]=we._jQueryInterface,ae.fn[le].Constructor=we,ae.fn[le].noConflict=function(){return ae.fn[le]=ue,we._jQueryInterface},we),kn=(Oe="popover",Pe="."+(ke="bs.popover"),Le=(Ne=e).fn[Oe],Re="bs-popover",je=new RegExp("(^|\\s)"+Re+"\\S+","g"),He=u({},On.Default,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'}),xe=u({},On.DefaultType,{content:"(string|element|function)"}),We="fade",Ue=".popover-header",Ke=".popover-body",Ve={HIDE:"hide"+Pe,HIDDEN:"hidden"+Pe,SHOW:(Me="show")+Pe,SHOWN:"shown"+Pe,INSERTED:"inserted"+Pe,CLICK:"click"+Pe,FOCUSIN:"focusin"+Pe,FOCUSOUT:"focusout"+Pe,MOUSEENTER:"mouseenter"+Pe,MOUSELEAVE:"mouseleave"+Pe},Be=function(t){var e,n;function i(){return t.apply(this,arguments)||this}n=t,(e=i).prototype=Object.create(n.prototype),(e.prototype.constructor=e).__proto__=n;var s=i.prototype;return s.isWithContent=function(){return this.getTitle()||this._getContent()},s.addAttachmentClass=function(t){Ne(this.getTipElement()).addClass(Re+"-"+t)},s.getTipElement=function(){return this.tip=this.tip||Ne(this.config.template)[0],this.tip},s.setContent=function(){var t=Ne(this.getTipElement());this.setElementContent(t.find(Ue),this.getTitle());var e=this._getContent();"function"==typeof e&&(e=e.call(this.element)),this.setElementContent(t.find(Ke),e),t.removeClass(We+" "+Me)},s._getContent=function(){return this.element.getAttribute("data-content")||this.config.content},s._cleanTipClass=function(){var t=Ne(this.getTipElement()),e=t.attr("class").match(je);null!==e&&0<e.length&&t.removeClass(e.join(""))},i._jQueryInterface=function(n){return this.each(function(){var t=Ne(this).data(ke),e="object"==typeof n?n:null;if((t||!/destroy|hide/.test(n))&&(t||(t=new i(this,e),Ne(this).data(ke,t)),"string"==typeof n)){if(void 0===t[n])throw new TypeError('No method named "'+n+'"');t[n]()}})},o(i,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return He}},{key:"NAME",get:function(){return Oe}},{key:"DATA_KEY",get:function(){return ke}},{key:"Event",get:function(){return Ve}},{key:"EVENT_KEY",get:function(){return Pe}},{key:"DefaultType",get:function(){return xe}}]),i}(On),Ne.fn[Oe]=Be._jQueryInterface,Ne.fn[Oe].Constructor=Be,Ne.fn[Oe].noConflict=function(){return Ne.fn[Oe]=Le,Be._jQueryInterface},Be),Pn=(Fe="scrollspy",Ge="."+(Ye="bs.scrollspy"),qe=(Qe=e).fn[Fe],ze={offset:10,method:"auto",target:""},Xe={offset:"number",method:"string",target:"(string|element)"},Ze={ACTIVATE:"activate"+Ge,SCROLL:"scroll"+Ge,LOAD_DATA_API:"load"+Ge+".data-api"},$e="dropdown-item",Je="active",tn={DATA_SPY:'[data-spy="scroll"]',ACTIVE:".active",NAV_LIST_GROUP:".nav, .list-group",NAV_LINKS:".nav-link",NAV_ITEMS:".nav-item",LIST_ITEMS:".list-group-item",DROPDOWN:".dropdown",DROPDOWN_ITEMS:".dropdown-item",DROPDOWN_TOGGLE:".dropdown-toggle"},en="offset",nn="position",sn=function(){function n(t,e){var n=this;this._element=t,this._scrollElement="BODY"===t.tagName?window:t,this._config=this._getConfig(e),this._selector=this._config.target+" "+tn.NAV_LINKS+","+this._config.target+" "+tn.LIST_ITEMS+","+this._config.target+" "+tn.DROPDOWN_ITEMS,this._offsets=[],this._targets=[],this._activeTarget=null,this._scrollHeight=0,Qe(this._scrollElement).on(Ze.SCROLL,function(t){return n._process(t)}),this.refresh(),this._process()}var t=n.prototype;return t.refresh=function(){var e=this,t=this._scrollElement===this._scrollElement.window?en:nn,s="auto"===this._config.method?t:this._config.method,r=s===nn?this._getScrollTop():0;this._offsets=[],this._targets=[],this._scrollHeight=this._getScrollHeight(),Qe.makeArray(Qe(this._selector)).map(function(t){var e,n=In.getSelectorFromElement(t);if(n&&(e=Qe(n)[0]),e){var i=e.getBoundingClientRect();if(i.width||i.height)return[Qe(e)[s]().top+r,n]}return null}).filter(function(t){return t}).sort(function(t,e){return t[0]-e[0]}).forEach(function(t){e._offsets.push(t[0]),e._targets.push(t[1])})},t.dispose=function(){Qe.removeData(this._element,Ye),Qe(this._scrollElement).off(Ge),this._element=null,this._scrollElement=null,this._config=null,this._selector=null,this._offsets=null,this._targets=null,this._activeTarget=null,this._scrollHeight=null},t._getConfig=function(t){if("string"!=typeof(t=u({},ze,t)).target){var e=Qe(t.target).attr("id");e||(e=In.getUID(Fe),Qe(t.target).attr("id",e)),t.target="#"+e}return In.typeCheckConfig(Fe,t,Xe),t},t._getScrollTop=function(){return this._scrollElement===window?this._scrollElement.pageYOffset:this._scrollElement.scrollTop},t._getScrollHeight=function(){return this._scrollElement.scrollHeight||Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)},t._getOffsetHeight=function(){return this._scrollElement===window?window.innerHeight:this._scrollElement.getBoundingClientRect().height},t._process=function(){var t=this._getScrollTop()+this._config.offset,e=this._getScrollHeight(),n=this._config.offset+e-this._getOffsetHeight();if(this._scrollHeight!==e&&this.refresh(),n<=t){var i=this._targets[this._targets.length-1];this._activeTarget!==i&&this._activate(i)}else{if(this._activeTarget&&t<this._offsets[0]&&0<this._offsets[0])return this._activeTarget=null,void this._clear();for(var s=this._offsets.length;s--;){this._activeTarget!==this._targets[s]&&t>=this._offsets[s]&&(void 0===this._offsets[s+1]||t<this._offsets[s+1])&&this._activate(this._targets[s])}}},t._activate=function(e){this._activeTarget=e,this._clear();var t=this._selector.split(",");t=t.map(function(t){return t+'[data-target="'+e+'"],'+t+'[href="'+e+'"]'});var n=Qe(t.join(","));n.hasClass($e)?(n.closest(tn.DROPDOWN).find(tn.DROPDOWN_TOGGLE).addClass(Je),n.addClass(Je)):(n.addClass(Je),n.parents(tn.NAV_LIST_GROUP).prev(tn.NAV_LINKS+", "+tn.LIST_ITEMS).addClass(Je),n.parents(tn.NAV_LIST_GROUP).prev(tn.NAV_ITEMS).children(tn.NAV_LINKS).addClass(Je)),Qe(this._scrollElement).trigger(Ze.ACTIVATE,{relatedTarget:e})},t._clear=function(){Qe(this._selector).filter(tn.ACTIVE).removeClass(Je)},n._jQueryInterface=function(e){return this.each(function(){var t=Qe(this).data(Ye);if(t||(t=new n(this,"object"==typeof e&&e),Qe(this).data(Ye,t)),"string"==typeof e){if(void 0===t[e])throw new TypeError('No method named "'+e+'"');t[e]()}})},o(n,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return ze}}]),n}(),Qe(window).on(Ze.LOAD_DATA_API,function(){for(var t=Qe.makeArray(Qe(tn.DATA_SPY)),e=t.length;e--;){var n=Qe(t[e]);sn._jQueryInterface.call(n,n.data())}}),Qe.fn[Fe]=sn._jQueryInterface,Qe.fn[Fe].Constructor=sn,Qe.fn[Fe].noConflict=function(){return Qe.fn[Fe]=qe,sn._jQueryInterface},sn),Ln=(an="."+(on="bs.tab"),ln=(rn=e).fn.tab,hn={HIDE:"hide"+an,HIDDEN:"hidden"+an,SHOW:"show"+an,SHOWN:"shown"+an,CLICK_DATA_API:"click"+an+".data-api"},cn="dropdown-menu",un="active",dn="disabled",fn="fade",_n="show",gn=".dropdown",mn=".nav, .list-group",pn=".active",vn="> li > .active",En='[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',Tn=".dropdown-toggle",Cn="> .dropdown-menu .active",yn=function(){function i(t){this._element=t}var t=i.prototype;return t.show=function(){var n=this;if(!(this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE&&rn(this._element).hasClass(un)||rn(this._element).hasClass(dn))){var t,i,e=rn(this._element).closest(mn)[0],s=In.getSelectorFromElement(this._element);if(e){var r="UL"===e.nodeName?vn:pn;i=(i=rn.makeArray(rn(e).find(r)))[i.length-1]}var o=rn.Event(hn.HIDE,{relatedTarget:this._element}),a=rn.Event(hn.SHOW,{relatedTarget:i});if(i&&rn(i).trigger(o),rn(this._element).trigger(a),!a.isDefaultPrevented()&&!o.isDefaultPrevented()){s&&(t=rn(s)[0]),this._activate(this._element,e);var l=function(){var t=rn.Event(hn.HIDDEN,{relatedTarget:n._element}),e=rn.Event(hn.SHOWN,{relatedTarget:i});rn(i).trigger(t),rn(n._element).trigger(e)};t?this._activate(t,t.parentNode,l):l()}}},t.dispose=function(){rn.removeData(this._element,on),this._element=null},t._activate=function(t,e,n){var i=this,s=("UL"===e.nodeName?rn(e).find(vn):rn(e).children(pn))[0],r=n&&In.supportsTransitionEnd()&&s&&rn(s).hasClass(fn),o=function(){return i._transitionComplete(t,s,n)};s&&r?rn(s).one(In.TRANSITION_END,o).emulateTransitionEnd(150):o()},t._transitionComplete=function(t,e,n){if(e){rn(e).removeClass(_n+" "+un);var i=rn(e.parentNode).find(Cn)[0];i&&rn(i).removeClass(un),"tab"===e.getAttribute("role")&&e.setAttribute("aria-selected",!1)}if(rn(t).addClass(un),"tab"===t.getAttribute("role")&&t.setAttribute("aria-selected",!0),In.reflow(t),rn(t).addClass(_n),t.parentNode&&rn(t.parentNode).hasClass(cn)){var s=rn(t).closest(gn)[0];s&&rn(s).find(Tn).addClass(un),t.setAttribute("aria-expanded",!0)}n&&n()},i._jQueryInterface=function(n){return this.each(function(){var t=rn(this),e=t.data(on);if(e||(e=new i(this),t.data(on,e)),"string"==typeof n){if(void 0===e[n])throw new TypeError('No method named "'+n+'"');e[n]()}})},o(i,null,[{key:"VERSION",get:function(){return"4.0.0"}}]),i}(),rn(document).on(hn.CLICK_DATA_API,En,function(t){t.preventDefault(),yn._jQueryInterface.call(rn(this),"show")}),rn.fn.tab=yn._jQueryInterface,rn.fn.tab.Constructor=yn,rn.fn.tab.noConflict=function(){return rn.fn.tab=ln,yn._jQueryInterface},yn);!function(t){if(void 0===t)throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");var e=t.fn.jquery.split(" ")[0].split(".");if(e[0]<2&&e[1]<9||1===e[0]&&9===e[1]&&e[2]<1||4<=e[0])throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")}(e),t.Util=In,t.Alert=An,t.Button=bn,t.Carousel=Dn,t.Collapse=Sn,t.Dropdown=wn,t.Modal=Nn,t.Popover=kn,t.Scrollspy=Pn,t.Tab=Ln,t.Tooltip=On,Object.defineProperty(t,"__esModule",{value:!0})}),function(){var t=-1<navigator.userAgent.toLowerCase().indexOf("webkit"),e=-1<navigator.userAgent.toLowerCase().indexOf("opera"),n=-1<navigator.userAgent.toLowerCase().indexOf("msie");(t||e||n)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1)}();
;(function($, window, document, undefined){
function Owl(element, options){
this.settings=null;
this.options=$.extend({}, Owl.Defaults, options);
this.$element=$(element);
this._handlers={};
this._plugins={};
this._supress={};
this._current=null;
this._speed=null;
this._coordinates=[];
this._breakpoint=null;
this._width=null;
this._items=[];
this._clones=[];
this._mergers=[];
this._widths=[];
this._invalidated={};
this._pipe=[];
this._drag={
time: null,
target: null,
pointer: null,
stage: {
start: null,
current: null
},
direction: null
};
this._states={
current: {},
tags: {
'initializing': [ 'busy' ],
'animating': [ 'busy' ],
'dragging': [ 'interacting' ]
}};
$.each([ 'onResize', 'onThrottledResize' ], $.proxy(function(i, handler){
this._handlers[handler]=$.proxy(this[handler], this);
}, this));
$.each(Owl.Plugins, $.proxy(function(key, plugin){
this._plugins[key.charAt(0).toLowerCase() + key.slice(1)]
= new plugin(this);
}, this));
$.each(Owl.Workers, $.proxy(function(priority, worker){
this._pipe.push({
'filter': worker.filter,
'run': $.proxy(worker.run, this)
});
}, this));
this.setup();
this.initialize();
}
Owl.Defaults={
items: 3,
loop: false,
center: false,
rewind: false,
checkVisibility: true,
mouseDrag: true,
touchDrag: true,
pullDrag: true,
freeDrag: false,
margin: 0,
stagePadding: 0,
merge: false,
mergeFit: true,
autoWidth: false,
startPosition: 0,
rtl: false,
smartSpeed: 250,
fluidSpeed: false,
dragEndSpeed: false,
responsive: {},
responsiveRefreshRate: 200,
responsiveBaseElement: window,
fallbackEasing: 'swing',
slideTransition: '',
info: false,
nestedItemSelector: false,
itemElement: 'div',
stageElement: 'div',
refreshClass: 'owl-refresh',
loadedClass: 'owl-loaded',
loadingClass: 'owl-loading',
rtlClass: 'owl-rtl',
responsiveClass: 'owl-responsive',
dragClass: 'owl-drag',
itemClass: 'owl-item',
stageClass: 'owl-stage',
stageOuterClass: 'owl-stage-outer',
grabClass: 'owl-grab'
};
Owl.Width={
Default: 'default',
Inner: 'inner',
Outer: 'outer'
};
Owl.Type={
Event: 'event',
State: 'state'
};
Owl.Plugins={};
Owl.Workers=[ {
filter: [ 'width', 'settings' ],
run: function(){
this._width=this.$element.width();
}}, {
filter: [ 'width', 'items', 'settings' ],
run: function(cache){
cache.current=this._items&&this._items[this.relative(this._current)];
}}, {
filter: [ 'items', 'settings' ],
run: function(){
this.$stage.children('.cloned').remove();
}}, {
filter: [ 'width', 'items', 'settings' ],
run: function(cache){
var margin=this.settings.margin||'',
grid = !this.settings.autoWidth,
rtl=this.settings.rtl,
css={
'width': 'auto',
'margin-left': rtl ? margin:'',
'margin-right': rtl ? '':margin
};
!grid&&this.$stage.children().css(css);
cache.css=css;
}}, {
filter: [ 'width', 'items', 'settings' ],
run: function(cache){
var width=(this.width() / this.settings.items).toFixed(3) - this.settings.margin,
merge=null,
iterator=this._items.length,
grid = !this.settings.autoWidth,
widths=[];
cache.items={
merge: false,
width: width
};
while (iterator--){
merge=this._mergers[iterator];
merge=this.settings.mergeFit&&Math.min(merge, this.settings.items)||merge;
cache.items.merge=merge > 1||cache.items.merge;
widths[iterator] = !grid ? this._items[iterator].width():width * merge;
}
this._widths=widths;
}}, {
filter: [ 'items', 'settings' ],
run: function(){
var clones=[],
items=this._items,
settings=this.settings,
view=Math.max(settings.items * 2, 4),
size=Math.ceil(items.length / 2) * 2,
repeat=settings.loop&&items.length ? settings.rewind ? view:Math.max(view, size):0,
append='',
prepend='';
repeat /=2;
while (repeat > 0){
clones.push(this.normalize(clones.length / 2, true));
append=append + items[clones[clones.length - 1]][0].outerHTML;
clones.push(this.normalize(items.length - 1 - (clones.length - 1) / 2, true));
prepend=items[clones[clones.length - 1]][0].outerHTML + prepend;
repeat -=1;
}
this._clones=clones;
$(append).addClass('cloned').appendTo(this.$stage);
$(prepend).addClass('cloned').prependTo(this.$stage);
}}, {
filter: [ 'width', 'items', 'settings' ],
run: function(){
var rtl=this.settings.rtl ? 1:-1,
size=this._clones.length + this._items.length,
iterator=-1,
previous=0,
current=0,
coordinates=[];
while (++iterator < size){
previous=coordinates[iterator - 1]||0;
current=this._widths[this.relative(iterator)] + this.settings.margin;
coordinates.push(previous + current * rtl);
}
this._coordinates=coordinates;
}}, {
filter: [ 'width', 'items', 'settings' ],
run: function(){
var padding=this.settings.stagePadding,
coordinates=this._coordinates,
css={
'width': Math.ceil(Math.abs(coordinates[coordinates.length - 1])) + padding * 2,
'padding-left': padding||'',
'padding-right': padding||''
};
this.$stage.css(css);
}}, {
filter: [ 'width', 'items', 'settings' ],
run: function(cache){
var iterator=this._coordinates.length,
grid = !this.settings.autoWidth,
items=this.$stage.children();
if(grid&&cache.items.merge){
while (iterator--){
cache.css.width=this._widths[this.relative(iterator)];
items.eq(iterator).css(cache.css);
}}else if(grid){
cache.css.width=cache.items.width;
items.css(cache.css);
}}
}, {
filter: [ 'items' ],
run: function(){
this._coordinates.length < 1&&this.$stage.removeAttr('style');
}}, {
filter: [ 'width', 'items', 'settings' ],
run: function(cache){
cache.current=cache.current ? this.$stage.children().index(cache.current):0;
cache.current=Math.max(this.minimum(), Math.min(this.maximum(), cache.current));
this.reset(cache.current);
}}, {
filter: [ 'position' ],
run: function(){
this.animate(this.coordinates(this._current));
}}, {
filter: [ 'width', 'position', 'items', 'settings' ],
run: function(){
var rtl=this.settings.rtl ? 1:-1,
padding=this.settings.stagePadding * 2,
begin=this.coordinates(this.current()) + padding,
end=begin + this.width() * rtl,
inner, outer, matches=[], i, n;
for (i=0, n=this._coordinates.length; i < n; i++){
inner=this._coordinates[i - 1]||0;
outer=Math.abs(this._coordinates[i]) + padding * rtl;
if((this.op(inner, '<=', begin)&&(this.op(inner, '>', end)))
|| (this.op(outer, '<', begin)&&this.op(outer, '>', end))){
matches.push(i);
}}
this.$stage.children('.active').removeClass('active');
this.$stage.children(':eq(' + matches.join('), :eq(') + ')').addClass('active');
this.$stage.children('.center').removeClass('center');
if(this.settings.center){
this.$stage.children().eq(this.current()).addClass('center');
}}
} ];
Owl.prototype.initializeStage=function(){
this.$stage=this.$element.find('.' + this.settings.stageClass);
if(this.$stage.length){
return;
}
this.$element.addClass(this.options.loadingClass);
this.$stage=$('<' + this.settings.stageElement + '>', {
"class": this.settings.stageClass
}).wrap($('<div/>', {
"class": this.settings.stageOuterClass
}));
this.$element.append(this.$stage.parent());
};
Owl.prototype.initializeItems=function(){
var $items=this.$element.find('.owl-item');
if($items.length){
this._items=$items.get().map(function(item){
return $(item);
});
this._mergers=this._items.map(function(){
return 1;
});
this.refresh();
return;
}
this.replace(this.$element.children().not(this.$stage.parent()));
if(this.isVisible()){
this.refresh();
}else{
this.invalidate('width');
}
this.$element
.removeClass(this.options.loadingClass)
.addClass(this.options.loadedClass);
};
Owl.prototype.initialize=function(){
this.enter('initializing');
this.trigger('initialize');
this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);
if(this.settings.autoWidth&&!this.is('pre-loading')){
var imgs, nestedSelector, width;
imgs=this.$element.find('img');
nestedSelector=this.settings.nestedItemSelector ? '.' + this.settings.nestedItemSelector:undefined;
width=this.$element.children(nestedSelector).width();
if(imgs.length&&width <=0){
this.preloadAutoWidthImages(imgs);
}}
this.initializeStage();
this.initializeItems();
this.registerEventHandlers();
this.leave('initializing');
this.trigger('initialized');
};
Owl.prototype.isVisible=function(){
return this.settings.checkVisibility
? this.$element.is(':visible')
: true;
};
Owl.prototype.setup=function(){
var viewport=this.viewport(),
overwrites=this.options.responsive,
match=-1,
settings=null;
if(!overwrites){
settings=$.extend({}, this.options);
}else{
$.each(overwrites, function(breakpoint){
if(breakpoint <=viewport&&breakpoint > match){
match=Number(breakpoint);
}});
settings=$.extend({}, this.options, overwrites[match]);
if(typeof settings.stagePadding==='function'){
settings.stagePadding=settings.stagePadding();
}
delete settings.responsive;
if(settings.responsiveClass){
this.$element.attr('class',
this.$element.attr('class').replace(new RegExp('(' + this.options.responsiveClass + '-)\\S+\\s', 'g'), '$1' + match)
);
}}
this.trigger('change', { property: { name: 'settings', value: settings }});
this._breakpoint=match;
this.settings=settings;
this.invalidate('settings');
this.trigger('changed', { property: { name: 'settings', value: this.settings }});
};
Owl.prototype.optionsLogic=function(){
if(this.settings.autoWidth){
this.settings.stagePadding=false;
this.settings.merge=false;
}};
Owl.prototype.prepare=function(item){
var event=this.trigger('prepare', { content: item });
if(!event.data){
event.data=$('<' + this.settings.itemElement + '/>')
.addClass(this.options.itemClass).append(item)
}
this.trigger('prepared', { content: event.data });
return event.data;
};
Owl.prototype.update=function(){
var i=0,
n=this._pipe.length,
filter=$.proxy(function(p){ return this[p] }, this._invalidated),
cache={};
while (i < n){
if(this._invalidated.all||$.grep(this._pipe[i].filter, filter).length > 0){
this._pipe[i].run(cache);
}
i++;
}
this._invalidated={};
!this.is('valid')&&this.enter('valid');
};
Owl.prototype.width=function(dimension){
dimension=dimension||Owl.Width.Default;
switch (dimension){
case Owl.Width.Inner:
case Owl.Width.Outer:
return this._width;
default:
return this._width - this.settings.stagePadding * 2 + this.settings.margin;
}};
Owl.prototype.refresh=function(){
this.enter('refreshing');
this.trigger('refresh');
this.setup();
this.optionsLogic();
this.$element.addClass(this.options.refreshClass);
this.update();
this.$element.removeClass(this.options.refreshClass);
this.leave('refreshing');
this.trigger('refreshed');
};
Owl.prototype.onThrottledResize=function(){
window.clearTimeout(this.resizeTimer);
this.resizeTimer=window.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
};
Owl.prototype.onResize=function(){
if(!this._items.length){
return false;
}
if(this._width===this.$element.width()){
return false;
}
if(!this.isVisible()){
return false;
}
this.enter('resizing');
if(this.trigger('resize').isDefaultPrevented()){
this.leave('resizing');
return false;
}
this.invalidate('width');
this.refresh();
this.leave('resizing');
this.trigger('resized');
};
Owl.prototype.registerEventHandlers=function(){
if($.support.transition){
this.$stage.on($.support.transition.end + '.owl.core', $.proxy(this.onTransitionEnd, this));
}
if(this.settings.responsive!==false){
this.on(window, 'resize', this._handlers.onThrottledResize);
}
if(this.settings.mouseDrag){
this.$element.addClass(this.options.dragClass);
this.$stage.on('mousedown.owl.core', $.proxy(this.onDragStart, this));
this.$stage.on('dragstart.owl.core selectstart.owl.core', function(){ return false });
}
if(this.settings.touchDrag){
this.$stage.on('touchstart.owl.core', $.proxy(this.onDragStart, this));
this.$stage.on('touchcancel.owl.core', $.proxy(this.onDragEnd, this));
}};
Owl.prototype.onDragStart=function(event){
var stage=null;
if(event.which===3){
return;
}
if($.support.transform){
stage=this.$stage.css('transform').replace(/.*\(|\)| /g, '').split(',');
stage={
x: stage[stage.length===16 ? 12:4],
y: stage[stage.length===16 ? 13:5]
};}else{
stage=this.$stage.position();
stage={
x: this.settings.rtl ?
stage.left + this.$stage.width() - this.width() + this.settings.margin :
stage.left,
y: stage.top
};}
if(this.is('animating')){
$.support.transform ? this.animate(stage.x):this.$stage.stop()
this.invalidate('position');
}
this.$element.toggleClass(this.options.grabClass, event.type==='mousedown');
this.speed(0);
this._drag.time=new Date().getTime();
this._drag.target=$(event.target);
this._drag.stage.start=stage;
this._drag.stage.current=stage;
this._drag.pointer=this.pointer(event);
$(document).on('mouseup.owl.core touchend.owl.core', $.proxy(this.onDragEnd, this));
$(document).one('mousemove.owl.core touchmove.owl.core', $.proxy(function(event){
var delta=this.difference(this._drag.pointer, this.pointer(event));
$(document).on('mousemove.owl.core touchmove.owl.core', $.proxy(this.onDragMove, this));
if(Math.abs(delta.x) < Math.abs(delta.y)&&this.is('valid')){
return;
}
event.preventDefault();
this.enter('dragging');
this.trigger('drag');
}, this));
};
Owl.prototype.onDragMove=function(event){
var minimum=null,
maximum=null,
pull=null,
delta=this.difference(this._drag.pointer, this.pointer(event)),
stage=this.difference(this._drag.stage.start, delta);
if(!this.is('dragging')){
return;
}
event.preventDefault();
if(this.settings.loop){
minimum=this.coordinates(this.minimum());
maximum=this.coordinates(this.maximum() + 1) - minimum;
stage.x=(((stage.x - minimum) % maximum + maximum) % maximum) + minimum;
}else{
minimum=this.settings.rtl ? this.coordinates(this.maximum()):this.coordinates(this.minimum());
maximum=this.settings.rtl ? this.coordinates(this.minimum()):this.coordinates(this.maximum());
pull=this.settings.pullDrag ? -1 * delta.x / 5:0;
stage.x=Math.max(Math.min(stage.x, minimum + pull), maximum + pull);
}
this._drag.stage.current=stage;
this.animate(stage.x);
};
Owl.prototype.onDragEnd=function(event){
var delta=this.difference(this._drag.pointer, this.pointer(event)),
stage=this._drag.stage.current,
direction=delta.x > 0 ^ this.settings.rtl ? 'left':'right';
$(document).off('.owl.core');
this.$element.removeClass(this.options.grabClass);
if(delta.x!==0&&this.is('dragging')||!this.is('valid')){
this.speed(this.settings.dragEndSpeed||this.settings.smartSpeed);
this.current(this.closest(stage.x, delta.x!==0 ? direction:this._drag.direction));
this.invalidate('position');
this.update();
this._drag.direction=direction;
if(Math.abs(delta.x) > 3||new Date().getTime() - this._drag.time > 300){
this._drag.target.one('click.owl.core', function(){ return false; });
}}
if(!this.is('dragging')){
return;
}
this.leave('dragging');
this.trigger('dragged');
};
Owl.prototype.closest=function(coordinate, direction){
var position=-1,
pull=30,
width=this.width(),
coordinates=this.coordinates();
if(!this.settings.freeDrag){
$.each(coordinates, $.proxy(function(index, value){
if(direction==='left'&&coordinate > value - pull&&coordinate < value + pull){
position=index;
}else if(direction==='right'&&coordinate > value - width - pull&&coordinate < value - width + pull){
position=index + 1;
}else if(this.op(coordinate, '<', value)
&& this.op(coordinate, '>', coordinates[index + 1]!==undefined ? coordinates[index + 1]:value - width)){
position=direction==='left' ? index + 1:index;
}
return position===-1;
}, this));
}
if(!this.settings.loop){
if(this.op(coordinate, '>', coordinates[this.minimum()])){
position=coordinate=this.minimum();
}else if(this.op(coordinate, '<', coordinates[this.maximum()])){
position=coordinate=this.maximum();
}}
return position;
};
Owl.prototype.animate=function(coordinate){
var animate=this.speed() > 0;
this.is('animating')&&this.onTransitionEnd();
if(animate){
this.enter('animating');
this.trigger('translate');
}
if($.support.transform3d&&$.support.transition){
this.$stage.css({
transform: 'translate3d(' + coordinate + 'px,0px,0px)',
transition: (this.speed() / 1000) + 's' + (
this.settings.slideTransition ? ' ' + this.settings.slideTransition:''
)
});
}else if(animate){
this.$stage.animate({
left: coordinate + 'px'
}, this.speed(), this.settings.fallbackEasing, $.proxy(this.onTransitionEnd, this));
}else{
this.$stage.css({
left: coordinate + 'px'
});
}};
Owl.prototype.is=function(state){
return this._states.current[state]&&this._states.current[state] > 0;
};
Owl.prototype.current=function(position){
if(position===undefined){
return this._current;
}
if(this._items.length===0){
return undefined;
}
position=this.normalize(position);
if(this._current!==position){
var event=this.trigger('change', { property: { name: 'position', value: position }});
if(event.data!==undefined){
position=this.normalize(event.data);
}
this._current=position;
this.invalidate('position');
this.trigger('changed', { property: { name: 'position', value: this._current }});
}
return this._current;
};
Owl.prototype.invalidate=function(part){
if($.type(part)==='string'){
this._invalidated[part]=true;
this.is('valid')&&this.leave('valid');
}
return $.map(this._invalidated, function(v, i){ return i });
};
Owl.prototype.reset=function(position){
position=this.normalize(position);
if(position===undefined){
return;
}
this._speed=0;
this._current=position;
this.suppress([ 'translate', 'translated' ]);
this.animate(this.coordinates(position));
this.release([ 'translate', 'translated' ]);
};
Owl.prototype.normalize=function(position, relative){
var n=this._items.length,
m=relative ? 0:this._clones.length;
if(!this.isNumeric(position)||n < 1){
position=undefined;
}else if(position < 0||position >=n + m){
position=((position - m / 2) % n + n) % n + m / 2;
}
return position;
};
Owl.prototype.relative=function(position){
position -=this._clones.length / 2;
return this.normalize(position, true);
};
Owl.prototype.maximum=function(relative){
var settings=this.settings,
maximum=this._coordinates.length,
iterator,
reciprocalItemsWidth,
elementWidth;
if(settings.loop){
maximum=this._clones.length / 2 + this._items.length - 1;
}else if(settings.autoWidth||settings.merge){
iterator=this._items.length;
if(iterator){
reciprocalItemsWidth=this._items[--iterator].width();
elementWidth=this.$element.width();
while (iterator--){
reciprocalItemsWidth +=this._items[iterator].width() + this.settings.margin;
if(reciprocalItemsWidth > elementWidth){
break;
}}
}
maximum=iterator + 1;
}else if(settings.center){
maximum=this._items.length - 1;
}else{
maximum=this._items.length - settings.items;
}
if(relative){
maximum -=this._clones.length / 2;
}
return Math.max(maximum, 0);
};
Owl.prototype.minimum=function(relative){
return relative ? 0:this._clones.length / 2;
};
Owl.prototype.items=function(position){
if(position===undefined){
return this._items.slice();
}
position=this.normalize(position, true);
return this._items[position];
};
Owl.prototype.mergers=function(position){
if(position===undefined){
return this._mergers.slice();
}
position=this.normalize(position, true);
return this._mergers[position];
};
Owl.prototype.clones=function(position){
var odd=this._clones.length / 2,
even=odd + this._items.length,
map=function(index){ return index % 2===0 ? even + index / 2:odd - (index + 1) / 2 };
if(position===undefined){
return $.map(this._clones, function(v, i){ return map(i) });
}
return $.map(this._clones, function(v, i){ return v===position ? map(i):null });
};
Owl.prototype.speed=function(speed){
if(speed!==undefined){
this._speed=speed;
}
return this._speed;
};
Owl.prototype.coordinates=function(position){
var multiplier=1,
newPosition=position - 1,
coordinate;
if(position===undefined){
return $.map(this._coordinates, $.proxy(function(coordinate, index){
return this.coordinates(index);
}, this));
}
if(this.settings.center){
if(this.settings.rtl){
multiplier=-1;
newPosition=position + 1;
}
coordinate=this._coordinates[position];
coordinate +=(this.width() - coordinate + (this._coordinates[newPosition]||0)) / 2 * multiplier;
}else{
coordinate=this._coordinates[newPosition]||0;
}
coordinate=Math.ceil(coordinate);
return coordinate;
};
Owl.prototype.duration=function(from, to, factor){
if(factor===0){
return 0;
}
return Math.min(Math.max(Math.abs(to - from), 1), 6) * Math.abs((factor||this.settings.smartSpeed));
};
Owl.prototype.to=function(position, speed){
var current=this.current(),
revert=null,
distance=position - this.relative(current),
direction=(distance > 0) - (distance < 0),
items=this._items.length,
minimum=this.minimum(),
maximum=this.maximum();
if(this.settings.loop){
if(!this.settings.rewind&&Math.abs(distance) > items / 2){
distance +=direction * -1 * items;
}
position=current + distance;
revert=((position - minimum) % items + items) % items + minimum;
if(revert!==position&&revert - distance <=maximum&&revert - distance > 0){
current=revert - distance;
position=revert;
this.reset(current);
}}else if(this.settings.rewind){
maximum +=1;
position=(position % maximum + maximum) % maximum;
}else{
position=Math.max(minimum, Math.min(maximum, position));
}
this.speed(this.duration(current, position, speed));
this.current(position);
if(this.isVisible()){
this.update();
}};
Owl.prototype.next=function(speed){
speed=speed||false;
this.to(this.relative(this.current()) + 1, speed);
};
Owl.prototype.prev=function(speed){
speed=speed||false;
this.to(this.relative(this.current()) - 1, speed);
};
Owl.prototype.onTransitionEnd=function(event){
if(event!==undefined){
event.stopPropagation();
if((event.target||event.srcElement||event.originalTarget)!==this.$stage.get(0)){
return false;
}}
this.leave('animating');
this.trigger('translated');
};
Owl.prototype.viewport=function(){
var width;
if(this.options.responsiveBaseElement!==window){
width=$(this.options.responsiveBaseElement).width();
}else if(window.innerWidth){
width=window.innerWidth;
}else if(document.documentElement&&document.documentElement.clientWidth){
width=document.documentElement.clientWidth;
}else{
console.warn('Can not detect viewport width.');
}
return width;
};
Owl.prototype.replace=function(content){
this.$stage.empty();
this._items=[];
if(content){
content=(content instanceof jQuery) ? content:$(content);
}
if(this.settings.nestedItemSelector){
content=content.find('.' + this.settings.nestedItemSelector);
}
content.filter(function(){
return this.nodeType===1;
}).each($.proxy(function(index, item){
item=this.prepare(item);
this.$stage.append(item);
this._items.push(item);
this._mergers.push(item.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1||1);
}, this));
this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition:0);
this.invalidate('items');
};
Owl.prototype.add=function(content, position){
var current=this.relative(this._current);
position=position===undefined ? this._items.length:this.normalize(position, true);
content=content instanceof jQuery ? content:$(content);
this.trigger('add', { content: content, position: position });
content=this.prepare(content);
if(this._items.length===0||position===this._items.length){
this._items.length===0&&this.$stage.append(content);
this._items.length!==0&&this._items[position - 1].after(content);
this._items.push(content);
this._mergers.push(content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1||1);
}else{
this._items[position].before(content);
this._items.splice(position, 0, content);
this._mergers.splice(position, 0, content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1||1);
}
this._items[current]&&this.reset(this._items[current].index());
this.invalidate('items');
this.trigger('added', { content: content, position: position });
};
Owl.prototype.remove=function(position){
position=this.normalize(position, true);
if(position===undefined){
return;
}
this.trigger('remove', { content: this._items[position], position: position });
this._items[position].remove();
this._items.splice(position, 1);
this._mergers.splice(position, 1);
this.invalidate('items');
this.trigger('removed', { content: null, position: position });
};
Owl.prototype.preloadAutoWidthImages=function(images){
images.each($.proxy(function(i, element){
this.enter('pre-loading');
element=$(element);
$(new Image()).one('load', $.proxy(function(e){
element.attr('src', e.target.src);
element.css('opacity', 1);
this.leave('pre-loading');
!this.is('pre-loading')&&!this.is('initializing')&&this.refresh();
}, this)).attr('src', element.attr('src')||element.attr('data-src')||element.attr('data-src-retina'));
}, this));
};
Owl.prototype.destroy=function(){
this.$element.off('.owl.core');
this.$stage.off('.owl.core');
$(document).off('.owl.core');
if(this.settings.responsive!==false){
window.clearTimeout(this.resizeTimer);
this.off(window, 'resize', this._handlers.onThrottledResize);
}
for (var i in this._plugins){
this._plugins[i].destroy();
}
this.$stage.children('.cloned').remove();
this.$stage.unwrap();
this.$stage.children().contents().unwrap();
this.$stage.children().unwrap();
this.$stage.remove();
this.$element
.removeClass(this.options.refreshClass)
.removeClass(this.options.loadingClass)
.removeClass(this.options.loadedClass)
.removeClass(this.options.rtlClass)
.removeClass(this.options.dragClass)
.removeClass(this.options.grabClass)
.attr('class', this.$element.attr('class').replace(new RegExp(this.options.responsiveClass + '-\\S+\\s', 'g'), ''))
.removeData('owl.carousel');
};
Owl.prototype.op=function(a, o, b){
var rtl=this.settings.rtl;
switch (o){
case '<':
return rtl ? a > b:a < b;
case '>':
return rtl ? a < b:a > b;
case '>=':
return rtl ? a <=b:a >=b;
case '<=':
return rtl ? a >=b:a <=b;
default:
break;
}};
Owl.prototype.on=function(element, event, listener, capture){
if(element.addEventListener){
element.addEventListener(event, listener, capture);
}else if(element.attachEvent){
element.attachEvent('on' + event, listener);
}};
Owl.prototype.off=function(element, event, listener, capture){
if(element.removeEventListener){
element.removeEventListener(event, listener, capture);
}else if(element.detachEvent){
element.detachEvent('on' + event, listener);
}};
Owl.prototype.trigger=function(name, data, namespace, state, enter){
var status={
item: { count: this._items.length, index: this.current() }}, handler=$.camelCase($.grep([ 'on', name, namespace ], function(v){ return v })
.join('-').toLowerCase()
), event=$.Event([ name, 'owl', namespace||'carousel' ].join('.').toLowerCase(),
$.extend({ relatedTarget: this }, status, data)
);
if(!this._supress[name]){
$.each(this._plugins, function(name, plugin){
if(plugin.onTrigger){
plugin.onTrigger(event);
}});
this.register({ type: Owl.Type.Event, name: name });
this.$element.trigger(event);
if(this.settings&&typeof this.settings[handler]==='function'){
this.settings[handler].call(this, event);
}}
return event;
};
Owl.prototype.enter=function(name){
$.each([ name ].concat(this._states.tags[name]||[]), $.proxy(function(i, name){
if(this._states.current[name]===undefined){
this._states.current[name]=0;
}
this._states.current[name]++;
}, this));
};
Owl.prototype.leave=function(name){
$.each([ name ].concat(this._states.tags[name]||[]), $.proxy(function(i, name){
this._states.current[name]--;
}, this));
};
Owl.prototype.register=function(object){
if(object.type===Owl.Type.Event){
if(!$.event.special[object.name]){
$.event.special[object.name]={};}
if(!$.event.special[object.name].owl){
var _default=$.event.special[object.name]._default;
$.event.special[object.name]._default=function(e){
if(_default&&_default.apply&&(!e.namespace||e.namespace.indexOf('owl')===-1)){
return _default.apply(this, arguments);
}
return e.namespace&&e.namespace.indexOf('owl') > -1;
};
$.event.special[object.name].owl=true;
}}else if(object.type===Owl.Type.State){
if(!this._states.tags[object.name]){
this._states.tags[object.name]=object.tags;
}else{
this._states.tags[object.name]=this._states.tags[object.name].concat(object.tags);
}
this._states.tags[object.name]=$.grep(this._states.tags[object.name], $.proxy(function(tag, i){
return $.inArray(tag, this._states.tags[object.name])===i;
}, this));
}};
Owl.prototype.suppress=function(events){
$.each(events, $.proxy(function(index, event){
this._supress[event]=true;
}, this));
};
Owl.prototype.release=function(events){
$.each(events, $.proxy(function(index, event){
delete this._supress[event];
}, this));
};
Owl.prototype.pointer=function(event){
var result={ x: null, y: null };
event=event.originalEvent||event||window.event;
event=event.touches&&event.touches.length ?
event.touches[0]:event.changedTouches&&event.changedTouches.length ?
event.changedTouches[0]:event;
if(event.pageX){
result.x=event.pageX;
result.y=event.pageY;
}else{
result.x=event.clientX;
result.y=event.clientY;
}
return result;
};
Owl.prototype.isNumeric=function(number){
return !isNaN(parseFloat(number));
};
Owl.prototype.difference=function(first, second){
return {
x: first.x - second.x,
y: first.y - second.y
};};
$.fn.owlCarousel=function(option){
var args=Array.prototype.slice.call(arguments, 1);
return this.each(function(){
var $this=$(this),
data=$this.data('owl.carousel');
if(!data){
data=new Owl(this, typeof option=='object'&&option);
$this.data('owl.carousel', data);
$.each([
'next', 'prev', 'to', 'destroy', 'refresh', 'replace', 'add', 'remove'
], function(i, event){
data.register({ type: Owl.Type.Event, name: event });
data.$element.on(event + '.owl.carousel.core', $.proxy(function(e){
if(e.namespace&&e.relatedTarget!==this){
this.suppress([ event ]);
data[event].apply(this, [].slice.call(arguments, 1));
this.release([ event ]);
}}, data));
});
}
if(typeof option=='string'&&option.charAt(0)!=='_'){
data[option].apply(data, args);
}});
};
$.fn.owlCarousel.Constructor=Owl;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
var AutoRefresh=function(carousel){
this._core=carousel;
this._interval=null;
this._visible=null;
this._handlers={
'initialized.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.autoRefresh){
this.watch();
}}, this)
};
this._core.options=$.extend({}, AutoRefresh.Defaults, this._core.options);
this._core.$element.on(this._handlers);
};
AutoRefresh.Defaults={
autoRefresh: true,
autoRefreshInterval: 500
};
AutoRefresh.prototype.watch=function(){
if(this._interval){
return;
}
this._visible=this._core.isVisible();
this._interval=window.setInterval($.proxy(this.refresh, this), this._core.settings.autoRefreshInterval);
};
AutoRefresh.prototype.refresh=function(){
if(this._core.isVisible()===this._visible){
return;
}
this._visible = !this._visible;
this._core.$element.toggleClass('owl-hidden', !this._visible);
this._visible&&(this._core.invalidate('width')&&this._core.refresh());
};
AutoRefresh.prototype.destroy=function(){
var handler, property;
window.clearInterval(this._interval);
for (handler in this._handlers){
this._core.$element.off(handler, this._handlers[handler]);
}
for (property in Object.getOwnPropertyNames(this)){
typeof this[property]!='function'&&(this[property]=null);
}};
$.fn.owlCarousel.Constructor.Plugins.AutoRefresh=AutoRefresh;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
var Lazy=function(carousel){
this._core=carousel;
this._loaded=[];
this._handlers={
'initialized.owl.carousel change.owl.carousel resized.owl.carousel': $.proxy(function(e){
if(!e.namespace){
return;
}
if(!this._core.settings||!this._core.settings.lazyLoad){
return;
}
if((e.property&&e.property.name=='position')||e.type=='initialized'){
var settings=this._core.settings,
n=(settings.center&&Math.ceil(settings.items / 2)||settings.items),
i=((settings.center&&n * -1)||0),
position=(e.property&&e.property.value!==undefined ? e.property.value:this._core.current()) + i,
clones=this._core.clones().length,
load=$.proxy(function(i, v){ this.load(v) }, this);
if(settings.lazyLoadEager > 0){
n +=settings.lazyLoadEager;
if(settings.loop){
position -=settings.lazyLoadEager;
n++;
}}
while (i++ < n){
this.load(clones / 2 + this._core.relative(position));
clones&&$.each(this._core.clones(this._core.relative(position)), load);
position++;
}}
}, this)
};
this._core.options=$.extend({}, Lazy.Defaults, this._core.options);
this._core.$element.on(this._handlers);
};
Lazy.Defaults={
lazyLoad: false,
lazyLoadEager: 0
};
Lazy.prototype.load=function(position){
var $item=this._core.$stage.children().eq(position),
$elements=$item&&$item.find('.owl-lazy');
if(!$elements||$.inArray($item.get(0), this._loaded) > -1){
return;
}
$elements.each($.proxy(function(index, element){
var $element=$(element), image,
url=(window.devicePixelRatio > 1&&$element.attr('data-src-retina'))||$element.attr('data-src')||$element.attr('data-srcset');
this._core.trigger('load', { element: $element, url: url }, 'lazy');
if($element.is('img')){
$element.one('load.owl.lazy', $.proxy(function(){
$element.css('opacity', 1);
this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
}, this)).attr('src', url);
}else if($element.is('source')){
$element.one('load.owl.lazy', $.proxy(function(){
this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
}, this)).attr('srcset', url);
}else{
image=new Image();
image.onload=$.proxy(function(){
$element.css({
'background-image': 'url("' + url + '")',
'opacity': '1'
});
this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
}, this);
image.src=url;
}}, this));
this._loaded.push($item.get(0));
};
Lazy.prototype.destroy=function(){
var handler, property;
for (handler in this.handlers){
this._core.$element.off(handler, this.handlers[handler]);
}
for (property in Object.getOwnPropertyNames(this)){
typeof this[property]!='function'&&(this[property]=null);
}};
$.fn.owlCarousel.Constructor.Plugins.Lazy=Lazy;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
var AutoHeight=function(carousel){
this._core=carousel;
this._previousHeight=null;
this._handlers={
'initialized.owl.carousel refreshed.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.autoHeight){
this.update();
}}, this),
'changed.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.autoHeight&&e.property.name==='position'){
this.update();
}}, this),
'loaded.owl.lazy': $.proxy(function(e){
if(e.namespace&&this._core.settings.autoHeight
&& e.element.closest('.' + this._core.settings.itemClass).index()===this._core.current()){
this.update();
}}, this)
};
this._core.options=$.extend({}, AutoHeight.Defaults, this._core.options);
this._core.$element.on(this._handlers);
this._intervalId=null;
var refThis=this;
$(window).on('load', function(){
if(refThis._core.settings.autoHeight){
refThis.update();
}});
$(window).resize(function(){
if(refThis._core.settings.autoHeight){
if(refThis._intervalId!=null){
clearTimeout(refThis._intervalId);
}
refThis._intervalId=setTimeout(function(){
refThis.update();
}, 250);
}});
};
AutoHeight.Defaults={
autoHeight: false,
autoHeightClass: 'owl-height'
};
AutoHeight.prototype.update=function(){
var start=this._core._current,
end=start + this._core.settings.items,
lazyLoadEnabled=this._core.settings.lazyLoad,
visible=this._core.$stage.children().toArray().slice(start, end),
heights=[],
maxheight=0;
$.each(visible, function(index, item){
heights.push($(item).height());
});
maxheight=Math.max.apply(null, heights);
if(maxheight <=1&&lazyLoadEnabled&&this._previousHeight){
maxheight=this._previousHeight;
}
this._previousHeight=maxheight;
this._core.$stage.parent()
.height(maxheight)
.addClass(this._core.settings.autoHeightClass);
};
AutoHeight.prototype.destroy=function(){
var handler, property;
for (handler in this._handlers){
this._core.$element.off(handler, this._handlers[handler]);
}
for (property in Object.getOwnPropertyNames(this)){
typeof this[property]!=='function'&&(this[property]=null);
}};
$.fn.owlCarousel.Constructor.Plugins.AutoHeight=AutoHeight;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
var Video=function(carousel){
this._core=carousel;
this._videos={};
this._playing=null;
this._handlers={
'initialized.owl.carousel': $.proxy(function(e){
if(e.namespace){
this._core.register({ type: 'state', name: 'playing', tags: [ 'interacting' ] });
}}, this),
'resize.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.video&&this.isInFullScreen()){
e.preventDefault();
}}, this),
'refreshed.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.is('resizing')){
this._core.$stage.find('.cloned .owl-video-frame').remove();
}}, this),
'changed.owl.carousel': $.proxy(function(e){
if(e.namespace&&e.property.name==='position'&&this._playing){
this.stop();
}}, this),
'prepared.owl.carousel': $.proxy(function(e){
if(!e.namespace){
return;
}
var $element=$(e.content).find('.owl-video');
if($element.length){
$element.css('display', 'none');
this.fetch($element, $(e.content));
}}, this)
};
this._core.options=$.extend({}, Video.Defaults, this._core.options);
this._core.$element.on(this._handlers);
this._core.$element.on('click.owl.video', '.owl-video-play-icon', $.proxy(function(e){
this.play(e);
}, this));
};
Video.Defaults={
video: false,
videoHeight: false,
videoWidth: false
};
Video.prototype.fetch=function(target, item){
var type=(function(){
if(target.attr('data-vimeo-id')){
return 'vimeo';
}else if(target.attr('data-vzaar-id')){
return 'vzaar'
}else{
return 'youtube';
}})(),
id=target.attr('data-vimeo-id')||target.attr('data-youtube-id')||target.attr('data-vzaar-id'),
width=target.attr('data-width')||this._core.settings.videoWidth,
height=target.attr('data-height')||this._core.settings.videoHeight,
url=target.attr('href');
if(url){
id=url.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
if(id[3].indexOf('youtu') > -1){
type='youtube';
}else if(id[3].indexOf('vimeo') > -1){
type='vimeo';
}else if(id[3].indexOf('vzaar') > -1){
type='vzaar';
}else{
throw new Error('Video URL not supported.');
}
id=id[6];
}else{
throw new Error('Missing video URL.');
}
this._videos[url]={
type: type,
id: id,
width: width,
height: height
};
item.attr('data-video', url);
this.thumbnail(target, this._videos[url]);
};
Video.prototype.thumbnail=function(target, video){
var tnLink,
icon,
path,
dimensions=video.width&&video.height ? 'width:' + video.width + 'px;height:' + video.height + 'px;':'',
customTn=target.find('img'),
srcType='src',
lazyClass='',
settings=this._core.settings,
create=function(path){
icon='<div class="owl-video-play-icon"></div>';
if(settings.lazyLoad){
tnLink=$('<div/>',{
"class": 'owl-video-tn ' + lazyClass,
"srcType": path
});
}else{
tnLink=$('<div/>', {
"class": "owl-video-tn",
"style": 'opacity:1;background-image:url(' + path + ')'
});
}
target.after(tnLink);
target.after(icon);
};
target.wrap($('<div/>', {
"class": "owl-video-wrapper",
"style": dimensions
}));
if(this._core.settings.lazyLoad){
srcType='data-src';
lazyClass='owl-lazy';
}
if(customTn.length){
create(customTn.attr(srcType));
customTn.remove();
return false;
}
if(video.type==='youtube'){
path="//img.youtube.com/vi/" + video.id + "/hqdefault.jpg";
create(path);
}else if(video.type==='vimeo'){
$.ajax({
type: 'GET',
url: '//vimeo.com/api/v2/video/' + video.id + '.json',
jsonp: 'callback',
dataType: 'jsonp',
success: function(data){
path=data[0].thumbnail_large;
create(path);
}});
}else if(video.type==='vzaar'){
$.ajax({
type: 'GET',
url: '//vzaar.com/api/videos/' + video.id + '.json',
jsonp: 'callback',
dataType: 'jsonp',
success: function(data){
path=data.framegrab_url;
create(path);
}});
}};
Video.prototype.stop=function(){
this._core.trigger('stop', null, 'video');
this._playing.find('.owl-video-frame').remove();
this._playing.removeClass('owl-video-playing');
this._playing=null;
this._core.leave('playing');
this._core.trigger('stopped', null, 'video');
};
Video.prototype.play=function(event){
var target=$(event.target),
item=target.closest('.' + this._core.settings.itemClass),
video=this._videos[item.attr('data-video')],
width=video.width||'100%',
height=video.height||this._core.$stage.height(),
html,
iframe;
if(this._playing){
return;
}
this._core.enter('playing');
this._core.trigger('play', null, 'video');
item=this._core.items(this._core.relative(item.index()));
this._core.reset(item.index());
html=$('<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>');
html.attr('height', height);
html.attr('width', width);
if(video.type==='youtube'){
html.attr('src', '//www.youtube.com/embed/' + video.id + '?autoplay=1&rel=0&v=' + video.id);
}else if(video.type==='vimeo'){
html.attr('src', '//player.vimeo.com/video/' + video.id + '?autoplay=1');
}else if(video.type==='vzaar'){
html.attr('src', '//view.vzaar.com/' + video.id + '/player?autoplay=true');
}
iframe=$(html).wrap('<div class="owl-video-frame" />').insertAfter(item.find('.owl-video'));
this._playing=item.addClass('owl-video-playing');
};
Video.prototype.isInFullScreen=function(){
var element=document.fullscreenElement||document.mozFullScreenElement ||
document.webkitFullscreenElement;
return element&&$(element).parent().hasClass('owl-video-frame');
};
Video.prototype.destroy=function(){
var handler, property;
this._core.$element.off('click.owl.video');
for (handler in this._handlers){
this._core.$element.off(handler, this._handlers[handler]);
}
for (property in Object.getOwnPropertyNames(this)){
typeof this[property]!='function'&&(this[property]=null);
}};
$.fn.owlCarousel.Constructor.Plugins.Video=Video;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
var Animate=function(scope){
this.core=scope;
this.core.options=$.extend({}, Animate.Defaults, this.core.options);
this.swapping=true;
this.previous=undefined;
this.next=undefined;
this.handlers={
'change.owl.carousel': $.proxy(function(e){
if(e.namespace&&e.property.name=='position'){
this.previous=this.core.current();
this.next=e.property.value;
}}, this),
'drag.owl.carousel dragged.owl.carousel translated.owl.carousel': $.proxy(function(e){
if(e.namespace){
this.swapping=e.type=='translated';
}}, this),
'translate.owl.carousel': $.proxy(function(e){
if(e.namespace&&this.swapping&&(this.core.options.animateOut||this.core.options.animateIn)){
this.swap();
}}, this)
};
this.core.$element.on(this.handlers);
};
Animate.Defaults={
animateOut: false,
animateIn: false
};
Animate.prototype.swap=function(){
if(this.core.settings.items!==1){
return;
}
if(!$.support.animation||!$.support.transition){
return;
}
this.core.speed(0);
var left,
clear=$.proxy(this.clear, this),
previous=this.core.$stage.children().eq(this.previous),
next=this.core.$stage.children().eq(this.next),
incoming=this.core.settings.animateIn,
outgoing=this.core.settings.animateOut;
if(this.core.current()===this.previous){
return;
}
if(outgoing){
left=this.core.coordinates(this.previous) - this.core.coordinates(this.next);
previous.one($.support.animation.end, clear)
.css({ 'left': left + 'px' })
.addClass('animated owl-animated-out')
.addClass(outgoing);
}
if(incoming){
next.one($.support.animation.end, clear)
.addClass('animated owl-animated-in')
.addClass(incoming);
}};
Animate.prototype.clear=function(e){
$(e.target).css({ 'left': '' })
.removeClass('animated owl-animated-out owl-animated-in')
.removeClass(this.core.settings.animateIn)
.removeClass(this.core.settings.animateOut);
this.core.onTransitionEnd();
};
Animate.prototype.destroy=function(){
var handler, property;
for (handler in this.handlers){
this.core.$element.off(handler, this.handlers[handler]);
}
for (property in Object.getOwnPropertyNames(this)){
typeof this[property]!='function'&&(this[property]=null);
}};
$.fn.owlCarousel.Constructor.Plugins.Animate=Animate;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
var Autoplay=function(carousel){
this._core=carousel;
this._call=null;
this._time=0;
this._timeout=0;
this._paused=true;
this._handlers={
'changed.owl.carousel': $.proxy(function(e){
if(e.namespace&&e.property.name==='settings'){
if(this._core.settings.autoplay){
this.play();
}else{
this.stop();
}}else if(e.namespace&&e.property.name==='position'&&this._paused){
this._time=0;
}}, this),
'initialized.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.autoplay){
this.play();
}}, this),
'play.owl.autoplay': $.proxy(function(e, t, s){
if(e.namespace){
this.play(t, s);
}}, this),
'stop.owl.autoplay': $.proxy(function(e){
if(e.namespace){
this.stop();
}}, this),
'mouseover.owl.autoplay': $.proxy(function(){
if(this._core.settings.autoplayHoverPause&&this._core.is('rotating')){
this.pause();
}}, this),
'mouseleave.owl.autoplay': $.proxy(function(){
if(this._core.settings.autoplayHoverPause&&this._core.is('rotating')){
this.play();
}}, this),
'touchstart.owl.core': $.proxy(function(){
if(this._core.settings.autoplayHoverPause&&this._core.is('rotating')){
this.pause();
}}, this),
'touchend.owl.core': $.proxy(function(){
if(this._core.settings.autoplayHoverPause){
this.play();
}}, this)
};
this._core.$element.on(this._handlers);
this._core.options=$.extend({}, Autoplay.Defaults, this._core.options);
};
Autoplay.Defaults={
autoplay: false,
autoplayTimeout: 5000,
autoplayHoverPause: false,
autoplaySpeed: false
};
Autoplay.prototype._next=function(speed){
this._call=window.setTimeout($.proxy(this._next, this, speed),
this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()
);
if(this._core.is('interacting')||document.hidden){
return;
}
this._core.next(speed||this._core.settings.autoplaySpeed);
}
Autoplay.prototype.read=function(){
return new Date().getTime() - this._time;
};
Autoplay.prototype.play=function(timeout, speed){
var elapsed;
if(!this._core.is('rotating')){
this._core.enter('rotating');
}
timeout=timeout||this._core.settings.autoplayTimeout;
elapsed=Math.min(this._time % (this._timeout||timeout), timeout);
if(this._paused){
this._time=this.read();
this._paused=false;
}else{
window.clearTimeout(this._call);
}
this._time +=this.read() % timeout - elapsed;
this._timeout=timeout;
this._call=window.setTimeout($.proxy(this._next, this, speed), timeout - elapsed);
};
Autoplay.prototype.stop=function(){
if(this._core.is('rotating')){
this._time=0;
this._paused=true;
window.clearTimeout(this._call);
this._core.leave('rotating');
}};
Autoplay.prototype.pause=function(){
if(this._core.is('rotating')&&!this._paused){
this._time=this.read();
this._paused=true;
window.clearTimeout(this._call);
}};
Autoplay.prototype.destroy=function(){
var handler, property;
this.stop();
for (handler in this._handlers){
this._core.$element.off(handler, this._handlers[handler]);
}
for (property in Object.getOwnPropertyNames(this)){
typeof this[property]!='function'&&(this[property]=null);
}};
$.fn.owlCarousel.Constructor.Plugins.autoplay=Autoplay;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
'use strict';
var Navigation=function(carousel){
this._core=carousel;
this._initialized=false;
this._pages=[];
this._controls={};
this._templates=[];
this.$element=this._core.$element;
this._overrides={
next: this._core.next,
prev: this._core.prev,
to: this._core.to
};
this._handlers={
'prepared.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.dotsData){
this._templates.push('<div class="' + this._core.settings.dotClass + '">' +
$(e.content).find('[data-dot]').addBack('[data-dot]').attr('data-dot') + '</div>');
}}, this),
'added.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.dotsData){
this._templates.splice(e.position, 0, this._templates.pop());
}}, this),
'remove.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.dotsData){
this._templates.splice(e.position, 1);
}}, this),
'changed.owl.carousel': $.proxy(function(e){
if(e.namespace&&e.property.name=='position'){
this.draw();
}}, this),
'initialized.owl.carousel': $.proxy(function(e){
if(e.namespace&&!this._initialized){
this._core.trigger('initialize', null, 'navigation');
this.initialize();
this.update();
this.draw();
this._initialized=true;
this._core.trigger('initialized', null, 'navigation');
}}, this),
'refreshed.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._initialized){
this._core.trigger('refresh', null, 'navigation');
this.update();
this.draw();
this._core.trigger('refreshed', null, 'navigation');
}}, this)
};
this._core.options=$.extend({}, Navigation.Defaults, this._core.options);
this.$element.on(this._handlers);
};
Navigation.Defaults={
nav: false,
navText: [
'<span aria-label="' + 'Previous' + '">&#x2039;</span>',
'<span aria-label="' + 'Next' + '">&#x203a;</span>'
],
navSpeed: false,
navElement: 'button type="button" role="presentation"',
navContainer: false,
navContainerClass: 'owl-nav',
navClass: [
'owl-prev',
'owl-next'
],
slideBy: 1,
dotClass: 'owl-dot',
dotsClass: 'owl-dots',
dots: true,
dotsEach: false,
dotsData: false,
dotsSpeed: false,
dotsContainer: false
};
Navigation.prototype.initialize=function(){
var override,
settings=this._core.settings;
this._controls.$relative=(settings.navContainer ? $(settings.navContainer)
: $('<div>').addClass(settings.navContainerClass).appendTo(this.$element)).addClass('disabled');
this._controls.$previous=$('<' + settings.navElement + '>')
.addClass(settings.navClass[0])
.html(settings.navText[0])
.prependTo(this._controls.$relative)
.on('click', $.proxy(function(e){
this.prev(settings.navSpeed);
}, this));
this._controls.$next=$('<' + settings.navElement + '>')
.addClass(settings.navClass[1])
.html(settings.navText[1])
.appendTo(this._controls.$relative)
.on('click', $.proxy(function(e){
this.next(settings.navSpeed);
}, this));
if(!settings.dotsData){
this._templates=[ $('<button role="button">')
.addClass(settings.dotClass)
.append($('<span>'))
.prop('outerHTML') ];
}
this._controls.$absolute=(settings.dotsContainer ? $(settings.dotsContainer)
: $('<div>').addClass(settings.dotsClass).appendTo(this.$element)).addClass('disabled');
this._controls.$absolute.on('click', 'button', $.proxy(function(e){
var index=$(e.target).parent().is(this._controls.$absolute)
? $(e.target).index():$(e.target).parent().index();
e.preventDefault();
this.to(index, settings.dotsSpeed);
}, this));
/*$el.on('focusin', function(){
$(document).off(".carousel");
$(document).on('keydown.carousel', function(e){
if(e.keyCode==37){
$el.trigger('prev.owl')
}
if(e.keyCode==39){
$el.trigger('next.owl')
}});
});*/
for (override in this._overrides){
this._core[override]=$.proxy(this[override], this);
}};
Navigation.prototype.destroy=function(){
var handler, control, property, override, settings;
settings=this._core.settings;
for (handler in this._handlers){
this.$element.off(handler, this._handlers[handler]);
}
for (control in this._controls){
if(control==='$relative'&&settings.navContainer){
this._controls[control].html('');
}else{
this._controls[control].remove();
}}
for (override in this.overides){
this._core[override]=this._overrides[override];
}
for (property in Object.getOwnPropertyNames(this)){
typeof this[property]!='function'&&(this[property]=null);
}};
Navigation.prototype.update=function(){
var i, j, k,
lower=this._core.clones().length / 2,
upper=lower + this._core.items().length,
maximum=this._core.maximum(true),
settings=this._core.settings,
size=settings.center||settings.autoWidth||settings.dotsData
? 1:settings.dotsEach||settings.items;
if(settings.slideBy!=='page'){
settings.slideBy=Math.min(settings.slideBy, settings.items);
}
if(settings.dots||settings.slideBy=='page'){
this._pages=[];
for (i=lower, j=0, k=0; i < upper; i++){
if(j >=size||j===0){
this._pages.push({
start: Math.min(maximum, i - lower),
end: i - lower + size - 1
});
if(Math.min(maximum, i - lower)===maximum){
break;
}
j=0, ++k;
}
j +=this._core.mergers(this._core.relative(i));
}}
};
Navigation.prototype.draw=function(){
var difference,
settings=this._core.settings,
disabled=this._core.items().length <=settings.items,
index=this._core.relative(this._core.current()),
loop=settings.loop||settings.rewind;
this._controls.$relative.toggleClass('disabled', !settings.nav||disabled);
if(settings.nav){
this._controls.$previous.toggleClass('disabled', !loop&&index <=this._core.minimum(true));
this._controls.$next.toggleClass('disabled', !loop&&index >=this._core.maximum(true));
}
this._controls.$absolute.toggleClass('disabled', !settings.dots||disabled);
if(settings.dots){
difference=this._pages.length - this._controls.$absolute.children().length;
if(settings.dotsData&&difference!==0){
this._controls.$absolute.html(this._templates.join(''));
}else if(difference > 0){
this._controls.$absolute.append(new Array(difference + 1).join(this._templates[0]));
}else if(difference < 0){
this._controls.$absolute.children().slice(difference).remove();
}
this._controls.$absolute.find('.active').removeClass('active');
this._controls.$absolute.children().eq($.inArray(this.current(), this._pages)).addClass('active');
}};
Navigation.prototype.onTrigger=function(event){
var settings=this._core.settings;
event.page={
index: $.inArray(this.current(), this._pages),
count: this._pages.length,
size: settings&&(settings.center||settings.autoWidth||settings.dotsData
? 1:settings.dotsEach||settings.items)
};};
Navigation.prototype.current=function(){
var current=this._core.relative(this._core.current());
return $.grep(this._pages, $.proxy(function(page, index){
return page.start <=current&&page.end >=current;
}, this)).pop();
};
Navigation.prototype.getPosition=function(successor){
var position, length,
settings=this._core.settings;
if(settings.slideBy=='page'){
position=$.inArray(this.current(), this._pages);
length=this._pages.length;
successor ? ++position:--position;
position=this._pages[((position % length) + length) % length].start;
}else{
position=this._core.relative(this._core.current());
length=this._core.items().length;
successor ? position +=settings.slideBy:position -=settings.slideBy;
}
return position;
};
Navigation.prototype.next=function(speed){
$.proxy(this._overrides.to, this._core)(this.getPosition(true), speed);
};
Navigation.prototype.prev=function(speed){
$.proxy(this._overrides.to, this._core)(this.getPosition(false), speed);
};
Navigation.prototype.to=function(position, speed, standard){
var length;
if(!standard&&this._pages.length){
length=this._pages.length;
$.proxy(this._overrides.to, this._core)(this._pages[((position % length) + length) % length].start, speed);
}else{
$.proxy(this._overrides.to, this._core)(position, speed);
}};
$.fn.owlCarousel.Constructor.Plugins.Navigation=Navigation;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
'use strict';
var Hash=function(carousel){
this._core=carousel;
this._hashes={};
this.$element=this._core.$element;
this._handlers={
'initialized.owl.carousel': $.proxy(function(e){
if(e.namespace&&this._core.settings.startPosition==='URLHash'){
$(window).trigger('hashchange.owl.navigation');
}}, this),
'prepared.owl.carousel': $.proxy(function(e){
if(e.namespace){
var hash=$(e.content).find('[data-hash]').addBack('[data-hash]').attr('data-hash');
if(!hash){
return;
}
this._hashes[hash]=e.content;
}}, this),
'changed.owl.carousel': $.proxy(function(e){
if(e.namespace&&e.property.name==='position'){
var current=this._core.items(this._core.relative(this._core.current())),
hash=$.map(this._hashes, function(item, hash){
return item===current ? hash:null;
}).join();
if(!hash||window.location.hash.slice(1)===hash){
return;
}
window.location.hash=hash;
}}, this)
};
this._core.options=$.extend({}, Hash.Defaults, this._core.options);
this.$element.on(this._handlers);
$(window).on('hashchange.owl.navigation', $.proxy(function(e){
var hash=window.location.hash.substring(1),
items=this._core.$stage.children(),
position=this._hashes[hash]&&items.index(this._hashes[hash]);
if(position===undefined||position===this._core.current()){
return;
}
this._core.to(this._core.relative(position), false, true);
}, this));
};
Hash.Defaults={
URLhashListener: false
};
Hash.prototype.destroy=function(){
var handler, property;
$(window).off('hashchange.owl.navigation');
for (handler in this._handlers){
this._core.$element.off(handler, this._handlers[handler]);
}
for (property in Object.getOwnPropertyNames(this)){
typeof this[property]!='function'&&(this[property]=null);
}};
$.fn.owlCarousel.Constructor.Plugins.Hash=Hash;
})(window.Zepto||window.jQuery, window, document);
;(function($, window, document, undefined){
var style=$('<support>').get(0).style,
prefixes='Webkit Moz O ms'.split(' '),
events={
transition: {
end: {
WebkitTransition: 'webkitTransitionEnd',
MozTransition: 'transitionend',
OTransition: 'oTransitionEnd',
transition: 'transitionend'
}},
animation: {
end: {
WebkitAnimation: 'webkitAnimationEnd',
MozAnimation: 'animationend',
OAnimation: 'oAnimationEnd',
animation: 'animationend'
}}
},
tests={
csstransforms: function(){
return !!test('transform');
},
csstransforms3d: function(){
return !!test('perspective');
},
csstransitions: function(){
return !!test('transition');
},
cssanimations: function(){
return !!test('animation');
}};
function test(property, prefixed){
var result=false,
upper=property.charAt(0).toUpperCase() + property.slice(1);
$.each((property + ' ' + prefixes.join(upper + ' ') + upper).split(' '), function(i, property){
if(style[property]!==undefined){
result=prefixed ? property:true;
return false;
}});
return result;
}
function prefixed(property){
return test(property, true);
}
if(tests.csstransitions()){
$.support.transition=new String(prefixed('transition'))
$.support.transition.end=events.transition.end[ $.support.transition ];
}
if(tests.cssanimations()){
$.support.animation=new String(prefixed('animation'))
$.support.animation.end=events.animation.end[ $.support.animation ];
}
if(tests.csstransforms()){
$.support.transform=new String(prefixed('transform'));
$.support.transform3d=tests.csstransforms3d();
}})(window.Zepto||window.jQuery, window, document);