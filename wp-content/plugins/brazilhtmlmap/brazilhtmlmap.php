<?php
/*
Plugin Name: Interactive Map of Brazil for WP
Plugin URI: https://www.fla-shop.com
Description: High-quality map plugin of Brazil for WordPress. The map depicts regions and features color, font, landing page and popup customization
Text Domain: brazil-html5-map
Domain Path: /languages
Version: 2.9.9.6
Author: Fla-shop.com
Author URI: https://www.fla-shop.com
License:
*/

add_action('plugins_loaded', 'brazil_html5map_plugin_load_domain' );
function brazil_html5map_plugin_load_domain() {
    load_plugin_textdomain( 'brazil-html5-map', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
if (isset($_REQUEST['action']) && $_REQUEST['action']=='brazil-html5-map-export') { brazil_html5map_plugin_export(); }
if (isset($_REQUEST['action']) && $_REQUEST['action']=='brazil-html5-map-export-csv') { brazil_html5map_plugin_export_csv(); }

add_action('admin_menu', 'brazil_html5map_plugin_menu');


function brazil_html5map_plugin_menu() {

    global $wp_roles;

    $role = "brazilhtml5map_manage_role";

    add_menu_page(__('Brazil Map', 'brazil-html5-map'), __('Brazil Map', 'brazil-html5-map'), $role, 'brazil-html5-map-options', 'brazil_html5map_plugin_options' );

    add_submenu_page('brazil-html5-map-options', __('General Settings', 'brazil-html5-map'), __('General Settings', 'brazil-html5-map'), $role, 'brazil-html5-map-options', 'brazil_html5map_plugin_options' );
    add_submenu_page('brazil-html5-map-options', __('Detailed settings', 'brazil-html5-map'), __('Detailed settings', 'brazil-html5-map'), $role, 'brazil-html5-map-states', 'brazil_html5map_plugin_states');
    add_submenu_page('brazil-html5-map-options', __('Groups settings', 'brazil-html5-map'), __('Groups settings', 'brazil-html5-map'), $role, 'brazil-html5-map-groups', 'brazil_html5map_plugin_groups');
    add_submenu_page('brazil-html5-map-options', __('Points settings', 'brazil-html5-map'), __('Points settings', 'brazil-html5-map'), $role, 'brazil-html5-map-points', 'brazil_html5map_plugin_points');
    add_submenu_page('brazil-html5-map-options', __('Tools', 'brazil-html5-map'), __('Tools', 'brazil-html5-map'), $role, 'brazil-html5-map-tools', 'brazil_html5map_plugin_tools');
    add_submenu_page('brazil-html5-map-options', __('Map Preview', 'brazil-html5-map'), __('Map Preview', 'brazil-html5-map'), $role, 'brazil-html5-map-view', 'brazil_html5map_plugin_view');

    add_submenu_page('brazil-html5-map-options', __('Maps dashboard', 'brazil-html5-map'), __('Maps', 'brazil-html5-map'), $role, 'brazil-html5-map-maps', 'brazil_html5map_plugin_maps');



}

function brazil_html5map_plugin_nav_tabs($page, $map_id)
{
?>
<h2 class="nav-tab-wrapper">
    <a href="?page=brazil-html5-map-options&map_id=<?php echo $map_id ?>" class="nav-tab <?php echo $page == 'options' ? 'nav-tab-active' : '' ?>"><?php _e('General settings', 'brazil-html5-map') ?></a>
    <a href="?page=brazil-html5-map-states&map_id=<?php echo $map_id ?>" class="nav-tab <?php echo $page == 'states' ? 'nav-tab-active' : '' ?>"><?php _e('Detailed settings', 'brazil-html5-map') ?></a>
    <a href="?page=brazil-html5-map-groups&map_id=<?php echo $map_id ?>" class="nav-tab <?php echo $page == 'groups' ? 'nav-tab-active' : '' ?>"><?php _e('Groups settings', 'brazil-html5-map') ?></a>
    <a href="?page=brazil-html5-map-points&map_id=<?php echo $map_id ?>" class="nav-tab <?php echo $page == 'points' ? 'nav-tab-active' : '' ?>"><?php _e('Points settings', 'brazil-html5-map') ?></a>
    <a href="?page=brazil-html5-map-tools&map_id=<?php echo $map_id ?>" class="nav-tab <?php echo $page == 'tools' ? 'nav-tab-active' : '' ?>"><?php _e('Tools', 'brazil-html5-map') ?></a>
    <a href="?page=brazil-html5-map-view&map_id=<?php echo $map_id ?>" class="nav-tab <?php echo $page == 'view' ? 'nav-tab-active' : '' ?>"><?php _e('Preview', 'brazil-html5-map') ?></a>
</h2>
<?php
}

function brazil_html5map_plugin_map_selector($page, $map_id, &$options) {
?>
<script type="text/javascript">
jQuery(function($){
    $('select[name=map_id]').change(function() {
        location.href='admin.php?page=brazil-html5-map-<?php echo $page ?>&map_id='+$(this).val();
    });
    $('.tipsy-q').tipsy({gravity: 'w'}).find('span').css('cursor', 'default');
});
</script>
<span class="title" style="width: 100px"><?php echo __('Select a map:', 'brazil-html5-map'); ?> </span>
    <select name="map_id" style="width: 185px;">
        <?php foreach($options as $id => $map_data) { ?>
            <option value="<?php echo $id; ?>" <?php echo ($id==$map_id) ? 'selected' : '';?>><?php echo $map_data['name'] . (isset($map_data['type']) ? " ($map_data[type])" : ''); ?></option>
        <?php } ?>
    </select>
    <span class="tipsy-q" original-title="<?php esc_attr_e('Select a map for editing and previewing', 'brazil-html5-map'); ?>">[?]</span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="admin.php?page=brazil-html5-map-maps" class="page-title-action tipsy-q" original-title="<?php esc_attr_e('List of all maps, creating a new map, backup', 'brazil-html5-map'); ?>"><?php _e('Maps dashboard', 'brazil-html5-map'); ?></a>
<?php
}

function brazil_html5map_plugin_messages($successes, $errors) {
    if ($successes and is_array($successes)) {
        echo "<div class=\"updated\"><ul>";
        foreach ($successes as $s) {
            echo "<li>" . (is_array($s) ? "<strong>$s[0]</strong>$s[1]" : $s) . "</li>";
        }
        echo "</ul></div>";
    }

    if ($errors and is_array($errors)) {
        echo "<div class=\"error\"><ul>";
        foreach ($errors as $s) {
            echo "<li>" . (is_array($s) ? "<strong>$s[0]</strong>$s[1]" : $s) . "</li>";
        }
        echo "</ul></div>";
    }
}

function brazil_html5map_plugin_options() {
    include('editmainconfig.php');
}

function brazil_html5map_plugin_states() {
    include('editstatesconfig.php');
}

function brazil_html5map_plugin_tools() {
    include('maptools.php');
}

function brazil_html5map_plugin_groups() {
    include('editgroupsconfig.php');
}
function brazil_html5map_plugin_points() {
    include('editpointsconfig.php');
}
function brazil_html5map_plugin_maps() {
    include('mapslist.php');
}

function brazil_html5map_plugin_view() {

    $options = brazil_html5map_plugin_get_options();
    $option_keys = is_array($options) ? array_keys($options) : array();
    $map_id  = (isset($_REQUEST['map_id'])) ? intval($_REQUEST['map_id']) : array_shift($option_keys) ;

?>
<div class="wrap">
    <h2><?php _e('Map Preview', 'brazil-html5-map') ?></h2>
    <br />
    <form method="POST" class="brazil-html5-map main">
    <?php brazil_html5map_plugin_map_selector('view', $map_id, $options) ?>
    <br /><br />
    </form>
    <style type="text/css">
        .brazilHtml5MapBold {font-weight: bold}
    </style>
<?php
    brazil_html5map_plugin_nav_tabs('view', $map_id);
    if (function_exists('sgPopupPluginLoaded')) {
        require_once(SG_APP_POPUP_PATH.'/javascript/sg_popup_javascript.php');
        if (function_exists('SgFrontendScripts'))
            SgFrontendScripts();
        elseif (class_exists('SgPopupBuilderConfig'))
            echo SgPopupBuilderConfig::popupJsDataInit();
    }
    echo '<p>'.sprintf(__('Use shortcode %s for install this map', 'brazil-html5-map'), '<span class="brazilHtml5MapBold">[brazilhtml5map id="'.$map_id.'"]</span>').'</p>';

    echo do_shortcode('<div style="width: 99%">[brazilhtml5map id="'.$map_id.'"]</div>');
    echo "</div>";
}

add_action('admin_init','brazil_html5map_plugin_scripts');

function brazil_html5map_plugin_scripts(){
    if ( is_admin() ){

        wp_register_style('jquery-tipsy', plugins_url('/static/css/tipsy.css', __FILE__));
        wp_enqueue_style('jquery-tipsy');
        wp_register_style('brazil-html5-map-adm', plugins_url('/static/css/mapadm.css', __FILE__), array(), '2.9.9.6');
        wp_enqueue_style('brazil-html5-map-adm');
        wp_register_style('brazil-html5-map-style', plugins_url('/static/css/map.css', __FILE__), array(), '2.9.9.6');
        wp_enqueue_style('brazil-html5-map-style');
        wp_enqueue_style('farbtastic');
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-dialog');
        wp_enqueue_style('wp-jquery-ui-dialog');
        wp_enqueue_script('farbtastic');
        wp_enqueue_script('tiny_mce');
        wp_register_script('jquery-tipsy', plugins_url('/static/js/jquery.tipsy.js', __FILE__));
        wp_enqueue_script('jquery-tipsy');
    }
    else {

        wp_register_style('brazil-html5-map-style', plugins_url('/static/css/map.css', __FILE__), array(), '2.9.9.6');
        wp_enqueue_style('brazil-html5-map-style');

        wp_register_script('raphael', plugins_url('/static/js/raphael.min.js', __FILE__));
        wp_enqueue_script('raphael');

        wp_enqueue_script('jquery');

    }

    wp_register_script('brazil-html5-map-nicescroll', plugins_url('/static/js/jquery.nicescroll.js', __FILE__));
    wp_enqueue_script('brazil-html5-map-nicescroll');
}

add_action('wp_enqueue_scripts', 'brazil_html5map_plugin_scripts_method');

function brazil_html5map_plugin_scripts_method() {
    wp_enqueue_script('jquery');
    wp_register_style('brazil-html5-map-style', plugins_url('/static/css/map.css', __FILE__));
    wp_enqueue_style('brazil-html5-map-style');

    wp_register_script('brazil-html5-map-nicescroll', plugins_url('/static/js/jquery.nicescroll.js', __FILE__));
    wp_enqueue_script('brazil-html5-map-nicescroll');
}


add_shortcode( 'brazilhtml5map', 'brazil_html5map_plugin_content' );

function brazil_html5map_plugin_enable_popup_scripts(&$options) {
    if (! function_exists('sgRegisterScripts'))
        return FALSE;

    if (strpos($options['map_data'], '#popup'))
        return TRUE;

    if (isset($options['points']) AND is_array($options['points'])) foreach ($options['points'] as $pt) {
        if (isset($pt['link']) AND $pt['link'] == '#popup')
            return TRUE;
    }

    if (isset($options['groups']) AND is_array($options['groups'])) foreach ($options['groups'] as $gr) {
        if ($gr['_act_over'] AND isset($gr['link']) AND $gr['link'] == '#popup')
            return TRUE;
    }

    return FALSE;
}
function brazil_html5map_plugin_enqueue_js($js = null) {
    static $arr = array();
    if ($js) {
        $arr[] = $js;
    } else {
        echo implode("", $arr);
    }
}

function brazil_html5map_plugin_escape_fonts($fonts, $addFonts = false) {
    $fonts = $fonts ? explode(',', $fonts) : array();
    if ($addFonts)
        $fonts = array_merge($fonts, explode(',', $addFonts));

    foreach ($fonts as &$f) {
        $f = '\''.trim($f, ' \'"').'\'';
    }
    return implode(',', $fonts);
}

function brazil_html5map_plugin_prepare_tooltip_css($options, $prefix) {
    $commentCss = '';
    if ( ! empty($options['popupCommentColor'])) {
        $commentCss .= "\t\t\t\tcolor: $options[popupCommentColor];\n";
    }
    if ( ! empty($options['popupCommentFontSize'])) {
        $commentCss .= "\t\t\t\tfont-size: $options[popupCommentFontSize]px;\n";
    }
    if ( ! empty($options['popupCommentFontFamily'])) {
        $commentCss .= "\t\t\t\tfont-family: ".brazil_html5map_plugin_escape_fonts($options['popupCommentFontFamily']).";\n";
    }

    $popupTitleCss = '';
    if ( ! empty($options['popupNameColor'])) {
        $popupTitleCss .= "\t\t\t\tcolor: $options[popupNameColor];\n";
    }
    if ( ! empty($options['popupNameFontSize'])) {
        $popupTitleCss .= "\t\t\t\tfont-size: $options[popupNameFontSize]px;\n";
    }
    if ( ! empty($options['popupNameFontFamily'])) {
        $popupTitleCss .= "\t\t\t\tfont-family: ".brazil_html5map_plugin_escape_fonts($options['popupNameFontFamily']).";\n";
    }
    $result = "$prefix .fm-tooltip-name {
$popupTitleCss
}
$prefix .fm-tooltip-comment {
$commentCss
}";
    return $result;
}

function brazil_html5map_plugin_content($atts, $content) {
    static $firstRun = true;
    $dir               = plugins_url('/static/', __FILE__);
    $siteURL           = get_site_url();
    $options           = brazil_html5map_plugin_get_options();
    $option_keys       = is_array($options) ? array_keys($options) : array();

    if (isset($atts['id'])) {
        $map_id  = intval($atts['id']);
        $options = $options[$map_id];
    } else {
        $map_id  = array_shift($option_keys);
        $options = array_shift($options);
    }
    $defOptions = brazil_html5map_plugin_map_defaults('', 1, true);
    foreach ($defOptions as $k => $v) {
        if (!isset($options[$k]))
            $options[$k] = $v;
    }
    $prfx              = "_$map_id";
    $isResponsive      = $options['isResponsive'];
    $stateInfoArea     = $options['statesInfoArea'];
    $respInfo          = $isResponsive ? ' htmlMapResponsive' : '';
    $type_id           = 0;
    $map_file          = "{$dir}js/map.js";
    $style             = (!empty($options['maxWidth']) && $isResponsive) ? 'max-width:'.intval($options['maxWidth']).'px' : '';

    static $count = 0;
    static $print_action_registered = false;

    $settings_file = brazil_html5map_plugin_settings_url($map_id, $options);

    wp_register_script('raphaeljs', "{$dir}js/raphael.min.js", array(), '2.1.4');
    wp_register_script('brazil-html5-map-mapjs_'.$type_id, $map_file, array('raphaeljs'));
    wp_register_script('brazil-html5-map-map_cfg_'.$map_id, $settings_file, array('raphaeljs', 'brazil-html5-map-mapjs_'.$type_id));
    wp_enqueue_script('brazil-html5-map-map_cfg_'.$map_id);

    $select_options = "";
    $states = json_decode($options['map_data'], true);
    foreach ($states as $sid => $s) {
        if ($options['areaListOnlyActive'] and !$s['link'])
            continue;
        $select_options .= "\t<option value=\"$sid\">".htmlspecialchars($s['name'])."</option>\n";
    }

    if (brazil_html5map_plugin_enable_popup_scripts($options)) {
        sgRegisterScripts();
    }
    $mapInit = "
        <!-- start Fla-shop.com HTML5 Map -->";
    $mapInit .= "
        <div class='brazilHtml5Map$stateInfoArea$respInfo' style='$style'>";

    $containerStyle  = '';
    $areasJs = '';
    $dropDownHtml = '';
    $dropDownJS = '';
    if ($options['areasList']) {

        $options['listWidth'] = intval($options['listWidth']) ;
        if ($options['listWidth']<=0) { $options['listWidth'] = 20; }

        $areasList = brazil_html5map_plugin_areas_list($options,$count);

        if ($areasList) {
            $areasJs = '
                jQuery(document).ready(function($) {

                    $( window ).resize(function() {
                        $("#brazil-html5-map-areas-list_'.$count.'").show().css({height: jQuery("#brazil-html5-map-map-container_'.$count.' .fm-map-container").height() + "px"}).niceScroll({cursorwidth:"8px"});
                    });

                    $("#brazil-html5-map-areas-list_'.$count.'").show().css({height: jQuery("#brazil-html5-map-map-container_'.$count.' .fm-map-container").height() + "px"}).niceScroll({cursorwidth:"8px"});

                    $("#brazil-html5-map-areas-list_'.$count.' a").click(function() {

                        var id  = $(this).data("key");
                        var map = brazilhtml5map_map_'.$count.';

                        html5map_onclick(null,id,map);

                        return false;
                    });

                    $("#brazil-html5-map-areas-list_'.$count.' a").on("mouseover",function() {

                        var id  = $(this).data("key");
                        var map = brazilhtml5map_map_'.$count.';

                        map.stateHighlightIn(id);

                    });

                    $("#brazil-html5-map-areas-list_'.$count.' a").on("mouseout",function() {

                        var id  = $(this).data("key");
                        var map = brazilhtml5map_map_'.$count.';

                        map.stateHighlightOut(id);

                    });

                });';

            $containerStyle = 'width: '.($options['statesInfoArea']!='right' ? 100-$options['listWidth'].'%' : 60-$options['listWidth'].'%' ).'; float: left';

            if ($options['areasListShowDropDown']) {
                $showOnMobile = '';
                if ($options['areasListShowDropDown'] == 'mobile') {
                    $showOnMobile = 'mobile-only';
                } else {
                    $areasList = '';
                    $areasJs = '';
                    $containerStyle = '';
                }
                $dropDownHtml = "<div class='brazilHtml5MapSelector $showOnMobile'><select id='brazil-html5-map-selector_{$count}'>
                    <option value=''>".__('Select an area', 'brazil-html5-map')."</option>
                    $select_options
                </select></div>";

                $dropDownJS = "jQuery('#brazil-html5-map-selector_{$count}').change(function() {
                        var sid = jQuery(this).val();
                        if (hightlighted)
                                brazilhtml5map_map_{$count}.stateHighlightOut(hightlighted);

                        hightlighted = sid;

                        if (sid) {
                            brazilhtml5map_map_{$count}.stateHighlightIn(sid);

                            html5map_onclick(null,sid,brazilhtml5map_map_{$count});
                        }
                    });\n";
            }

            $mapInit.= $areasList;
        }
    }


    $mapInit.="$dropDownHtml<div id='brazil-html5-map-map-container_{$count}' class='brazilHtml5MapContainer' data-map-variable='brazilhtml5map_map_{$count}'></div>";

    if ($options['statesInfoArea']=='bottom') { $mapInit.="<div style='clear:both; height: 20px;'></div>"; }

    $customJs = "";
    if ($options['customJs']) {
        $customJs = "(function (map, containerId) {\n{$options['customJs']}\n})(brazilhtml5map_map_{$count}, 'brazil-html5-map-map-container_{$count}');";
    }

    $mapInit.= "
            <style>
                #brazil-html5-map-map-container_{$count} {
                    $containerStyle
                }
                ".brazil_html5map_plugin_prepare_tooltip_css($options, "#brazil-html5-map-map-container_{$count}")."
                @media only screen and (max-width: 480px) {
                    #brazil-html5-map-map-container_{$count} {
                        float: none;
                        width: 100%;
                    }
                }
            </style>";
    $mapJs =  "<script type=\"text/javascript\">
            jQuery(function(){
                var hightlighted = null;
                brazilhtml5map_map_{$count} = new FlaShopBrazilMap(brazilhtml5map_map_cfg_{$map_id});
                brazilhtml5map_map_{$count}.draw('brazil-html5-map-map-container_{$count}');
                brazilhtml5map_map_{$count}.on('mousein', function(ev, sid, map) {
                    if (hightlighted && sid != hightlighted) {
                        map.stateHighlightOut(hightlighted);
                        hightlighted = null;
                    }
                });
                $areasJs
                $customJs

                var html5map_onclick = function(ev, sid, map) {
                var cfg      = brazilhtml5map_map_cfg_{$map_id};
                var link     = map.fetchStateAttr(sid, 'link');
                var is_group = map.fetchStateAttr(sid, 'group');
                var popup_id = map.fetchStateAttr(sid, 'popup-id');
                var is_group_info = false;

                if (typeof cfg.map_data[sid] !== 'undefined')
                        jQuery('#brazil-html5-map-selector_{$count}').val(sid);
                    else
                        jQuery('#brazil-html5-map-selector_{$count}').val('');

                if (is_group==undefined) {

                    if (sid.substr(0,1)=='p') {
                        popup_id = map.fetchPointAttr(sid, 'popup_id');
                        link         = map.fetchPointAttr(sid, 'link');
                    }

                } else if (typeof cfg.groups[is_group]['ignore_link'] == 'undefined' || ! cfg.groups[is_group].ignore_link)  {
                    link = cfg.groups[is_group].link;
                    popup_id = cfg.groups[is_group]['popup_id'];
                    is_group_info = true;
                }
                if (link=='#popup') {

                    if (typeof SG_POPUP_DATA == \"object\") {
                        if (popup_id in SG_POPUP_DATA) {

                            SGPopup.prototype.showPopup(popup_id,false);

                        } else {

                            jQuery.ajax({
                                type: 'POST',
                                url: '{$siteURL}/index.php?brazilhtml5map_get_popup',
                                data: {popup_id:popup_id},
                            }).done(function(data) {
                                jQuery('body').append(data);
                                SGPopup.prototype.showPopup(popup_id,false);
                            });

                        }
                    }

                    return false;
                }
                if (link == '#info') {
                    var id = is_group_info ? is_group : (sid.substr(0,1)=='p' ? sid : map.fetchStateAttr(sid, 'id'));
                    jQuery('#brazil-html5-map-state-info_{$count}').html('". __('Loading...', 'brazil-html5-map') ."');
                    jQuery.ajax({
                        type: 'POST',
                        url: '{$siteURL}/index.php?brazilhtml5map_get_'+(is_group_info ? 'group' : 'state')+'_info='+id+'&map_id={$map_id}',
                        success: function(data, textStatus, jqXHR){
                            jQuery('#brazil-html5-map-state-info_{$count}').html(data);
                            " . (($options['statesInfoArea'] == 'bottom' AND $options['autoScrollToInfo']) ? "
                            jQuery(\"html, body\").animate({ scrollTop: jQuery('#brazil-html5-map-state-info_{$count}').offset().top - ".$options['autoScrollOffset']." }, 1000);" : "") . "
                        },
                        dataType: 'text'
                    });

                    return false;
                }

                    if (ev===null && link!='') {
                        if (!jQuery('.html5dummilink').length) {
                            jQuery('body').append('<a href=\"#\" class=\"html5dummilink\" style=\"display:none\"></a>');
                        }

                        jQuery('.html5dummilink').attr('href',link).attr('target',(map.fetchStateAttr(sid, 'isNewWindow') ? '_blank' : '_self'))[0].click();

                    }

                };
                brazilhtml5map_map_{$count}.on('click',html5map_onclick);

                $dropDownJS

            });
            </script>";
    if ( ! $options['delayCodeOutput'])
        $mapInit .= $mapJs;
    $mapInit.= "<div id='brazil-html5-map-state-info_{$count}' class='brazilHtml5MapStateInfo'>".
            (empty($options['defaultAddInfo']) ? '' : apply_filters('the_content',$options['defaultAddInfo']))
            ."</div>
            </div>
            <div style='clear: both'></div>
            <!-- end HTML5 Map -->
    ";

    $count++;

    if ($options['delayCodeOutput']) {
        brazil_html5map_plugin_enqueue_js($options['minimizeOutput'] ? preg_replace('/\s+/', ' ', $mapJs) : $mapJs);

        if ( ! $print_action_registered) {
            if (is_admin()) {
                add_action('admin_footer', 'brazil_html5map_plugin_enqueue_js', 1000);
            } else {
                add_action('wp_footer', 'brazil_html5map_plugin_enqueue_js', 1000);
            }
            $print_action_registered = true;
        }
    }

    if ($options['minimizeOutput'])
        $mapInit = preg_replace('/\s+/', ' ', $mapInit);
    return $mapInit;
}


$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'brazil_html5map_plugin_settings_link' );

function brazil_html5map_plugin_settings_link($links) {
    $settings_link = '<a href="admin.php?page=brazil-html5-map-options">'.__('Settings', 'brazil-html5-map').'</a>';
    array_push($links, $settings_link);
    return $links;
}


add_action('init', 'brazil_html5map_plugin_settings', 100);

function brazil_html5map_plugin_settings() {

    $is_map_call = false;
    foreach($_REQUEST as $key => $value) { if (strpos($key,'brazilhtml5map')!==false) { $is_map_call = true; break; } }
    if (!$is_map_call) { return false; } else {
        remove_all_actions( 'wp_head' );
        remove_all_actions( 'wp_footer' );
    }

    $req_start = microtime(TRUE);
    if (isset($_REQUEST['brazilhtml5map_js_data']) or
        isset($_REQUEST['brazilhtml5map_get_state_info']) or
        isset($_REQUEST['brazilhtml5map_get_group_info'])) {
        $map_id  = intval($_REQUEST['map_id']);
        $options = brazil_html5map_plugin_get_options();
        $options = $options[$map_id];
        if ($options)
            $options['map_data'] = str_replace('\\\\n','\\n',$options['map_data']);
    } else if (isset($_REQUEST['brazilhtml5map_get_popup']) ) {

        $popup = do_shortcode('[sg_popup id="'.intval($_REQUEST['popup_id']).'"][/sg_popup]');
        //$popup = substr($popup,0,strpos($popup,'</script>')+9);
        echo $popup; exit();
    }


    if( isset($_GET['brazilhtml5map_js_data']) ) {

        header( 'Content-Type: application/javascript' );
        brazil_html5map_plugin_print_map_settings($map_id, $options);
        echo '// Generated in '.(microtime(TRUE)-$req_start).' secs.';
        exit;
    }

    if(isset($_GET['brazilhtml5map_get_state_info'])) {
        $stateId = $_GET['brazilhtml5map_get_state_info'];

        $info = $options['state_info'][$stateId];
        echo apply_filters('the_content',$info);

        exit;
    }

    if(isset($_GET['brazilhtml5map_get_group_info'])) {
        $gid = $_GET['brazilhtml5map_get_group_info'];

        $info = isset($options['groups'][$gid]['info']) ? $options['groups'][$gid]['info'] : '';
        echo apply_filters('the_content',$info);

        exit;
    }
}

function brazil_html5map_plugin_print_map_settings($map_id, &$map_options) {
    if ( ! $map_options) {
        ?>
        var	map_cfg = {
            map_data: {}
        };
        <?php
        return;
    }
    $data = json_decode($map_options['map_data'], true);
    $protected_shortnames = array();
    $siteURL           = get_site_url();
    foreach ($data as $sid => &$d)
    {
        if (isset($d['comment']) AND $d['comment'] AND preg_match('/\[([\w-_]+)([^\]]*)?\](?:(.+?)?\[\/\1\])?/', $d['comment']))
            $d['comment'] = do_shortcode($d['comment']);
        if (isset($d['_hide_name'])) {
            unset($d['_hide_name']);
            $d['name'] = '';
        }
        if (isset($map_options['hideSN']) AND ! in_array($sid, $protected_shortnames))
            $d['shortname'] = '';
        if (isset($d['link']))
            $d['link'] = strpos($d['link'], 'javascript:brazilhtml5map_set_state_text') === 0 ? '#info' : $d['link'];
    }
    unset($d);
    $map_options['map_data'] = json_encode($data);
    $grps = array();
    if (isset($map_options['groups']) AND is_array($map_options['groups'])) {
        foreach ($map_options['groups'] as $gid => $grp) {
            $grps[$gid] = array();
            if ($grp['_popup_over']) {
                $grps[$gid]['name'] = $grp['name'];
                $grps[$gid]['comment'] = $grp['comment'];
                $grps[$gid]['image'] = $grp['image'];
            }
            if ($grp['_act_over']) {
                $grps[$gid]['link'] = strpos($grp['link'], 'javascript:brazilhtml5map_set_state_text') === 0 ? '#info' : $grp['link'];
                $grps[$gid]['isNewWindow'] = empty($grp['isNewWindow']) ? FALSE : TRUE;
                $grps[$gid]['popup_id']    = isset($grp['popup-id']) ? intval($grp['popup-id']) : -1;
            } else {
                $grps[$gid]['ignore_link'] = true;
            }
            if ($grp['_clr_over']) {
                $grps[$gid]['color'] = $grp['color_map'];
                $grps[$gid]['colorOver'] = $grp['color_map_over'];
            }
            if ($grp['_ignore_group']) {
                $grps[$gid]['ignoreMouse'] = true;
            }
            if (!$grps[$gid])
                unset($grps[$gid]);
        }
    }
    $defOptions = brazil_html5map_plugin_map_defaults('', 1, true);
    foreach ($defOptions as $k => $v) {
        if (!isset($map_options[$k]))
            $map_options[$k] = $v;
    }
    if (isset($map_options['points']) AND is_array($map_options['points'])) {
        foreach ($map_options['points'] as $pid => &$p) {
            if(isset($p['link']))
                $p['link'] = strpos($p['link'], 'javascript:brazilhtml5map_set_state_text') === 0 ? '#info' : $p['link'];
        }
        unset($p);
    }
    ?>

    var	brazilhtml5map_map_cfg_<?php echo $map_id ?> = {

    <?php  if(!$map_options['isResponsive']) { ?>
    mapWidth		: <?php echo $map_options['mapWidth']; ?>,
    mapHeight		: <?php echo $map_options['mapHeight']; ?>,
    <?php }     else { ?>
    mapWidth		: 0,
    <?php } ?>
    zoomEnable              : <?php echo $map_options['zoomEnable'] ? 'true' : 'false'; ?>,
    zoomOnlyOnMobile        : <?php echo $map_options['zoomOnlyOnMobile'] ? 'true' : 'false'; ?>,
    zoomEnableControls      : <?php echo $map_options['zoomEnableControls'] ? 'true' : 'false'; ?>,
    zoomIgnoreMouseScroll   : <?php echo $map_options['zoomIgnoreMouseScroll'] ? 'true' : 'false'; ?>,
    zoomMax   : <?php echo $map_options['zoomMax']; ?>,
    zoomStep   : <?php echo $map_options['zoomStep']; ?>,
    pointColor            : "<?php echo $map_options['pointColor']?>",
    pointColorOver        : "<?php echo $map_options['pointColorOver']?>",
    pointNameColor        : "<?php echo $map_options['pointNameColor']?>",
    pointNameColorOver    : "<?php echo $map_options['pointNameColorOver']?>",
    pointNameStrokeColor        : "<?php echo $map_options['pointNameStrokeColor']?>",
    pointNameStrokeColorOver    : "<?php echo $map_options['pointNameStrokeColorOver']?>",
    pointNameFontFamily   : "<?php echo brazil_html5map_plugin_escape_fonts($map_options['pointNameFontFamily'] ? $map_options['pointNameFontFamily'] : $map_options['nameFontFamily'], 'Arial, sans-serif'); ?>",
    pointNameFontSize     : "<?php echo intval($map_options['pointNameFontSize']).'px'?>",
    pointNameFontWeight   : "bold",
    pointNameStroke       : true,

    pointBorderWidth      : 0.5,
    pointBorderColor      : "<?php echo $map_options['pointBorderColor']?>",
    pointBorderColorOver  : "<?php echo $map_options['pointBorderColorOver']?>",
    shadowAllow             : <?php echo $map_options['shadowAllow'] ? 'true' : 'false'; ?>,
    shadowWidth		: <?php echo $map_options['shadowWidth']; ?>,
    shadowOpacity		: <?php echo $map_options['shadowOpacity']; ?>,
    shadowColor		: "<?php echo $map_options['shadowColor']; ?>",
    shadowX			: <?php echo $map_options['shadowX']; ?>,
    shadowY			: <?php echo $map_options['shadowY']; ?>,

    iPhoneLink		: <?php echo $map_options['iPhoneLink']; ?>,

    isNewWindow		: <?php echo $map_options['isNewWindow']; ?>,

    borderWidth     : "<?php echo $map_options['borderWidth']; ?>",
    borderColor		: "<?php echo $map_options['borderColor']; ?>",
    borderColorOver		: "<?php echo $map_options['borderColorOver']; ?>",

    nameColor		: "<?php echo $map_options['nameColor']; ?>",
    nameColorOver		: "<?php echo $map_options['nameColorOver']; ?>",
    nameFontFamily		: "<?php echo brazil_html5map_plugin_escape_fonts($map_options['nameFontFamily'], 'Arial, sans-serif'); ?>",
    nameFontSize		: "<?php echo $map_options['nameFontSize'].'px'; ?>",
    nameFontWeight		: "<?php echo $map_options['nameFontWeight']; ?>",

    overDelay		: <?php echo $map_options['overDelay']; ?>,
    nameStroke		: <?php echo $map_options['nameStroke']?'true':'false'; ?>,
    nameStrokeColor		: "<?php echo $map_options['nameStrokeColor']; ?>",
    nameStrokeColorOver	: "<?php echo $map_options['nameStrokeColorOver']; ?>",
    freezeTooltipOnClick: <?php echo $map_options['freezeTooltipOnClick']?'true':'false'; ?>,

    tooltipOnHighlightIn: <?php echo $map_options['tooltipOnHighlightIn']?'true':'false'; ?>,
    tooltipOnMobileCentralize: <?php echo $map_options['tooltipOnMobileCentralize']?'true':'false'; ?>,
    tooltipOnMobileWidth: "<?php echo $map_options['tooltipOnMobileWidth']; ?>",
    tooltipOnMobileVPosition: "<?php echo $map_options['tooltipOnMobileVPosition']; ?>",

    mapId: "<?php echo $map_options['mapId'] ?>",

    map_data        : <?php echo $map_options['map_data']; ?>
    ,groups          : <?php echo $grps ? json_encode($grps) : '{}'; ?>
    ,points         : <?php echo (isset($map_options['points']) AND $map_options['points']) ? json_encode($map_options['points']) : '{}'; ?>
    };

    <?php
    if (file_exists($params_file = dirname(__FILE__).'/static/paths.json')) {
        echo "brazilhtml5map_map_cfg_$map_id.map_params = ".file_get_contents($params_file).";\n";
    }
}


function brazil_html5map_plugin_map_defaults($name='New map', $type=1, $baseOnly=false) {
    $defaults = array(
        'mapWidth'          =>500,
        'mapHeight'         =>500,
        'maxWidth'          =>800,
        'shadowAllow'       => true,
        'zoomEnable'            => false,
        'zoomEnableControls'    => true,
        'zoomIgnoreMouseScroll' => false,
        'zoomOnlyOnMobile'      => false,
        'zoomMax'               => 2,
        'zoomStep'              => 0.2,
        'defaultPointRadius'    => 4,
        'pointColor'            => "#FFC480",
        'pointColorOver'        => "#DC8135",
        'pointNameColor'        => "#000",
        'pointNameColorOver'    => "#222",
        'pointNameFontFamily'   => '',
        'pointNameFontSize'     => "12",
        'pointNameFontWeight'   => "bold",
        'pointNameStroke'       => true,
        'pointNameStrokeColor'  => "#FFFFFF",
        'pointNameStrokeColorOver'  => "#FFFFFF",

        'pointBorderWidth'      => 0.5,
        'pointBorderColor'      => "#ffffff",
        'pointBorderColorOver'  => "#eeeeee",
        'shadowWidth'       => 1.5,
        'shadowOpacity'     => 0.2,
        'shadowColor'       => "black",
        'shadowX'           => 0,
        'shadowY'           => 0,
        'iPhoneLink'        => "true",
        'isNewWindow'       => "false",
        'borderWidth'       => 1.01,
        'borderColor'       => "#ffffff",
        'borderColorOver'   => "#ffffff",
        'nameColor'         => "#ffffff",
        'nameColorOver'     => "#ffffff",
        'nameFontFamily'    => '',
        'nameFontSize'      =>10,
        'nameFontWeight'    => "bold",
        'overDelay'         => 300,
        'statesInfoArea'    => "bottom",
        'autoScrollToInfo'  => 0,
        'autoScrollOffset'  => 0,
        'isResponsive'      => "1",
        'nameStroke'        => true,
        'nameStrokeColor'   => "#000000",
        'nameStrokeColorOver'=> "#000000",
        'freezeTooltipOnClick' => false,

        'areasList'         =>false,
        'areasListShowDropDown' => false,
        'areaListOnlyActive'=> false,
        'listWidth'         => '20',
        'listFontSize'      => '14px',
        'popupNameColor'    => "#000000",
        'popupNameFontFamily'   => "",
        'popupNameFontSize'     => "20",
        'popupCommentColor'     => '',
        'popupCommentFontFamily'=> '',
        'popupCommentFontSize'  => '',
        'tooltipOnHighlightIn'  => true,
        'tooltipOnMobileCentralize' => true,
        'tooltipOnMobileWidth' => '80%',
        'tooltipOnMobileVPosition' => 'bottom',
        'minimizeOutput' => true,
        'delayCodeOutput' => false,
        'customJs' => '',

    );

    $initialStatesPath = dirname(__FILE__).'/static/settings_tpl.json';
    $defaults['mapId'] = 'Z1CJhP0';
    if ($baseOnly)
        return $defaults;
    $defaults['name']           = $name;
    $defaults['update_time']    = time();
    $defaults['map_data']       = file_get_contents($initialStatesPath);
    $defaults['cacheSettings']  = is_writable(dirname(__FILE__).'/static');
    $arr = json_decode($defaults['map_data'], true);
    foreach ($arr as $i) {
        $defaults['state_info'][$i['id']] = '';
    }

    return $defaults;
}

function brazil_html5map_plugin_settings_url($map_id, &$map_options) {
    $cacheURL   = plugins_url('/static/cache', __FILE__);
    $siteURL    = get_site_url();
    $phpURL     = "{$siteURL}/index.php?brazilhtml5map_js_data=true&map_id=$map_id&r=".rand(11111,99999);

    if ( ! $map_options['update_time'])
        return $phpURL;

    if ( ! (isset($map_options['cacheSettings']) and $map_options['cacheSettings']))
        return $phpURL;

    $cache_name = "brazil-html5-map-{$map_id}-{$map_options['update_time']}.js";
    $static_path = dirname(__FILE__).'/static';
    $cache_path  = "$static_path/cache";

    if (!is_writable($static_path))
        return $phpURL;

    if (file_exists("$cache_path/$cache_name"))
        return "$cacheURL/$cache_name";

    if (!file_exists($cache_path)) {
        if (is_writable($static_path))
            mkdir($cache_path);
        else
            return $phpURL;
    }

    if (brazil_html5map_plugin_generate_cache($map_id, $map_options, $cache_path, $cache_name))
        return "$cacheURL/$cache_name";
    else
        return $phpURL;
}

function brazil_html5map_plugin_generate_cache($map_id, &$map_options, $cache_path, $cache_name) {
    $name_prefix = "brazil-html5-map-{$map_id}";
    $dh = opendir($cache_path);
    if (!$dh)
        return false;
    while ($file = readdir($dh)) {
        if (strpos($file, $name_prefix) !== false)
            unlink("$cache_path/$file");
    }
    closedir($dh);

    ob_start();
    brazil_html5map_plugin_print_map_settings($map_id, $map_options);
    $cntnt = ob_get_clean();
    if (file_put_contents("$cache_path/$cache_name", $cntnt))
        return true;
    else
        return false;
}

function brazil_html5map_plugin_group_defaults($name) {
    return array(
        'group_name' => $name,
        '_popup_over' => false,
        '_act_over' => false,
        '_clr_over' => false,
        '_ignore_group' => false,
        'name' => $name,
        'comment' => '',
        'info' => '',
        'image' => '',
        'link' => '',
        'color_map' => '#ffffff',
        'color_map_over' => '#ffffff'
    );
}

function brazil_html5map_plugin_get_options($blog_id = null, $option_name = "brazilhtml5map_options") {
    $res = is_multisite() ?
        get_blog_option(is_null($blog_id) ? get_current_blog_id() : $blog_id, 'brazilhtml5map_options') :
        get_site_option($option_name);
    return $res ? $res : array();
}

function brazil_html5map_plugin_save_options(&$options, $blog_id = null, $option_name = "brazilhtml5map_options") {
    if ( is_multisite() ) {
        update_blog_option(is_null($blog_id) ? get_current_blog_id() : $blog_id, 'brazilhtml5map_options', $options);
    } else {
        update_site_option($option_name,$options);
    }
}

function brazil_html5map_plugin_delete_options($blog_id = null) {
    if ( is_multisite() ) {
        delete_blog_option(is_null($blog_id) ? get_current_blog_id() : $blog_id, 'brazilhtml5map_options');
    } else {
        delete_site_option('brazilhtml5map_options');
    }
}


function brazil_html5map_plugin_sort_states_by_name($a, $b) {
    return strcmp($a['name'], $b['name']);
}

register_activation_hook( __FILE__, 'brazil_html5map_plugin_activation' );

function brazil_html5map_plugin_activation() {

    $options = array(0 => brazil_html5map_plugin_map_defaults());

    add_site_option('brazilhtml5map_options', $options);

}

register_deactivation_hook( __FILE__, 'brazil_html5map_plugin_deactivation' );

function brazil_html5map_plugin_deactivation() {

}

register_uninstall_hook( __FILE__, 'brazil_html5map_plugin_uninstall' );

function brazil_html5map_plugin_uninstall() {
    delete_site_option('brazilhtml5map_options');
}

add_filter('widget_text', 'do_shortcode');


function brazil_html5map_plugin_export() {
    $maps = array();
    if (isset($_REQUEST['map_id']) and is_array($_REQUEST['map_id']))
        $maps = $_REQUEST['map_id'];
    elseif (isset($_REQUEST['maps']))
        $maps = explode(',', $_REQUEST['maps']);
    if ( ! $maps)
        return;
    $options = brazil_html5map_plugin_get_options();

    foreach($options as $map_id => $option) {
        if (!in_array($map_id,$maps)) {
            unset($options[$map_id]);
        }
        unset($options[$map_id]['point_editor_settings']);
    }

    if (count($options)>0) {
        $options = json_encode($options);

        header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK');
        header('Content-Type: text/json');
        header('Content-Length: ' . (strlen($options)));
        header('Connection: close');
        header('Content-Disposition: attachment; filename="maps.json";');
        echo $options;

        exit();
    }

}

function brazil_html5map_plugin_get_csv_import_export_keys($type) {
    switch ($type) {
        case 'states':
        return array(
            'id'            => 'id',
            'shortname'     => 'shortname',
            'name'          => 'name',
            'comment'       => 'comment',
            'info'          => 'info',
            'image'         => 'image',
            'link'          => 'link',
            'isNewWindow'   => 'isNewWindow',
            'clickAction'   => 'clickAction',
            'popupId'       => 'popup-id',
            'color'         => 'color_map',
            'colorOver'     => 'color_map_over',
            'hideName'      => '_hide_name',
            'groupId'       => 'group'
        );
        case 'groups':
        return array(
            'groupId'       => 'id',
            'groupName'     => 'group_name',
            'name'          => 'name',
            'comment'       => 'comment',
            'info'          => 'info',
            'image'         => 'image',
            'link'          => 'link',
            'isNewWindow'   => 'isNewWindow',
            'clickAction'   => 'clickAction',
            'popupId'       => 'popup-id',
            'color'         => 'color_map',
            'colorOver'     => 'color_map_over',
            'overridePopup' => '_popup_over',
            'overrideAction'=> '_act_over',
            'overrideColors'=> '_clr_over',
            'ignoreMouse'   => '_ignore_group',
        );
        case 'points':
        return array(
            'pointId'       => 'id',
            'shortname'     => 'shortname',
            'name'          => 'name',
            'comment'       => 'comment',
            'info'          => 'info',
            'image'         => 'image',
            'link'          => 'link',
            'isNewWindow'   => 'isNewWindow',
            'clickAction'   => 'clickAction',
            'popupId'       => 'popup_id',
            'color'         => 'color',
            'colorOver'     => 'colorOver',
            'borderColor'           => 'borderColor',
            'borderColorOver'       => 'borderColorOver',
            'nameColor'             => 'nameColor',
            'nameColorOver'         => 'nameColorOver',
            'nameStrokeColor'       => 'nameStrokeColor',
            'nameStrokeColorOver'   => 'nameStrokeColorOver',
            'nameFontSize'  => 'nameFontSize',
            'textPos'       => 'textPos',
            'x'             => 'x',
            'y'             => 'y',
            'radius'        => 'radius',
            'pointType'     => 'pointType'
        );
    }
    return array();
}

function brazil_html5map_plugin_detect_export_click_action(&$row) {
    $action = "none";
    if (!isset($row['link'])) $action = "none";
    elseif(stripos($row['link'], "javascript:[\w_]+_set_state_text") !== false or $row['link'] == '#info' ) $action = "info";
    elseif(trim($row['link']) == "#popup") $action = "popup";
    elseif(trim($row['link']) != "") $action = "link";
    else $action = "none";
    $row['clickAction'] = $action;
}

function brazil_html5map_plugin_export_csv() {
    if ( ! is_admin())
        return;
    remove_all_actions('wp_head');
    remove_all_actions('wp_footer');

    $all_options = brazil_html5map_plugin_get_options();
    $options_keys = array_keys($all_options);
    $def_map_id = reset($options_keys);

    $map_id = isset($_GET['map_id']) ? (int)$_GET['map_id'] : $def_map_id;

    $map_options = &$all_options[$map_id];

    $field_delimiters = array(
        ',' => ',',
        ';' => ';',
        ':' => ':',
        'sp'=> ' ',
        'tb'=> "\t"
    );
    $text_delimiters = array(
        "'" => "'",
        '"' => '"',
        'n' => null
    );

    $fd = stripslashes($_REQUEST['field-delimiter']);
    $td = stripslashes($_REQUEST['text-delimiter']);
    if ( ! array_key_exists($fd, $field_delimiters)) {
        $fd = ',';
    }
    else {
        $fd = $field_delimiters[$fd];
    }
    if ( ! array_key_exists($td, $text_delimiters)) {
        $td = '"';
    }
    else {
        $td = $text_delimiters[$td];
    }

    $tmp_name = tempnam(sys_get_temp_dir(), 'mapcsv');
    $fh = fopen($tmp_name, 'w');
    $import_export_keys = brazil_html5map_plugin_get_csv_import_export_keys('states');
    $header = array_keys($import_export_keys);
    $fields = array_values($import_export_keys);
    fputcsv($fh, $header, $fd, $td);
    $st_params = json_decode($map_options['map_data'], true);
    foreach ($st_params as $id => $params) {
        $params['id'] = $id;
        $params['info'] = isset($map_options['state_info'][$iid = preg_replace('/\D+/', '', $id)]) ?$map_options['state_info'][$iid] : '';
        brazil_html5map_plugin_detect_export_click_action($params);
        $data = array();
        foreach(array('_hide_name', 'isNewWindow') as $f) $params[$f] = empty($params[$f]) ? '' : 'yes';
        foreach ($fields as $f) if ($f)
            $data[$f] = isset($params[$f]) ? $params[$f] : '';
        if ($params['clickAction'] !== 'link') $data['link'] = '';
        fputcsv($fh, $data, $fd, $td);
    }
    if (!empty($map_options['groups'])) {
        $import_export_keys = brazil_html5map_plugin_get_csv_import_export_keys('groups');
        $header = array_keys($import_export_keys);
        $fields = array_values($import_export_keys);
        fputcsv($fh, $header, $fd, $td);
        foreach ($map_options['groups'] as $id => $params) {
            $params['id'] = $id;
            brazil_html5map_plugin_detect_export_click_action($params);
            $data = array();
            foreach(array('isNewWindow', '_popup_over', '_act_over', '_clr_over', '_ignore_group') as $f) $params[$f] = empty($params[$f]) ? '' : 'yes';
            foreach ($fields as $f) if ($f)
                $data[$f] = isset($params[$f]) ? $params[$f] : '';
            if ($params['clickAction'] !== 'link') $data['link'] = '';
            fputcsv($fh, $data, $fd, $td);
        }
    }
    if (!empty($map_options['points'])) {
        $import_export_keys = brazil_html5map_plugin_get_csv_import_export_keys('points');
        $header = array_keys($import_export_keys);
        $fields = array_values($import_export_keys);
        fputcsv($fh, $header, $fd, $td);
        foreach ($map_options['points'] as $id => $params) {
            $params['id'] = $id;
            $params['info'] = isset($map_options['state_info'][$id]) ?$map_options['state_info'][$id] : '';
            brazil_html5map_plugin_detect_export_click_action($params);
            $data = array();
            foreach(array('isNewWindow') as $f) $params[$f] = empty($params[$f]) ? '' : 'yes';
            foreach ($fields as $f) if ($f)
                $data[$f] = isset($params[$f]) ? $params[$f] : '';
            if ($params['clickAction'] !== 'link') $data['link'] = '';
            fputcsv($fh, $data, $fd, $td);
        }
    }
    fclose($fh);
    header('Content-type: text/csv');
    header('Content-length: '.filesize($tmp_name));
    header('Connection: close');
    header('Content-Disposition: attachment; filename="brazilhtml5map-'.$map_id.'.csv";');
    readfile($tmp_name);
    unlink($tmp_name);
    exit;
}


function brazil_html5map_plugin_import(&$errors) {
    $errors = array();
    $csv_types = array('text/csv','text/comma-separated-values','application/vnd.ms-excel');
    if(is_uploaded_file($_FILES['import_file']["tmp_name"])) {

        if (in_array($_FILES['import_file']['type'], $csv_types))
        {
            $errors[] = sprintf(__('CSV import should be done on the "<a href="%s">Import / Export</a>" tab', 'brazil-html5-map'), "admin.php?page=brazil-html5-map-ie");
            return false;
        }

        $hwnd = fopen($_FILES['import_file']["tmp_name"],'r');
        $data = fread($hwnd,filesize($_FILES['import_file']["tmp_name"]));
        fclose($hwnd);

        $data    = json_decode($data, true);

        if ($data) {
            $def_settings = file_get_contents(dirname(__FILE__).'/static/settings_tpl.json');
            $def_settings = json_decode($def_settings, true);
            $states_count = count($def_settings);
            $options = brazil_html5map_plugin_get_options();

            foreach($data as $map_id => $map_data) {
                if (isset($map_data['map_data']) and $map_data['map_data']) {

                    $data = json_decode($map_data['map_data'], true);
                    $cur_count = $data ? count($data) : 0;
                    $c = $options ? max(array_keys($options))+1 : 0;
                    if ($cur_count != $states_count) {
                        $errors[] = sprintf(__('Failed to import "%s", looks like it is a wrong map. Got %d states when expected states count was: %d', 'brazil-html5-map'), $map_data['name'], $cur_count, $states_count);
                        continue;
                    }
                    $map_data['update_time'] = time();
                    $map_data['map_data'] = preg_replace("/javascript:[\w_]+_set_state_text[^\(]*\([^\)]+\);/", "#info", $map_data['map_data']);
                    $options[]              = $map_data;
                } else {
                   $errors[] = sprintf(__('Section "%s" skipped cause it has no "map_data" block.', 'brazil-html5-map'), $map_id);
                }

            }
            brazil_html5map_plugin_save_options($options);
        } else {
            $errors[] = __('Failed to parse uploaded file. Is it JSON?', 'brazil-html5-map');
        }

        unlink($_FILES['import_file']["tmp_name"]);

    } else {
        $errors[] = __('File uploading error!', 'brazil-html5-map');
    }
    return !count($errors);
}

function brazil_html5map_plugin_areas_list($options,$count) {

    $map_data = (array)json_decode($options['map_data']);
    $areas    = array();
    foreach($map_data as $key => $area) {
        if ($options['areaListOnlyActive'] and !$area->link)
            continue;
        $areas[$area->name] = array(
            'id'   => $area->id,
            'key'  => $key,
            'name' => $area->name,
        );
    }

    if (empty($areas))
        return '';

    ksort($areas);

    $options['listFontSize'] = intval($options['listFontSize'])>0 ? $options['listFontSize'] : 16;

    $html = "<div class=\"brazilHtml5Map-areas-list\" id=\"brazil-html5-map-areas-list_{$count}\" style=\"width: ".$options['listWidth']."%;\" data-count=\"$count\">";

    foreach ($areas as $area) {
        $html.="<div class=\"brazilHtml5Map-areas-item\"><a href=\"#\" style=\"font-size: ".$options['listFontSize']."px\" data-key=\"".$area['key']."\" data-id=\"".$area['id']."\" >".$area['name']."</a></div>";
    }

    $html.= "</div>";

    return $html;
}

function brazil_html5map_plugin_user_has_cap($allcaps, $cap, $args) {

    $user_id      = get_current_user_id();
    $current_user = get_user_by('id', $user_id);
    $allowed      = brazil_html5map_plugin_get_options(null, 'brazilhtml5map_goptions');

    if (!(isset($allowed['roles']) and is_array($allowed['roles']))) {
        $allowed['roles'] = array();
    }

    $allowed['roles']['administrator'] = true;

    foreach($allowed['roles'] as $role => $val) {
        if ($val && $current_user && in_array($role, (array)$current_user->roles)) {
            $allcaps['brazilhtml5map_manage_role'] = true;
            break;
        }
    }

    return $allcaps;
}
add_filter( 'user_has_cap', 'brazil_html5map_plugin_user_has_cap', 10, 3 );
